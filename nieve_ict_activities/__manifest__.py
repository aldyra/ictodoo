
{
    "name": "Manage Activity",
    "version": "1.0",
    "depends": ["base", "nieve_invoice_milestone"],
    "author": "Nieve Technology",
    "description": """Last Update 15-11-2018.
    """,
    "website": "nievetechnology.com",
    "category": "Custom",
    "init_xml": [],
    "demo_xml": [],
    "data": [
        "security/ir.model.access.csv",
        "wizard/ict_things_todo_wizard.xml",
        "views/ict_daily_activity_view.xml",
        "views/ict_things_todo_view.xml",
    ],
    "active": False,
    "installable": True,
}
