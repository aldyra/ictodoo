from odoo import api, fields, models


class ThingsTodoWizard(models.TransientModel):
    _name = 'things.todo.wizard'

    name = fields.Char('Subject')
    quotation_id = fields.Many2one(
        'sale.order', string="Quotation")
    ttd_update = fields.Char("Update")
    ttd_description = fields.Char("Things to do")
    ttd_state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('confirm', 'Confirmed'),
            ('done', 'Done'),
            ('drop', 'Drop')
        ], default='draft', string='State')

    @api.multi
    def confirm(self):
        sale_obj = self.env['sale.order']
        things_obj = self.env['ict.things.todo']
        order = sale_obj.browse(self._context.get('active_ids'))[0]
        if order:
            things_val = {
                'name': self.name,
                'ttd_update': self.ttd_update,
                'ttd_description': self.ttd_description,
                'ttd_state': self.ttd_state,
                'quotation_id': order.id
            }
            things_obj.create(things_val)

            write_val = {
                'kk_update': self.ttd_update,
                'kk_things': self.ttd_description,
                'kk_state_act': self.ttd_state,
            }
            order.write(write_val)
