from odoo import api, fields, models


class IctThingsTodo(models.Model):

    _name = 'ict.things.todo'
    _inherit = 'mail.thread'
    _order = "id desc"

    @api.model
    def _get_default_requested_by(self):
        return self.env['res.users'].browse(self.env.uid)

    user_create_id = fields.Many2one(
        'res.users', default=_get_default_requested_by,
        string='Created By', readonly=1)
    name = fields.Char('Subject')
    quotation_id = fields.Many2one(
        'sale.order', string="Quotation")
    ttd_update = fields.Char("Update Status")
    ttd_description = fields.Char("Things to do")
    ttd_state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('confirm', 'Confirmed'),
            ('done', 'Done'),
            ('drop', 'Drop')
        ], default='draft', string='State')
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('confirm', 'Confirmed'),
            ('cancel', 'Canceled')
        ], default='draft', string='State')

    @api.multi
    def confirm(self):
        if self.quotation_id:
            quotation = self.env['sale.order'].search(
                [('id', '=', self.quotation_id.id)])
            write_val = {
                'kk_update': self.ttd_update,
                'kk_things': self.ttd_description,
                'kk_state_act': self.ttd_state
            }
            quotation.write(write_val)
        self.write({'state': 'confirm'})


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    things_todo_ids = fields.One2many(
        'ict.things.todo', 'quotation_id',
        string="Things To Do")
