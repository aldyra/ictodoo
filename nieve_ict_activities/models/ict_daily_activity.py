from odoo import api, fields, models, tools, SUPERUSER_ID
from odoo.tools.translate import _
from odoo.exceptions import UserError, AccessError, ValidationError
import datetime

class IctDailyActivity(models.Model):

    _name = 'ict.daily.activity'
    _inherit = 'mail.thread'

    @api.model
    def _get_default_requested_by(self):
        return self.env['res.users'].browse(self.env.uid)

    user_create_id = fields.Many2one('res.users', default=_get_default_requested_by, string='Created By',readonly=1)
    name = fields.Char('Subject')
    partner_id = fields.Many2one('res.partner',string="Partner")
    activity_date = fields.Datetime("Date")
    activity_objective = fields.Char("Objective")
    activity_description = fields.Text("Description")
    state = fields.Selection([('draft','Draft'),('confirm','Confirmed'),('cancel','Canceled')],default='draft',string='State')