from odoo import fields, models
from odoo import api, fields, models, _

class InheritCustomerSalesTeam(models.Model):
    _inherit = "res.partner"

    product_team_id         = fields.Many2one('crm.team',string="Product Team",compute="_compute_product_team",store=True)
    maintenance_team_id     = fields.Many2one('crm.team',string="Maintenance Team",compute="_compute_maintenance_team",store=True)

    @api.multi
    @api.depends('user_id','product_team_id')
    def _compute_product_team(self):
        for item in self:
            if item.user_id:
                item.product_team_id = item.user_id.sale_team_id.id
    
    @api.multi
    @api.depends('user_maintenance_id','maintenance_team_id')
    def _compute_maintenance_team(self):
        for item in self:
            if item.user_maintenance_id:
                item.maintenance_team_id = item.user_maintenance_id.sale_team_id.id