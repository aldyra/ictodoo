# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Nieve Customer Sales Team",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.sriwijayaair.co.id',
    'category': 'Sales',
    'summary': 'Customer Sales Team',
    "description": """
    Module to group SO to Cost Sheet Last Updated 13-05-2020 

    """,
    "depends": ['nieve_cost_sheet'
    ],
    'data': [
             'views/res_partner_sales_team.xml'
       
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
