# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Nieve Stock Inventory",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.sriwijayaair.co.id',
    'category': 'Warehouse',
    'summary': 'Stock Inventory with Price',
    "description": """
        Stock Inventory with Price
    """,
    "depends": [
        'sale', 'stock'],
    'data': [
        "views/stock_inventory_views.xml",
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
