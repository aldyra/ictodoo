# -*- coding: utf-8 -*-
# © 2018 Nieve Technology
{
    'name': 'ICT - Accounting',
    'version': '10.0.0.1.0',
    'category': 'Localization',
    'description': """
PT. ICT - Chart of accounts.
========================================================
* generic ICT chart of accounts

""",
    'author': 'Nieve Technology',
    'website': 'http://nievetechnology.com',
    'depends': [
        'base',
        'account',
        'base_iban',
        'base_vat',
        'account_parent',
    ],
    'demo': [],
    'test': [],
    'data': [
        'data/data_account_type.xml',
        'data/l10n_ict_chart_data.xml',
        'data/account.account.template.csv',
        'data/account.chart.template.csv',
        'data/account.tax.template.csv',
        'data/account_chart_template_data.yml',
    ],
    'css': [],
    'js': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}