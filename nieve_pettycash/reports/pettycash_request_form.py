from odoo import models, fields, api, _, osv
from odoo.addons.nieve_amount2text_idr import amount_to_text_id


class PettyCashRequestForm(models.AbstractModel):
    _name = 'report.nieve_pettycash.pettycash_request_form_pdf'

    def get_header(self,data):
        return self.env['pettycash.out'].search([('id', '=', data.id)])

    def get_detail(self,data):
        return self.env['pettycash.out.line'].search([('pettycash_out_id', '=', data.id)])

    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    @api.model
    def get_report_values(self, docids, data=None):
        get_data = self.env['account.payment'].search([('id', '=', docids[0])])
        docargs = {
            'get_datax': get_data,
            'get_detail':self.get_detail,
            'convert':self.convert,
            'get_header': self.get_header,
            'doc_ids': docids,
            'doc_model': 'account.payment',
            'docs': self,
        }
        return docargs
