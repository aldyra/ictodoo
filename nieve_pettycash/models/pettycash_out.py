from odoo import models, fields, api, _, osv
from odoo.exceptions import except_orm, Warning, RedirectWarning
from datetime import datetime, date, time, timedelta
import time
from dateutil.relativedelta import relativedelta

class InheritAccountPayment(models.Model):
    _inherit = "account.payment"

    pettycash_out_id = fields.Many2one('pettycash.out',string='Petty Cash Out')

class PettycashOut(models.Model):
    _inherit = ['mail.thread']
    _name = 'pettycash.out'

    @api.multi
    def unlink(self):
        for o in self:
            if o.state not in ('draft'):
                raise osv.except_osv(_('Warning !'), _('You cannot delete an Petty Cash which is not draft.'))
            elif o.number:
                raise osv.except_osv(_('Warning !'), _(
                    'You cannot delete an Petty Cash which is Number document Created. \n You can cancel Transaction'))
        return super(PettycashOut, self).unlink()

    name = fields.Char(string='Name',default='New',readonly=True)
    user_id = fields.Many2one('res.users', string='User', track_visibility='always', default=lambda self: self.env.user,readonly=True)
    company_id = fields.Many2one('res.company', string='Company', track_visibility='always', default=lambda self: self.env.user.company_id,readonly=True)
    # employee_id = fields.Many2one('hr.employee',string='Employee')
    department_id = fields.Many2one('hr.department', string='Department')

    transaction_name = fields.Char(string='Transaction')
    transaction_date = fields.Date(string='Closing Date')
    date = fields.Date(string='Create Date')
    # force_period = fields.Many2one('account.period',string='Period')
    journal_id = fields.Many2one('account.journal',string='Cash/Bank',required=True)
    currency_id = fields.Many2one('res.currency', string='Currency',compute='_compute_journal_currency',readonly=True,store=True)
    pettycash_request_id = fields.Many2one('pettycash.request',string='Petty Cash Request')
    picked = fields.Boolean(string='Picked')
    total_amount = fields.Float(string='Total Amount',store=True,readonly=True,compute='_compute_total_amount')
    state = fields.Selection([('draft', 'Draft'),('submit', 'Submitted'),('approve', 'Manager Approved'),('verify', 'Finance Verify'),
                              ('close', 'Finance Approved'),
                              ('cancel', 'Cancelled')], default='draft',
                             track_visibility='onchange')
    pettycash_out_line = fields.One2many('pettycash.out.line', 'pettycash_out_id', 'Petty Cash Out Line')

    @api.multi
    @api.depends('journal_id')
    def _compute_journal_currency(self):
        currency = self.journal_id.currency_id.id
        if currency == False:
            self.currency_id = 13
        else:
            self.currency_id = currency

    @api.multi
    @api.depends('pettycash_out_line')
    def _compute_total_amount(self):
        data = self.env['pettycash.out.line'].search([('pettycash_out_id','=',self.id)])
        total = 0.0
        for i in data:
            total = total + i.amount
        self.total_amount = total

    @api.multi
    @api.depends('employee_id')
    def _compute_department_user(self):
        department = self.employee_id.department_id.id if self.employee_id.department_id.id else 0
        self.department_id = department

    @api.multi
    def approve(self):
        self.write({'state':'approve'})

    @api.multi
    def verify(self):
        self.write({'state': 'verify'})

    @api.multi
    def close(self):
        now = datetime.now().strftime('%Y-%m-%d')
        self.transaction_date = now
        self.picked = True
        for rec in self:

            # if rec.state != 'submit':
            #     raise RedirectWarning(
            #         _("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            ##Edit
            move_pool = self.env['account.move']
            move_line_pool = self.env['account.move.line']
            company_currency = rec.company_id.currency_id

            # create Move ID
            sign = -1
            journal_id = rec.journal_id.id

            if (self.pettycash_out_line):
                # Create Journal AP
                line_mv_ids = rec.create_journal_ap(rec, journal_id, company_currency, sign, move_line_pool)
                move_id = rec.create_move_id(rec, journal_id, move_pool, line_mv_ids)

            payment_obj = self.env['account.payment']

            payment_methods = self.journal_id.outbound_payment_method_ids
            channel_transaction_data = {
                'payment_type': 'transfer',
                'journal_id': rec.journal_id.id,
                'destination_journal_id': rec.journal_id.id,
                'payment_method_id': payment_methods.id,
                'amount': rec.total_amount,
                'currency_id': rec.currency_id.id,
                'payment_date': rec.transaction_date,
                'communication':'tes',
                'partner_type':'customer',
                'pettycash_out_id':rec.id
            }

            data = payment_obj.create(channel_transaction_data)
            data.post()

        self.write({'state': 'close'})

    @api.multi
    def conv_amount_currency(self, line, rec, amount):
        """
            this method use to conv amount currency
        :return:
        """
        self.with_context()
        move_line = line
        payment_date = rec.transaction_date
        currency_payment = move_line.pettycash_out_id.currency_id
        voucher_rate = \
            self.env['res.currency'].with_context({'date': payment_date}).browse(currency_payment.id)['rate']
        company_currency = rec.currency_id
        payment_currency = rec.currency_id and rec.currency_id or company_currency

        tcurrency_id = move_line.pettycash_out_id.currency_id.with_context(date=payment_date)


    @api.multi
    def create_move_id(self, rec, journal_id, move_pool, line_mv_ids):
        """
            create move id in journal entries
        :return: move ID
        """
        name = rec.name
        ref = rec.name
        date = rec.transaction_date

        move = {
            'name': name,
            'journal_id': journal_id,
            # 'narration'     : ref,
            'date': date,
            'ref': ref,
            'line_ids': line_mv_ids
        }
        move_id = move_pool.create(move)
        return move_id

    @api.multi
    def create_journal_ap(self, rec, journal_id, company_currency, sign, move_line_pool):
        """
            this method use to create move live for journal AP
        :return: journal accont payable
        """
        list_ap_mv = []
        for line_debit in rec.pettycash_out_line:
            account_credit_id = line_debit.account_id.id
            line = line_debit
            amount = line_debit.amount
            validation_currency = False if line_debit.pettycash_out_id.currency_id.id == rec.company_id.currency_id.id else True
            move_line_debit = {
                'name': line_debit.transaction_name or '/',
                'debit': line_debit.pettycash_out_id.currency_id != company_currency and rec.currency_id.compute(
                    line_debit.amount or 0.0,
                    company_currency) or line_debit.amount,
                'credit': 0.0,
                'account_id': account_credit_id,
                'journal_id': journal_id,
                'partner_id': rec.user_id.id,
                'currency_id': line_debit.pettycash_out_id.currency_id != rec.company_id.currency_id and line_debit.pettycash_out_id.currency_id.id or False,
                # 'amount_currency': 1 * (
                # rec.conv_amount_currency(line, rec, amount) or 0.0) if validation_currency == True else 1 * (
                # rec.currency_id != rec.company_id.currency_id and rec.amount or 0.0),
                'amount_currency': 1 * (rec.currency_id != rec.company_id.currency_id and line_debit.amount or 0.0),
                'quantity': 1
                # 'payment_id': rec.id
            }
            list_ap_mv.append((0, 0, move_line_debit))

        move_line_credit = {
            'name': rec.journal_id.name or '/',
            'debit': 0.0,
            'credit': rec.currency_id != company_currency and rec.currency_id.compute(rec.total_amount or 0.0,
                                                                                      company_currency) or rec.total_amount,
            'account_id': rec.journal_id.default_credit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.user_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': sign * (rec.currency_id != rec.company_id.currency_id and rec.total_amount or 0.0),
            'quantity': 1
            # 'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_credit))
        return list_ap_mv

    @api.multi
    def submit(self):
        for o in self:
            # self.emp_name = self.employee_id.name_related

            fmt = '%Y-%m-%d'
            if o.name == 'New':
                now = datetime.now().strftime('%Y-%m-%d')
                date = now
                conv_date = datetime.strptime(str(date), fmt)
                month = conv_date.month
                year = conv_date.year

                # change integer to roman
                if type(month) != type(1):
                    raise TypeError("expected integer, got %s" % str(type(month)))
                if not 0 < month < 4000:
                    raise ValueError("Argument must be between 1 and 3999")
                ints = (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1)
                nums = ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
                result = ""
                for i in range(len(ints)):
                    count = int(month / ints[i])
                    result += nums[i] * count
                    month -= ints[i] * count
                month = result

                number = self.env['ir.sequence'].next_by_code('pettycash.out.number')
                name = str(number) + '/' + 'PTCO' + '/' + str(month) + '/' + str(year)
            else:
                name = o.name

        return self.write({'state': 'submit', 'name': name})


class PettycashOutLine(models.Model):
    _name = 'pettycash.out.line'

    pettycash_out_id = fields.Many2one('pettycash.out',string='Petty Cash Out Line')
    product_id = fields.Many2one('product.product',string='Product')
    transaction_name = fields.Char(string='Transaction',required=True)
    account_id = fields.Many2one('account.account',string='Account',required=True)
    amount = fields.Float(string='Amount',required=True)
    # amount_currency = fields.Float(string='Amount Currency')
    employee_receive = fields.Many2one('hr.employee',string='Employee Receive',required=True)
    transaction_date = fields.Date(string='Transaction Date',required=True)
    # currency_id = fields.Many2one('res.currency',string='Currency',related='pettycash_out_id.currency_id')

    @api.multi
    @api.onchange('product_id')
    def onchange_product(self):
        if self.product_id:
            product_obj = self.env['product.product']
            for product in product_obj.browse(self.product_id.id):
                expense_account_id = product.property_account_expense_id.id
            self.account_id = expense_account_id or False

