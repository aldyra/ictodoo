from odoo import models, fields, api, _, osv
from odoo.exceptions import except_orm, Warning, RedirectWarning
from datetime import datetime, date, time, timedelta
import time
from dateutil.relativedelta import relativedelta


class PettycashRequest(models.Model):
    _inherit = ['mail.thread']
    _name = 'pettycash.request'

    name = fields.Char(string='Name',default='New',readonly=True)
    user_id = fields.Many2one('res.users', string='User', track_visibility='always', default=lambda self: self.env.user)
    department_id = fields.Char(string='Department')
    petty_journal_id = fields.Many2one('account.journal',string='for Petty Cash')
    journal_id = fields.Many2one('account.journal',string='Bank to Transfer')
    date = fields.Date(string='Date')
    # period = fields.Many2one('account.period',string='Period')
    plafond = fields.Float(string='Plafond')
    amount_request = fields.Float(string='Amount Request')
    description = fields.Text(string='Description')
    state = fields.Selection([('draft', 'Draft'),('confirm', 'Confirmed'), ('cancel', 'Cancelled')], default='draft',
                             track_visibility='onchange')
    pettycash_req_line = fields.One2many('pettycash.request.line', 'pettycash_req_id', 'Petty Cash Req Line')

    @api.multi
    def confirm(self):
        for o in self:
            # self.emp_name = self.employee_id.name_related

            fmt = '%Y-%m-%d'
            if o.name == 'New':
                now = datetime.now().strftime('%Y-%m-%d')
                date = now
                conv_date = datetime.strptime(str(date), fmt)
                month = conv_date.month
                year = conv_date.year

                # change integer to roman
                if type(month) != type(1):
                    raise TypeError(_("expected integer, got %s") % type(month))
                if not 0 < month < 4000:
                    raise ValueError('Argument must be between 1 and 3999')
                ints = (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1)
                nums = ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
                result = ""
                for i in range(len(ints)):
                    count = int(month / ints[i])
                    result += nums[i] * count
                    month -= ints[i] * count
                month = result

                number = self.env['ir.sequence'].next_by_code('pettycash.form.number')
                name = str(number) + '/' + 'PTREQ' + '/' + str(month) + '/' + str(year)
            else:
                name = o.name

        return self.write({'state': 'confirm', 'name': name})

    @api.multi
    def cancel(self):
        self.write({'state': 'cancel'})

class PettycashRequestLine(models.Model):

    _name='pettycash.request.line'

    pettycash_req_id = fields.Many2one('pettycash.request', string='Petty Cash Req Line')
    petty_journal_id = fields.Many2one('account.journal', string='for Petty Cash',related='pettycash_req_id.petty_journal_id')
    number = fields.Many2one('pettycash.out',string='Number')
    journal_id = fields.Many2one('account.journal',string='Cash/Bank')
    date_out = fields.Date(string='Cash Out Date')
    # period = fields.Char(string='Period')
    name = fields.Char(string='Transaction')
    user_id = fields.Many2one('res.users', string='User', track_visibility='always', default=lambda self: self.env.user)
    # account_id = fields.Many2one('account.account', string='Account')
    currency_id = fields.Many2one('res.currency', string='Currency')
    amount = fields.Float(string='Total')

    @api.onchange('petty_journal_id')
    def _compute_filter_pettycash_out(self):
        result = []
        data = self.env['pettycash.out'].search([('state','=','confirm'),('journal_id','=',self.petty_journal_id.id)])
        for item in data:
            result.append(item.journal_id.id)
        if result == []:
            return {
                'domain': {
                    'number': [('journal_id', '=', False)]
                }
            }
        else:
            return {
                'domain':{
                    'number':[('journal_id','in',result)]
                }
            }


    @api.onchange('number')
    def onchange_out(self):
        data = self.env['pettycash.out'].search([('id','=',self.number.id)])
        for item in data:
            self.date_out = item.date
            for line in self.env['pettycash.out.line'].search([('pettycash_out_id','=',item.id)]):
                self.name = line.transaction_name
                self.amount = line.amount