from openerp import models, fields, api, _, osv
from openerp.exceptions import except_orm, Warning, RedirectWarning
from datetime import datetime, date, time, timedelta
import time
from dateutil.relativedelta import relativedelta


class PettycashRequestOperationWizard(models.TransientModel):
    _name = 'pettycash.request.operation.wizard'

    user_id = fields.Many2one('res.users', string='User', track_visibility='always', default=lambda self: self.env.user,
                              readonly=True)
    department_id = fields.Many2one('hr.department', string='Department')
    petty_journal_id = fields.Many2one('account.journal', string='for Petty Cash')
    journal_id = fields.Many2one('account.journal', string='Bank to Transfer')
    description = fields.Char(string='Description',size=268,required=True,readonly=False)
    date = fields.Date(string='Date',required=True,readonly=False)
    plafond = fields.Float(string='Plafond',required=True)
    amount_request = fields.Float(string='Amount Request',required=True)

    @api.model
    def default_get(self,fields,context = None):
        if context is None: context = {}
        res = super(PettycashRequestOperationWizard, self).default_get(fields)
        petty_ids = self.env.context.get('active_ids',[])
        active_model = self.env.context.get('active_model')

        if not petty_ids:
            return res
        assert active_model in ('pettycash.out'), 'Bad context propagation'

        pettys = self.env['pettycash.out'].browse(petty_ids)
        amount_request = 0.0
        for petty in pettys:
            if petty.state != 'confirm':
                model, action_id = self.env['ir.model.data'].get_object_reference('account',
                                                                                   'action_account_form')
                msg = _(
                    "You should configure the 'Loss Exchange Rate Account' to manage automatically the booking of accounting entries related to differences between exchange rates.")
                raise RedirectWarning(msg, action_id, _('Go to the configuration panel'))
            if petty.picked == True:
                raise RedirectWarning(_('Action Invalid'),
                                     _('You Can not Select Transaction %s already requested') % petty.number)

            for l in petty.pettycash_out_line:
                amount_request += l.amount

        res.update(amount_request=amount_request)
        return res

    @api.multi
    def create_pettycash_request(self,context=None):

        petty_obj = self.env['pettycash.out']
        petty_req_obj = self.env['pettycash.request']
        petty_ids = self._context.get('active_ids')
        mod_obj = self.env['ir.model.data']
        if context is None:
            context = {}

        vals = {}
        vals = {
            'user_id': self.user_id.id,
            'department_id': 0,
            'petty_journal_id': self.petty_journal_id.id,
            'journal_id': self.journal_id.id,
            'date': self.date,
            'plafond': self.journal_id.id,
            'amount_request': self.amount_request,
            'description': self.description
        }

        petty_req_id = self.env['pettycash.request'].create(vals)

        # for this in self.browse(context['active_ids']):
        #     vals_line ={
        #         'pettycash_req_id' : petty_req_id.id,
        #         'petty_journal_id' : this.journal_id.id,
        #         'number': this.id,
        #         'journal_id': this.journal_id.id,
        #         'date_out' : this.date,
        #         'name' : this.transaction_name,
        #         'account_id' : 0,
        #         'currency_id' : this.currency_id.id,
        #         'amount': this.total_amount
        #     }
        #     self.env['pettycash.request.line'].create(vals_line)
        #     petty_obj.write({'pettycash_request_id': petty_req_id,
        #                  'picked': True})

        return {
            'domain': "[('id','in', [" + str(petty_req_id.id) + "])]",
            'name': _('Petty Cash Request'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'pettycash.request',
            'view_id': False,
            'type': 'ir.actions.act_window',
            # 'search_view_id': id['res_id']
        }
