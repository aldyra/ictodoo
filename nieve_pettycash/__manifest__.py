{
    "name": "Petty Cash",
    "version": "1.0",
    "depends": [
        "base", "account",
        "hr", "mail"
    ],
    "author": "Nieve Technology",
    "description": """Last Update 22-07-2018 This module is aimed to handle Petty Cash.
    """,
    "website": "nievetechnology.com",
    "category": "Custom",
    "init_xml": [],
    "demo_xml": [],
    'test': [],
    "data": [
        "data/sequences.xml",
        "views/pettycash_request_view.xml",
        "views/pettycash_out_view.xml",
        "views/pettycash_request_form_pdf.xml",
        "wizards/pettycash_request_operation_wizard_view.xml",
        "reports/reports.xml",
        "views/inherit_account_payment_view.xml"

    ],
    "active": False,
    "installable": True,
}
