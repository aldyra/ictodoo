from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    
    @api.depends('discount')
    def _compute_amount(self):
        for line in self:
            price_unit = False
            # This is always executed for allowing other modules to use this
            # with different conditions than discount != 0
            price = line._get_discounted_price_unit()
            if price != line.price_unit:
                # Only change value if it's different
                price_unit = line.price_unit
                line.price_unit = price
            super(PurchaseOrderLine, line)._compute_amount()
            
            if price_unit:
                line.price_unit = price_unit
    
    discount_type   = fields.Selection([('amount','Amount'),('percentage','Percentage')], string='Disc. Type', default='percentage')
    discount        = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0)
    discount_amount = fields.Float(string='Discount Amount', default=0.0)
    
    
    def _get_discounted_price_unit(self):
        """Inheritable method for getting the unit price after applying
        discount(s).

        :rtype: float
        :return: Unit price after discount(s).
        """
        self.ensure_one()
        if self.discount:
            #return self.price_unit * (1 - self.discount / 100)
            return self.price_unit - self.discount_amount
        return self.price_unit
    
    @api.onchange('discount','product_qty','price_unit')
    def onchange_discount_percentage(self):
        discount_type   = self.discount_type
        price_unit      = self.price_unit
        product_qty     = self.product_qty
        discount        = self.discount
        
        discount_amount_conv    = (discount/100) * (price_unit*product_qty)
        self.discount_amount    = discount_amount_conv
        
    @api.onchange('discount_amount','product_qty','price_unit')
    def onchange_discount_amount(self):
        discount_type   = self.discount_type
        discount_amount = self.discount_amount
        price_unit      = self.price_unit
        product_qty     = self.product_qty
        
        discount_conv   = (discount_amount != 0.0 and (discount_amount / (price_unit*product_qty)) * 100) or 0.0
        self.discount   = discount_conv
 
#     @api.multi
#     def _prepare_invoice_line(self, qty):
#         """
#         Prepare the dict of values to create the new invoice line for a sales order line.
# 
#         :param qty: float quantity to invoice
#         """
#         self.ensure_one()
#         res = {}
#         account = self.product_id.property_account_income_id or self.product_id.categ_id.property_account_income_categ_id
#         if not account:
#             raise UserError(_('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') %
#                 (self.product_id.name, self.product_id.id, self.product_id.categ_id.name))
# 
#         fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
#         if fpos:
#             account = fpos.map_account(account)
# 
#         res = {
#             'name': self.name,
#             'sequence': self.sequence,
#             'origin': self.order_id.name,
#             'account_id': account.id,
#             'price_unit': self.price_unit,
#             'quantity': qty,
#             'discount_type': self.discount_type,
#             'discount_amount': self.discount_amount,
#             'discount': self.discount,
#             'uom_id': self.product_uom.id,
#             'product_id': self.product_id.id or False,
#             'layout_category_id': self.layout_category_id and self.layout_category_id.id or False,
#             'product_id': self.product_id.id or False,
#             'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],
#             'account_analytic_id': self.order_id.analytic_account_id.id,
#             'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
#         }
#         return res
    