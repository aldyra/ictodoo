from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    discount_type   = fields.Selection([('amount','Amount'),('percentage','Percentage')], string='Disc. Type', default='percentage')
    discount_amount = fields.Float(string='Discount Amount', default=0.0)
    
    @api.onchange('discount_type','discount_amount','price_unit','product_uom_qty')
    def onchange_discount_amount(self):
        discount_type   = self.discount_type
        discount_amount = self.discount_amount
        price_unit      = self.price_unit
        product_uom_qty = self.product_uom_qty
        discount        = 0.0

        if discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0 and product_uom_qty != 0.0:
            if discount_type=='amount':
                discount = (discount_amount / (price_unit*product_uom_qty)) * 100
            elif discount_type=='percentage':
                discount = discount_amount
            self.discount = discount
            return
        else:
            self.discount_amount    = 0.0
            self.discount           = 0.0

    # @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    # def _compute_amount(self):
    #     """
    #     Compute the amounts of the SO line.
    #     """
    #     for line in self:
    #         if line.discount_type == 'amount':
    #             price_subtotal = (line.price_unit * line.product_uom_qty) - line.discount_amount
    #             tax = (10/100) * price_subtotal
    #             price_total = price_subtotal - tax
    #             line.update({
    #                 'price_tax': tax,
    #                 'price_total': price_total,
    #                 'price_subtotal': price_subtotal,
    #             })
    #         else:
    #             price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
    #             taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
    #             line.update({
    #                 'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
    #                 'price_total': taxes['total_included'],
    #                 'price_subtotal': taxes['total_excluded'],
    #             })

    @api.multi
    def _prepare_invoice_line(self, qty):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        """
        self.ensure_one()
        res = {}
        account = self.product_id.property_account_income_id or self.product_id.categ_id.property_account_income_categ_id
        if not account:
            raise UserError(_('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') %
                (self.product_id.name, self.product_id.id, self.product_id.categ_id.name))

        fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
        if fpos:
            account = fpos.map_account(account)

        res = {
            'name': self.name,
            'sequence': self.sequence,
            'origin': self.order_id.name,
            'account_id': account.id,
            'price_unit': self.price_unit,
            'quantity': qty,
            'discount_type': self.discount_type,
            'discount_amount': self.discount_amount,
            'discount': self.discount,
            'uom_id': self.product_uom.id,
            'product_id': self.product_id.id or False,
            'layout_category_id': self.layout_category_id and self.layout_category_id.id or False,
            'product_id': self.product_id.id or False,
            'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],
            'account_analytic_id': self.order_id.analytic_account_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
        }
        return res
    