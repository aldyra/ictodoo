from itertools import groupby
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    def _prepare_invoice_line_from_po_line(self, line):
        data = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        data['discount_type']   = 'amount'
        data['discount']        = line.discount
        data['discount_amount'] = line.discount_amount
        
        return data
        
#         if line.product_id.purchase_method == 'purchase':
#             qty = line.product_qty - line.qty_invoiced
#         else:
#             qty = line.qty_received - line.qty_invoiced
#         if float_compare(qty, 0.0, precision_rounding=line.product_uom.rounding) <= 0:
#             qty = 0.0
#         taxes = line.taxes_id
#         invoice_line_tax_ids = line.order_id.fiscal_position_id.map_tax(taxes)
#         invoice_line = self.env['account.invoice.line']
#         data = {
#             'purchase_line_id': line.id,
#             'name': line.order_id.name+': '+line.name,
#             'origin': line.order_id.origin,
#             'uom_id': line.product_uom.id,
#             'product_id': line.product_id.id,
#             'account_id': invoice_line.with_context({'journal_id': self.journal_id.id, 'type': 'in_invoice'})._default_account(),
#             'price_unit': line.order_id.currency_id.with_context(date=self.date_invoice).compute(line.price_unit, self.currency_id, round=False),
#             'quantity': qty,
#             'discount': 0.0,
#             'account_analytic_id': line.account_analytic_id.id,
#             'analytic_tag_ids': line.analytic_tag_ids.ids,
#             'invoice_line_tax_ids': invoice_line_tax_ids.ids
#         }
#         account = invoice_line.get_invoice_line_account('in_invoice', line.product_id, line.order_id.fiscal_position_id, self.env.user.company_id)
#         if account:
#             data['account_id'] = account.id
#         return data

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
    
    discount_type   = fields.Selection([('amount','Amount'),('percentage','Percentage')], string='Disc. Type', default='percentage')
    discount_amount = fields.Float(string='Discount Amount', default=0.0)
    discount        = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'),
        default=0.0,compute="_compute_discount_amount",store=True)          

    
    
    @api.multi
    @api.depends('quantity','discount_type','discount_amount','price_unit','discount')
    def _compute_discount_amount(self):
        for item in self:
            discount_type   = item.discount_type
            discount_amount = item.discount_amount
            price_unit      = item.price_unit
            product_uom_qty = item.quantity
            discount        = 0.0
            if product_uom_qty != 0.0 and discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0:
                if discount_type=='amount':
                    discount = (discount_amount / (price_unit)) * 100
                elif discount_type=='percentage':
                    discount = discount_amount
                item.discount = discount
                print ('discount',item.discount)
            else: 
                item.discount_amount    = 0.0
                item.discount           = 0.0
            
