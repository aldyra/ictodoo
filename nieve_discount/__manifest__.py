# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Nieve Discount",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.sriwijayaair.co.id',
    'category': 'General',
    'summary': 'Discount',
    "description": """
        Last Update 17 oct 2019
        Discount Fix Amount
        PLEASE CHANGE DECIMAL ACCURACY TO "10"
    """,
    "depends": [
        'sale','purchase','product'],
    'data': [
        "views/sale_view.xml",
        "views/purchase_view.xml",
        "views/account_invoice_view.xml",
        "data/product_data.xml",
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
