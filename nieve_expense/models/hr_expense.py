import re

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import email_split, float_is_zero

from odoo.addons import decimal_precision as dp


class HrExpense(models.Model):
    _inherit = 'hr.expense'
    
#     @api.multi
#     @api.depends('kk_cost_expense_id')
    def _get_cost_expense(self):
        exp_obj = self.env['hr.expense']
        cost_expense_plan   = 0.0
        cost_expense_actual_and_onprocess = 0.0
        for exp in self:
            rate = exp.currency_id.rate
            
            cost_expense_plan               = exp.kk_cost_expense_id.expense_plan
            amount_onprocess = rate * sum(i.total_amount for i in exp_obj.search([('id','!=',exp.id),('kk_cost_expense_id','=',exp.kk_cost_expense_id.id),('state','in',['approve1','approve2','reported'])]))
            cost_expense_actual_and_onprocess = exp.kk_cost_expense_id.expense_actual + amount_onprocess
            
            exp.cost_expense_plan   = cost_expense_plan
            exp.cost_expense_actual_and_onprocess = cost_expense_actual_and_onprocess
    
    creator_team_id = fields.Many2one('crm.team', string='Team', track_visibility='onchange', default=lambda self: self.env.user.sale_team_id.id)
    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet', string='Cost Sheet')
    kk_cost_expense_id = fields.Many2one('kk.cost.expense.line', 'Cost Expense')
    cost_expense_plan = fields.Float(string='Plan',compute='_get_cost_expense')
    cost_expense_actual_and_onprocess = fields.Float(string='Actual+Process',compute='_get_cost_expense')
    
    payment_mode = fields.Selection([
        ("own_account", "Employee (to reimburse)"),
        ("company_account", "Company")
    ], default='company_account', states={'done': [('readonly', True)], 'post': [('readonly', True)], 'submitted': [('readonly', True)]}, string="Payment By")
    
    state = fields.Selection([
        ('draft', 'To Submit'),
        ('approve1', 'Waiting Manager'),
        ('approve2', 'Waiting General Mgr'),
        ('reported', 'Reported'),
        ('done', 'Posted'),
        ('refused', 'Refused')
        ], compute='_compute_state', string='Status', copy=False, index=True, readonly=True, store=True,
        help="Status of the expense.")
    
    @api.onchange('kk_cost_sheet_id')
    def onchange_cost_sheet(self):
        if self.kk_cost_sheet_id:
            self.kk_cost_expense_id = False
            self.sale_order_id      = self.kk_cost_sheet_id.kk_sale_order_id.id 
        else:
            self.kk_cost_expense_id = False
            self.sale_order_id      = False
    
    @api.multi
    def check_cost_expense(self):
        total_amount = 0.0
        exp_obj = self.env['hr.expense']
        for exp in self:
            rate = exp.currency_id.rate
            
            total_amount = rate * sum(i.total_amount for i in exp_obj.search([('id','in',self.ids),('kk_cost_expense_id','=',exp.kk_cost_expense_id.id)]))
            
            if total_amount > (exp.cost_expense_plan - exp.cost_expense_actual_and_onprocess):
                raise UserError(
                    _("Over Budget in Item : %s") % (exp.kk_cost_expense_id.name))
        #return True
    
    @api.multi
    def submit_expenses(self):
        self.check_cost_expense()
        return super(HrExpense, self).submit_expenses()
    
    @api.multi
    def _prepare_move_line_value(self):
        self.ensure_one()
        if self.account_id:
            account = self.account_id
        elif self.product_id:
            account = self.product_id.product_tmpl_id._get_product_accounts()['expense']
            if not account:
                raise UserError(
                    _("No Expense account found for the product %s (or for its category), please configure one.") % (self.product_id.name))
        else:
            account = self.env['ir.property'].with_context(force_company=self.company_id.id).get('property_account_expense_categ_id', 'product.category')
            if not account:
                raise UserError(
                    _('Please configure Default Expense account for Product expense: `property_account_expense_categ_id`.'))
        aml_name = self.employee_id.name + ': ' + self.name.split('\n')[0][:64]
        
        move_line = {
            'type': 'src',
            'name': aml_name,
            'price_unit': self.unit_amount,
            'quantity': self.quantity,
            'price': self.total_amount,
            'kk_cost_sheet_id': self.kk_cost_sheet_id.id,
            'kk_cost_expense_id': self.kk_cost_expense_id.id,
            'account_id': account.id,
            'product_id': self.product_id.id,
            'uom_id': self.product_uom_id.id,
            'analytic_account_id': self.analytic_account_id.id,
            'expense_id': self.id,
        }
        return move_line
    
    def _prepare_move_line(self, line):
        #raise UserError(_('XXX'))
        
        '''
        This function prepares move line of account.move related to an expense
        '''
        partner_id = self.employee_id.address_home_id.commercial_partner_id.id
        return {
            'date_maturity': line.get('date_maturity'),
            'partner_id': partner_id,
            'name': line['name'][:64],
            'debit': line['price'] > 0 and line['price'],
            'credit': line['price'] < 0 and - line['price'],
            'kk_cost_sheet_id': line.get('kk_cost_sheet_id'),
            'kk_cost_expense_id': line.get('kk_cost_expense_id'),
            'account_id': line['account_id'],
            'analytic_line_ids': line.get('analytic_line_ids'),
            'amount_currency': line['price'] > 0 and abs(line.get('amount_currency')) or - abs(line.get('amount_currency')),
            'currency_id': line.get('currency_id'),
            'tax_line_id': line.get('tax_line_id'),
            'tax_ids': line.get('tax_ids'),
            'quantity': line.get('quantity', 1.00),
            'product_id': line.get('product_id'),
            'product_uom_id': line.get('uom_id'),
            'analytic_account_id': line.get('analytic_account_id'),
            'payment_id': line.get('payment_id'),
            'expense_id': line.get('expense_id'),
        }
class HrExpenseSheet(models.Model):

    _inherit = "hr.expense.sheet"
    
    creator_team_id = fields.Many2one('crm.team', string='Team', track_visibility='onchange', default=lambda self: self.env.user.sale_team_id.id)

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x
        
        
    
    @api.multi
    def hr_expense_filter_dynamic(self):
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
#         search_view_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'hr_duty_trip_view_search_es')[1]
        
#         tree_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_tree_es')[1]
#         form_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_form_es')[1]
        
#         action_window_id = data_obj.get_object_reference('nieve_expense', 'hr_expense.action_hr_expense_sheet_all_to_approve')[1]
#         action_window    = self.env['ir.actions.act_window'].browse([action_window_id])
        
        cost_sheet = self
        
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('hr_expense.group_hr_expense_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('hr_expense.group_hr_expense_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_expense.group_hr_expense_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_expense.group_hr_expense_director_sales')
        finance_allowed_access          = self.env['res.users'].has_group('nieve_expense.group_hr_expense_finance')
        
        team_ids = []
        filter_domain = []
        if finance_allowed_access or drc_sales_allowed_access:
            filter_domain = []
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid)
            if team_id:
                employee_Arr =[]
                for item in team_id:
                    employee_Arr.append(item.get('id_res_user'))
                    filter_domain = [('create_uid','in',employee_Arr)]
            else:
                filter_domain = [('id','=',None)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid)
            if team_id:
                employee_Arr =[]
                for item in team_id:
                    employee_Arr.append(item.get('id_res_user'))
                    filter_domain = [('create_uid','in',employee_Arr)]
            else:
                filter_domain = [('id','=',None)]
        else:
            filter_domain = [('create_uid','=',login_uid)]
            
        #Special for Admin
        if login_uid==1:
            filter_domain = []
        
#         if type=='sale':
#             filter_domain.append(('state', 'not in', ('draft', 'sent', 'cancel')))
        action = {
            #'id'        : action_view_id,
            'name'      : _('Expenses'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'hr.expense.sheet',
            #'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            #'context'   : context_action,
        }
        return action

    state = fields.Selection([('submit', 'Waiting Manager'),
                              ('approve0', 'Waiting General Mgr'),
                              ('approve1', 'Waiting Sales Director'),
                              ('approve', 'Waiting Finance'),
                              ('post', 'Posted'),
                              ('done', 'Paid'),
                              ('cancel', 'Refused')
                              ], string='Status', index=True, readonly=True, track_visibility='onchange', copy=False, default='submit', required=True,help='Expense Report State')
    
    @api.multi
    def approve_expense_sheets(self):
        if not self.user_has_groups('hr_expense.group_hr_expense_manager'):
            #raise UserError(_("Only HR Officers can approve expenses"))
            raise UserError(_("Only Manager can be Approve"))
        self.expense_line_ids.check_cost_expense()
        self.write({'state': 'approve0', 'responsible_id': self.env.user.id})

    @api.multi
    def approve1_expense_sheets(self):
        if not self.user_has_groups('nieve_expense.group_hr_expense_general_manager'):
            raise UserError(_("Only General Manager can be Approve"))
        self.expense_line_ids.check_cost_expense()
        self.write({'state': 'approve1', 'responsible_id': self.env.user.id})

    @api.multi
    def approve2_expense_sheets(self):
        if not self.user_has_groups('nieve_expense.group_hr_expense_director_sales'):
            raise UserError(_("Only General Manager can be Approve"))
        self.expense_line_ids.check_cost_expense()
        self.write({'state': 'approve', 'responsible_id': self.env.user.id})
        
        
    @api.multi
    def cancel(self):
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'hr.expense.refuse.wizard',
            'target': 'new',
            'action_id': self.env.ref('hr_expense.hr_expense_refuse_wizard_action').read()[0],
            'context': {'hr_expense_refuse_model' : 'hr.expense.sheet'}
            #refuse_model = self.env.context.get('hr_expense_refuse_model')
#             'context': {
#                 'default_expense_line_ids': [line.id for line in self],
#                 'default_employee_id': self[0].employee_id.id,
#                 'default_name': self[0].name if len(self.ids) == 1 else ''
#             }
        }