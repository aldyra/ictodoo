# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "CS Expenses",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.sriwijayaair.co.id',
    'category': 'Invoicing Management',
    'summary': 'Expenses Linked CS',
    "description": """Last Updated : 03-01-2021
    CS Expeneses

    """,
    "depends": [
        'hr_expense','sale_expense','nieve_cost_sheet'
    ],
    'data': [
        'security/hr_expense_security.xml',
        'views/hr_expense_views.xml',
        'views/product_view.xml',
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
