from odoo import fields, models


class InheritVendorBills(models.Model):
    _inherit = 'account.invoice'

    so_id = fields.Many2one('sale.order', 'SO Ref.')
    customer_id = fields.Many2one('res.partner', 'Customer')


class InheritStockPicking(models.Model):
    _inherit = 'stock.picking'

    ship_by = fields.Char('Ship By')


class InheritResPartner(models.Model):
    _inherit = 'res.partner'

    fax = fields.Char(string='Fax Number')
