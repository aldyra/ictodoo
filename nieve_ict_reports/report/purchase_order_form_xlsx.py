import datetime
from datetime import datetime, timedelta
from openerp import api, fields, models, _
from odoo import models
import xlsxwriter
from urllib.request import urlopen
from io import BytesIO
import os

from odoo.addons.nieve_amount2text_idr import amount_to_text_id

class PurchaseOrderFormXlsx(models.AbstractModel):
    _name = 'report.nieve_ict_reports.purchase.order.form.xlsx'
    _inherit = 'report.report_xlsx.abstract'




    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    def generate_xlsx_report(self, workbook, data, partners):
        for obj in partners:
            report_name = obj.name

            sheet = workbook.add_worksheet(report_name[:31])
            bold1 = workbook.add_format({'bold': True,'font_size': 14,'align':'left'})
            bold2 = workbook.add_format({'font_size': 12})
            string_italic = workbook.add_format({'font_size': 12,'underline':True})
            bold2_bold = workbook.add_format({'bold': True,'font_size': 12})
            bold2line = workbook.add_format({'font_size': 12,'bottom':2,'top':2,})
            bold2line_bottom = workbook.add_format({'font_size': 12,'bottom':2,})
            string = workbook.add_format({'font_size': 20,'align':'center','bottom':2,'top':2,'bold': True,})
            string2 = workbook.add_format({'font_size': 18,'align':'center'})
            bold3 = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left'})
            header = workbook.add_format({'font_size': 12,'bold': True,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
            bold9 = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
            bold9left = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left'})
            bold9leftbold = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left','bold': True,})
            bold9right = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'right'})
            bold9rightbold = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'right','bold': True})
            bold5 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'center'})
            bold6 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'right'})
            string_left = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'left'})
            string_right = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'right'})
            string_left_bold = workbook.add_format({'bold': True,'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left'})
            string_right_bold = workbook.add_format({'bold': True,'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'right'})
            string_right_bold_value = workbook.add_format({'bold': True,'font_size': 12,'align':'right','bottom':2,'top':2,})
            string_right_bold_value_o = workbook.add_format({'bold': True,'font_size': 12,'align':'right','bottom':2,'top':2,'right':2})
            string_right_bold_value_e = workbook.add_format({'bold': True,'font_size': 12,'align':'right','bottom':2,'top':2,'right':2})
            string_right_bold_value_s = workbook.add_format({'bold': True,'font_size': 12,'align':'right'})
            string_left_bold_value = workbook.add_format({'bold': True,'font_size': 12,'bottom':2,'top':2,'right':2,'align':'right','num_format':'#,##0.00'})
            string_center_bold = workbook.add_format({'bold': True,'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'center'})
            string_right = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'right'})
            string_center = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
            string_center_qty = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'center','valign': 'vcenter',})
            string_left_wraptex = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left','text_wrap': True})
            bold4 = workbook.add_format({'font_size': 12,'align':'left'})
            bold = workbook.add_format({'bold': True,'font_size': 14})
            bold_font = workbook.add_format({'bold': True,'valign': 'center','font_size': 11,'top':6})
            ship = workbook.add_format({'bold': True,'font_size': 12,'align':'left'})


            user=self.env['res.users'].browse(self.env.uid)
            partner = self.env['res.partner'].browse(user.company_id.id)

            partner_name = partner.name



            script_dir = os.path.dirname(__file__)
            logo_file_path = os.path.join(script_dir, '../static/src/description/ict.png')
            sheet.insert_image('A2',logo_file_path,{'x_scale': 0.6, 'y_scale': 0.6})

            sheet.hide_gridlines(2)



            sheet.set_column('A:A', 20)
            sheet.set_column('B:B', 40)
            sheet.set_column('C:C', 30)
            sheet.set_column('D:D', 5)
            sheet.set_column('E:E', 5)
            sheet.set_column('F:F', 20)

            sheet.write('B2','PT. INFRACOM TECHNOLOGY',bold1)
            sheet.write('B3','Gedung MD Place Tower I, 1st Floor',bold2)
            sheet.write('B4','Jl. Setiabudi Selatan No. 7',bold2)
            sheet.write('B5','Jakarta Selatan 12910',bold2)
            sheet.write('B6','Tel: +62-21 29669271',bold2)
            sheet.write('B7','Fax: +62-21 29669273',bold2)

            # date_invoice = datetime.strptime(obj.date_order, '%Y-%m-%d').strftime("%d-%m-%Y") if obj.date_order else ''
            sheet.merge_range('A9:F10','PURCHASE ORDER',string)



            sheet.write('A13',str('PO NO'),bold2)
            sheet.write('B13',str(': ')+str(obj.name or ''),bold2)
            sheet.write('A14',str('To'),bold2)
            sheet.write('B14',str(': ')+str(obj.partner_id.name or ''),bold2)
            sheet.write('A15',str('Contact Person'),bold2)
            sheet.write('B15',str(': '),bold2)
            sheet.write('A16',str('Address'),bold2)
            sheet.write('B16',str(': ')+str(obj.partner_id.street or ''),bold2)
            sheet.write('B17',str('  ')+str(obj.partner_id.street2 or ''),bold2)
            sheet.write('B18',str('  ')+str(obj.partner_id.city or ''),bold2)
            sheet.write('B19',str('  ')+str(obj.partner_id.state_id.name or ''),bold2)
            sheet.write('B20',str('  ')+str(obj.partner_id.zip or ''),bold2)


            sheet.write('A21',str('Phone Number'),bold2)
            sheet.write('B21',str(': ')+str(obj.partner_id.phone or ''),bold2)
            sheet.write('A22',str('Fax Number'),bold2)
            sheet.write('B22',str(': ')+str(obj.partner_id.fax or ''),bold2)
            sheet.write('A23',str('Date'),bold2)
            sheet.write('B23',str(': ')+str(obj.date_order or ''),bold2)
            sheet.write('A24',str('Ref. CS No'),bold2)
            sheet.write('B24',str(': ')+str(obj.kk_cost_sheet_id.name or ''),bold2)
            sheet.write('A25',str('PO/SPK/Contract No'),bold2)
            sheet.write('B25',str(': ')+str(obj.partner_ref or ''),bold2)
            sheet.write('A26',str('Ref. Quotation'),bold2)
            sheet.write('B26',str(': ')+str(obj.ref_quotation or ''),bold2)
            sheet.write('A27',str('PO Description'),bold2)
            sheet.write('B27',str(': ')+str(obj.po_desc or ''),bold2)



            sheet.write('A29','Part Number',header)
            sheet.merge_range('B29:C29','SKU Description',header)
            sheet.write('D29','Qty',header)
            sheet.merge_range('E29:F29','Tot. Price After Disc.',header)

            row =30


            for i in obj.order_line:
                price_subtotal= str('{:,.2f}'.format(i.price_subtotal or 0.0))
                sheet.merge_range('A'+str(row)+str(':')+'D'+str(row),str(i.product_id.name or ''),string_left_bold)
                sheet.write('E'+str(row),str(i. currency_id.symbol),string_right_bold_value)
                sheet.write('F'+str(row),str(price_subtotal),string_right_bold_value_o)
                for detail in i.kk_order_line_ids:
                    row +=1
                    sheet.write('A'+str(row),detail.product_code or '',string_left_wraptex)
                    sheet.merge_range('B'+str(row)+str(':')+'C'+str(row),str(detail.name or ''),string_left_wraptex)
                    sheet.write('D'+str(row),detail.product_uom_qty or 0.0 ,string_center_qty)
                    sheet.merge_range('E'+str(row)+str(':')+'F'+str(row),'',string_right_bold_value_e)
                row +=1
            amount_untaxed= '{:,.2f}'.format(obj.amount_untaxed or 0.0)
            amount_tax= '{:,.2f}'.format(obj.amount_tax or 0.0)
            amount_total= '{:,.2f}'.format(obj.amount_total or 0.0)
            sheet.merge_range('A'+str(row)+str(':')+'D'+str(row),'Total',string_right_bold)
            sheet.merge_range('A'+str(row+1)+str(':')+'D'+str(row+1),'Total Tax',string_right_bold)
            sheet.merge_range('A'+str(row+2)+str(':')+'D'+str(row+2),'GRAND TOTAL',string_right_bold)
            sheet.write('E'+str(row),str(i. currency_id.symbol),string_right_bold_value)
            sheet.write('E'+str(row+1),str(i. currency_id.symbol),string_right_bold_value)
            sheet.write('E'+str(row+2),str(i. currency_id.symbol),string_right_bold_value)
            sheet.write('F'+str(row),str(amount_untaxed),string_left_bold_value)
            sheet.write('F'+str(row+1),str(amount_tax),string_left_bold_value)
            sheet.write('F'+str(row+2),str(amount_total),string_left_bold_value)

            sheet.write('A'+str(row+3),'Terms and Conditions :',ship)
            sheet.write('A'+str(row+4),obj.notes,bold2)
            sheet.write('A'+str(row+8),'Best Regards,',ship)
            sheet.write('A'+str(row+13),'Stefanus Sudyatmiko',string_italic)
            sheet.write('A'+str(row+14),'Director',bold2)
