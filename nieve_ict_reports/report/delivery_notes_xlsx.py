
import datetime
from datetime import datetime, timedelta
from openerp import api, fields, models, _
from odoo import models
import xlsxwriter
import os

class PartnerXlsx(models.AbstractModel):
    _name = 'report.nieve_ict_reports.delivery.notes.xlsx'
    _inherit = 'report.report_xlsx.abstract'



    def generate_xlsx_report(self, workbook, data, partners):
        for obj in partners:
            report_name = obj.name

            sheet = workbook.add_worksheet(report_name[:31])
            bold1 = workbook.add_format({'bold': True,'font_size': 14,'align':'left'})
            bold2 = workbook.add_format({'font_size': 12})
            string = workbook.add_format({'font_size': 18,'align':'center','bottom':2,'top':2,})
            string2 = workbook.add_format({'font_size': 18,'align':'center'})
            bold3 = workbook.add_format({'font_size': 12,'bold': True,'bottom':2,'top':2,'left':2,'right':2,'align':'left'})
            bold3_string = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left', 'text_wrap' : True})
            bold3_string.set_align('top')
            bold3_center = workbook.add_format({'font_size': 12,'bold': True,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
            bold3_center_qty = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
            bold3_center_qty.set_align('center')
            bold3_center_qty.set_align('vcenter')
            bold9 = workbook.add_format({'font_size': 12,'bold': True,'bottom':2,'top':2,'left':2,'right':2,'align':'right'})
            bold5 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'center'})
            bold6 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'right'})
            bold7 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'left'})
            bold4 = workbook.add_format({'font_size': 12,'align':'left'})
            bold = workbook.add_format({'bold': True,'font_size': 14})
            bold_font = workbook.add_format({'bold': True,'valign': 'center','font_size': 11,'top':6})
            ship = workbook.add_format({'bold': True,'font_size': 12,'align':'left'})
            sheet.insert_image('A2','/nieve_ict_reports/static/src/description/ict.png',{'x_scale': 0.6, 'y_scale': 0.6})
            sheet.hide_gridlines(2)

            script_dir = os.path.dirname(__file__)
            logo_file_path = os.path.join(script_dir, '../static/src/description/ict.png')
            sheet.insert_image('A2',logo_file_path,{'x_scale': 0.6, 'y_scale': 0.6})

            sheet.set_column('A:A', 20)
            sheet.set_row(40)
            sheet.set_column('B:B', 30)
            sheet.set_column('C:C', 60)
            sheet.set_column('D:D', 10)
            sheet.set_column('E:E', 10)
            sheet.set_column('F:F', 20)

            sheet.write('B2','PT. INFRACOM TECHNOLOGY',bold1)
            sheet.write('B3','Gedung MD Place Tower I, 1st Floor',bold2)
            sheet.write('B4','Jl. Setiabudi Selatan No. 7',bold2)
            sheet.write('B5','Jakarta Selatan 12910',bold2)
            sheet.write('B6','Tel: +62-21 29669271',bold2)
            sheet.write('B7','Fax: +62-21 29669273',bold2)

            sheet.merge_range('A9:D10','DELIVERY NOTES',string)

            sheet.write('A12','Ref.	No',bold2)
            sheet.write('A13','Date',bold2)
            sheet.write('A14','PO/SPK/Contract	No',bold2)

            date2 = datetime.strptime(obj.scheduled_date, '%Y-%m-%d %H:%M:%S').strftime("%d-%m-%Y %H:%M:%S") if obj.scheduled_date else ''
            sheet.write('B12',str(': ')+str(obj.name or ''),bold2)
            sheet.write('B13',str(': ')+str(date2 or ''),bold2)
            sheet.write('B14',str(': ')+str(obj.client_order_ref or '') ,bold2)

            sheet.merge_range('A16:A17','SHIP TO',string2)
            sheet.merge_range('A18:B19',str(obj.cost_sheet_id.partner_id.name or ''),bold1)
            date3 = datetime.strptime(obj.scheduled_date, '%Y-%m-%d %H:%M:%S').strftime("%d-%m-%Y") if obj.scheduled_date else ''

            sheet.merge_range('A21:B21','Product',bold3_center)
            sheet.write('C21','Description',bold3_center)
            sheet.write('D21','Qty',bold3_center)
            row =22

            for i in obj.move_lines:
                sheet.merge_range('A'+str(row)+str(':')+'B'+str(row),i.product_id.name or '',bold3_string)
                sheet.write('C'+str(row),i.name or '',bold3_string)
                sheet.write('D'+str(row),i.product_uom_qty or '' ,bold3_center_qty)
                row += 1

            sheet.write('B'+str(row+3),'SHIP BY',ship)
            sheet.write('D'+str(row+3),'RECEIVED BY',ship)
            sheet.write('B'+str(row+5),'Name :',bold2)
            sheet.write('D'+str(row+5),'Name :',bold2)

            sheet.write('B'+str(row+11),'Date : '+ str(date3 or ''),bold2)
            sheet.write('D'+str(row+11),'Date : '+ str(date3 or ''),bold2)
