# from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsxAbstract
from odoo import models


class CashFlowXlsx(models.AbstractModel):
    _name = 'report.nieve_ict_reports.cash_flow_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def get_cashflow_category_line(self,id):
        res = self.env['sub.category.line'].search([('category_id','=',id)])
        return res

    def get_move_id(self,date_from,date_to):
        result = []
        self.env.cr.execute("""
            select aml.id as id from account_move_line aml join account_account aa on aml.account_id = aa.id
            where internal_type = 'liquidity' and aml.date >= %s and aml.date <= %s
        """,(date_from,date_to))
        result = [x[0] for x in self.env.cr.fetchall()]
        return result

    def get_move_line_id(self,date_from,date_to):
        result = []
        self.env.cr.execute("""
                    select aml.move_id as move_id from account_move_line aml join account_account aa on aml.account_id = aa.id
                    where internal_type = 'liquidity' and aml.date >= %s and aml.date <= %s
                """, (date_from, date_to))
        result = [x[0] for x in self.env.cr.fetchall()]
        return result

    def get_beginning_amount(self, date_from):
        result = []
        self.env.cr.execute("""
                       select aml.balance as balance from account_move_line aml join account_account aa on aml.account_id = aa.id
                       where internal_type = 'liquidity' and aml.date < '%s'
                   """ % (date_from))
        result = [x[0] for x in self.env.cr.fetchall()]
        return result

    def get_move_line(self,date_from,date_to,categ_id):
        data_move_id = self.get_move_id(date_from, date_to)
        data_move_line_id = self.get_move_line_id(date_from, date_to)
        if data_move_line_id:
            self.env.cr.execute("""
                                select aa.name,aa.code,aml.account_id,sum(balance) as balance from account_move_line aml
                                join account_account aa on aml.account_id = aa.id
                                where move_id in %s and aml.id not in %s and aa.sub_cashflow_category_id = %s
                                group by aa.name,aa.code,aml.account_id
                            """, (tuple(data_move_line_id),tuple(data_move_id),categ_id))
            result = self.env.cr.dictfetchall()
            return result

    def generate_xlsx_report(self, workbook, data, partners):
        report_name = "Cash Flow Report"
        # One sheet by partner
        sheet = workbook.add_worksheet(report_name[:31])

        bold = workbook.add_format({'bold': True, 'font_size': 11,'align':'center','font_color':'black'})
        bold2 = workbook.add_format({'bold': True, 'font_size': 16,'align':'center','font_color':'#483D8B'})
        bold4 = workbook.add_format({'bold': True, 'font_size': 11,'align':'left','font_color':'#483D8B'})
        bold5 = workbook.add_format({'num_format':'#,##0.00;(#,##0.00)','bold': True, 'font_size': 11,'align':'right','font_color':'#483D8B'})
        bold3 = workbook.add_format({'bold': True, 'font_size': 9,'align':'center','font_color':'#8B0000'})
        bold_font = workbook.add_format({'num_format':'#,##0.00;(#,##0.00)','bold': True, 'font_size': 11,'top':True})
        bold_font_i = workbook.add_format({'bold': True, 'italic':True,'font_size': 11})
        regular_font_i = workbook.add_format({'bold': False, 'italic':True,'font_size': 11})
        bold_font_wborder = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                 'align': 'center', 'text_wrap': True, 'valign': 'vcenter',
                                                 'fg_color': '#C0C0C0','font_color':'black'})
        bold_font_wborder2 = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                 'align': 'center', 'text_wrap': True, 'valign': 'vcenter', 'font_color': 'black'})
        bold_font_wborder_right= workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                  'align': 'right', 'text_wrap': True,
                                                  'font_color': 'black'})
        bold_font_wborder_left = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                       'align': 'left', 'text_wrap': True, 'valign': 'vcenter',
                                                       'font_color': 'black'})
        regular_font = workbook.add_format({'num_format':'#,##0.00;(#,##0.00)','bold': False, 'font_size': 11})
        regular_font2 = workbook.add_format({'num_format':'#,##0.00;(#,##0.00)','bold': True, 'font_size': 11})
        regular_font_address = workbook.add_format({'bold': False, 'font_size': 10, 'valign': 'top'})
        regular_font_wborder = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                    'align': 'left', 'fg_color': '#696969', 'font_color': 'white'})
        regular_font_wborder2 = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                    'align': 'left'})
        regular_font_wborder_i = workbook.add_format({'bold': True,'italic': True, 'font_size': 11, 'border': True,
                                                    'align': 'left'})
        regular_font_wborder_i2 = workbook.add_format({'bold': False, 'italic': True, 'font_size': 11, 'border': True,
                                                      'align': 'left'})
        normal_font_wborder = workbook.add_format({'num_format':'#,##0.00','bold': False, 'font_size': 11, 'border': True, })

        # number_bold.set_num_format('#,##0.00;(#,##0.00)')
        sheet.hide_gridlines(2)
        sheet.set_column('A:A', 25)
        sheet.set_column('B:B', 35)
        sheet.set_column('C:C', 10)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 20)
        sheet.set_column('H:H', 20)
        sheet.set_column('I:I', 20)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 20)

        sheet.merge_range('A1:D2','PT Infracom Tech',bold)
        sheet.merge_range('A3:D4','Statement of Cash Flow',bold2)
        sheet.merge_range('A5:D6','Senin, 01 Januari 2018 - Rabu, 31 Januari 2018',bold3)

        date_from = partners.date_from
        date_to = partners.date_to
        move_id = self.get_move_id(date_from,date_to)
        move_line_id = self.get_move_line_id(date_from,date_to)
        beginning_amount_data = self.get_beginning_amount(date_from)
        begin_amt = 0
        for amt in beginning_amount_data:
            begin_amt = begin_amt + amt

        # move_line_data = self.get_move_line(date_from,date_to)


        # print (move_line_data)

        row = 8
        sign = -1
        sum_balance = 0
        tot_balance = 0
        end_balance = 0
        net_cash = 0
        for cat in self.env['cash.flow.category'].search([]):
            net_cash = 0

            sheet.merge_range('A'+str(row)+':'+'D'+str(row),cat.name or '',bold4)
            for sub in self.get_cashflow_category_line(cat.id):
                sum_balance = 0
                row+=1
                sheet.merge_range('A'+str(row)+':'+'D'+str(row),'   '+(sub.name or ''),regular_font)
                for acc in self.get_move_line(date_from,date_to,sub.id):
                    row+=1

                    sheet.write('A' + str(row),'           '+(acc['code'] or ''),regular_font)
                    sheet.write('B' + str(row),acc['name'] or '',regular_font)
                    sheet.write('C' + str(row),acc['balance']*sign or '0.0',regular_font)
                    sum_balance = sum_balance + acc['balance']*sign
                    tot_balance = tot_balance + sum_balance
                sheet.merge_range('A'+str(row+1)+':'+'B'+str(row+1),'   '+'Total '+str(sub.name or ''),regular_font2)
                sheet.write('C'+str(row+1),sum_balance,bold_font)
                row += 2
                net_cash += sum_balance
            sheet.merge_range('A' + str(row + 1) + ':' + 'B' + str(row + 1), 'Net cash provided by '+str(sub.name or ''),
                              bold4)
            sheet.write('C' + str(row + 1), net_cash or 0.0, bold5)
            row+=4
            end_balance = tot_balance + begin_amt
            sheet.merge_range('A' + str(row) + ':' + 'B' + str(row),'Net Cash Increase / Decrease ',
                              bold4)
            sheet.write('C'+str(row ), tot_balance or 0.0, bold5)
            sheet.merge_range('A' + str(row + 2) + ':' + 'B' + str(row + 2),
                              'Cash at beginning of period ', bold4)
            sheet.write('C'+str(row + 2), begin_amt or 0.0, bold5)
            sheet.merge_range('A' + str(row + 4) + ':' + 'B' + str(row + 4),
                              'Cash at end of period ',
                              bold4)
            sheet.write('C'+str(row + 4), end_balance or 0.0, bold5)
        workbook.close()

# CostSheetFormXls('report.cost.sheet.xlsx', 'kk.cost.sheet')
