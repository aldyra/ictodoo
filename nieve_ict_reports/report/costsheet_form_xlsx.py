# from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsxAbstract
from odoo import models
import datetime
from datetime import datetime, timedelta

class CostSheetFormXls(models.AbstractModel):
    _name = 'report.nieve_ict_reports.costsheet_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def get_data(self,id):
        res = self.env['kk.cost.sheet.line'].search([('kk_cost_sheet_id','=',id)])
        self.env['product.category'].search([('id','=',res.product_id.categ_id.id)])
        return res

    def get_prod_categ(self,id):
        res = self.env['kk.cost.sheet.line'].search([('kk_cost_sheet_id','=',id)])
        product_category_ids = []
        for i in res:
            product_category_ids.append(i.product_id.categ_id.id)

        return self.env['product.category'].browse(list(set(product_category_ids)))

    def get_exp_categ(self,id):
        res = self.env['kk.cost.expense.line'].search([('kk_cost_sheet_id','=',id)])
        product_category_ids = []
        for i in res:
            product_category_ids.append(i.product_id.categ_id.id)

        return self.env['product.category'].browse(list(set(product_category_ids)))

    def prod_categ(self,):
        res = self.env['product.category'].search([])
        return res

    def get_detail(self,categ_id,cost_sheet_id):
        product_ids = self.env['product.product'].search([('categ_id','=',categ_id)])

        cs_line = self.env['kk.cost.sheet.line'].search([('product_id', 'in', product_ids.ids),('kk_cost_sheet_id','=',cost_sheet_id)])

        return cs_line

    def get_expense(self, categ_id, cost_sheet_id):
        product_ids = self.env['product.product'].search([('categ_id','=',categ_id)])

        cs_line = self.env['kk.cost.expense.line'].search([('product_id', 'in', product_ids.ids),('kk_cost_sheet_id','=',cost_sheet_id)])

        return cs_line



    def generate_xlsx_report(self, workbook, data, partners):
        report_name = "Cost Sheet Form"
        # One sheet by partner
        sheet = workbook.add_worksheet(report_name[:31])
        bold = workbook.add_format({'bold': True, 'font_size': 11,'align':'center','fg_color':'#C0C0C0','font_color':'black'})
        bold2 = workbook.add_format({'bold': True, 'font_size': 11,'align':'left','fg_color':'#C0C0C0','font_color':'black','border':True})
        bold4 = workbook.add_format({'bold': True, 'font_size': 11,'align':'left','fg_color':'#F5F5F5','font_color':'black','border':True})
        bold3 = workbook.add_format({'bold': True, 'font_size': 11,'align':'right','fg_color':'#F5F5F5','font_color':'black','border':True})
        bold_font = workbook.add_format({'bold': True, 'font_size': 11})
        bold_font_i = workbook.add_format({'bold': True, 'italic':True,'font_size': 11})
        regular_font_i = workbook.add_format({'bold': False, 'italic':True,'font_size': 11})
        bold_font_wborder = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                 'align': 'center', 'text_wrap': True, 'valign': 'vcenter',
                                                 'fg_color': '#C0C0C0','font_color':'black'})
        bold_font_wborder2 = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                 'align': 'center', 'text_wrap': True, 'valign': 'vcenter', 'font_color': 'black'})
        bold_font_wborder_right= workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                  'align': 'right', 'text_wrap': True,
                                                  'font_color': 'black'})
        bold_font_wborder_left = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                       'align': 'left', 'text_wrap': True, 'valign': 'vcenter',
                                                       'font_color': 'black'})
        regular_font = workbook.add_format({'bold': False, 'font_size': 11})
        regular_font_address = workbook.add_format({'bold': False, 'font_size': 10, 'valign': 'top'})
        regular_font_wborder = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                    'align': 'left', 'fg_color': '#696969', 'font_color': 'white'})
        regular_font_wborder2 = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                    'align': 'left'})
        regular_font_wborder_i = workbook.add_format({'bold': True,'italic': True, 'font_size': 11, 'border': True,
                                                    'align': 'left'})
        regular_font_wborder_i2 = workbook.add_format({'bold': False, 'italic': True, 'font_size': 11, 'border': True,
                                                      'align': 'left'})
        normal_font_wborder = workbook.add_format({'num_format':'#,##0.00','bold': False, 'font_size': 11, 'border': True, })
        font_wborder_right = workbook.add_format({'bold': False, 'font_size': 11, 'border': True, 'align':'right'})

        sheet.set_column('A:A', 25)
        sheet.set_column('B:B', 35)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 20)
        sheet.set_column('H:H', 10)
        sheet.set_column('I:I', 20)
        sheet.set_column('J:J', 20)
        sheet.set_column('K:K', 20)

        sheet.merge_range('A1:K1','COST SHEET (INTERNAL ONLY)',bold)
        sheet.write('A2','Project Information',bold_font)
        sheet.write('A3','Project ID',regular_font)
        sheet.write('A4','Revision No. / Date',regular_font)
        sheet.write('A5','Customer Name',regular_font)
        sheet.write('A6','Project Name',regular_font)
        sheet.write('A7','PO/SPK/Contract No.',regular_font)
        sheet.write('A8','Currency',regular_font)
        sheet.write('A9','Account Manager',regular_font)

        sheet.merge_range('A11:A12','Source and Part #',bold_font_wborder)
        sheet.merge_range('B11:B12','Description',bold_font_wborder)
        sheet.merge_range('C11:C12','Qty',bold_font_wborder)
        sheet.merge_range('D11:F11','Selling Price',bold_font_wborder)
        sheet.write('D12','List Price',bold_font_wborder)
        sheet.write('E12','Discounted Price',bold_font_wborder)
        sheet.write('F12','% Disc',bold_font_wborder)
        sheet.merge_range('G11:H11','',bold_font_wborder)
        sheet.write('G12','Cost',bold_font_wborder)
        sheet.write('H12','% Disc',bold_font_wborder)
        sheet.merge_range('I11:J11','Gross Profit',bold_font_wborder)
        sheet.write('I12','Total',bold_font_wborder)
        sheet.write('J12','%',bold_font_wborder)
        sheet.write('K11','Status PO Out',bold_font_wborder)
        sheet.write('K12','PO No & Date',bold_font_wborder)

        row = 13

        for data in partners:
            sheet.write('B3',': '+str(data.name) or '',regular_font)
            sheet.write('B4',': ',regular_font)
            sheet.write('B5',': '+str(data.partner_id.name) or '',regular_font)
            sheet.write('B6',': '+str(data.project_name) or '',regular_font)
            sheet.write('B7',': ',regular_font)
            sheet.write('B8',': '+str(data.currency_id.name) or '',regular_font)
            sheet.write('B9',': ',regular_font)
            sum_cost_price_total =0.0
            sum_cost_discount_amount =0.0
            sum_cost_price_unit =0.0
            sum_discount_amount =0.0
            for item in self.get_prod_categ(data.id):
                sheet.merge_range('A'+str(row)+':K'+str(row),item.name or '',bold2)
                row += 1
                for detail in self.get_detail(item.id, data.id):
                    sum_cost_price_total += detail['cost_price_total']
                    sum_cost_discount_amount += detail['cost_discount_amount']
                    sum_cost_price_unit += detail['cost_price_unit']
                    sum_discount_amount += detail['discount_amount']
                    sheet.write('A'+str(row), detail.product_id.name or '',normal_font_wborder)
                    sheet.write('B'+str(row), detail['name'] or '', normal_font_wborder)
                    sheet.write('C'+str(row), detail['product_uom_qty'] or 0,normal_font_wborder)
                    sheet.write('D'+str(row), detail['price_unit'] or 0,normal_font_wborder)
                    sheet.write('E'+str(row),detail['discount_amount'] or 0,normal_font_wborder)
                    sheet.write('F'+str(row),detail['discount'] or 0,normal_font_wborder)
                    sheet.write('G'+str(row),detail['cost_price_unit'] or 0,normal_font_wborder)
                    sheet.write('H'+str(row),detail['cost_discount_amount'] or 0,normal_font_wborder)
                    sheet.write('I'+str(row),detail['cost_price_total'] or 0,normal_font_wborder)
                    sheet.write('J'+str(row),detail['gross_profit_percentage'] or 0,normal_font_wborder)
                    sheet.write('K'+str(row),0,normal_font_wborder)
                    row+=1
                sheet.merge_range('A'+str(row)+':C'+str(row),'Sub Total',bold3)
                sheet.write('D'+str(row),'',bold4)
                sheet.write('E'+str(row),'Rp. '+str('{:,.2f}'.format(sum_discount_amount or 0.0)),bold3)
                sheet.write('F'+str(row),'',bold4)
                sheet.write('G'+str(row),'Rp. '+str('{:,.2f}'.format(sum_cost_price_unit or 0.0)),bold3)
                sheet.write('H'+str(row),'Rp. '+str('{:,.2f}'.format(sum_cost_discount_amount or 0.0)),bold3)
                sheet.write('I'+str(row),'Rp. '+str('{:,.2f}'.format(sum_cost_price_total or 0.0)),bold3)
                sheet.write('J'+str(row),'',bold4)
                sheet.write('K'+str(row),'',bold4)
            sum_cost_expend =0.0
            for exp in self.get_exp_categ(data.id):
                sheet.merge_range('A' + str(row+1) + ':K' + str(row+1), exp.name or '', bold2)
                row+=1
                for expense in self.get_expense(exp.id,data.id):
                    sum_cost_expend += expense.expense_plan
                    sheet.write('A' + str(row+1), expense.product_id.name or '', normal_font_wborder)
                    sheet.write('B' + str(row+1), expense.name or '', normal_font_wborder)
                    sheet.write('C' + str(row+1),'',normal_font_wborder)
                    sheet.write('D' + str(row+1), expense.expense_plan or 0, normal_font_wborder)
                    row+=1
                sheet.merge_range('A' + str(row+1) + ':C' + str(row+1), 'Sub Total', bold3)
                sheet.write('D' + str(row+1),'Rp. '+str('{:,.2f}'.format(sum_cost_expend or 0.0)), bold3)
            row = row + 3
            sheet.write('C'+ str(row),'PAYMENT SCHEDULE:',bold_font_i)
            sheet.write('C'+ str(row + 1),'Term of Payment',bold_font_wborder2)
            sheet.write('D'+ str(row + 1),'Percentage (%)',bold_font_wborder2)
            sheet.write('E'+ str(row + 1),'Estimate to Invoice',bold_font_wborder2)
            sheet.write('F'+ str(row + 1),'Total Invoice',bold_font_wborder2)
            sheet.write('G'+ str(row + 1),'Target Date',bold_font_wborder2)
            sheet.write('I' + str(row),'APPROVALS:',bold_font_i)
            if data.state=='post':
                sheet.merge_range('J' + str(row) + ':K'+ str(row),'(by signature or email)',regular_font_i)
                sheet.merge_range('I' + str(row + 1) + ':I'+ str(row + 2),'Division Manager',regular_font_wborder2)
                sheet.write('J' + str(row + 1),'Approval Manager',regular_font_wborder2)
                sheet.write('J' + str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('K' + str(row + 1) + ':K'+ str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 3) + ':I' + str(row + 4), 'Service Manager', regular_font_wborder2)
                sheet.write('J' + str(row + 3), 'Approval G. Manager', regular_font_wborder2)
                sheet.write('J' + str(row + 4), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 3) + ':K'+ str(row + 4),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 5) + ':I' + str(row + 6), 'President Director', regular_font_wborder2)
                sheet.write('J' + str(row + 5), 'Approval Director', regular_font_wborder2)
                sheet.write('J' + str(row + 6), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 5) + ':K' + str(row + 6), '', regular_font_wborder2)
            elif data.state=='appr_drc':
                sheet.merge_range('J' + str(row) + ':K'+ str(row),'(by signature or email)',regular_font_i)
                sheet.merge_range('I' + str(row + 1) + ':I'+ str(row + 2),'Division Manager',regular_font_wborder2)
                sheet.write('J' + str(row + 1),'Approval Manager',regular_font_wborder2)
                sheet.write('J' + str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('K' + str(row + 1) + ':K'+ str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 3) + ':I' + str(row + 4), 'Service Manager', regular_font_wborder2)
                sheet.write('J' + str(row + 3), 'Approval G. Manager', regular_font_wborder2)
                sheet.write('J' + str(row + 4), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 3) + ':K'+ str(row + 4),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 5) + ':I' + str(row + 6), 'President Director', regular_font_wborder2)
                sheet.write('J' + str(row + 5), '', regular_font_wborder2)
                sheet.write('J' + str(row + 6), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 5) + ':K' + str(row + 6), '', regular_font_wborder2)
            elif data.state=='appr_gm':
                sheet.merge_range('J' + str(row) + ':K'+ str(row),'(by signature or email)',regular_font_i)
                sheet.merge_range('I' + str(row + 1) + ':I'+ str(row + 2),'Division Manager',regular_font_wborder2)
                sheet.write('J' + str(row + 1),'Approval Manager',regular_font_wborder2)
                sheet.write('J' + str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('K' + str(row + 1) + ':K'+ str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 3) + ':I' + str(row + 4), 'Service Manager', regular_font_wborder2)
                sheet.write('J' + str(row + 3), '', regular_font_wborder2)
                sheet.write('J' + str(row + 4), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 3) + ':K'+ str(row + 4),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 5) + ':I' + str(row + 6), 'President Director', regular_font_wborder2)
                sheet.write('J' + str(row + 5), '', regular_font_wborder2)
                sheet.write('J' + str(row + 6), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 5) + ':K' + str(row + 6), '', regular_font_wborder2)
            else:
                sheet.merge_range('J' + str(row) + ':K'+ str(row),'(by signature or email)',regular_font_i)
                sheet.merge_range('I' + str(row + 1) + ':I'+ str(row + 2),'Division Manager',regular_font_wborder2)
                sheet.write('J' + str(row + 1),'',regular_font_wborder2)
                sheet.write('J' + str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('K' + str(row + 1) + ':K'+ str(row + 2),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 3) + ':I' + str(row + 4), 'Service Manager', regular_font_wborder2)
                sheet.write('J' + str(row + 3), '', regular_font_wborder2)
                sheet.write('J' + str(row + 4), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 3) + ':K'+ str(row + 4),'',regular_font_wborder2)
                sheet.merge_range('I' + str(row + 5) + ':I' + str(row + 6), 'President Director', regular_font_wborder2)
                sheet.write('J' + str(row + 5), '', regular_font_wborder2)
                sheet.write('J' + str(row + 6), '', regular_font_wborder2)
                sheet.merge_range('K' + str(row + 5) + ':K' + str(row + 6), '', regular_font_wborder2)

            row2 =row +2
            total_inv_sum =0.0
            for top in data.fal_invoice_milestone_line_date_ids:
                total_inv_sum += top.total_inv
                date_top = datetime.strptime(top.date, '%Y-%m-%d').strftime("%d-%m-%Y") if top.date else ''
                sheet.write('C' + str(row2), top.name or '',normal_font_wborder)
                sheet.write('D' + str(row2), top.percentage or '',normal_font_wborder)
                sheet.write('E' + str(row2), date_top or '',font_wborder_right)
                sheet.write('F' + str(row2), top.total_inv or 0,normal_font_wborder)
                sheet.write('G' + str(row2), '', regular_font_wborder2)
                row2 +=1


            date_time=datetime.now()
            date_now1 =date_time.date()
            date_taget = datetime.strptime(str(date_now1), '%Y-%m-%d').strftime("%B %Y")
            sheet.merge_range('C' + str(row2) + ':E' + str(row2), 'Total', bold_font_wborder_right)
            sheet.write('F' + str(row2), 'Rp. '+str('{:,.2f}'.format(total_inv_sum or 0.0)), bold_font_wborder_right)
            sheet.write('G' + str(row2), str(date_taget), bold_font_wborder2)




# CostSheetFormXls('report.cost.sheet.xlsx', 'kk.cost.sheet')
