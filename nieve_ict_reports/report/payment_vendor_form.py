from odoo import models, api
from odoo.addons.nieve_amount2text_idr import amount_to_text_id


class PaymentVendorForm(models.AbstractModel):
    _name = 'report.nieve_ict_reports.payment_vendor_form_pdf'

    def get_line(self, data):
        result = self.env['account.payment.line'].search([('payment_id', '=', data.id)])
        return result

    def get_move_line(self, data):
        result = self.env['account.move.line'].search([('id', '=', data.id)])
        return result

    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    @api.model
    def get_report_values(self, docids, data=None):
        get_data = self.env['account.payment'].search([('id', '=', docids[0])])
        val_docs = self.env['account.payment'].browse(docids[0])
        docargs = {
            'get_datax': get_data,
            'get_line': self.get_line,
            'get_move_line': self.get_move_line,
            'convert': self.convert,
            'doc_ids': docids,
            'doc_model': 'account.payment',
            'docs': val_docs,
        }
        return docargs
