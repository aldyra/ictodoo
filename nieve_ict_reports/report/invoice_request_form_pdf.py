from odoo import models, api
from odoo.addons.nieve_amount2text_idr import amount_to_text_id


class InvoiceRequestForm(models.AbstractModel):
    _name = 'report.nieve_ict_reports.invoice_request_form_pdf'

    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    def get_default_requested_by(self):
        return self.env['res.users'].browse(self.env.uid)

    @api.model
    def get_report_values(self, docids, data=None):
        get_data = self.env['fal.invoice.term.line'].search([('id', '=', docids[0])])
        print ('get_data=-----',self.get_default_requested_by)
        docargs = {
            'get_datax': get_data,
            'convert': self.convert,
            'get_default_requested_by': self.get_default_requested_by,
            'doc_ids': docids,
            'doc_model': 'fal.invoice.term.line',
            'docs': get_data,
        }
        return docargs
