# from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsxAbstract
from odoo import models


class PipelineFormXls(models.AbstractModel):
    _name = 'report.nieve_ict_reports.saleorder_pipeline_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def get_data(self, id):
        res = self.env['sale.order.line'].search([('order_id','=',id)])
        return res

    def prod_detail(self, id):
        res = self.env['kk.order.line'].search([('so_line_id','=',id)])
        return res

    def generate_xlsx_report(self, workbook, data, partners):
        report_name = "Quotation Form"
        # One sheet by partner
        sheet = workbook.add_worksheet(report_name[:31])
        bold = workbook.add_format({'bold': True,'font_size': 14})
        bold_font = workbook.add_format({'bold': True,'font_size': 11})
        bold_font_wborder = workbook.add_format({'bold': True,'font_size': 11,'border':True,
                                                 'align':'center','text_wrap':True,'valign':'vcenter','fg_color':'#228B22'})
        bold_font_wborder2 = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                 'align': 'center', 'text_wrap': True, 'valign': 'vcenter',
                                                 'fg_color': '#DC143C','font_color':'white'})
        regular_font = workbook.add_format({'bold': False,'font_size': 11})
        regular_font_address = workbook.add_format({'bold': False,'font_size': 11,'valign':'top'})
        regular_font_wborder = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,'text_wrap':True,'valign':'vcenter',
                                                     'align': 'center','fg_color': '#DCDCDC','font_color':'black'})
        normal_font_wborder_no = workbook.add_format({'bold': False, 'font_size': 11, 'border': True,'align':'center'})
        normal_font_wborder = workbook.add_format({'num_format':'#,##0.00','bold': False, 'font_size': 11, 'border': True,'align':'right'})
        normal_font_wborder_percent = workbook.add_format({'num_format': '0.00%','bold': False, 'font_size': 11, 'border': True,'align':'right'})

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 20)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 25)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 20)
        sheet.set_column('G:G', 30)
        sheet.set_column('H:H', 20)
        sheet.set_column('I:I', 40)
        sheet.set_column('J:J', 40)
        sheet.set_column('K:K', 20)
        sheet.set_column('L:L', 20)
        sheet.set_column('M:M', 20)
        sheet.set_column('N:N', 25)
        sheet.set_column('O:O', 25)
        sheet.set_column('P:P', 25)
        sheet.set_column('Q:Q', 25)
        sheet.set_column('R:R', 25)
        sheet.set_column('S:S', 20)
        sheet.set_column('T:T', 35)
        sheet.set_column('U:U', 20)
        sheet.set_column('V:V', 20)
        sheet.set_column('W:W', 35)
        sheet.set_column('X:X', 20)
        sheet.set_column('Y:Y', 45)
        sheet.set_column('Z:Z', 35)
        sheet.set_column('AA:AA', 15)
        sheet.set_row(5, 30)
        sheet.freeze_panes(6, 6)

        sheet.merge_range('A5:F5', 'Source Date', bold_font_wborder)
        sheet.write('A6', 'NO', regular_font_wborder)
        sheet.write('B6','MM/DD/YY',regular_font_wborder)
        sheet.write('C6','Quarter & Week Closed',regular_font_wborder)
        sheet.write('D6','End User',regular_font_wborder)
        sheet.write('E6','Project Name',regular_font_wborder)
        sheet.write('F6','Quote Number',regular_font_wborder)
        sheet.merge_range('G5:I5','CUSTOMER',bold_font_wborder)
        sheet.write('G6','CP',regular_font_wborder)
        sheet.write('H6','TLP',regular_font_wborder)
        sheet.write('I6','Email',regular_font_wborder)
        sheet.merge_range('J5:M5','PRINCIPAL',bold_font_wborder)
        sheet.write('J6','Principal',regular_font_wborder)
        sheet.write('K6','Sales Rep',regular_font_wborder)
        sheet.write('L6','Tlp',regular_font_wborder)
        sheet.write('M6','Email',regular_font_wborder)
        sheet.merge_range('N5:Q5','DISTRIBUTOR',bold_font_wborder)
        sheet.write('N6','Disti',regular_font_wborder)
        sheet.write('O6','Sales Rep',regular_font_wborder)
        sheet.write('P6','TLP',regular_font_wborder)
        sheet.write('Q6','Email',regular_font_wborder)
        sheet.write('R5','LEAD BY',bold_font_wborder)
        sheet.write('R6','Partner/Principal/ICT',regular_font_wborder)
        sheet.merge_range('S5:V5','OPPORTUNITY DETAILS',bold_font_wborder)
        sheet.write('S6','Amount (COGS)',regular_font_wborder)
        sheet.write('T6','Amount (SELL)',regular_font_wborder)
        sheet.write('U6','GP',regular_font_wborder)
        sheet.write('V6','%GP',regular_font_wborder)
        sheet.merge_range('W5:W6','PRODUCT NAME',bold_font_wborder)
        sheet.merge_range('X5:X6','BoM Description',bold_font_wborder)
        sheet.merge_range('Y5:Y6','UPDATE',bold_font_wborder2)
        sheet.merge_range('Z5:Z6','THINGS TO DO',bold_font_wborder2)
        sheet.merge_range('AA5:AA6','Status Done/Drop',bold_font_wborder2)

        no = 1
        row = 7
        diff = 0
        amountsell = 0

        for data in partners:
            diff = 0
            amountsell = 0
            for line in self.get_data(data.id):
                diff = diff + 1
                amountsell = line.price_unit * line.product_uom_qty
                if diff > 1:
                    sheet.write('A' + str(row), '', normal_font_wborder_no)
                    sheet.write('B' + str(row), '', normal_font_wborder)
                    sheet.write('C' + str(row), '', normal_font_wborder)
                    sheet.write('D' + str(row), '', normal_font_wborder)
                    sheet.write('E' + str(row), '', normal_font_wborder)
                    sheet.write('F' + str(row), '', normal_font_wborder)
                    sheet.write('G' + str(row), '', normal_font_wborder)
                    sheet.write('H' + str(row), '', normal_font_wborder)
                    sheet.write('I' + str(row), '', normal_font_wborder)
                    sheet.write('J' + str(row), '', normal_font_wborder)
                    sheet.write('K' + str(row), '', normal_font_wborder)
                    sheet.write('L' + str(row), '', normal_font_wborder)
                    sheet.write('M' + str(row), '', normal_font_wborder)
                    sheet.write('N' + str(row), '', normal_font_wborder)
                    sheet.write('O' + str(row), '', normal_font_wborder)
                    sheet.write('P' + str(row), '', normal_font_wborder)
                    sheet.write('Q' + str(row), '', normal_font_wborder)
                    sheet.write('R' + str(row), '', normal_font_wborder)
                    sheet.write('Y' + str(row), '', normal_font_wborder)
                    sheet.write('Z' + str(row), '', normal_font_wborder)
                    sheet.write('AA' + str(row),'', normal_font_wborder)
                else:
                    sheet.write('A'+str(row),no,normal_font_wborder_no)
                    # sheet.write('B'+str(row),data.confirmation_date and time.strftime('%d-%m-%y', time.strptime( data.confirmation_date,'%Y-%m-%d')) or '',normal_font_wborder)
                    sheet.write('B'+str(row),data.date_order or '',normal_font_wborder)
                    sheet.write('C'+str(row),data.kk_week_closed or '',normal_font_wborder)
                    sheet.write('D'+str(row),data.partner_id.name or '',normal_font_wborder)
                    sheet.write('E'+str(row),data.kk_cost_sheet_id.name or '',normal_font_wborder)
                    sheet.write('F'+str(row),data.name or '',normal_font_wborder)
                    sheet.write('G'+str(row),data.partner_id.name or '',normal_font_wborder)
                    sheet.write('H'+str(row),data.partner_id.phone or '',normal_font_wborder)
                    sheet.write('I'+str(row),data.partner_id.email or '',normal_font_wborder)
                    sheet.write('J'+str(row),data.kk_pr_name or '',normal_font_wborder)
                    sheet.write('K'+str(row),data.kk_pr_sales_rep or '',normal_font_wborder)
                    sheet.write('L'+str(row),data.kk_pr_phone or '',normal_font_wborder)
                    sheet.write('M'+str(row),data.kk_pr_email or '',normal_font_wborder)
                    sheet.write('N'+str(row),data.kk_dist_name or '',normal_font_wborder)
                    sheet.write('O'+str(row),data.kk_dist_sales_rep or '',normal_font_wborder)
                    sheet.write('P'+str(row),data.kk_dist_phone or '',normal_font_wborder)
                    sheet.write('Q'+str(row),data.kk_dist_email or '',normal_font_wborder)
                    sheet.write('R'+str(row),data.kk_lead_by or '',normal_font_wborder)
                    sheet.write('Y' + str(row), data.kk_update or '', normal_font_wborder)
                    sheet.write('Z' + str(row), data.kk_things or '', normal_font_wborder)
                    sheet.write('AA' + str(row), data.kk_state_act or '', normal_font_wborder)
                    no+=1
                sheet.write('S'+str(row),line.kk_cogs or 0,normal_font_wborder)
                sheet.write_number('T'+str(row),amountsell or 0,normal_font_wborder)
                sheet.write_formula('U'+str(row),'= T'+str(row)+'-'+'S'+str(row),normal_font_wborder)
                sheet.write_formula('V'+str(row),'= U'+str(row)+'/T'+str(row),normal_font_wborder_percent)
                sheet.write('W'+str(row),line.product_id.name or '',normal_font_wborder)
                sheet.write('X'+str(row),'',normal_font_wborder)
                row+=1

        workbook.close()


# PipelineFormXls('report.sale.order.pipeline.xlsx', 'sale.order')
