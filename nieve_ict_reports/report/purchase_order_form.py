from odoo import models, api


class PurchaseOrderForm(models.AbstractModel):
    _name = 'report.nieve_ict_reports.purchase_order_form_pdf'

    def get_header(self, data):
        result = self.env['purchase.order.line'].search([('order_id', '=', data.id)])
        return result

    def get_prod_detail(self, data):
        result = self.env['kk.order.line'].search([('po_line_id', '=', data.id)])
        return result

    @api.model
    def get_report_values(self, docids, data=None):
        get_data = self.env['purchase.order'].search([('id', '=', docids[0])])
        docargs = {
            'get_datax': get_data,
            'get_prod_detail':self.get_prod_detail,
            'get_header': self.get_header,
            'doc_ids': docids,
            'doc_model': 'purchase.order',
            'docs': self,
        }
        return docargs
