from odoo import models, api

from odoo import models, api
from odoo.addons.nieve_amount2text_idr import amount_to_text_id


class SalesInvoicePdf(models.AbstractModel):
    _name = 'report.nieve_ict_reports.sales_invoice_form_pdf'

    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    def get_default_requested_by(self):
        return self.env['res.users'].browse(self.env.uid)

    @api.model
    def get_report_values(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        get_data = self.env['account.invoice'].search([('date_invoice', '>=', docs.date_from), ('move_id', '!=', False),
                                                       ('date_invoice', '<=', docs.date_to),
                                                       ('type', '=', 'out_invoice')])
        # print('get_data---',get_data)
        docargs = {
            'get_datax': get_data,
            'date_from' : docs.date_from,
            'date_to' : docs.date_to,
            'convert': self.convert,
            'get_default_requested_by': self.get_default_requested_by,
            'doc_ids': docids,
            'doc_model': 'account.invoice',
            # 'docs': get_data,
        }
        return docargs
