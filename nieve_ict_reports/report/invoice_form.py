from odoo import models, api
from odoo.addons.nieve_amount2text_idr import amount_to_text_id
import datetime
from datetime import datetime, timedelta


class InvoiceForm(models.AbstractModel):
    _name = 'report.nieve_ict_reports.invoice_form_pdf'

    def get_header(self, data):
        result = self.env['account.invoice.line'].search([('invoice_id', '=', data.id)])
        return result

    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    @api.model
    def get_report_values(self, docids, data=None):
        get_data = self.env['account.invoice'].search([('id', '=', docids[0])])
        date_time=datetime.now()
        date_now1 =date_time.date()
        date_now = datetime.strptime(str(date_now1), '%Y-%m-%d').strftime("%d %B %Y")

        docargs = {
            'get_datax': get_data,
            'date_now': date_now,
            'get_header': self.get_header,
            'convert': self.convert,
            'doc_ids': docids,
            'doc_model': 'account.invoice',
            'docs': self,
        }
        return docargs
