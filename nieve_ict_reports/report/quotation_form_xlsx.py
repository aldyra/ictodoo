# from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsxAbstract
from odoo import models


class QuotationFormXls(models.AbstractModel):
    _name = 'report.nieve_ict_reports.quotation_form_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def get_data(self, id):
        res = self.env['sale.order.line'].search([('order_id','=',id)])
        return res

    def prod_detail(self, id):
        res = self.env['kk.order.line'].search([('so_line_id','=',id)])
        return res

    def generate_xlsx_report(self, workbook, data, partners):
        report_name = "Quotation Form"
        # One sheet by partner
        sheet = workbook.add_worksheet(report_name[:31])
        bold = workbook.add_format({'bold': True,'font_size': 14})
        bold_font = workbook.add_format({'bold': True,'font_size': 11})
        bold_font_u = workbook.add_format({'bold': True,'underline':True,'font_size': 11})
        bold_font_wborder = workbook.add_format({'bold': True,'font_size': 11,'border':True,
                                                 'align':'center','text_wrap':True,'valign':'vcenter','fg_color':'#228B22'})
        regular_font = workbook.add_format({'bold': False,'font_size': 11})
        regular_font2 = workbook.add_format({'bold': False,'font_size': 11,'text_wrap':True})
        regular_font_address = workbook.add_format({'bold': False,'font_size': 11,'valign':'top'})
        regular_font_wborder = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,
                                                     'align': 'left','fg_color': '#696969','font_color':'white'})
        normal_font_wborder = workbook.add_format({'bold': False, 'font_size': 11, 'border': True,})
        normal_font_wborder2 = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,'fg_color':'#1E90FF'})
        right_font_wborder = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,'align':'right','num_format':'#,##0.00'})
        right_font_wborder2 = workbook.add_format({'bold': True,'fg_color':'#1E90FF','font_size': 11, 'border': True,'align':'right','num_format':'#,##0.00'})

        sheet.set_column('A:A', 20)
        sheet.set_column('B:B', 30)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_row(21, 30)
        sheet.set_row(11, 60)

        sheet.write('B7','Quotation',bold)
        sheet.write('A9','To:',bold_font)
        sheet.write('A10','Customer:',bold_font)
        sheet.write('A11','Contact Person:',regular_font)
        sheet.write('A12','Address:',regular_font_address)
        sheet.write('A13','Phone Number',regular_font)
        sheet.write('A14','Fax Number',regular_font)
        sheet.write('A15','Date',regular_font)
        sheet.write('A16','Quotation Number',regular_font)
        sheet.write('A17','Subject',regular_font)
        sheet.write('A22','Item',bold_font_wborder)
        sheet.write('B22','Description',bold_font_wborder)
        sheet.write('C22','Qty',bold_font_wborder)
        sheet.write('D22','List Price',bold_font_wborder)
        sheet.write('E22','Total Pricelist',bold_font_wborder)
        sheet.write('F22','Price After Discount',bold_font_wborder)
        sheet.write('G22','Total Price After Discount',bold_font_wborder)
        sheet.write('H22','% Discount',bold_font_wborder)

        address = ''
        row = 23
        for obj in partners:
            address = str(obj.partner_id.street) or ' '+'\n'+ str(obj.partner_id.street2) or ' '+'\n'\
                      +str(obj.partner_id.city) or ' '+', '+str(obj.partner_id.state_id.name) or ' '+' '+str(obj.partner_id.zip) or ' '+'\n'+\
                      str(obj.partner_id.country_id.name) or ' '
            sheet.write('B9',obj.partner_id.name or '',bold_font)
            sheet.write('B10',obj.partner_id.name or '',bold_font)
            sheet.write('B11',obj.partner_id.phone or '',regular_font)
            sheet.write('B12',address or '',regular_font_address)
            sheet.write('B13',obj.partner_id.phone or '',regular_font)
            sheet.write('B14',obj.partner_id.fax or '',regular_font)
            sheet.write('B15',obj.confirmation_date or '',regular_font)
            sheet.write('B16',obj.name or '',regular_font)

            for item in self.get_data(obj.id):
                sheet.merge_range('A'+str(row)+':'+'H'+str(row), item.product_id.name,regular_font_wborder)
                row += 1
                for detail in self.prod_detail(item.id):
                    sheet.write('A'+str(row),detail.product_code or '',normal_font_wborder)
                    sheet.write('B'+str(row),detail.name or '',normal_font_wborder)
                    sheet.write('C'+str(row),detail.product_uom_qty or '',normal_font_wborder)
                    sheet.write('D'+str(row),'',normal_font_wborder)
                    sheet.write('E'+str(row),'',normal_font_wborder)
                    sheet.write('F'+str(row),'',normal_font_wborder)
                    sheet.write('G'+str(row),'',normal_font_wborder)
                    sheet.write('H'+str(row),'',normal_font_wborder)
                    row+=1
                sheet.write('A'+str(row),'',normal_font_wborder)
                sheet.write('B'+str(row),'Subtotal',right_font_wborder)
                sheet.write('C'+str(row),'',normal_font_wborder)
                sheet.write('D'+str(row),'',normal_font_wborder)
                sheet.write('E'+str(row),'',normal_font_wborder)
                sheet.write('F'+str(row),'',normal_font_wborder)
                sheet.write('G'+str(row),item.price_total or '0',right_font_wborder)
                sheet.write('H'+str(row),'',normal_font_wborder)
                row+=1

            sheet.write('A' + str(row + 1),'',normal_font_wborder2)
            sheet.write('B' + str(row + 1),'TOTAL',normal_font_wborder2)
            sheet.write('C' + str(row + 1),'',normal_font_wborder2)
            sheet.write('D' + str(row + 1),'',normal_font_wborder2)
            sheet.write('E' + str(row + 1),'',normal_font_wborder2)
            sheet.write('F' + str(row + 1),'',normal_font_wborder2)
            sheet.write('G' + str(row + 1),obj.amount_total or 0,right_font_wborder2)
            sheet.write('H' + str(row + 1),'',normal_font_wborder2)

            sheet.write('A' + str(row + 3), 'Terms : ',bold_font)
            sheet.merge_range('A' + str(row + 4) + ':D'+ str(row + 4), obj.note or '',regular_font2)
            sheet.write('A' + str(row + 8), 'Best Regards,',bold_font)
            sheet.write('A' + str(row + 13), 'Andy Suwarna',bold_font_u)
            sheet.write('A' + str(row + 14), 'Account Manager',bold_font)
        workbook.close()
