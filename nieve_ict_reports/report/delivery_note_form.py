from odoo import models, api


class DeliveryNoteForm(models.AbstractModel):
    _name = 'report.nieve_ict_reports.delivery_note_form_pdf'

    def get_header(self, data):
        result = self.env['stock.move'].search([('picking_id', '=', data.id)])
        return result

    def get_prod_detail(self, data):
        result = self.env['kk.order.line'].search([('move_line_id', '=', data.id)])
        return result

    @api.model
    def get_report_values(self, docids, data=None):
        get_data = self.env['stock.picking'].search([('id', '=', docids[0])])
        docargs = {
            'get_datax': get_data,
            'get_prod_detail': self.get_prod_detail,
            'get_header': self.get_header,
            'doc_ids': docids,
            'doc_model': 'stock.picking',
            'docs': get_data,
        }
        return docargs
