from odoo import models, api

from odoo import models, api
from odoo.addons.nieve_amount2text_idr import amount_to_text_id


class PurchaseCustomerPdf(models.AbstractModel):
    _name = 'report.nieve_ict_reports.purchase_customer_form'

    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    def get_customer_data(self,date_from,date_to):
        result = []
        self.env.cr.execute("""
                                SELECT partner_id,rp.name from account_invoice ai join res_partner rp on ai.partner_id = rp.id 
                                where date_invoice >= %s AND date_invoice <= %s AND
                                ai.type = 'in_invoice' AND move_id is not null group by partner_id,rp.name
        """,(date_from,date_to))
        result = self.env.cr.dictfetchall()
        print ('result-----',result)
        return result

    def get_invoices(self,partner_id):
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        result = self.env['account.invoice'].search([('date_invoice', '>=', docs.date_from), ('move_id', '!=', False),
                                            ('date_invoice', '<=', docs.date_to),('partner_id','=',partner_id),
                                            ('type', '=', 'in_invoice')])
        return result

    @api.model
    def get_report_values(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        list_data = self.get_customer_data(docs.date_from,docs.date_to)
        docargs = {
            'get_customer': list_data,
            'get_invoices': self.get_invoices,
            'date_from': docs.date_from,
            'date_to': docs.date_to,
            'convert': self.convert,
            'doc_ids': docids,
            'doc_model': 'account.invoice',
            # 'docs': get_data,
        }
        return docargs
