
import datetime
from datetime import datetime, timedelta
from openerp import api, fields, models, _
from odoo import models
import xlsxwriter
import os
from odoo.addons.nieve_amount2text_idr import amount_to_text_id

class InvoiceFormXlsx(models.AbstractModel):
    _name = 'report.nieve_ict_reports.invoice.form.xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def convert(self, amount, cur):
        amt_id = amount_to_text_id.amount_to_text(amount, 'id', cur)
        return amt_id

    def generate_xlsx_report(self, workbook, data, partners):
        for obj in partners:
            report_name = obj.name

            sheet = workbook.add_worksheet(report_name[:31])
            bold1 = workbook.add_format({'bold': True,'font_size': 14,'align':'left'})
            bold2 = workbook.add_format({'font_size': 12})
            boldcenter = workbook.add_format({'font_size': 12,'align':'center'})
            boldcenteritalic = workbook.add_format({'font_size': 12,'align':'center','italic':True})
            boldcenterunderline = workbook.add_format({'font_size': 12,'align':'center','bold': True,'underline':True})
            bold2line = workbook.add_format({'font_size': 12,'bottom':2,'top':2,})
            bold2line_bottom = workbook.add_format({'font_size': 12,'bottom':2,})
            string = workbook.add_format({'font_size': 20,'align':'center','bottom':2,'top':2,'bold': True,})
            string2 = workbook.add_format({'font_size': 18,'align':'center'})
            bold3 = workbook.add_format({'font_size': 12,'bold': True,'bottom':2,'top':2,'left':2,'right':2,'align':'left'})
            header = workbook.add_format({'font_size': 12,'bold': True,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
            bold9 = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
            bold9left = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left'})
            bold9leftbold = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'left','bold': True,})
            bold9right = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'right'})
            bold9rightbold = workbook.add_format({'font_size': 12,'bottom':2,'top':2,'left':2,'right':2,'align':'right','bold': True})
            bold5 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'center'})
            bold6 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'right'})
            bold7 = workbook.add_format({'font_size': 12,'bottom':6,'top':6,'left':6,'right':6,'align':'left'})
            bold4 = workbook.add_format({'font_size': 12,'align':'left'})
            bold = workbook.add_format({'bold': True,'font_size': 14})
            bold_font = workbook.add_format({'bold': True,'valign': 'center','font_size': 11,'top':6})
            ship = workbook.add_format({'bold': True,'font_size': 12,'align':'left'})
            script_dir = os.path.dirname(__file__)
            logo_file_path = os.path.join(script_dir, '../static/src/description/ict.png')
            sheet.insert_image('A2',logo_file_path,{'x_scale': 0.6, 'y_scale': 0.6})
            sheet.hide_gridlines(2)

            sheet.set_column('A:A', 16)
            sheet.set_column('B:B', 40)
            sheet.set_column('C:C', 30)
            sheet.set_column('D:D', 10)
            sheet.set_column('E:E', 30)
            sheet.set_column('F:F', 30)

            sheet.write('B2','PT. INFRACOM TECHNOLOGY',bold1)
            sheet.write('B3','Gedung MD Place Tower I, 1st Floor',bold2)
            sheet.write('B4','Jl. Setiabudi Selatan No. 7',bold2)
            sheet.write('B5','Jakarta Selatan 12910',bold2)
            sheet.write('B6','Tel: +62-21 29669271',bold2)
            sheet.write('B7','Fax: +62-21 29669273',bold2)

            date_invoice = datetime.strptime(obj.date_invoice, '%Y-%m-%d').strftime("%d-%m-%Y") if obj.date_invoice else ''
            sheet.merge_range('A9:F10','INVOICE',string)
            sheet.write('A11',str('Invoice No : ')+str(obj.number or ''),bold2line)
            sheet.write('B11','',bold2line)
            sheet.write('C11',str('Ref.	No : ')+str(obj.so_id.name or ''),bold2line)
            sheet.write('D11','',bold2line)
            sheet.write('E11',str('Date : ')+str(date_invoice or ''),bold2line)
            sheet.write('F11','',bold2line)
            sheet.write('A13',str('To'),bold2)
            sheet.write('B13',str(': ')+str(obj.partner_id.name or ''),bold2)
            sheet.write('B14',str('  ')+str(obj.partner_id.street or ''),bold2)
            sheet.write('B15',str('  ')+str(obj.partner_id.street2 or ''),bold2)
            sheet.write('B16',str('  ')+str(obj.partner_id.city or ''),bold2)
            sheet.write('B17',str('  ')+str(obj.partner_id.state_id.name or ''),bold2)
            sheet.write('B18',str('  ')+str(obj.partner_id.zip or ''),bold2)
            sheet.write('A19',str('Ph'),bold2)
            sheet.write('B19',str(': ')+str(obj.partner_id.phone or ''),bold2)
            sheet.write('A20',str('Attn'),bold2)
            sheet.write('B20',str(': ')+str(obj.customer_id.name or ''),bold2)
            sheet.write('A21','',bold2line_bottom)
            sheet.write('B21','',bold2line_bottom)
            sheet.write('C21','',bold2line_bottom)
            sheet.write('D21','',bold2line_bottom)
            sheet.write('E21','',bold2line_bottom)
            sheet.write('F21','',bold2line_bottom)

            sheet.write('A23','No',header)
            sheet.merge_range('B23:C23','Description',header)
            sheet.write('D23','Qty',header)
            sheet.write('E23','Unit Price',header)
            sheet.write('F23','Total Price',header)


            row =24
            no = 1
            for i in obj.invoice_line_ids:
                sheet.write('A'+str(row),no,bold9)
                sheet.merge_range('B'+str(row)+str(':')+'C'+str(row),str(i.product_id.name or ''),bold9left)
                sheet.write('D'+str(row),i.quantity or '',bold9)
                sheet.write('E'+str(row),str(i. currency_id.symbol)+' '+str('{:,.2f}'.format(i.price_unit or '')),bold9right)
                sheet.write('F'+str(row),str(i. currency_id.symbol)+' '+str('{:,.2f}'.format(i.price_subtotal or '')),bold9right)
                no = no + 1
                row += 1

                sheet.write('E'+str(row),'Total',bold9left)
                sheet.write('E'+str(row+1),'90% First Payment',bold9left)
                sheet.write('E'+str(row+2),'VAT 10%',bold9left)
                sheet.write('E'+str(row+3),'Grand Total',bold9leftbold)
                sheet.write('F'+str(row),str(i. currency_id.symbol)+' '+str('{:,.2f}'.format(obj.amount_untaxed)),bold9rightbold)
                sheet.write('F'+str(row+1),str('-'),bold9right)
                sheet.write('F'+str(row+2),str(i. currency_id.symbol)+' '+str('{:,.2f}'.format(obj.amount_tax or '')),bold9right)
                sheet.write('F'+str(row+3),str(i. currency_id.symbol)+' '+str('{:,.2f}'.format(obj.amount_total or '')),bold9rightbold)
            sheet.merge_range('B'+str(row)+str(':')+'D'+str(row),'Term of Payment : 14 Days after invoice received',bold2)
            sheet.merge_range('A'+str(row+3)+str(':')+'D'+str(row+3),str('Terbilang : ')+str(self.convert(obj.amount_total,'IDR')) or '',bold9left)

            sheet.merge_range('A'+str(row+6)+str(':')+'C'+str(row+6+6),obj.comment or '',bold2)
            date_time=datetime.now()
            date_now1 =date_time.date()
            date_now = datetime.strptime(str(date_now1), '%Y-%m-%d').strftime("%d %B %Y")
            sheet.merge_range('E'+str(row+6)+str(':')+'F'+str(row+6),str('Jakarta, ')+str(date_now) or '',boldcenter)
            sheet.merge_range('E'+str(row+12)+str(':')+'F'+str(row+12),str('Stefanus Sudyatmiko') or '',boldcenterunderline)
            sheet.merge_range('E'+str(row+13)+str(':')+'F'+str(row+13),str('Director') or '',boldcenteritalic)
