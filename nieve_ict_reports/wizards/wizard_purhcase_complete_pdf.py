from odoo import api, fields, models, _


class WizardPurchaseCompletePdf(models.TransientModel):
    _name='wizard.purchase.complete.pdf'

    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    report_type = fields.Selection([('all','All'),('customer','Customer')],string='Report Type',default='all')


    @api.cr_uid_ids_context
    def print(self, context=None):
        if context is None:
            context = {}
        print ("AAAAAA")
        datas = {'ids': self.env.context.get('active_ids')}
        datas['model'] = 'wizard.purchase.complete.pdf'
        datas['form'] = self.read(self.env.context)[0]
        # print "BBBB",datas

        if datas['form']['report_type'] == 'all':
            return self.env.ref('nieve_ict_reports.action_purchase_complete').report_action(self,data=datas)
        else:
            return self.env.ref('nieve_ict_reports.action_purchase_customer').report_action(self,data=datas)