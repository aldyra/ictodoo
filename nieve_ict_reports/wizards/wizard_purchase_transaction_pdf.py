from odoo import api, fields, models, _


class WizardPurchaseTransactionPdf(models.TransientModel):
    _name='wizard.purchase.transaction.pdf'

    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')


    @api.cr_uid_ids_context
    def print(self, context=None):
        if context is None:
            context = {}
        print ("AAAAAA")
        datas = {'ids': self.env.context.get('active_ids')}
        datas['model'] = 'wizard.purchase.transaction.pdf'
        datas['form'] = self.read(self.env.context)[0]

        return self.env.ref('nieve_ict_reports.action_purchase_transaction_pdf').report_action(self,data=datas)