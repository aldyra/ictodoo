import logging
import os
import csv
import tempfile
import base64
from odoo.exceptions import UserError, Warning
from odoo import api, fields, models, _

# ##XLSX
# from xlwt import easyxf
from xlrd import open_workbook
# from xlutils.copy import copy
# from difflib import get_close_matches
# from difflib import SequenceMatcher
# ##

import xlrd, mmap

_logger = logging.getLogger(__name__)


class ImportCostSheet(models.TransientModel):
    _name = "wizard.import.cost.sheet"

    file_data = fields.Binary('File', required=False,)
    file_name = fields.Char('File Name')
    partner_id = fields.Many2one(
        'res.partner', string='Partner', required=True)
    
    @api.multi
    def download_document(self):
        field_binary    = 'image_cs_template' 
        record_id       = self.env['res.company'].search([], limit=1).id
        filename        = self.env['res.company'].search([], limit=1).cs_template_name
         
        final_url = self.env['res.company'].get_cs_template_file(field_binary,record_id,filename)
        
        if not final_url:
            raise Warning(_('No File Attached.'))
        
        return {'type': 'ir.actions.act_url', 'url':final_url, 'target': 'self',}
    

    def XLSDictReader(self, f, sheet_index=0):
        book    = xlrd.open_workbook(file_contents=mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ))
        sheet   = book.sheet_by_index(sheet_index)
        headers = dict( (i, sheet.cell_value(0, i) ) for i in range(sheet.ncols) ) 
    
        return ( dict( (headers[j], sheet.cell_value(i, j)) for j in headers ) for i in range(1, sheet.nrows) )
    
    def import_button(self):
        cost_sheet_obj = self.env['kk.cost.sheet']
        product_obj = self.env['product.product']
        cost_sheet_line_obj = self.env['kk.cost.sheet.line']
        order_line_obj = self.env['kk.order.line']
        
        if not self.file_data:
            raise UserError(_("You must Upload Cost Sheet Template First"))
        
        file_extension = str(self.file_name).split(".")[-1]
        
        #if not self.csv_validator(self.file_name):
        #    raise UserError(_("The file must be of extension .csv"))
        
        
        if file_extension in ['xls','xlsx']:
            file_path = tempfile.gettempdir() + '/file.xlsx'
            data = self.file_data
            
            f = open(file_path, 'wb')
            f.write(base64.b64decode(data))
            f.close()
            archive = self.XLSDictReader(open(file_path))
            
        elif file_extension=='csv':
            file_path = tempfile.gettempdir() + '/file.csv'
            data = self.file_data
        
            f = open(file_path, 'wb')
            f.write(base64.b64decode(data))
            f.close()
            archive = csv.DictReader(open(file_path))
    
    
        archive_lines = []
        for line in archive:
            archive_lines.append(line)
        self.valid_columns_keys(archive_lines)
        self.valid_product_code(archive_lines, product_obj)
        self.valid_prices(archive_lines, file_extension)
        vals = {
            'partner_id': self.partner_id.id,
            'apply_odc': False,
        }
        cost_sheet_id = cost_sheet_obj.create(vals)
        cont = 0
        for line in archive_lines:
            cont += 1
            code = str(line.get('code', "")).strip()
            product_id = product_obj.search([('name', '=', code)])
            #product_id = product_obj.search([('default_code', '=', code)])
            
            description = line.get('description', "")
            quantity = line.get('quantity', 0)
            price_unit = self.get_valid_price(line.get('price', ''), cont, file_extension)
            disc_type = str(line.get('disc_type', "percentage")).strip()
            disc_amount = line.get('disc_amount', 0)

            cost_quantity = line.get('cost_quantity', 0)
            cost_price_unit = self.get_valid_price(line.get('cost_price', ''), cont, file_extension)
            cost_disc_type = str(line.get('cost_disc_type', "percentage")).strip()
            cost_disc_amount = line.get('cost_disc_amount', 0)
            if cost_sheet_id and product_id:
                uom = product_id.product_tmpl_id.uom_id.id
                vals = {
                    'kk_cost_sheet_id': cost_sheet_id.id,
                    'product_id': product_id.id,
                    'name': description,
                    'product_uom_qty': float(quantity),
                    'price_unit': price_unit,
                    'discount_type': disc_type,
                    'discount_amount': disc_amount,

                    'cost_product_uom_qty': float(cost_quantity),
                    'cost_price_unit': cost_price_unit,
                    'cost_discount_type': cost_disc_type,
                    'cost_discount_amount': cost_disc_amount,
                }
                cost_sheet_line = cost_sheet_line_obj.create(vals)
                cost_sheet_line.onchange_discount_amount()
                cost_sheet_line.onchange_cost_discount_amount()

            name_desc = line.get('name', "")
            product_code_desc = line.get('product_code', "")
            line_quantity = line.get('qty_detail', 0)
            vals = {
                'cs_line_id': cost_sheet_line.id,
                'name': name_desc,
                'product_code': product_code_desc,
                'product_uom': uom,
                'product_uom_qty': line_quantity,
            }
            order_line_obj.create(vals)
        cost_sheet_id._amount_all()

        return {
            'name': _('Cost Sheet'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'kk.cost.sheet',
            'type': 'ir.actions.act_window',
            'res_id': cost_sheet_id.id,
            'target': 'current',
        }
        # return {'type': 'ir.actions.act_window_close'}

    @api.model
    def valid_prices(self, archive_lines, file_extension):
        cont = 0
        for line in archive_lines:
            cont += 1
            price = line.get('price', '')
            if price != '':
                if price != "" and file_extension == 'csv':
                    price = price.replace("$", "").replace(",", ".")
                try:
                    price_float = float(price)
                except:
                    raise UserError('The price of the product of the line %s, It does not have an adequate format. Suitable formats, example: "$100,00"-"100,00"-"100"' % cont)
        return True

    @api.model
    def get_valid_price(self, price, cont, file_extension):
        if price != '':
            if price != "" and file_extension == 'csv':
                price = price.replace("$","").replace(",",".")
            try:
                price_float = float(price)
                return price_float
            except:
                raise UserError('The price of the product of get_valid_price the line %s, It does not have an adequate format. Suitable formats, example: "$100,00"-"100,00"-"100"'%cont)
            return False

    @api.model
    def valid_product_code(self, archive_lines, product_obj):
        cont = 0
        for line in archive_lines:
            cont += 1
            code = str(line.get('code', "")).strip()
            if code != '':
                product_id = product_obj.search([('name', '=', code)])
                #product_id = product_obj.search([('default_code', '=', code)])
                
                if len(product_id) > 1:
                    raise UserError("The product code of the line %s, is duplicated in the system." % cont)
                if not product_id:
                    raise UserError("The product code of the line %s, is duplicated in the system." % cont)

    @api.model
    def valid_columns_keys(self, archive_lines):
        columns = archive_lines[0].keys()
        text = "The csv file must contain the following columns: code, quantity and price. \n The following columns are not found in the Archive:"; text2 = text
        if not 'code' in columns:
            text +="\n[ code ]"
        if not 'quantity' in columns:
            text +="\n[ quantity ]"
        if not 'price' in columns:
            text +="\n[ price ]"
        if text !=text2:
            raise UserError(text)
        return True

    @api.model
    def csv_validator(self, xml_name):
        name, extension = os.path.splitext(xml_name)
        return True if extension == '.csv' else False
