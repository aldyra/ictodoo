# -*- coding: utf-8 -*-
{
    "name": "Advances and Settlements",
    "version": "1.0",
    'author': 'Databit Solusi Indonesia',
    "description": """
        This Module handle advance request and it's settlements
    """,
    "depends": [
        'account', 'hr', 'nieve_cost_sheet'],
    'data': [
        'security/access_security.xml',
        'security/ir.model.access.csv',
        'views/res_company_inherit.xml',
        'views/advance_sequence.xml',
        'views/expense_type_view.xml',
        'views/advance_view.xml',
        'views/settlement_view.xml',
        'views/mail_notif_template.xml',
    ],
    'css': [],
    'js': [
    ],
    'installable': True,
    'active': False,
    'application': False,
}