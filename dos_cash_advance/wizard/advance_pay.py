# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time

from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError


class AdvancePayment(models.TransientModel):
    _name = "advance.payment"
    _description = "Advance Payment"

    @api.model
    def _default_journal(self):
        if self._context.get('default_journal_id', False):
            return self.env['account.journal'].browse(
                self._context.get('default_journal_id')
            )
        company_id = self._context.get(
            'company_id', self.env.user.company_id.id)
        domain = [
            ('type', 'in', ('bank', 'cash')),
            ('company_id', '=', company_id),
        ]
        return self.env['account.journal'].search(domain, limit=1)

    journal_id = fields.Many2one(
        'account.journal', string='Bank/ Cash Payment',
        default=_default_journal, required=True)
    payment_date = fields.Date(
        string='Payment Date', readonly=True,
        help="Effective date for accounting entries",
        required=True)
