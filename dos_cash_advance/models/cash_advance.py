# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import api, fields, models, _
from odoo.tools import float_compare
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF

import odoo.addons.decimal_precision as dp


class ResCompany(models.Model):
    _inherit = 'res.company'

    advance_account_id = fields.Many2one(
        'account.account',
        string="Advance Account")
    advance_days_deadline = fields.Float(
        string="Settlement Days", default='7')


class CashAdvance(models.Model):
    _name = "cash.advance"
    _description = "Cash Advance"
    _inherit = ['mail.thread']
    _order = "date desc, id desc"

    @api.model
    def _default_journal(self):
        if self._context.get('default_journal_id', False):
            return self.env['account.journal'].browse(
                self._context.get('default_journal_id')
            )
        company_id = self._context.get(
            'company_id', self.env.user.company_id.id)
        domain = [
            ('type', 'in', ('bank', 'cash')),
            ('company_id', '=', company_id),
        ]
        return self.env['account.journal'].search(domain, limit=1)

    @api.one
    @api.depends('line_ids.amount', 'currency_id', 'company_id')
    def _compute_amount(self):
        self.amount_total = sum((line.amount) for line in self.line_ids)

    @api.model
    def _get_account_advance(self):
        account = self.env.user.company_id.advance_account_id.id
        return account

    description = fields.Char(string='Description', size=256, required=True)
    name = fields.Char(
        string='Name', readonly=True,
        states={'draft': [('readonly', False)]})
    user_id = fields.Many2one(
        'res.users', string="Responsible",
        readonly=True, default=lambda self: self.env.user)
    employee_id = fields.Many2one(
        'hr.employee', string="Employee",
        readonly=True, states={'draft': [('readonly', False)]})
    department_id = fields.Many2one(
        'hr.department', string="Departement",
        readonly=True, states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one(
        'res.partner', string="Partner", domain="[('is_employee','=',True)]",
        readonly=True, states={'draft': [('readonly', False)]})
    date = fields.Date(
        string='Date', readonly=True, states={'draft': [('readonly', False)]},
        help="Effective date for accounting entries",
        default=lambda self: fields.Date.from_string(
            fields.Date.context_today(self)))
    payment_date = fields.Date(
        string='Payment Date',
        help="Effective date for accounting entries")
    journal_id = fields.Many2one(
        'account.journal', string='Bank/ Cash Payment',
        default=_default_journal)
    account_id = fields.Many2one(
        'account.account', string='Account', required=False,
        readonly=True, states={'draft': [('readonly', False)]})
    line_ids = fields.One2many(
        'cash.advance.line', 'advance_id',
        string='Voucher Lines', readonly=True,
        states={'draft': [('readonly', False)]})
    narration = fields.Text(
        'Notes', readonly=True, states={'draft': [('readonly', False)]})
    currency_id = fields.Many2one(
        'res.currency', string='Currency',
        required=False, readonly=True,
        states={'draft': [('readonly', False)]},
        track_visibility='always')
    company_id = fields.Many2one(
        'res.company', 'Company', required=False, readonly=True,
        states={'draft': [('readonly', False)]},
        default=lambda self: self.env['res.company']._company_default_get('cash.advance'))
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('proforma', 'Waiting Approval'),
            ('approve', 'Waiting Payment'),
            ('posted', 'Waiting Settlement'),
            ('done', 'Done'),
            ('cancel', 'Cancelled')
        ], 'State', readonly=True, size=32, default='draft',
        help='')
    account_advance_id = fields.Many2one(
        'account.account',
        string='Advance Account',
        default=_get_account_advance)
    amount_total = fields.Monetary(
        string='Total',
        store=True, readonly=True,
        compute='_compute_amount',
        currency_field='currency_id')
    move_id = fields.Many2one('account.move', string="Journal Entries")
    permanent_advance = fields.Boolean('Permanent Advance')

    @api.onchange('journal_id')
    def _onchange_journal_id(self):
        if self.journal_id:
            self.currency_id = self.journal_id.currency_id.id or self.journal_id.company_id.currency_id.id

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if self.employee_id:
            self.department_id = self.employee_id.department_id
            self.partner_id = self.employee_id.address_home_id.id

    @api.multi
    def create_move(self, post_move=True):
        created_moves = self.env['account.move']
        for line in self:
            prec = self.env['decimal.precision'].precision_get('Account')
            name = self.name + ' - ' + self.description
            company_currency = line.company_id.currency_id
            current_currency = line.currency_id
            amount = current_currency.compute(
                line.amount_total, company_currency)
            sign = (line.journal_id.type == 'bank' or line.journal_id.type == 'cash' and 1) or -1
            move_line_1 = {
                'name': name,
                'account_id': line.account_advance_id.id,
                'credit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'debit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': line.journal_id.id,
                'partner_id': line.partner_id.id,
#                 'analytic_account_id': category_id.account_analytic_id.id if category_id.type == 'sale' else False,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and sign * line.amount or 0.0,
            }
            move_line_2 = {
                'name': name,
                'account_id': line.journal_id.default_credit_account_id.id,
                'debit': 0.0 if float_compare(amount, 0.0, precision_digits=prec) > 0 else -amount,
                'credit': amount if float_compare(amount, 0.0, precision_digits=prec) > 0 else 0.0,
                'journal_id': line.journal_id.id,
                'partner_id': line.partner_id.id,
                'currency_id': company_currency != current_currency and current_currency.id or False,
                'amount_currency': company_currency != current_currency and - sign * line.amount or 0.0,
            }
            move_vals = {
                'ref': line.name,
                'date': line.payment_date or False,
                'journal_id': line.journal_id.id,
                'line_ids': [(0, 0, move_line_1), (0, 0, move_line_2)],
            }
            move = self.env['account.move'].create(move_vals)
            self.write({'move_id': move.id})

        return [x.id for x in created_moves]

    @api.multi
    def create_settlement(self):
        settle_obj = self.env['cash.settlement']
        settle_line_obj = self.env['settlement.lines']
        n_days = self.env.user.company_id.advance_days_deadline
        for o in self:
            setllmemtn = self.env['ir.sequence'].next_by_code('settlement.code')
            advance = o.name
            name = setllmemtn + ' - ' + advance

            end_date = datetime.strptime(o.date, DF) + timedelta(days=n_days)
            val = {
                'advance_id': o.id,
                'advance_date': o.date,
                'advance_amount': o.amount_total,
                'partner_id': o.partner_id.id,
                'currency_id': o.currency_id.id,
                'name': name,
                'department_id': o.department_id.id,
                'responsible_id': o.employee_id.id,
                'deadline': end_date,
                'advance_description': o.description,
                'permanent_advance': o.permanent_advance
            }
            settle = settle_obj.create(val)
            for x in o.line_ids:
                val_line = {
                    'name': x.name,
                    'product_id': x.product_id.id,
                    'cost_sheet_id': x.cost_sheet_id.id,
                    'cost_expense_line_id': x.cost_expense_line_id.id,
                    'adv_line_amount': x.amount,
                    'account_id': x.product_id.product_tmpl_id._get_product_accounts()['expense'].id,
                    'price_unit': x.unit_amount,
                    'settlement_id': settle.id,
                }
                settle_line_obj.create(val_line)
        return

    @api.multi
    def action_request_send(self):
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference(
                'dos_cash_advance', 'email_template_edi_advance')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'cash.advance',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def propose(self, vals):
        for line in self.line_ids:
            if line.cost_sheet_id and not line.cost_expense_line_id:
                raise UserError(_('Please choose Cost Expense if you pick Cost Sheet!'))
        if not vals.get('name', False):
            vals['name'] = self.env['ir.sequence'].next_by_code('advance.code')
        vals['state'] = 'proforma'
        return self.write(vals)

    @api.multi
    def approve(self, vals):
        vals['state'] = 'approve'
        return self.write(vals)

    @api.multi
    def cancel(self, vals):
        vals['state'] = 'cancel'
        return self.write(vals)

    @api.multi
    def validate(self):
        # lots of duplicate calls to action_invoice_open,
        # so we remove those already open
        advance_obj = self.filtered(lambda inv: inv.state == 'approve')
        advance_obj.create_move()
        advance_obj.create_settlement()
        self.write({'state': 'posted'})
        return True


class CashAdvanceLines(models.Model):
    _name = "cash.advance.line"
    _description = "Cash Advance Lines"
    _order = 'move_line_id'

    @api.depends('quantity', 'unit_amount', 'currency_id')
    def _compute_amount(self):
        for expense in self:
            expense.amount = expense.unit_amount * expense.quantity

    advance_id = fields.Many2one(
        'cash.advance', string='Advance', required=1, ondelete='cascade')
    name = fields.Char(
        string='Description', size=256)
    partner_id = fields.Many2one(
        'res.partner',
        related='advance_id.partner_id', store=True, string='Partner')
    move_line_id = fields.Many2one(
        'account.move.line', string='Journal Item')

    cost_sheet_id = fields.Many2one(
        'kk.cost.sheet', string='Cost Sheet')
    cost_expense_line_id = fields.Many2one(
        'kk.cost.expense.line', string='Cost Expense')

    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('proforma', 'Waiting Approval'),
            ('approve', 'Waiting Payment'),
            ('posted', 'Paid'),
            ('refused', 'Cancelled')
        ], 'State', readonly=True, related='advance_id.state',
        store=True, default='draft')
    product_id = fields.Many2one(
        'product.product', string='Product', readonly=True,
        states={'draft': [('readonly', False)]}, required=True)
    product_uom_id = fields.Many2one(
        'product.uom', string='Unit of Measure', required=True,
        readonly=True,
        states={'draft': [('readonly', False)], 'refused': [('readonly', False)]},
        default=lambda self: self.env['product.uom'].search([], limit=1, order='id'))
    unit_amount = fields.Float(
        string='Unit Price', readonly=True, required=True,
        states={'draft': [('readonly', False)], 'refused': [('readonly', False)]},
        digits=dp.get_precision('Product Price')
    )
    quantity = fields.Float(
        required=True, readonly=True,
        states={'draft': [('readonly', False)], 'refused': [('readonly', False)]},
        digits=dp.get_precision('Product Unit of Measure'),
        default=1)
    amount = fields.Monetary(
        string='Amount', compute='_compute_amount',
        digits=dp.get_precision('Account'))
    company_id = fields.Many2one(
        'res.company', string='Company', readonly=True,
        states={'draft': [('readonly', False)], 'refused': [('readonly', False)]})
    currency_id = fields.Many2one(
        'res.currency', string='Currency', readonly=True,
        states={'draft': [('readonly', False)], 'refused': [('readonly', False)]},
        default=lambda self: self.env.user.company_id.currency_id)

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            self.unit_amount = self.product_id.price_compute('standard_price')[self.product_id.id]
            self.product_uom_id = self.product_id.uom_id
