# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp


class CashSettlement(models.Model):
    _name = "cash.settlement"
    _inherit = ['mail.thread']
    _description = "Advance Settlements"

    @api.depends('line_ids.price_total')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        advance = 0.0
        for order in self:
            untaxed_amount = tax = 0.0
            for line in order.line_ids:
                untaxed_amount += line.price_subtotal
                tax += line.price_tax
            if order.advance_id:
                advance = order.advance_amount
            order.update({
                'untaxed_amount': order.company_id.currency_id.round(untaxed_amount),
                'taxed_amount': order.company_id.currency_id.round(tax),
                'total_amount': untaxed_amount + tax,
                'diff_amount': untaxed_amount + tax - advance
            })

    @api.model
    def _default_journal(self):
        if self._context.get('default_journal_id', False):
            return self.env['account.journal'].browse(self._context.get('default_journal_id'))
        company_id = self._context.get('company_id', self.env.user.company_id.id)
        domain = [
            ('type', '=', 'purchase'),
            ('company_id', '=', company_id),
        ]
        return self.env['account.journal'].search(domain, limit=1)

    name = fields.Char(string='Name')
    partner_id = fields.Many2one('res.partner', string="Partner", domain="[('is_employee','=',True)]")
    responsible_id = fields.Many2one('hr.employee', string="Employee")
    department_id = fields.Many2one('hr.department', string="Department")
    advance_description = fields.Char(string="Advance Description")
    date_submitted = fields.Date(string="Date Submit")
# #     journal_id = fields.Many2one('account.journal', string='Journal', required=False, readonly=True, states={'draft': [('readonly', False)]},default=_default_journal,
#         domain="[('type', '=', ('purchase')")
    currency_id = fields.Many2one('res.currency', compute='_get_journal_currency',
        string='Currency', readonly=True, required=True, default=lambda self: self._get_currency())
    advance_currency = fields.Many2one('res.currency',string="Advance Currency")
    company_id = fields.Many2one('res.company', 'Company', required=False, 
                                 readonly=True,  default=lambda self: self.env['res.company']._company_default_get('cash.settlement'))
    advance_id = fields.Many2one('cash.advance',string='Advance No')
    advance_date = fields.Date(string="Advance Date")
    advance_currency = fields.Many2one('res.currency',string="Currency")
    advance_amount = fields.Float(string="Advance Amount",)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirm','Propose'),
            ('waiting', 'Waiting Audit'),
            ('approve', 'Waiting Payment'),
            ('done', 'Settled'),
            ], 'State', readonly=True, size=32, default='draft',
            help='')
    line_ids = fields.One2many('settlement.lines','settlement_id', string="Lines")
    untaxed_amount = fields.Monetary(string='Untaxed', store=True, readonly=True, track_visibility='always', compute='_amount_all')
    taxed_amount = fields.Monetary(string='Tax', store=True, readonly=True, track_visibility='always', compute='_amount_all')
    total_amount = fields.Monetary(string='Total', store=True, readonly=True, track_visibility='always', compute='_amount_all')
    diff_amount = fields.Monetary(string="Dif Amount",store=True, readonly=True, track_visibility='always', compute='_amount_all')
    journal_bank_id = fields.Many2one('account.journal', string='Bank/ Cash Payment', required=False, readonly=False,
        domain="[('type', 'in', ('bank','cash'))]")
    journal_id = fields.Many2one('account.journal', string='Expense Journal', required=False, readonly=False, 
        domain="[('type', '=', 'purchase')]", default=_default_journal)
    move_id = fields.Many2one('account.move', 'Journal Entry', copy=False)
    voucher_type = fields.Selection([
        ('sale', 'Sale'),
        ('purchase', 'Purchase')
        ], string='Type', oldname="type", default='purchase')
    tax_correction = fields.Monetary(readonly=True,
        help='In case we have a rounding problem in the tax, use this field to correct it')
    payment_date = fields.Date('Date Payment')
    deadline = fields.Date(string="Due Date")
    permanent_advance = fields.Boolean('Permanent Advance')

    @api.onchange('company_id')
    def _onchange_company_id(self):
        if self.company_id:
            self.currency_id = self.company_id.currency_id.id

    @api.model
    def _get_currency(self):
        journal = self.env['account.journal'].browse(self._context.get('journal_id', False))
        if journal.currency_id:
            return journal.currency_id.id
        return self.env.user.company_id.currency_id.id

    @api.one
    @api.depends('journal_id', 'company_id')
    def _get_journal_currency(self):
        self.currency_id = self.journal_id.currency_id.id or self.company_id.currency_id.id
    
    @api.multi
    def first_move_line_get(self, move_id, company_currency, current_currency):
        debit = credit = 0.0
        if self.voucher_type == 'purchase':
            credit = self._convert_amount(self.advance_amount)
        elif self.voucher_type == 'sale':
            debit = self._convert_amount(self.advance_amount)
        if debit < 0.0: debit = 0.0
        if credit < 0.0: credit = 0.0
        sign = debit - credit < 0 and -1 or 1
        #set the first line of the voucher
        move_line = {
                'name': self.name or '/',
                'debit': debit,
                'credit': credit,
                'account_id': self.advance_id.account_advance_id.id,
                'move_id': move_id,
                'journal_id': self.journal_id.id,
                'partner_id': self.partner_id.id,
                'currency_id': company_currency != current_currency and current_currency or False,
                'amount_currency': (sign * abs(self.total_amount)  # amount < 0 for refunds
                    if company_currency != current_currency else 0.0),
                'date': self.date_submitted,
#                 'date_maturity': self.date_due
            }
        return move_line

    @api.multi
    def account_move_get(self):
#         if self.name:
#             name = self.name
        if self.journal_id.sequence_id:
            if not self.journal_id.sequence_id.active:
                raise UserError(_('Please activate the sequence of selected journal !'))
            name = self.journal_id.sequence_id.with_context(ir_sequence_date=self.date_submitted).next_by_id()
        else:
            raise UserError(_('Please define a sequence on the journal.'))

        move = {
            'name': name,
            'journal_id': self.journal_id.id,
            #'narration': self.narration,
            'date': self.date_submitted,
            'ref': name,
        }
        return move

    @api.multi
    def _convert_amount(self, amount):
        '''
        This function convert the amount given in company currency. It takes either the rate in the voucher (if the
        payment_rate_currency_id is relevant) either the rate encoded in the system.
        :param amount: float. The amount to convert
        :param voucher: id of the voucher on which we want the conversion
        :param context: to context to use for the conversion. It may contain the key 'date' set to the voucher date
            field in order to select the good rate to use.
        :return: the amount in the currency of the voucher's company
        :rtype: float
        '''
        for voucher in self:
            return voucher.currency_id.compute(amount, voucher.company_id.currency_id)

    @api.multi
    def voucher_move_line_create(self, line_total, move_id, company_currency, current_currency):
        '''
        Create one account move line, on the given account move, per voucher line where amount is not 0.0.
        It returns Tuple with tot_line what is total of difference between debit and credit and
        a list of lists with ids to be reconciled with this format (total_deb_cred,list_of_lists).

        :param voucher_id: Voucher id what we are working with
        :param line_total: Amount of the first line, which correspond to the amount we should totally split among all voucher lines.
        :param move_id: Account move wher those lines will be joined.
        :param company_currency: id of currency of the company to which the voucher belong
        :param current_currency: id of currency of the voucher
        :return: Tuple build as (remaining amount not allocated on voucher lines, list of account_move_line created in this method)
        :rtype: tuple(float, list of int)
        '''
        for line in self.line_ids:
            #create one move line per voucher line where amount is not 0.0
            if not line.price_subtotal:
                continue
            # convert the amount set on the voucher line into the currency of the voucher's company
            # this calls res_curreny.compute() with the right context,
            # so that it will take either the rate on the voucher if it is relevant or will use the default behaviour
            amount = self._convert_amount(line.price_unit*line.quantity)
            move_line = {
                'journal_id': self.journal_id.id,
                'name': line.name or '/',
                'account_id': line.account_id.id,
                'move_id': move_id,
                'partner_id': self.partner_id.id,
                'kk_cost_sheet_id': line.cost_sheet_id.id,
                'kk_cost_expense_id': line.cost_expense_line_id.id,
                'quantity': 1,
                'credit': abs(amount) if self.voucher_type == 'sale' else 0.0,
                'debit': abs(amount) if self.voucher_type == 'purchase' else 0.0,
                'date': self.date_submitted,
                'tax_ids': [(4,t.id) for t in line.settlement_line_tax_ids],
                'amount_currency': line.price_subtotal if current_currency != company_currency else 0.0,
                 'currency_id': company_currency != current_currency and current_currency or False,
            }
            self.env['account.move.line'].with_context(apply_taxes=True).create(move_line)
            self.write({'move_id': move_id})
        return line_total

    @api.multi
    def action_move_line_create(self):
        '''
        Confirm the vouchers given in ids and create the journal entries for each of them
        '''
        for voucher in self:
            if voucher.permanent_advance:
                voucher._action_advance_create()
            local_context = dict(
                self._context, force_company=voucher.journal_id.company_id.id)
#             if voucher.move_id:
#                 continue
            company_currency = voucher.journal_id.company_id.currency_id.id
            current_currency = voucher.currency_id.id or company_currency
            # we select the context to use accordingly if it's a multicurrency case or not
            # But for the operations made by _convert_amount, we always need to give the date in the context
            ctx = local_context.copy()
            ctx['date'] = voucher.date_submitted
            ctx['check_move_validity'] = False
            # Create the account move record.
            move = self.env['account.move'].create(voucher.account_move_get())
            # Get the name of the account_move just created
            # Create the first line of the voucher
            move_line = self.env['account.move.line'].with_context(ctx).create(voucher.first_move_line_get(move.id, company_currency, current_currency))
            line_total = move_line.debit - move_line.credit
            if voucher.voucher_type == 'sale':
                line_total = line_total - voucher._convert_amount(voucher.tax_amount)
            elif voucher.voucher_type == 'purchase':
                line_total = line_total + voucher._convert_amount(voucher.taxed_amount)
            # Create one move line per voucher line where amount is not 0.0
            line_total = voucher.with_context(ctx).voucher_move_line_create(line_total, move.id, company_currency, current_currency)

            if voucher.diff_amount != 0.0 :
                debit=credit=0.0
                account=False
                move_lines = self.env['account.move.line'].search([('move_id', '=', move.id)])
                diff = voucher.diff_amount
                if diff < 0 :
                    name = 'Penerimaan Pengembalian Advance'
                    debit = diff
                    account = voucher.journal_bank_id.default_debit_account_id.id
                elif diff > 0 :
                    name = 'Pembayaran Kekurangan Advance'
                    credit = diff
                    account = voucher.journal_bank_id.default_credit_account_id.id
                vals  = {
                        'name': name,
                        'journal_id': voucher.journal_bank_id.id,
                        'account_id': account,
                        'partner_id': voucher.partner_id.id,
        #                 'analytic_account_id': line.account_analytic_id and line.account_analytic_id.id or False,
                        'quantity': 1,
                        'credit': abs(credit),
                        'debit': abs(debit),
                        'date': voucher.payment_date,
                        'move_id':move.id
                    }
                self.env['account.move.line'].create(vals)
#             Add tax correction to move line if any tax correction specified
            if voucher.tax_correction != 0.0:
                tax_move_line = self.env['account.move.line'].search([('move_id', '=', move.id), ('tax_line_id', '!=', False)], limit=1)
                if len(tax_move_line):
                    tax_move_line.write({'debit': tax_move_line.debit + voucher.tax_correction if tax_move_line.debit > 0 else 0,
                        'credit': tax_move_line.credit + voucher.tax_correction if tax_move_line.credit > 0 else 0})

            # We post the voucher.
            voucher.write({
                'move_id': move.id,
                 'state': 'done',
            })
            move.post()
            #change status of Advance
            if voucher.advance_id :
                voucher.advance_id.write({'state': 'done'})
        return True

    @api.multi
    def cancel(self, vals):
        vals['state'] = 'draft'
        return self.write(vals)

    @api.multi
    def propose(self, vals):
        for line in self.line_ids:
            if line.cost_sheet_id and not line.cost_expense_line_id:
                raise UserError(_('Please choose Cost Expense if you pick Cost Sheet!'))
        vals['state'] = 'confirm'
        return self.write(vals)

    @api.multi
    def approve(self, vals):
        vals['state'] = 'waiting'
        return self.write(vals)

    @api.multi
    def validate(self, vals):
        vals['state'] = 'approve'
        return self.write(vals)

    @api.multi
    def _prepare_advance(self, group_id=False):
        self.ensure_one()
        seq_obj = self.env['ir.sequence']
        nama = seq_obj.next_by_code('advance.code') or 'New'
        vals = {
            'name': nama,
            'employee_id': self.responsible_id.id,
            'department_id': self.department_id.id,
            'description': "Permanent Advance from " + self.name,
            'currency_id': self.currency_id.id,
            'permanent_advance': self.permanent_advance,
        }
        return vals

    @api.multi
    def _action_advance_create(self):
        """
        Create Advance based on the rest of settlement.
        """
        for settle in self:
            vals = settle._prepare_advance(group_id=settle.id)
            advance = settle.env["cash.advance"].create(vals)
            unit_amount = abs(self.advance_amount)
            for line in settle.line_ids:
                vals = line._prepare_advance_line(
                    group_id=line.id, amount=unit_amount)
                vals.update(
                    {'advance_id': advance.id}
                )
                self.env["cash.advance.line"].create(vals)

    @api.multi
    def create_advance(self, vals):
        for advance in self:
            advance._action_advance_create()

    @api.multi
    def set_done(self, vals):
        vals['state'] = 'draft'
        return self.write(vals)


class SettlementLines(models.Model):
    _name = "settlement.lines"
    _description = "Settlement Lines"
    _inherit = ['mail.thread']

    @api.depends('quantity', 'price_unit', 'settlement_line_tax_ids')
    def _compute_price(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line.price_unit
            taxes = line.settlement_line_tax_ids.compute_all(
                price, line.settlement_id.currency_id,
                line.quantity, product=None,
                partner=line.settlement_id.partner_id
            )
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    def _get_default_uom_id(self):
        return self.env["product.uom"].search([], limit=1, order='id').id

    name = fields.Text(string='Description', required=True)
    date = fields.Date(string="Date")
    uom_id = fields.Many2one(
        'product.uom',
        string='Unit of Measure',
        default=_get_default_uom_id,
        ondelete='set null', index=True,
        oldname='uos_id'
    )
    settlement_id = fields.Many2one('cash.settlement',string="Settlement")
    product_id = fields.Many2one('product.product', string='Product',
        ondelete='restrict', index=True, domain=[('can_be_expensed', '=', True)])
    account_id = fields.Many2one('account.account', string='Account',
        required=True, domain=[('deprecated', '=', False)],
        help="The income or expense account related to the selected product.")

    cost_sheet_id = fields.Many2one(
        'kk.cost.sheet', string='Cost Sheet')
    cost_expense_line_id = fields.Many2one(
        'kk.cost.expense.line', string='Cost Expense')

    price_unit = fields.Float(string='Unit Price', required=True, digits=dp.get_precision('Product Price'))
    adv_line_amount = fields.Float(string="Advance Amount", readonly=True)
    price_tax = fields.Monetary(string='Tax',
        store=True, readonly=True, compute='_compute_price')
    price_total = fields.Monetary(string='Total',
        store=True, readonly=True, compute='_compute_price')
    price_subtotal = fields.Monetary(string='Subtotal',
        store=True, readonly=True, compute='_compute_price')
    quantity = fields.Float(string='Quantity', digits=dp.get_precision('Product Unit of Measure'),
        required=True, default=1)
    company_id = fields.Many2one('res.company', string='Company',
        related='settlement_id.company_id', store=True, readonly=True)
    partner_id = fields.Many2one('res.partner', string='Partner',
        related='settlement_id.partner_id', store=True, readonly=True)
    currency_id = fields.Many2one('res.currency', related='settlement_id.currency_id', store=True)
    #company_currency_id = fields.Many2one('res.currency', related='settlement_id.company_currency_id', readonly=True)
    settlement_line_tax_ids = fields.Many2many('account.tax',
        'settlement_line_tax', 'settlement_line_id', 'tax_id',
        string='Taxes', domain=[('type_tax_use','!=','none'), '|', ('active', '=', False), ('active', '=', True)], oldname='invoice_line_tax_id')
    reference = fields.Char(string="Bill Reference")
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'settlement.lines')], string='Attachments')
    supplier_id = fields.Many2one('res.partner',string="Supplier", domain=[('supplier','=',True)])

    @api.multi
    def _compute_attachment_number(self):
        attachment_data = self.env['ir.attachment'].read_group([('res_model', '=', 'settlement.lines'), ('res_id', 'in', self.ids)], ['res_id'], ['res_id'])
        attachment = dict((data['res_id'], data['res_id_count']) for data in attachment_data)
        for expense in self:
            expense.attachment_number = attachment.get(expense.id, 0)

    @api.multi
    def action_get_attachment_view(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('base', 'action_attachment')
        res['domain'] = [('res_model', '=', 'settlement.lines'), ('res_id', 'in', self.ids)]
        res['context'] = {'default_res_model': 'settlement.lines', 'default_res_id': self.id}
        return res

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            if not self.name:
                self.name = self.product_id.display_name or ''
            self.unit_amount = self.product_id.price_compute('standard_price')[self.product_id.id]
            self.product_uom_id = self.product_id.uom_id
            self.tax_ids = self.product_id.supplier_taxes_id
            account = self.product_id.product_tmpl_id._get_product_accounts()['expense']
            if account:
                self.account_id = account

    @api.multi
    def _prepare_advance_line(self, group_id=False, amount=False):
        self.ensure_one()
        vals = {
            'name': self.name,
            'product_id': self.product_id.id,
            'product_uom_id': self.uom_id.id,
            'unit_amount': amount,
            'quantity': 1,
        }
        return vals
