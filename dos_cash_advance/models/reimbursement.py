# -*- coding: utf-8 -*-

import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.tools import float_is_zero, float_compare
from odoo.tools.misc import formatLang

from odoo.exceptions import UserError, RedirectWarning, ValidationError

import odoo.addons.decimal_precision as dp

class Reimbursement(models.Model):
    _name ="reimbursement"
    _inherit = ['mail.thread']
    _description ="Reimbursement"
    
    name = fields.Char(string='Name')
    partner_id = fields.Many2one('res.partner', string="Create By",)
    responsible_id = fields.Many2one('res.partner', string="Responsible", domain="[('is_employee','=',True)]")
    date_submitted = fields.Date(string="Date Submit")
    description = fields.Char(string="Description")