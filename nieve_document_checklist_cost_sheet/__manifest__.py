# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Nieve Document Checklist Cost Sheet",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.nievetechnology.com',
    'category': 'General',
    'summary': 'Discount',
    "description": """
        Last Update 9 Sep 2020
        Add list Default Checklist
    """,
    "depends": [
        'nieve_document_checklist'],
    'data': [
        'views/inherit_doc_kk_cost_sheet_view.xml',
        'views/inherit_base_config_cost_sheet.xml',
        'views/view_master_config_document_check.xml'
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

