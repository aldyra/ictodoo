from odoo import fields, models
from odoo import api, fields, models, _

class ResConfigSettingsDocumentChecklis(models.TransientModel):
    _inherit = "res.config.settings"

    document_check_config_ids = fields.One2many(
        'document.check.config', 'config_document_check_id','document checklist',related='company_id.document_check_config_ids')

class ResCompanyDocumentChecklist(models.Model):
    _inherit = "res.company"

    document_check_config_ids = fields.One2many(
        'document.check.config', 'config_document_check_id','document checklist')