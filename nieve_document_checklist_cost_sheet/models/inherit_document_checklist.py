import base64
from odoo import api, fields, models, _


class InheritDocumentCheck(models.Model):
    _inherit = 'document.check'
    
    
    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet',string="Cost sheet")
    upload_file = fields.Binary(string='Upload File')
    upload_name = fields.Char(string="Upload Name")
    document_type =fields.Char(string="Document Type")
    is_doc_check_config = fields.Boolean(string="is doc config", default=False)
    document_id = fields.Many2one('master.document.check',string="Document Type")
    