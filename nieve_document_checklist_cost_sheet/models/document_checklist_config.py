from odoo import api, fields, models, _

class InheritDocumentCheckConfig(models.Model):
    _name = 'document.check.config'
    
    name = fields.Char(string="Name")
    upload_file = fields.Binary(string='Upload File')
    upload_name = fields.Char(string="Upload Name")
    document_type =fields.Char(string="Document Type")
    validate = fields.Boolean(string="Validate",default=False)
    config_document_check_id = fields.Many2one('res.company','config Document')
    document_id = fields.Many2one('master.document.check',string="Document Type")


class InheritMasterDocument(models.Model):
    _name = 'master.document.check'

    name = fields.Char(string="Name")
    in_active = fields.Boolean(string="In Active",default=False)