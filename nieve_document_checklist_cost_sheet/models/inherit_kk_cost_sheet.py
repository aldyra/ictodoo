from odoo import api, fields, models, _
from openerp.exceptions import UserError

class InheritDocumentCheckCS(models.Model):
    _inherit = 'kk.cost.sheet'

    document_checklist_ids = fields.One2many('document.check','kk_cost_sheet_id',string="Document Checklist")

    @api.multi
    def action_close(self):
        for ltop in self.fal_invoice_milestone_line_date_ids:
            if ltop.invoice_id:
                if ltop.invoice_id.state != 'paid':
                    raise UserError(_("You can not Close Cost Sheet when All Invoice not Paid"))
        for po in self.kk_purchase_order_ids:            
            state = [vb.state for vb in po.invoice_ids]
            for state_inv in state: 
                if state_inv !='paid':
                    raise UserError(_("You can not Close Cost Sheet when All Vendor Bills not Paid "))
            
        self.action_recalculate_cof()
        return self.write({'state': 'close'})

    @api.multi
    def create_doc_checklis(self):
        for item in self:

            # =============dinonaktifkan ====================
            # if not item.document_checklist_ids:
            #     obj_part = self.env['document.check'].search([('partner_id','=',item.partner_id.id)])
            #     print ('ssssss')
            #     for line in obj_part:
            #         value={'name': line.name,      
            #                 'kk_cost_sheet_id':item.id
            #                 }
            #         line.create(value)
            if not item.document_checklist_ids:
                obj_part = self.env['document.check.config'].search([])
                for line in obj_part:
                    if line.config_document_check_id:
                        value={'document_type': line.document_type,      
                                'kk_cost_sheet_id':item.id,
                                'name':line.name,
                                'is_doc_check_config': True
                                }
                        obj_document = self.env['document.check'].create(value)

    @api.model
    def create(self, vals):
        vals['name'] = _('Draft')

        company = self.env['res.company']._company_default_get(
            'kk.cost.sheet')

        detail_list = []
        for dtl in company.kk_cost_exp_line_ids:
            data = (
                0, 0, {
                    'product_id': dtl.product_id.id,
                    'name': dtl.name,
                    'expense_plan': dtl.expense_plan,
                }
            )
            detail_list.append(data)
        vals['kk_cost_exp_line_ids'] = detail_list
        detail_checklist = []
        for check in company.document_check_config_ids:
            data_check = (
                0, 0, {
                    'document_id': check.document_id.id,
                    'name':check.name,
                    'is_doc_check_config': True
                }
            )
            detail_checklist.append(data_check)
        vals['document_checklist_ids'] = detail_checklist
        res = super(InheritDocumentCheckCS, self).create(vals)
        return res



    @api.multi
    def action_confirm(self):
        for check in self.document_checklist_ids:
            if check.is_doc_check_config == True and not check.upload_file:
                raise UserError(_("File attachment is missing"))
        # Check Total Invoice and Term of Payment.
        self.check_total_top()
        #Dimatikan
        #self.action_recalculate_cof()
        self.check_profit()
        if self.name == _('Draft'):
            self.name = self.env['ir.sequence'].next_by_code('cost.sheet')
        self.create_doc_checklis()
        return self.write({'state': 'appr_mgr', 'lock_exp_lines': True, 'lock_cof_lines': True})

    