# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models, tools, SUPERUSER_ID
from odoo.tools.translate import _
from odoo.exceptions import UserError, AccessError, ValidationError
import datetime

class CashFlowCategory(models.Model):
    _name = 'cash.flow.category'

    name = fields.Char('Name', size=256, required=True)
    sequence = fields.Integer('Sequence',required=True)
    sub_category_line = fields.One2many('sub.category.line','category_id','Lines', required=True)

class SubCategoryLine(models.Model):
    _name = 'sub.category.line'

    category_id = fields.Many2one('cash.flow.category','Category')
    name = fields.Char('Name', size=256, required=True)
    sequence = fields.Integer('Sequence',required=True)

class AccountAccount(models.Model):
    _inherit = 'account.account'

    ar_ap = fields.Boolean('AR/AP')
    sub_cashflow_category_id = fields.Many2one('sub.category.line','Cash Flow Sub Category')

