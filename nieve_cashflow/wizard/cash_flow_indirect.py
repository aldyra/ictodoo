# -*- coding: utf-8 -*-
##############################################################################
#
#   OpenERP, Open Source Management Solution    
#   Copyright (C) 2013 ADSOft (<http://www.adsoft.co.id>). All Rights Reserved
#
##############################################################################

from odoo import api, fields, models

class CashFlowIndirect(models.TransientModel):
    _name = 'cash.flow.indirect'
    _description = 'Cash Flow Indirect'


    company_id = fields.Many2one('res.company', 'Company', required=True, default='get_default_company')
    without_zero = fields.Boolean('Without zero amount', help="Check this if report without zero budget")
    date_start = fields.Date('Date Start',required=True)
    date_stop = fields.Date('Date Start',required=True)

    def get_default_company(self):
        return self.env['res.users'].browse(self.env.uid).company_id.id

    def print_cash_flow(self, context):
        # print "--------------------------------->>"
        datas = {'ids': self.env.context.get('active_ids')}
        datas['model'] = 'cash.flow.indirect'
        datas['form'] = self.read(self.env.context)[0]
    
        return {
            'type': 'ir.actions.report.xml',
            #'report_name': 'cash.flow.report',
            'report_name': 'cash.flow.indirect.report.xls',
            'report_type': 'webkit',
            'datas': datas,
        }


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: