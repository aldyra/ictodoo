from lxml import etree

from odoo import api, fields, models, _
from odoo.osv.orm import setup_modifiers
from openerp.exceptions import UserError

class CostSheetPriceUpdateWizard(models.TransientModel):

    _name = 'cost.sheet.price.update.wizard'
    
    amount      = fields.Float(string='Unit Price Cost', required=True)
    cs_line_id  = fields.Many2one('kk.cost.sheet.line')
    
    @api.model
    def default_get(self, fields):
        res = super(CostSheetPriceUpdateWizard, self).default_get(fields)
        cs_line_id = self._context.get('active_ids')
        
        
        cs_line = self.env['kk.cost.sheet.line'].browse(cs_line_id)
        #if 'name' in fields:
        res.update({'amount'         : cs_line.cost_price_unit,'cs_line_id' : cs_line.id})
                    #'cost_sheet_line_m2m'   : [(6, 0, asset.kk_cost_sheet_line_ids.ids)]
        return res
    
    @api.multi
    def update(self):
        print ("update")
        ##Update History
        vals = {'date'          : fields.Datetime.now(),
                'amount'        : self.cs_line_id.cost_price_unit,
                'cs_line_id'    : self.cs_line_id.id}
        self.env['cs.cost.price.log'].create(vals)
        print ("update1")
        self.cs_line_id.write({'cost_price_unit': self.amount})
        print ("update2")
        return True
        