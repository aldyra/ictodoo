from lxml import etree

from odoo import api, fields, models, _
from odoo.osv.orm import setup_modifiers
from openerp.exceptions import UserError
from odoo.addons.nieve_cost_sheet.models.common import year_list
import time
import datetime
from dateutil.relativedelta import relativedelta
from datetime import datetime
YEAR_LIST = year_list()

class ForecastingCostSheetReportWizard(models.TransientModel):
    _name = 'forecasting.ticket.wizz'
    
    year_period = fields.Selection(YEAR_LIST, string='Year', required=True, default=lambda self: self._default_year())

    def _default_year(self):
        return int(time.strftime('%Y'))
    
    @api.cr_uid_ids_context
    def generate(self, context=None):
        if context is None:
            context = {}
        datas = {'ids': self.env.context.get('active_ids')}
        datas['model'] = 'forecasting.ticket.wizz'
        datas['form'] = self.read(self.env.context)[0]
        print (datas)
        report_name = 'nieve_cost_sheet.forecasting_report_xlsx'
        report = self.env['ir.actions.report'].search(
            [('report_name', '=', report_name)], limit=1)        
        return report.report_action(self,data=datas)
    