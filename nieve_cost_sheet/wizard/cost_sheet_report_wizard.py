from lxml import etree

from odoo import api, fields, models, _
from odoo.osv.orm import setup_modifiers
from openerp.exceptions import UserError

class CostSheetReportWizard(models.TransientModel):
    _name = 'cost.sheet.report.wizard'
    
    type    = fields.Selection([('sales','Sales'), ('sales_director','Sales Director'), ('finance_director','Finance Director')], string='Type', required=True)
    filter_option   = fields.Selection([('state', 'State'),('date', 'Date'),('cs', 'Cost Sheet')], string='Filter Option', required=True)
    filter_state   = fields.Selection([('draft', 'Draft'),
                            ('appr_mgr', 'Approval Manager'),
                            ('appr_gm', 'Approval G.Manager'),
                            ('appr_drc', 'Approval Director'),
                            ('post', 'Post'),
                            ('close', 'Closed'),
                            ('cancel', 'Cancel'),
                            ], string='Status', required=False)
    filter_date_from   = fields.Datetime(string='Date From', required=False)
    filter_date_to     = fields.Datetime(string='Date To', required=False)
    filter_cost_sheet_ids  = fields.Many2many('kk.cost.sheet',string='Cost Sheet')
    
    @api.multi
    def generate(self, report_type):
        if self.type=='sales':
            report_name = 'nieve_cost_sheet.report_cost_sheet_xlsx'
        elif self.type=='sales_director':
            report_name = 'nieve_cost_sheet.report_cost_sheet_sales_director_xlsx'
        elif self.type=='finance_director':
            report_name = 'nieve_cost_sheet.report_cost_sheet_finance_director_xlsx'
        report = self.env['ir.actions.report'].search([('report_name', '=', report_name)], limit=1)
        return report.report_action(self)