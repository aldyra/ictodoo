from lxml import etree

from odoo import api, fields, models, _
from odoo.osv.orm import setup_modifiers
from openerp.exceptions import UserError

class CostSheetWizard(models.TransientModel):

    _name = 'cost.sheet.wizard'
    
    @api.multi
    def call_line_wiz_top(self):
        print ("call wizard fucntion")
        wizard_form = self.env.ref('nieve_cost_sheet.view_costsheet_line_wizz', False)
        context = dict(self.env.context or {})
        if context.get('cost_sheet_id'):
            context['active_id'] = self.id
            
        return {
                'name': _('Term of Payment'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'cost.sheet.line.wizard',
                'view_id': wizard_form.id,
                'type': 'ir.actions.act_window',
                'res_id': self.env.context.get('cost_sheet_id'),
                'context': context,
                'target': 'new'
            }
    
    @api.model
    def default_get(self, fields):
        res = super(CostSheetWizard, self).default_get(fields)
        asset_id = self._context.get('active_ids')
        
        
        asset = self.env['kk.cost.sheet'].browse(asset_id)
        #if 'name' in fields:
        print ("asset.kk_cost_sheet_line_ids.ids----->>", asset.kk_cost_sheet_line_ids.ids)
        res.update({'cost_sheet_id'         : asset.id,})
                    #'cost_sheet_line_m2m'   : [(6, 0, asset.kk_cost_sheet_line_ids.ids)]
        return res
    
    @api.depends("cost_sheet_line_m2m")
    def _get_csl_domain_wizz(self):
        csl_ids = []
        context = self.env.context
        cost_sheet = context.get('active_id')
        cs_obj = self.env['kk.cost.sheet'].browse(cost_sheet)
        for line in cs_obj.kk_cost_sheet_line_ids:
            csl_ids.append(line.id)
        return [('id', 'in', csl_ids)]

    @api.depends('amount','discount')
    def _amount_all(self):
        self.update({'total_inv': self.amount - self.discount})
    

    name = fields.Char(string='Description')
    cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet')
    cost_sheet_line_m2m = fields.Many2many(
        'kk.cost.sheet.line', 'term_cost_sheet_line_m2m_rel',
        'term_id', 'cs_line_id', 'Cost Sheet Line',
        #domain=[('id','in',domain_list)]
        #domain=lambda self: self._get_csl_domain_wizz()
        #domain=lambda self: [("id", "in", self.cost_sheet_id.kk_cost_sheet_line_ids.ids)]
    )
    percentage = fields.Float('Percentage (%)', track_visibility="onchange")
    amount     = fields.Float('Amount', track_visibility="onchange")
    discount  = fields.Float('Discount', )
    total_inv = fields.Float('Grand Total', compute="_amount_all")
    date = fields.Date('Estimate to Invoice', track_visibility="onchange")
    item_invoice_term_line = fields.One2many('item.invoice.term.line.wizz', 'cost_sheet_wizard_id', string='Item Invoice Terms Line')
    
    @api.onchange('percentage', 'cost_sheet_line_m2m','total_inv')
    def onchange_percentage(self):
        self.item_invoice_term_line = False
        total_sale = 0.0
        item_invoice_term_line_list = []
        
        sum_total_sale_selected = 0.0
        sum_total_sale_selected = sum(line.price_total for line in self.cost_sheet_line_m2m)
        
        total_amount_line = 0.0
        for cost in self.cost_sheet_line_m2m:
            print ("self.total_inv--->", self.total_inv)
            amount = round(((self.total_inv) / sum_total_sale_selected) * cost.price_total, 2)
            print ("amount--->", amount)
            vals = {'cost_sheet_line_id': cost.id,
                    'product_id'    : cost.product_id.id,
                    'name'          : cost.name,
                    'amount'        : amount
                    }
            item_invoice_term_line_list.append((0,0, vals))
            total_sale += cost.price_total
            total_amount_line += amount
        
        if total_amount_line != self.amount:
            diff = self.total_inv - total_amount_line
            item_invoice_term_line_list and item_invoice_term_line_list[0][2].update({'amount' : item_invoice_term_line_list[0][2]['amount'] + diff}) 
        
        if self.percentage != 0.0:
            amount = (self.percentage / 100) * total_sale
            self.amount = round(amount,2)
        
        self.item_invoice_term_line = item_invoice_term_line_list
    
    @api.onchange('name')
    def _onchange_name(self):
        csl_ids = []
        for line in self.cost_sheet_id.kk_cost_sheet_line_ids:
            csl_ids.append(line.id)
        return {'domain': {'cost_sheet_line_m2m': [('id', 'in', csl_ids)]}}
    
#     @api.onchange('name')
#     def onchange_name(self):
#         self._get_csl_domain_wizz()

    @api.multi
    def confirm(self):
        cost_sheet = self.env.context.get('active_id')
        cs = self.env['kk.cost.sheet']
        cs_obj = cs.browse(cost_sheet)
        term_obj = self.env['fal.invoice.term.line']
        
        #Check Validation
        if self.item_invoice_term_line:
            total_check = 0.0
            for l in self.item_invoice_term_line:
                total_check += l.amount
            if round(self.total_inv, 2) != round(total_check, 2) and (abs(round(self.total_inv, 2) - round(total_check, 2)) > 0.5):
                raise UserError(_('Total Invoice should be same with detail'))
            
        
        if cs_obj:
            cs_line_list = []
            for cs in self.cost_sheet_line_m2m:
                cs_line_list.append((4, cs.id))
            
            item_cs_line_list = []
            for l in self.item_invoice_term_line:
                vals = {'cost_sheet_line_id':l.cost_sheet_line_id.id,
                        'product_id'    : l.product_id.id,
                        'name'          : l.name,
                        'amount'        : l.amount}
                item_cs_line_list.append((0,0, vals))
            cs_val = {
                'name': self.name,
                'cost_sheet_line_m2m': cs_line_list,
                'item_invoice_term_line': item_cs_line_list,
                'percentage': self.percentage,
                'amount': self.amount,
                'discount': self.discount,
                'total_inv': self.total_inv,
                'date': self.date,
                'cs_cost_sheet_id': cs_obj.id
            }
            term_obj.create(cs_val)
            
class ItemInvoiceTermLineWizz(models.TransientModel):
    _name = 'item.invoice.term.line.wizz'
    
    cost_sheet_line_id  = fields.Many2one('kk.cost.sheet.line', string='Cost Sheet Line')
    product_id          = fields.Many2one('product.product', string='Product')
    name                = fields.Char(string='Description')
    amount              = fields.Float(string='Amount')
    cost_sheet_wizard_id= fields.Many2one('cost.sheet.wizard', string='CS Wizard')
