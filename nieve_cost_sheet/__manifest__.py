# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Cost Sheet",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.sriwijayaair.co.id',
    'category': 'Sales',
    'summary': 'Cost Sheet',
    "description": """
    Module to group SO to Cost Sheet Last Updated 03-01-2021 

    """,
    "depends": [
        'base','sale_management', 'purchase_requisition','purchase',
        'nieve_invoice_milestone', 'nieve_payment', 'sale','stock','sale_stock',
        'hr_expense', 'nieve_discount', 'account','report_xlsx'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/access_security.xml',
        'data/costsheet_sequence.xml',
        'wizard/cost_sheet_update_price_wizard_view.xml',
        'wizard/cost_sheet_wizard_views.xml',
        'wizard/cost_sheet_report_wizard_views.xml',
        'wizard/forecasting_report_wizard_views.xml',
        'views/kk_cost_sheet_views.xml',
        'views/kk_cost_sheet_only_views.xml',
        'views/sale_views.xml',
        'views/sales_team_view.xml',
        'views/purchase_views.xml',
        'views/account_move_views.xml',
        'views/account_payment_views.xml',
        'views/res_config_settings_views.xml',
        'views/res_partner_cs.xml',
        'reports/reports.xml',
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
