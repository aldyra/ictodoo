import calendar
from datetime import datetime,date,time,timedelta
from dateutil.relativedelta import *
import itertools
import logging
from psycopg2 import OperationalError


# 2 :  imports of odoo
from odoo import models, fields, api, exceptions
from odoo.tools.translate import _
from odoo import SUPERUSER_ID
import odoo
import odoo.addons.decimal_precision as dp
from odoo.tools import float_compare, float_is_zero
from odoo.exceptions import ValidationError
from odoo import tools
from odoo.tools import (drop_view_if_exists)

class v_invoice_line_term_line(models.Model):

        _name = 'v.invoice.line.term.line'
        _description = 'Report v invoice line term line'
        _auto = False

        id = fields.Integer('ID')
        cs_cost_sheet_id = fields.Many2one('kk.cost.sheet')
        date_month = fields.Integer('Date month')
        year_period = fields.Integer('year Period')

        
        @api.model_cr
        def init(self):
            tools.drop_view_if_exists(self._cr, 'v_invoice_line_term_line')
            self._cr.execute("""      
            CREATE OR REPLACE FUNCTION function_get_partner_name_id(i_partner_id int)
                        RETURNS char LANGUAGE plpgsql AS $BODY$
                        DECLARE
                            partner_name varchar;
                        begin
                            SELECT
                                name into partner_name
                            FROM res_partner rp
                            WHERE rp.id::integer = i_partner_id;
                        RETURN partner_name ;
                        END
                        $BODY$;  

                        create or replace view v_invoice_line_term_line as
                        select 
                        row_number() over()id,
                        cs_cost_sheet_id,
                                to_char(fitl.date,'mm') date_month,
                                to_char(fitl.date,'yyyy') year_period
                                from fal_invoice_term_line fitl
                                    left join kk_cost_sheet kcs
                                    on kcs.id = fitl.cs_cost_sheet_id
                                    where kcs.state ='post'
                                    """)
