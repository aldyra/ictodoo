from odoo import api, fields, models, _
from odoo.exceptions import UserError
import odoo.addons.decimal_precision as dp


class PurchaseRequisition(models.Model):
    _inherit = 'purchase.requisition'

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_type_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            type_sales
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'
                        GROUP BY type_sales
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_group_sale(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where ru.id ='%s'
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    kk_cost_sheet_id = fields.Many2one(
        'kk.cost.sheet', 'Cost Sheet', copy=False)
    kk_sale_id = fields.Many2one(
        'sale.order', 'Sale Order')
    kk_is_preload = fields.Selection(
        [
            ('nonpre', 'Non Preload'),
            ('pre', 'Preload'),
        ],
        'Preload Status', default='nonpre')

    po_desc = fields.Char('PO Description')
    pr_note = fields.Text('Notes')
    group_id = fields.Many2one('procurement.group', string="Procurement Group", copy=False)

    state = fields.Selection(
        [
            ('draft', 'Draft'),
            #('appr_mgr', 'Approval Manager'),
            #('appr_drc', 'Approval Director'),
            #('appr_presdir', 'Approval Presdir'),
            ('in_progress', 'Confirmed'),
            ('open', 'Bid Selection'),
            ('done', 'Done'),
            ('cancel', 'Cancelled')
        ], 'Status', track_visibility='onchange', required=True,
        copy=False, default='draft')
    kk_desc_product = fields.Char('Description Product')

    @api.multi
    def purchase_requisition_filter_dynamic(self):
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
        search_view_id = data_obj.get_object_reference('purchase_requisition', 'view_purchase_requisition_filter')[1]

        action_window_id = data_obj.get_object_reference('purchase_requisition', 'action_purchase_requisition')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])

        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access                    = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access                = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access               = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access                = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        product_allowed_access                  = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_user_product')
        group_purchase_requisition_admin_user   = self.env['res.users'].has_group('nieve_cost_sheet.group_purchase_requisition_admin_user')
        

        team_ids = []
        filter_domain = []
        if group_purchase_requisition_admin_user:
            filter_domain = []
        elif drc_sales_allowed_access:
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            arrCos = []
                            for cos in self.env['kk.cost.sheet'].search([('creator_team_id','in',user_team_product)]):
                                arrCos.append(cos.id)
                            filter_domain = [('kk_cost_sheet_id','in',arrCos)]
                else:
                    print ('1 tipe sales')
                    team_id = self.qwery_team_director(login_uid) 
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        else:          
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]


        if login_uid==1:
            filter_domain = []
        
        action = {
            'name'      : _('Purchase Requisition'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,kanban,form',
            'res_model' : 'purchase.requisition',
            'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            'context'   : {"search_default_user_id":login_uid},
        }
        return action

    
    @api.onchange('kk_cost_sheet_id')
    def onchange_cost_sheet(self):
        self.origin = self.kk_cost_sheet_id.kk_sale_order_id.name
    
    @api.multi
    def copy_pr(self):
        pr_copy_id = self.copy(default={'kk_cost_sheet_id': self.kk_cost_sheet_id.id, 'vendor_id': False})
        
        return {
            'view_type': 'form',
            'view_mode': 'form',
            #'view_id': view_id,
            'type': 'ir.actions.act_window',
            'res_model': 'purchase.requisition',
            'res_id': pr_copy_id.id,
            'view_type': 'form',
        }
    
    @api.multi
    def action_confirm(self):
        return self.action_in_progress()
        #return self.write({'state': 'appr_mgr'})

#     @api.multi
#     def action_appr_mgr(self):
#         if self.kk_is_preload == 'nonpre':
#             return self.action_in_progress()
#         else:
#             return self.write({'state': 'appr_drc'})
#
#     @api.multi
#     def action_appr_drc(self):
#         return self.write({'state': 'appr_presdir'})

    def _prepare_tender_values(
        self, product_id, product_qty, product_uom,
        location_id, name, origin, values
    ):
        sale_line_id = values.get('sale_line_id')
        sale_line_obj = self.env['sale.order.line'].browse(sale_line_id)
        detail_ids = sale_line_obj.kk_order_line_ids
        list_dtl = []
        for line in detail_ids:
            vals = (0, 0, {
                'pr_line_id': self.id,
                'product_code': line.product_code,
                'name': line.name,
                'product_uom': line.product_uom.id,
                'product_uom_qty': line.product_uom_qty
            })
            list_dtl.append(vals)
        return{
            'user_id': sale_line_obj.order_id.creator_user_id.id,
            'origin': origin,
            'date_end': values['date_planned'],
            'warehouse_id': values.get('warehouse_id') and values['warehouse_id'].id or False,
            'company_id': values['company_id'].id,
            'kk_sale_id': sale_line_obj.order_id.id,
            'kk_cost_sheet_id': sale_line_obj.order_id.kk_cost_sheet_id.id,
            'dest_address_id': sale_line_obj.order_id.partner_id.id,
            'kk_desc_product': sale_line_obj.name,
            'group_id': sale_line_obj.order_id.procurement_group_id.id,
            'line_ids': [(0, 0, {
                'product_id': product_id.id,
                'name': sale_line_obj.name,
                'product_uom_id': product_uom.id,
                'product_qty': product_qty,
                'move_dest_id': values.get('move_dest_ids') and values['move_dest_ids'][0].id or False,
                'sale_line_id': sale_line_obj.id,
                'kk_order_line_ids': list_dtl
            })],
        }


class PurchaseRequisitionLine(models.Model):
    _inherit = 'purchase.requisition.line'

    name    = fields.Char(string='Description')
    sale_line_id    = fields.Many2one('sale.order.line', string='Sale Order Line')
    kk_order_line_ids = fields.One2many(
        'kk.order.line', 'pr_line_id', 'Order Detail')

    @api.multi
    def _prepare_purchase_order_pr_line(self, name, product_qty=0.0, price_unit=0.0, taxes_ids=False):
        print ("##_prepare_purchase_order_pr_line")
        self.ensure_one()
        requisition = self.requisition_id
        print ("self.sale_line_id.id--->>", self.sale_line_id.id)
        return {
            'name': name,
            'product_id': self.product_id.id,
            'product_uom': self.product_id.uom_po_id.id,
            'product_qty': product_qty,
            'price_unit': price_unit,
            'taxes_id': [(6, 0, taxes_ids)],
            'date_planned': requisition.schedule_date or fields.Date.today(),
            'account_analytic_id': self.account_analytic_id.id,
            'move_dest_ids': self.move_dest_id and [(4, self.move_dest_id.id)] or [],
            'kk_pr_line_id': self.id,
            'sale_line_id': self.sale_line_id.id
        }


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_type_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            type_sales
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'
                        GROUP BY type_sales
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_group_sale(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where ru.id ='%s'
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    @api.multi
    @api.depends('invoice_ids')
    def _get_payment_date(self):
        for po in self:
            payment_date = False
            print ("po--->", po)
            for inv in po.invoice_ids and po.invoice_ids[0] or []:
                payment_date = inv.payment_date
            po.payment_date = payment_date
    
    kk_cost_sheet_id = fields.Many2one(
        'kk.cost.sheet', 'Cost Sheet', copy=False)
    is_sales = fields.Selection([('sales', 'Sales'),('operation', 'Operation')],'Sales Status')
    kk_sale_id = fields.Many2one(
        'sale.order', 'Sale Order')
    state = fields.Selection(
        [
            ('draft', 'RFQ'),
            ('sent', 'RFQ Sent'),
            ('to approve', 'To Approve'),
            ('appr_sls', 'Approval Sales'),
            ('appr_mgr', 'Approval Sales Manager'),
            ('appr_fin_mgr', 'Approval Finance Manager'),
            ('appr_drc', 'Approval Finance Director'),
            ('purchase', 'Purchase Order'),
            ('done', 'Locked'),
            ('cancel', 'Cancelled')
        ], string='Status', readonly=True, index=True,
        copy=False, default='draft', track_visibility='onchange')
    po_desc = fields.Char('PO Description')
    is_sales_appr = fields.Boolean(compute='_methods_sales_appr')
    ref_quotation = fields.Char('Ref Quotation')
    contact_partner_id = fields.Many2one('res.partner', string='Contact')
    payment_date    = fields.Date(string='Payment Date', compute='_get_payment_date', store=False)

    @api.multi
    @api.depends('name')
    def name_get(self):
        result = []
        for po in self:
            name = po.name
            # if po.partner_ref:
            #     name += ' ('+po.partner_ref+')'
            # if self.env.context.get('show_total_amount') and po.amount_total:
            #     name += ': ' + formatLang(self.env, po.amount_total, currency_obj=po.currency_id)
            result.append((po.id, name))
        return result

    @api.multi
    def purchase_filter_dynamic(self, type):
        print ("purchase_filter_dynamic--->")
        print ("type-->", type)
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
        search_view_id = data_obj.get_object_reference('purchase', 'view_purchase_order_filter')[1]
        if type=='rfq':
            action_window_id = data_obj.get_object_reference('purchase', 'purchase_rfq')[1]
        else:
            action_window_id = data_obj.get_object_reference('purchase', 'purchase_form_action')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])

        
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access                = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access           = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        product_allowed_access              = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_user_product')
        group_purchaseorder_user            = self.env['res.users'].has_group('nieve_cost_sheet.group_purchaseorder_user')
        group_purchaseorder_finance_manager = self.env['res.users'].has_group('nieve_cost_sheet.group_purchaseorder_finance_manager')
        
        team_ids = []
        filter_domain = []
        if group_purchaseorder_user or group_purchaseorder_finance_manager:
            filter_domain = []
        elif drc_sales_allowed_access:
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            arrCos = []
                            for cos in self.env['kk.cost.sheet'].search([('creator_team_id','in',user_team_product)]):
                                arrCos.append(cos.id)
                            filter_domain = [('kk_cost_sheet_id','in',arrCos)]
                else:
                    print ('1 tipe sales')
                    team_id = self.qwery_team_director(login_uid) 
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        else:            
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]


        if login_uid==1:
            filter_domain = []

        
        if type=='rfq':
            label_name = "Requests for Quotation"
            AND = ['&']
            AND.extend(filter_domain)
            AND.append(('state','in',('draft','sent','cancel', 'confirmed')))
            filter_domain = AND
        elif type=='po':
            label_name = "Purchase Orders"
            AND = ['&']
            AND.extend(filter_domain)
            AND.append(('state','not in',('draft','sent', 'confirmed')))
            filter_domain = AND
            
        filter_domain.append(('is_sales','=','sales'))
        
        action = {
            'name'      : label_name,
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,kanban,form,pivot,graph,calendar',
            'res_model' : 'purchase.order',
            'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            'context'   : {'default_is_sales': 'sales', 'search_default_todo':1, 'show_purchase': False},
        }
        return action

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = _('Draft')
        res = super(PurchaseOrder, self).create(vals)
        res.kk_cost_sheet_id = res.requisition_id.kk_cost_sheet_id
        res.kk_sale_id = res.requisition_id.kk_sale_id
        return res

    @api.multi
    @api.depends('name')
    def _methods_sales_appr(self):
        for x in self:
            login_uid = self.env.context.get('uid', False)
            sales_uid = x.kk_sale_id.user_id.id
            if login_uid == sales_uid:
                x.is_sales_appr = True

    @api.multi
    def check_delivery_qty(self):
        for po in self:
            for l in po.order_line:
                if not l.kk_delivery_line_ids:
                    continue
                qty = 0
                for ldlv in l.kk_delivery_line_ids:
                    qty += ldlv.product_uom_qty
                if l.product_qty != qty:
                    raise UserError(_("Quantity order : ' %s ' and delivery should be equal.") % l.name)
        return True

    @api.multi
    def action_confirm(self):
        self.check_delivery_qty()
        if self.name == _('Draft'):
            self.name = self.env['ir.sequence'].next_by_code('purchase.order')
        if self.is_sales == 'operation':
            return self.write({'state': 'appr_fin_mgr'})
        if self.requisition_id:
            self.requisition_id.write({'state' : 'done'})
        return self.write({'state': 'appr_sls'})

    @api.multi
    def action_confirm_sales(self):
        return self.write({'state': 'appr_drc'})
        #return self.write({'state': 'appr_mgr'})

#     @api.multi
#     def action_appr_mgr(self):
#         return self.write({'state': 'appr_drc'})

    # Override Create Picking
    @api.multi
    def _create_picking(self):
        StockPicking = self.env['stock.picking']
        for order in self:
            print ("Self---->>", self)
            if any([ptype in ['product', 'consu'] for ptype in order.order_line.mapped('product_id.type')]):
                detail_obj = self.env['kk.delivery.line'].search([('order_id', '=', self.id)], order='picking_type_id, dest_address_id')
                print ("##detail_obj--->", detail_obj)
                pickings = order.picking_ids.filtered(lambda x: x.state not in ('done','cancel'))
                if not pickings:
                    pick_type = False
                    pick_addr = False
                    default_picking_id = False

                    if detail_obj:
                        for dtl in detail_obj:
                            if pick_type != dtl.picking_type_id.id or pick_addr != dtl.dest_address_id.id:
                                res = dtl._prepare_picking_ict()
                                picking = StockPicking.create(res)
                                print ("dtl.picking_type_id-->", dtl.picking_type_id, "##order.picking_type_id--->",order.picking_type_id)
                                if dtl.picking_type_id == order.picking_type_id:
                                    default_picking_id = picking
                            pick_type = dtl.picking_type_id.id
                            pick_addr = dtl.dest_address_id.id
                            moves = dtl.po_line_id._create_stock_moves_ict(picking, dtl.product_uom_qty)
                            moves = moves.filtered(lambda x: x.state not in ('done', 'cancel'))._action_confirm()
                        ##Create Move##
                        if not default_picking_id:
                            res = order._prepare_picking()
                            default_picking_id = StockPicking.create(res)
                        for l in order.order_line:
                            if not l.kk_delivery_line_ids:
                                moves = l._create_stock_moves(default_picking_id)
                                moves = moves.filtered(lambda x: x.state not in ('done', 'cancel'))._action_confirm()
                        #raise UserError(_("###Trial###"))
                    else:
                        res = order._prepare_picking()
                        res.update({'client_order_ref'  : order.kk_sale_id.client_order_ref,
                                    'cost_sheet_id'     : order.kk_cost_sheet_id.id})

                        picking = StockPicking.create(res)

                        moves = order.order_line._create_stock_moves(picking)
                        moves = moves.filtered(lambda x: x.state not in ('done', 'cancel'))._action_confirm()

                    seq = 0
                    for move in sorted(moves, key=lambda move: move.date_expected):
                        seq += 5
                        move.sequence = seq
                    moves._action_assign()
                    picking.message_post_with_view('mail.message_origin_link',
                        values={'self': picking, 'origin': order},
                        subtype_id=self.env.ref('mail.mt_note').id)
                else:
                    picking = pickings[0]
        return True

    @api.onchange('requisition_id')
    def _onchange_requisition_id(self):
        if not self.requisition_id:
            return

        requisition = self.requisition_id
        if self.partner_id:
            partner = self.partner_id
        else:
            partner = requisition.vendor_id
        payment_term = partner.property_supplier_payment_term_id
        currency = partner.property_purchase_currency_id or requisition.company_id.currency_id

        FiscalPosition = self.env['account.fiscal.position']
        fpos = FiscalPosition.get_fiscal_position(partner.id)
        fpos = FiscalPosition.browse(fpos)

        self.partner_id = partner.id
        self.po_desc    = requisition.po_desc
        self.fiscal_position_id = fpos.id
        self.payment_term_id = payment_term.id
        self.company_id = requisition.company_id.id
        self.currency_id = currency.id
        ##
        self.group_id = requisition.group_id.id
        if requisition.kk_cost_sheet_id:
            self.kk_cost_sheet_id = requisition.kk_cost_sheet_id.id
            self.is_sales = 'sales'
        ##
        if not self.origin or requisition.name not in self.origin.split(', '):
            if self.origin:
                if requisition.name:
                    self.origin = self.origin + ', ' + requisition.name
            else:
                self.origin = requisition.name
        self.notes = requisition.description
        self.date_order = requisition.date_end or fields.Datetime.now()
        self.picking_type_id = requisition.picking_type_id.id

        if requisition.type_id.line_copy != 'copy':
            return

        # Create PO lines if necessary
        order_lines = []
        for line in requisition.line_ids:
            # Compute name
            product_lang = line.product_id.with_context({
                'lang': partner.lang,
                'partner_id': partner.id,
            })
            name = product_lang.display_name
            if product_lang.description_purchase:
                name += '\n' + product_lang.description_purchase

            # Compute taxes
            if fpos:
                taxes_ids = fpos.map_tax(line.product_id.supplier_taxes_id.filtered(lambda tax: tax.company_id == requisition.company_id)).ids
            else:
                taxes_ids = line.product_id.supplier_taxes_id.filtered(lambda tax: tax.company_id == requisition.company_id).ids

            # Compute quantity and price_unit
            if line.product_uom_id != line.product_id.uom_po_id:
                product_qty = line.product_uom_id._compute_quantity(line.product_qty, line.product_id.uom_po_id)
                price_unit = line.product_uom_id._compute_price(line.price_unit, line.product_id.uom_po_id)
            else:
                product_qty = line.product_qty
                price_unit = line.price_unit

            if requisition.type_id.quantity_copy != 'copy':
                product_qty = 0

            # Compute price_unit in appropriate currency
            if requisition.company_id.currency_id != currency:
                price_unit = requisition.company_id.currency_id.compute(price_unit, currency)

            # Create PO line
            order_line_values = line._prepare_purchase_order_pr_line(
                name=name, product_qty=product_qty, price_unit=price_unit,
                taxes_ids=taxes_ids)
            order_lines.append((0, 0, order_line_values))
        self.order_line = order_lines
        self.dest_address_id = self.requisition_id.kk_cost_sheet_id.partner_id.id
        #Update PR to DONE
        # requisition.write({'state' : 'done'})

class DeliveryLineIds(models.Model):
    _name = 'kk.delivery.line'

    READONLY_STATES = {
        'purchase': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    @api.model
    def _default_picking_type(self):
        type_obj = self.env['stock.picking.type']
        company_id = self.env.context.get('company_id') or self.env.user.company_id.id
        types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id.company_id', '=', company_id)])
        if not types:
            types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id', '=', False)])
        return types[:1]

    po_line_id = fields.Many2one('purchase.order.line', 'PO Line', cascade="ondelete")
    order_id = fields.Many2one(
        'purchase.order', string='Order Reference',
        related='po_line_id.order_id', store=True)
    picking_type_id = fields.Many2one(
        'stock.picking.type', 'Deliver To',
        required=True, default=_default_picking_type,
        help="This will determine picking type of incoming shipment")
    dest_address_id = fields.Many2one(
        'res.partner', string='Delivery Address',
        related='po_line_id.order_id.kk_cost_sheet_id.partner_id',
        help="Put an address if you want to deliver directly from the vendor to the customer. "\
             "Otherwise, keep empty to deliver to your own company.")
    default_location_dest_id_usage = fields.Selection(related='picking_type_id.default_location_dest_id.usage', string='Destination Location Type',\
        help="Technical field used to display the Drop Ship Address", readonly=True)
    product_uom_qty = fields.Float(
        string='Quantity',
        digits=dp.get_precision('Product Unit of Measure'),
        default=1.0)

    @api.multi
    def _get_destination_location(self):
        self.ensure_one()
        if self.picking_type_id.name == 'Dropship':
            return self.dest_address_id.property_stock_customer.id
        return self.picking_type_id.default_location_dest_id.id

    @api.model
    def _prepare_picking_ict(self):
        print ("##Self--->>", self)
        if not self.order_id.group_id:
            self.order_id.group_id = self.order_id.group_id.create({
                'name': self.order_id.name,
                'partner_id': self.order_id.partner_id.id
            })
        if not self.order_id.partner_id.property_stock_supplier.id:
            raise UserError(_("You must set a Vendor Location for this partner %s") % self.order_id.partner_id.name)
        return {
            'picking_type_id': self.picking_type_id.id,
            'partner_id': self.order_id.partner_id.id,
            'date': self.order_id.date_order,
            'origin': self.order_id.name,
            'location_dest_id': self._get_destination_location(),
            'location_id': self.order_id.partner_id.property_stock_supplier.id,
            'company_id': self.order_id.company_id.id,
            'client_order_ref': self.order_id.kk_sale_id.client_order_ref,
            'cost_sheet_id': self.order_id.kk_cost_sheet_id.id
        }


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    kk_order_line_ids = fields.One2many(
        'kk.order.line', 'po_line_id', 'Order Detail')
    kk_delivery_line_ids = fields.One2many(
        'kk.delivery.line', 'po_line_id', 'Delivery Detail')
    kk_pr_line_id = fields.Many2one('purchase.requisition.line', 'PR Line')
    sale_line_id    = fields.Many2one('sale.order.line', string='Sale Order Line')

    @api.model
    def create(self, vals):
        res = super(PurchaseOrderLine, self).create(vals)
        if res.order_id.requisition_id:
            for line in res.order_id.requisition_id.line_ids:
                if line.product_id == res.product_id:
                    detail_ids = line.kk_order_line_ids
                    list_dtl = []
                    for line in detail_ids:
                        vals = (0, 0, {
                            'po_line_id': res.id,
                            'product_code': line.product_code,
                            'name': line.name,
                            'product_uom': line.product_uom.id,
                            'product_uom_qty': line.product_uom_qty
                        })
                        list_dtl.append(vals)
                    res.kk_order_line_ids = list_dtl
        return res

    @api.multi
    def _prepare_stock_moves_ict(self, picking, delivery_qty):
        """ Prepare the stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        self.ensure_one()
        res = []
        if self.product_id.type not in ['product', 'consu']:
            return res
        qty = 0.0
        price_unit = self._get_stock_move_price_unit()
        for move in self.move_ids.filtered(lambda x: x.state != 'cancel'):
            qty += move.product_qty

        detail_ids = self.kk_order_line_ids
        list_dtl = []

        for line in detail_ids:
            vals = (0, 0, {
                # 'move_line_id': self.id,
                'product_code': line.product_code,
                'name': line.name,
                'product_uom': line.product_uom.id,
                'product_uom_qty': line.product_uom_qty
            })
            list_dtl.append(vals)
        template = {
            'name': self.name or '',
            'product_id': self.product_id.id,
            'product_uom': self.product_uom.id,
            'date': self.order_id.date_order,
            'date_expected': self.date_planned,
            'location_id': self.order_id.partner_id.property_stock_supplier.id,
            'location_dest_id': self.order_id._get_destination_location(),
            'picking_id': picking.id,
            'partner_id': self.order_id.dest_address_id.id,
            'move_dest_id': False,
            'state': 'draft',
            'purchase_line_id': self.id,
            'company_id': self.order_id.company_id.id,
            'price_unit': price_unit,
            'picking_type_id': self.order_id.picking_type_id.id,
            'group_id': self.order_id.group_id.id,
            'procurement_id': False,
            'origin': self.order_id.name,
            'route_ids': self.order_id.picking_type_id.warehouse_id and [(6, 0, [x.id for x in self.order_id.picking_type_id.warehouse_id.route_ids])] or [],
            'warehouse_id': self.order_id.picking_type_id.warehouse_id.id,
            'product_uom_qty': delivery_qty,
            'kk_order_line_ids': list_dtl
        }
        res.append(template)
        return res

    @api.multi
    def _create_stock_moves_ict(self, picking, delivery_qty):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        for line in self:
            for val in line._prepare_stock_moves_ict(picking, delivery_qty):
                done += moves.create(val)
        return done


class StockMove(models.Model):
    _inherit = 'stock.move'

    kk_order_line_ids   = fields.One2many('kk.order.line', 'move_line_id', 'Order Detail')
    cost_sheet_id       = fields.Many2one('kk.cost.sheet', string='Cost Sheet', related='picking_id.cost_sheet_id')


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    client_order_ref = fields.Char(
        string='Customer Reference')

    cost_sheet_id = fields.Many2one('kk.cost.sheet', string='Cost Sheet')
