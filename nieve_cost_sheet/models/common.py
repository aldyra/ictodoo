# -*- coding: utf-8 -*-
# © 2018 Nieve Technology
import re
import pytz
import datetime as dt
from datetime import datetime
from odoo import _
from odoo.exceptions import ValidationError, UserError


x = dt.datetime.now()
if x.hour == 17:
    hour = 24
elif x.hour == 18:
    hour = 1
elif x.hour == 19:
    hour = 2
elif x.hour == 20:
    hour = 3
elif x.hour == 21:
    hour = 4
elif x.hour == 22:
    hour = 5
elif x.hour == 23:
    hour = 6
else:
    hour = x.hour + 7

def month_list():
    return [
        (1, _('January')),
        (2, _('February')),
        (3, _('March')),
        (4, _('April')),
        (5, _('May')),
        (6, _('June')),
        (7, _('July')),
        (8, _('August')),
        (9, _('September')),
        (10, _('October')),
        (11, _('November')),
        (12, _('December'))
    ]

def year_list():
    current_year = datetime.now().year
    return [
        (int(y), str(y))
        for y in range(current_year, 2010, -1)
    ]

def year_new_list():
    current_year = datetime.now().year
    return [
        (int(y), str(y))
        for y in range(current_year, 2010, -1)
    ]

def date_month_list():
    current_year = datetime.now().year
    return [('-'.join([str(a), str(b) if b >= 10 else '0' + str(b), '01']),
          '/'.join(['01', str(b) if b >= 10 else '0' + str(b), str(a)]))
            for a in range(current_year, 2013, -1) for b in range(12, 0, -1)]

def roman_list():
    return [
        (0,'-'), (1,'I'), (2,'II'), (3,'III'),
        (4,'IV'), (5,'V'), (6,'VI'), (7,'VII'),
        (8,'VIII'), (9,'IX'), (10,'X'), (11,'XI'),
        (12,'XII')
    ]

def validate_acc_number(acc_number):
    if not re.match(r'^([a-zA-Z0-9]+)$', acc_number):
        raise ValidationError('Account Number: Only alpha-numeric(a-z, 0-9) allowed.')

def document_type():
    return [
        (_('401'), _('401 - PENERIMAAN')),
        (_('402'), _('402 - PENGELUARAN')),
        (_('403'), _('403 - MEMORIAL')),
        (_('404'), _('404 - PRODUKSI')),
        (_('405'), _('405 - HUTANG/PIUTANG')),
        (_('405'), _('411 - PENERIMAAN - KOREKSI')),
        (_('405'), _('412 - PENGELUARAN - KOREKSI')),
        (_('413'), _('413 - MEMORIAL - KOREKSI')),
        (_('414'), _('414 - PRODUKSI - KOREKSI')),
        (_('415'), _('415 - HUTANG/PIUTANG - KOREKSI'))
    ]

def partner_types():
    return [
        (_('BROKER'), _('BROKER')),
        (_('REINSURER'), _('REINSURER')),
        (_('SUPPLIER'), _('SUPPLIER')),
        (_('CUSTOMER'), _('CUSTOMER')),
        (_('UMUM'), _('UMUM')),
        (_('KONSORSIUM'), _('KONSORSIUM')),
        (_('INSURER'), _('INSURER')),
        (_('CUSTOMER-BROKER'), _('CUSTOMER, BROKER')),
        (_('REINSURER-INSURER'), _('REINSURER, INSURER')),
        (_('BANK'), _('BANK')),
    ]

def partner_states():
    return [
        (_('ACQUIRED'), _('ACQUIRED')),
        (_('VALID'), _('VALID')),
        (_('POTENTIAL'), _('POTENTIAL')),
    ]

def get_print_date():
    jkt = datetime.now(pytz.timezone('Asia/Jakarta' or 'UTC'))
    print_datetime_now = str(
        datetime(jkt.year, jkt.month, jkt.day, jkt.hour, jkt.minute, jkt.second).strftime("%d-%m-%Y %H:%M:%S %p"))
    # return str(dt.datetime(x.year, x.month, x.day, hour, x.minute, x.second).strftime("%d-%m-%Y %H:%M:%S"))
    return print_datetime_now

def month_indo():
    return [
        (0, _('')),
        (1, _('Januari')),
        (2, _('Februari')),
        (3, _('Maret')),
        (4, _('April')),
        (5, _('Mei')),
        (6, _('Juni')),
        (7, _('Juli')),
        (8, _('Agustus')),
        (9, _('September')),
        (10, _('Oktober')),
        (11, _('November')),
        (12, _('Desember'))
    ]


    # [('401', '401 - PENERIMAAN'),('402', '402 - PENGELUARAN'),('403', '403 - MEMORIAL'),('404', '404 - PRODUKSI'),('405', '405 - HUTANG/PIUTANG'),('413', '413 - MEMORIAL - KOREKSI'),('414', '414 - PRODUKSI - KOREKSI'),('415', '415 - HUTANG/PIUTANG - KOREKSI')]


