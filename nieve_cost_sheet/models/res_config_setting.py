from odoo import fields, models
from odoo import api, fields, models, _

class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    cof_percentage = fields.Float(
        string='COF Percentage',
        related='company_id.cof_percentage')
    kk_cost_exp_line_ids = fields.One2many(
        'kk.cost.expense.line', 'config_company_id',
        related='company_id.kk_cost_exp_line_ids')
    
    gm_cs_approval_gp   = fields.Float(string='GM CS Approval', related='company_id.gm_cs_approval_gp')
    drc_cs_approval_gp  = fields.Float(string='Director CS Approval', related='company_id.drc_cs_approval_gp')
    
    image_cs_template       = fields.Binary('Upload CS csv Template', related='company_id.image_cs_template')
    cs_template_name        = fields.Char('Job CS csv Template Name', related='company_id.cs_template_name')


class ResCompany(models.Model):
    _inherit = "res.company"

    cof_percentage = fields.Float(string='COF Percentage')
    kk_cost_exp_line_ids = fields.One2many(
        'kk.cost.expense.line', 'config_company_id')
    
    gm_cs_approval_gp   = fields.Float(string='GM CS Approval')
    drc_cs_approval_gp  = fields.Float(string='Director CS Approval')
    
    image_cs_template       = fields.Binary('Upload CS csv Template')
    cs_template_name    = fields.Char('Job CS csv Template Name')
    
    @api.model
    def get_cs_template_file(self,field_binary=False, record_id=False, filename=False):
        field_binary    = field_binary
        record_id       = record_id
        filename        = filename
        
        for o in self.browse(record_id):
            if not o.image_cs_template:
                final_url = False
            else:
                final_url = '/web/content/?model=res.company&field=%s&id=%s&filename_field=%s&download=true&filename=%s' % (field_binary, record_id, filename, filename)
        return final_url

class ResPartnerCS(models.Model):
    _inherit = "res.partner"

    user_maintenance_id     = fields.Many2one('res.users',string="Sales Maintenance")