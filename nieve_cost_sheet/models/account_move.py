# -*- coding: utf-8 -*-

import json
import re
import uuid
from functools import partial

from lxml import etree
from dateutil.relativedelta import relativedelta
from werkzeug.urls import url_encode

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.osv.orm import setup_modifiers

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_type_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            type_sales
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'
                        GROUP BY type_sales
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_group_sale(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where ru.id ='%s'
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    @api.multi
    def customer_invoice_filter_dynamic(self):
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
        
        action_window_id = data_obj.get_object_reference('account', 'action_invoice_tree1')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])
        
        cost_sheet = self
        
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        product_allowed_access          = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_user_product')
        group_invoice_admin             = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_admin')
        group_invoice_fin_mgr           = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_fin_mgr')
        group_invoice_accounting        = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_accounting')    
        group_invoice_tax               = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_tax')   

        team_ids = []
        filter_domain = []
        if product_allowed_access or group_invoice_admin or group_invoice_fin_mgr or group_invoice_accounting or group_invoice_tax:
            filter_domain = [('type','=','out_invoice')]
        elif drc_sales_allowed_access:
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            arrCost = []
                            for cos in self.env['kk.cost.sheet'].search([('creator_team_id','in',user_team_product)]):
                                arrCost.append(cos.id)
                            filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
                else:
                    print ('1 tipe sales')
                    team_id = self.qwery_team_director(login_uid) 
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('creator_team_id','in',user_team_product)]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
                        
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('creator_team_id','in',user_team_product)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
                        
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('creator_team_id','in',user_team_product)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
                        
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
        else:
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids),('type','=','out_invoice')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids),('type','=','out_invoice')]       
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','out_invoice')]
            
            
        #Special for Admin
        if login_uid==1:
            filter_domain = [('type','=','out_invoice')]
        context_action={'type':'out_invoice', 'journal_type': 'sale'}
        
        action = {
            #'id'        : action_view_id,
            'name'      : _('Customer Invoices'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'account.invoice',
            #'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            'context'   : context_action,
        }
        return action
    

    @api.multi
    def vendor_bills_filter_dynamic(self):
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
        
        action_window_id = data_obj.get_object_reference('account', 'action_invoice_tree2')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])
        
        cost_sheet = self
        
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        product_allowed_access          = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_user_product')
        group_invoice_admin             = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_admin')
        group_invoice_fin_mgr           = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_fin_mgr')
        group_invoice_accounting        = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_accounting')    
        group_invoice_tax               = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_tax')   

        team_ids = []
        filter_domain = []
        if product_allowed_access or group_invoice_admin or group_invoice_fin_mgr or group_invoice_accounting or group_invoice_tax:
            filter_domain = [('type','=','in_invoice')]
        elif drc_sales_allowed_access:
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            arrCost = []
                            for cos in self.env['kk.cost.sheet'].search([('creator_team_id','in',user_team_product)]):
                                arrCost.append(cos.id)
                            filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
                else:
                    print ('1 tipe sales')
                    team_id = self.qwery_team_director(login_uid) 
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]       
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
        else:
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids),('type','=','in_invoice')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids),('type','=','in_invoice')]      
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost),('type','=','in_invoice')]
            
            
        #Special for Admin
        if login_uid==1:
            filter_domain = [('type','=','in_invoice')]
        context_action={'default_type': 'in_invoice', 'type': 'in_invoice', 'journal_type': 'purchase'}
        
        action = {
            #'id'        : action_view_id,
            'name'      : _('Vendor Bills'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'account.invoice',
            #'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            'context'   : context_action,
        }
        return action

    @api.onchange('invoice_line_ids')
    def _onchange_origin(self):
        purchase_ids = self.invoice_line_ids.mapped('purchase_id')
        if purchase_ids:
            self.origin = ', '.join(purchase_ids.mapped('name'))
            self.reference = ', '.join(purchase_ids.filtered('partner_ref').mapped('partner_ref')) or self.reference
            self.reference = False
    
    
    def _get_date_paid_invoice(self):
        for line in self:
            date_paid_invoice = ''
            for lp in line.payment_ids:
                date_paid_invoice = date_paid_invoice + str(lp.payment_date_temp) + '\n'
            
            line.update({
                        'date_paid_invoice' : date_paid_invoice,
                        })
    
    @api.multi
    @api.depends('payment_ids')
    def _get_payment_date(self):
        for inv in self:
            payment_date = False
            print ("inv--->", inv)
            for payment in inv.payment_ids and inv.payment_ids[0] or []:
                print ("payment--->", payment)
                payment_date = payment.payment_date
            inv.payment_date = payment_date

    kk_cost_sheet_id = fields.Many2one(
        'kk.cost.sheet', 'Cost Sheet', copy=False)
    project_name = fields.Char(related='kk_cost_sheet_id.project_name',string="Project Name", readonly=True)
    company_partner_id = fields.Many2one(
        'res.partner', 'Company Partner',
        related='company_id.partner_id', copy=False)
    invoice_bank_ids = fields.Many2many(
        'res.partner.bank', 'invoice_bank_rel',
        'invoice_id', 'bank_id',
        'Bank Transfer', copy=False,
        # domain="[('partner_id','=',self.env.user.company_id.partner_id)]"
    )
    nomor_faktur_pajak = fields.Char('Faktur Pajak')
    is_sales_appr = fields.Boolean(compute='_methods_sales_appr')
    readonly_inv_line_flag = fields.Boolean(compute='_methods_invoice_line_flag')
    invoice_line_ids = fields.One2many('account.invoice.line', 'invoice_id', string='Invoice Lines', oldname='invoice_line',readonly=False, states={}, copy=True)
    tax_line_ids = fields.One2many('account.invoice.tax', 'invoice_id', string='Tax Lines', oldname='tax_line',readonly=False, copy=True)
    invoice_vendor_number = fields.Char('Invoice Vendor No.')
    payment_date    = fields.Date(string='Payment Date', compute='_get_payment_date', store=True)
    reference = fields.Char(string='Vendor Quotation No.', copy=False,
        help="The partner reference of this invoice.", readonly=True, states={'draft': [('readonly', False)]})
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('proforma', 'Pro-forma'),
            ('proforma2', 'Pro-forma'),
            ('appr_slsmgr', 'Approval Sales Manager'),
            ('appr_tax', 'Tax Validate'),
            ('appr_sls', 'Approval Sales'),
            ('appr_mgr', 'Director Sales'),
            ('appr_accounting', 'Accounting Mgr'),
            ('open', 'Open'),
            ('payment', 'Payment'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], string='Status', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False,
        help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
             " * The 'Pro-forma' status is used when the invoice does not have an invoice number.\n"
             " * The 'Open' status is used when user creates invoice, an invoice number is generated. It stays in the open status till the user pays the invoice.\n"
             " * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
             " * The 'Cancelled' status is used when user cancel invoice.")
    
    date_paid_invoice   = fields.Text(string='Paid Date',compute='_get_date_paid_invoice')
    purchase_order_id   = fields.Many2one('purchase.order',compute="_compute_number_po")
    name = fields.Char(string='Reference/Description', index=True,
        readonly=False, states={'draft': [('readonly', False)]}, copy=False, help='The name that will be used on account move lines')
    receive_date        =fields.Date('Receive Date')


    @api.multi
    @api.depends('purchase_order_id','origin')
    def _compute_number_po(self):
        for item in self:
            if item.type =='in_invoice':
                obj_po = self.env['purchase.order'].search([('name','=',item.origin)])
                if obj_po:
                    item.purchase_order_id = obj_po.id
    
    
#     @api.model
#     def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
#         res = super(AccountInvoice, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
#         invoice_id = self._context.get('active_id')
#         active_model = self._context.get('active_model')
#
#         print ("invoice_id", self._context.get('active_id'), "active_model-->", active_model)
#         if not invoice_id:
#             return res
#         invoice = self.env['account.invoice'].browse([invoice_id], limit=1)
#
#         if self._context.get('type'):
#             print ("fields_view_get___1")
#             doc = etree.XML(res['arch'])
#             for node in doc.xpath("//field[@name='invoice_line_ids']"):
#                 print ("fields_view_get___2")
#                 print ("##self.state-->>", self)
#                 if invoice.state=='appr_tax':
#                     print ("fields_view_get___3-1")
#                     node.set('readonly', "False")
#                 else:
#                     node.set('attrs', "{'readonly': [('state', '!=', 'draft')]}")
#                     print ("fields_view_get___3-2")
#                 setup_modifiers(node, res['fields']['invoice_line_ids'])
#             res['arch'] = etree.tostring(doc, encoding='unicode')
#         return res

    @api.model
    def invoice_line_move_line_get(self):
        res = []
        for line in self.invoice_line_ids:
            if line.quantity==0:
                continue
            tax_ids = []
            for tax in line.invoice_line_tax_ids:
                tax_ids.append((4, tax.id, None))
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        tax_ids.append((4, child.id, None))
            analytic_tag_ids = [(4, analytic_tag.id, None) for analytic_tag in line.analytic_tag_ids]

            move_line_dict = {
                'invl_id': line.id,
                'type': 'src',
                'name': line.name.split('\n')[0][:64],
                'price_unit': line.price_unit,
                'quantity': line.quantity,
                'price': line.price_subtotal,
                'account_id': line.account_id.id,
                'product_id': line.product_id.id,
                'uom_id': line.uom_id.id,
                'account_analytic_id': line.account_analytic_id.id,
                'tax_ids': tax_ids,
                'invoice_id': self.id,
                'analytic_tag_ids': analytic_tag_ids,
                'kk_cost_sheet_id' : line.kk_cost_sheet_id.id,
                'kk_cost_expense_id' : line.kk_cost_expense_id.id
            }
            res.append(move_line_dict)
        return res

    @api.multi
    @api.depends('name')
    def _methods_sales_appr(self):
        for x in self:
            login_uid = self.env.context.get('uid', False)
            sales_uid = x.kk_cost_sheet_id.kk_sale_order_id.user_id.id
            if login_uid == sales_uid:
                x.is_sales_appr = True

    @api.multi
    def _methods_invoice_line_flag(self):
        login_uid = self.env.context.get('uid', False)
        tax_allowed_access          = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_tax')
        accounting_allowed_access   = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_accounting')

        flag = True
        for o in self:
            if o.state == 'appr_tax' and tax_allowed_access:
                flag = False
            elif o.state == 'appr_accounting' and accounting_allowed_access:
                flag = False
            elif o.state == 'draft':
                flag = False
            o.readonly_inv_line_flag = flag

    @api.multi
    def action_confirm(self):
        for item in self:
            qty_line = [line.quantity for line in item.invoice_line_ids]
            for qty in qty_line:           
                if qty < 1:
                    raise UserError(_("Quantity lines cannot be 0"))
            if item.type == 'in_invoice':
                sum_po_ai = 0.0
                for ai in self.env['account.invoice'].search([('origin','=',item.origin)]):
                    sum_po_ai += ai.amount_total
                    print ('sum_invoice',sum_po_ai)
                obj_po = self.env['purchase.order'].search([('name','=',item.origin)])
                sum_invoice = '{:20,.2f}'.format(sum_po_ai)
                sum_po = '{:20,.2f}'.format(obj_po.amount_total)
                if obj_po and (sum_invoice > sum_po):
                    raise UserError(_("Amount Invoice must not be greater than amount Purchase Order"))
                else:
                    if item.so_id:
                        item.write({'state': 'appr_tax'})
                        item.action_number_squence_bills()
                    else:
                        item.write({'state': 'appr_tax'})
                        item.action_number_squence_bills()
                        
            elif item.type == 'out_invoice':
                return item.write({'state': 'appr_tax'})

    @api.multi
    def action_confirm_sales(self):
        return self.write({'state': 'appr_mgr'})

    @api.multi
    def action_number_squence_bills(self):
        if not self.move_name:
            sequence = self.env['ir.sequence'].next_by_code('seq.vendor.bills') or '/'
            return self.write({'move_name': str(sequence)})
            

#     @api.multi
#     def action_appr_sls_mgr(self):
#         return self.write({'state': 'appr_tax'})
# appr_accounting


    @api.multi
    def action_tax(self):
        if self.type == 'in_invoice':
            if self.so_id:
                return self.write({'state': 'appr_sls'})
            else:
                return self.write({'state': 'appr_accounting'})
        else:
            self.action_invoice_open()



    @api.multi
    def action_accounting(self):
        return self.write({'state': 'appr_accounting'})

    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if self.purchase_id:
            self.so_id = self.purchase_id.kk_cost_sheet_id.kk_sale_order_id.id
            self.customer_id = self.purchase_id.kk_cost_sheet_id.kk_sale_order_id.partner_id.id
            self.kk_cost_sheet_id = self.purchase_id.kk_cost_sheet_id.id
        res = super(AccountInvoice, self).purchase_order_change()
        return res
    
    # @api.multi
    # def action_invoice_open_tax(self):
    #     # lots of duplicate calls to action_invoice_open, so we remove those already open
    #     to_open_invoices = self.filtered(lambda inv: inv.state == 'draft')
    #     if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2','appr_tax', 'appr_accounting', 'draft']):
    #         raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))
    #     to_open_invoices.action_date_assign()
    #     to_open_invoices.action_move_create()
    #     return to_open_invoices.invoice_validate_tax()
    
    # @api.multi
    # def invoice_validate_tax(self):
    #     for invoice in self.filtered(lambda invoice: invoice.partner_id not in invoice.message_partner_ids):
    #         invoice.message_subscribe([invoice.partner_id.id])
    #     self._check_duplicate_supplier_reference()
    #     return self.write({'state': 'appr_tax'})

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            if not inv.date_due:
                inv.with_context(ctx).write({'date_due': inv.date_invoice})
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)

            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total, inv.date_invoice)[0]
                res_amount_currency = total_currency
                ctx['date'] = inv._get_currency_rate_date()
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)

            date = inv.date or inv.date_invoice
            move_vals = {
                'ref': inv.name,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True


    @api.multi
    def action_invoice_open(self):
        # return self.write({'state': 'open'})
        # lots of duplicate calls to action_invoice_open, so we remove those already open
        to_open_invoices = self.filtered(lambda inv: inv.state != 'open')
        if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2','appr_tax', 'appr_accounting', 'draft']):
            raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))
        to_open_invoices.action_date_assign()
        to_open_invoices.action_move_create()
        return to_open_invoices.invoice_validate()

    @api.multi
    def invoice_validate(self):
        for invoice in self.filtered(lambda invoice: invoice.partner_id not in invoice.message_partner_ids):
            invoice.message_subscribe([invoice.partner_id.id])
        self._check_duplicate_supplier_reference()
        return self.write({'state': 'open'})

    @api.multi
    def action_invoice_cancel(self):
        #if self.filtered(lambda inv: inv.state not in ['proforma2', 'appr_mgr', 'appr_sls', 'appr_slsmgr', 'draft', 'open']):
        if self.filtered(lambda inv: inv.state in ['cancel']):
            raise UserError(_("Invoice must be in draft, Pro-forma, Approval Manager or open state in order to be cancelled."))
        return self.action_cancel()

    @api.onchange('invoice_bank_ids')
    def _onchange_invoice_bank_ids(self):
        if self.company_partner_id.comment:
            comment = "%s \n" % (
                self.company_partner_id.comment,
            )
        else:
            comment = "%s \n" % (
                "Please transfer to PT. Infracom Technology (Full Amount)",
            )
        number = 0
        for bank in self.invoice_bank_ids:
            number += 1
            comment += "%s) %s \n    Bank : %s \n    Address : %s %s %s \n    Account no : %s (%s)\n    Swift Code : %s\n" % (
                number,
                self.company_partner_id.name,
                bank.bank_id.name,
                bank.bank_id.street or '',
                bank.bank_id.street2 or '',
                bank.bank_id.city or '',
                bank.acc_number,
                bank.currency_id.name,
                bank.bank_id.bic,
            )
        self.comment = comment

class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet')
    kk_cost_expense_id = fields.Many2one('kk.cost.expense.line', 'Cost Expense')

    @api.onchange('kk_cost_sheet_id')
    def onchange_cost_sheet(self):
        self.kk_cost_expense_id = False

class FalInvoiceTermLine(models.Model):
    _inherit = 'fal.invoice.term.line'
    _order  = 'name'
    
    date_paid_invoice   = fields.Text(string='Paid Date',related='invoice_id.date_paid_invoice')

    @api.multi
    def action_invoice_create(self):
        res = super(FalInvoiceTermLine, self).action_invoice_create()
        if not self.fal_sale_order_id.kk_cost_sheet_id:
            raise UserError(_('Please relate Sales with Cost Sheet.'))
        res.kk_cost_sheet_id = self.fal_sale_order_id.kk_cost_sheet_id.id
        return res


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet')
    kk_cost_expense_id = fields.Many2one(
        'kk.cost.expense.line', 'Cost Expense')




class AccountMove(models.Model):
    _inherit = 'account.move'

    no_po = fields.Char('PO Number')
    inv_vendor_no = fields.Char('Invoice Vendor No')
    no_faktur_pajak = fields.Char('No Faktur Pajak')

    def qwery_po_number_vb(self,move_id):
        self.env.cr.execute("""  
                        select 
                            ai.origin,
                            ai.invoice_vendor_number,
                            ai.nomor_faktur_pajak,
                            am.id move_id
                        from account_invoice ai
                            left join (select id,no_po from account_move) am
                            on ai.move_id = am.id
                        where ai.move_id ='%s'    
                        """ % (str(move_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_ap_number(self,ap_id):
        self.env.cr.execute("""  
                        select 
                            ap.name,
                            apl.move_line_id,
                            ai.origin,
                                                ai.invoice_vendor_number,
                                                ai.nomor_faktur_pajak,
                            am.name
                        from account_payment ap
                        left join(select id,payment_id,move_line_id from account_payment_line)apl
                        on apl.payment_id = ap.id
                        left join(select id,name,move_id from account_move_line)aml
                        on apl.move_line_id = aml.id
                        left join (select id,name from account_move) am
                        on aml.move_id = am.id
                        left join (select id,origin,
                                                invoice_vendor_number,
                                                nomor_faktur_pajak,move_id from account_invoice) ai	
                            on ai.move_id = am.id
                            where ap.payment_type ='outbound' and ap.name ='%s'
                        """ % (str(ap_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def button_po_number_vb(self):
        for item in self:
            for line in self.qwery_po_number_vb(item.id):
                item.write({
                    'no_po':line.get('origin'),
                    'inv_vendor_no':line.get('invoice_vendor_number'),
                    'no_faktur_pajak':line.get('nomor_faktur_pajak')
                    })
            no_po =[]
            invoice_vendor_number =[]
            no_faktur_pajak =[]
            for ap in self.qwery_ap_number(item.name):
                no_po.append(ap.get('origin'))
                invoice_vendor_number.append(ap.get('invoice_vendor_number'))
                no_faktur_pajak.append(ap.get('no_faktur_pajak'))
                item.write({
                    'no_po':no_po,
                    'inv_vendor_no':invoice_vendor_number,
                    'no_faktur_pajak':no_faktur_pajak
                    })

    @api.multi
    def assert_balanced(self):
        if not self.ids:
            return True
        prec = self.env['decimal.precision'].precision_get('Account')

        self._cr.execute("""\
            SELECT      move_id
            FROM        account_move_line
            WHERE       move_id in %s
            GROUP BY    move_id
            HAVING      abs(sum(debit) - sum(credit)) > %s
            """, (tuple(self.ids), 10 ** (-max(5, prec))))
        if len(self._cr.fetchall()) != 0:
            raise UserError(_("Cannot create unbalanced journal entry."))
        return True


class account_payment(models.Model):
    _inherit = "account.payment"

    cost_sheet_id = fields.Many2one(
        'kk.cost.sheet', string='Cost Sheet', track_visibility='always')

class ResPartnerBank(models.Model):
    _inherit = "res.partner.bank"

    @api.multi
    @api.depends('acc_number', 'bank_id')
    def name_get(self):
        result = []
        for account in self:
            name = account.acc_number + ' (' + account.bank_id.name + ')'
            result.append((account.id, name))
        return result

class inherit_account_journal(models.Model):
    _inherit = "account.journal"

    code = fields.Char(string='Short Code', size=10, required=True, help="The journal entries of this journal will be named using this prefix.")
