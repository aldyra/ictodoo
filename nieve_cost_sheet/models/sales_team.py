from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError

class CrmTeam(models.Model):
    _inherit = 'crm.team'
    
    user_id     = fields.Many2one('res.users', string='Manager Team')
    gm_user_id  = fields.Many2one('res.users', string='GM Team')
    dir_user_id = fields.Many2one('res.users', string='Director Team')
    type_sales  = fields.Selection([('sales_product','Sales Product'),('sales_maintenance','Sales Maintenance')],default="sales_product",string="Type Sales")