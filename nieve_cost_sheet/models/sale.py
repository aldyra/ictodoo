from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_type_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            type_sales
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'
                        GROUP BY type_sales
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_sale(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where ru.id ='%s'
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    @api.depends('order_line.price_total','fal_invoice_milestone_line_date_ids.total_inv')
    def _amount_all(self):
        """
        Compute the total amounts of the SO.
        """
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                amount_tax += line.price_tax
            rounding_discount = sum(i.discount for i in order.fal_invoice_milestone_line_date_ids)
            amount          = order.pricelist_id.currency_id.round(amount_untaxed)
            amount_untaxed  = amount - rounding_discount
            amount_tax      = 0.1 * amount_untaxed#0.1 * (order.pricelist_id.currency_id.round(amount_untaxed))
            order.update({
                'amount'            : amount,
                'rounding_discount' : rounding_discount,
                'amount_untaxed'    : amount_untaxed,
                
                'amount_tax'        : amount_tax,#order.pricelist_id.currency_id.round(amount_tax),
                'amount_total'      : amount_untaxed + amount_tax,
            })
    
    amount = fields.Monetary(string='Amount', store=True, readonly=True, compute='_amount_all')
    rounding_discount = fields.Monetary(string='Rounding Discount', store=True, readonly=True, compute='_amount_all')
    creator_user_id = fields.Many2one('res.users', string='SO Created by', track_visibility='onchange', readonly=True, default=lambda self: self.env.user.id)
    kk_cost_sheet_id = fields.Many2one(
        'kk.cost.sheet', 'Cost Sheet', copy=False)
    type_sales = fields.Selection([('sales_product','Sales Product'),('sales_maintenance','Sales Maintenance')],compute="_compute_type_sales_team",store=True)

    _sql_constraints = [
        ('cost_sheet_unique', 'unique(kk_cost_sheet_id)', 'Reason Cost Sheet must be unique!')
    ]

    @api.multi
    @api.depends('team_id','type_sales')
    def _compute_type_sales_team(self):
        for item in self:
            if item.team_id:
                item.type_sales = item.team_id.type_sales
    
    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        if not self.kk_cost_sheet_id:
            raise UserError(_('Please relate Sales with Cost Sheet.'))
        return res
    
        
    @api.multi
    def sale_filter_dynamic(self, type):
        print ("sale_filter_dynamic--->")
        print ("type-->", type)
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
        search_view_id = data_obj.get_object_reference('sale', 'sale_order_view_search_inherit_sale')[1]
#         tree_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_tree_es')[1]
#         form_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_form_es')[1]
        action_window_id = data_obj.get_object_reference('sale', 'action_orders')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])
         
        cost_sheet = self
         
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        group_invoice_admin             = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_admin')
        group_invoice_fin_mgr           = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_fin_mgr')
        group_invoice_accounting        = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_accounting')    
        group_invoice_tax               = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_tax')    

        team_ids = []
        filter_domain = []
        if group_invoice_accounting or group_invoice_fin_mgr or group_invoice_tax or group_invoice_admin:
            filter_domain = []
        elif drc_sales_allowed_access:   
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team=[]
                        for item in team_id:
                            user_team.append(item.get('id_team'))
                            filter_domain = [('team_id','in',user_team)]
                else:
                    print ('1 tipe sales') 
                    team_id = self.qwery_team_director(login_uid)   
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                print ('masuk sales product')
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                            if item.get('type_sales')=='sales_maintenance':
                                print ('masuk sales maintenance')
                                user_team.append(item.get('id_res_user'))
                                filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        elif gmgr_sales_allowed_access:     
            team_id = self.qwery_team_gm(login_uid)       
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid)
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        else:
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]

            
        if login_uid==1:
            filter_domain = []
        
         
        action = {
            'name'      : _('Sales Orders'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,kanban,form,calendar,pivot,graph',
            'res_model' : 'sale.order',
            'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            'context'   : {},#context_action,
        }
        return action
    
    
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    kk_order_line_ids = fields.One2many(
        'kk.order.line', 'so_line_id', 'Order Detail', copy=True)
    kk_cost_sheet_line_id = fields.Many2one(
        'kk.cost.sheet.line', 'Cost Sheet Line')
    kk_cogs = fields.Float(
        string='COGS')


class OrderLineIds(models.Model):
    _name = 'kk.order.line'

    so_line_id = fields.Many2one('sale.order.line', 'SO Line')
    pr_line_id = fields.Many2one('purchase.requisition.line', 'PR Line')
    po_line_id = fields.Many2one('purchase.order.line', 'PO Line')
    move_line_id = fields.Many2one('stock.move', 'Stock Move Line')
    cs_line_id = fields.Many2one('kk.cost.sheet.line', 'CS Line')
    product_code = fields.Char('Product Code')
    name = fields.Char('Description')
    serial_number = fields.Text('S/N')
    product_uom = fields.Many2one(
        'product.uom', string='Unit of Measure')
    product_uom_qty = fields.Float(
        string='Quantity', digits=dp.get_precision('Product Unit of Measure'),
        default=1.0)


class FalInvoiceTermLine(models.Model):
    _inherit = 'fal.invoice.term.line'
    _order = 'cost_sheet_id'

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_type_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            type_sales
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'
                        GROUP BY type_sales
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_sale(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where ru.id ='%s'
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    @api.multi
    def invoice_milestone_filter_dynamic(self, type):
        context = self._context
        data_obj = self.env['ir.model.data']
        cs_obj = self.env['kk.cost.sheet']
        search_view_id = data_obj.get_object_reference('nieve_invoice_milestone', 'fal_invoice_term_line_search')[1]
         
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        product_allowed_access          = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_user_product')
        group_invoice_admin             = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_admin')
        group_invoice_fin_mgr           = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_fin_mgr')
        group_invoice_accounting        = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_accounting')    
        group_invoice_tax               = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_tax')    

        
        team_ids = []
        filter_domain = []
        if product_allowed_access or group_invoice_admin or group_invoice_fin_mgr or group_invoice_accounting or group_invoice_tax:
            filter_domain = []
        elif drc_sales_allowed_access:   
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            arrCos = []
                            for cos in self.env['kk.cost.sheet'].search([('creator_team_id','in',user_team_product)]):
                                arrCos.append(cos.id)
                            filter_domain = [('cost_sheet_id','in',arrCos)]
                else:
                    print ('1 tipe sales') 
                    team_id = self.qwery_team_director(login_uid) 
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                arrCos = []
                                for cos in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                                    arrCos.append(cos.id)
                                filter_domain = [('cost_sheet_id','in',arrCos)]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                arrCos = []
                                for cos in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                                    arrCos.append(cos.id)
                                filter_domain = [('cost_sheet_id','in',arrCos)]
            else:
                arrCos = []
                for cos in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCos.append(cos.id)
                filter_domain = [('cost_sheet_id','in',arrCos)]  
        elif gmgr_sales_allowed_access: 
            team_id = self.qwery_team_gm(login_uid)           
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCos = []
                        for cos in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCos.append(cos.id)
                        filter_domain = [('cost_sheet_id','in',arrCos)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCos = []
                        for cos in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCos.append(cos.id)
                        filter_domain = [('cost_sheet_id','in',arrCos)]
            else:
                arrCos = []
                for cos in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCos.append(cos.id)
                filter_domain = [('cost_sheet_id','in',arrCos)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid)
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCos = []
                        for cos in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCos.append(cos.id)
                        filter_domain = [('cost_sheet_id','in',arrCos)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCos = []
                        for cos in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCos.append(cos.id)
                        filter_domain = [('cost_sheet_id','in',arrCos)]
            else:
                arrCos = []
                for cos in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCos.append(cos.id)
                filter_domain = [('cost_sheet_id','in',arrCos)]
        else:
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('cost_sheet_id','in',obj_kk_cost.ids)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('cost_sheet_id','in',obj_kk_cost.ids)]
            else:
                arrCos = []
                for cos in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCos.append(cos.id)
                filter_domain = [('cost_sheet_id','in',arrCos)]
           

        
        if login_uid==1:
            filter_domain = []
        
        action = {
            'name'      : _('Invoice Milestone'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'fal.invoice.term.line',
            'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            'context'   : {"search_default_groupby_saleorder" : True, "search_default_groupby_costsheet" : True}
        }
        return action


    @api.multi
    def _get_csl_domain(self):
        csl_ids = []
        cost_sheet = self.env.context.get('params', {}).get('id', {})
        cs_obj = self.env['kk.cost.sheet'].browse(cost_sheet)
        for line in cs_obj.kk_cost_sheet_line_ids:
            csl_ids.append(line.id)
        return [('id', 'in', csl_ids)]

    @api.multi
    def _get_sol_domain(self):
        sol_ids = []
        so = self.env.context.get('params', {}).get('id', {})
        so_obj = self.env['sale.order'].browse(so)
        for line in so_obj.order_line:
            sol_ids.append(line.id)
        return [('id', 'in', sol_ids)]

    cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet',related='fal_sale_order_id.kk_cost_sheet_id',copy=True, store=True)
    cs_cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet',copy=True, store=True)
    state_cs = fields.Selection("CS State",related='cs_cost_sheet_id.state',store=True, default="draft")
    cost_sheet_line_m2m = fields.Many2many('kk.cost.sheet.line', 'cost_sheet_line_m2m_rel','term_id', 'cs_line_id', 'Cost Sheet Line', domain=_get_csl_domain)
    item_invoice_term_line = fields.One2many('item.invoice.term.line', 'fal_invoice_term_line_id', string='Item Invoice Terms Line')
    
    so_line_m2m = fields.Many2many('sale.order.line', 'so_line_m2m_rel','term_id', 'so_line_id', 'Sale Order Line',)

    @api.onchange('cs_cost_sheet_id')
    def onchange_cs_cost_sheet_id(self):
        self.csrel_cost_sheet_id = self.cs_cost_sheet_id

    @api.onchange('name')
    def onchange_name(self):
        self._get_sol_domain()

    @api.onchange('percentage')
    def onchange_percentage(self):
        if not self.cost_sheet_line_m2m:
            total_sale = 0.0
            for so in self.so_line_m2m:
                total_sale += so.price_subtotal
            total_inv = (self.percentage / 100) * total_sale
            self.total_inv = total_inv
        else:
            total_sale = 0.0
            for cost in self.cost_sheet_line_m2m:
                total_sale += cost.price_total
            total_inv = (self.percentage / 100) * total_sale
            self.total_inv = total_inv

    @api.onchange('total_inv')
    def onchange_total_inv(self):
        if not self.cost_sheet_line_m2m:
            total_sale = 0.0
            for so in self.so_line_m2m:
                total_sale += so.price_subtotal
            if total_sale != 0.0:
                percentage = (self.total_inv / total_sale) * 100
                self.percentage = percentage
        else:
            total_sale = 0.0
            for cost in self.cost_sheet_line_m2m:
                total_sale += cost.price_total
            if total_sale:
                percentage = (self.total_inv / total_sale) * 100
                self.percentage = percentage

    @api.onchange('cost_sheet_line_m2m')
    def onchange_cost_sheet_line_m2m(self):
        self.onchange_percentage()

    @api.multi
    def _prepare_top_line_sale_order(self, kk_cost_sheet_id):
        self.ensure_one()
        so_line_list = []
        for cs in self.cost_sheet_line_m2m:
            so_line_list.append((4, cs.so_line_id.id))

        vals = {
            'name': self.name,
            'so_line_m2m': so_line_list,
            'date': self.date,
            'percentage': self.percentage,
            'total_inv': self.total_inv}
        return vals

    @api.multi
    def _prepare_top_cof_value(self):
        self.ensure_one()
        vals = {
            'name': self.name,
            'cof_in_date': self.date,
            'sales_value': self.total_inv}
        return vals
    
class ItemInvoiceTermLine(models.Model):
    _name = 'item.invoice.term.line'
    
    cost_sheet_line_id  = fields.Many2one('kk.cost.sheet.line', string='Cost Sheet Line')
    sale_line_id          = fields.Many2one('sale.order.line', string='Sale Order Line')
    product_id          = fields.Many2one('product.product', string='Product')
    name                = fields.Char(string='Description')
    amount              = fields.Float(string='Amount')
    fal_invoice_term_line_id= fields.Many2one('fal.invoice.term.line', string='Invoice term Line', ondelete='cascade')
