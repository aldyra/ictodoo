from odoo import api, fields, models, _
import odoo.addons.decimal_precision as dp
from datetime import datetime
from openerp.exceptions import UserError
import datetime as dt


def months_between(date1, date2):
    if date1 > date2:
        date1, date2 = date2, date1
    m1 = date1.year * 12 + date1.month
    m2 = date2.year * 12 + date2.month
    months = m2 - m1
    return months


class CostSheet(models.Model):
    _name = 'kk.cost.sheet'
    _description = 'Cost Sheet'
    _inherit = ['mail.thread']
    _order = 'name'
    
#     @api.multi
#     @api.depends('fal_invoice_milestone_line_date_ids', 
#                  'kk_sale_order_id.fal_invoice_milestone_line_date_ids')
#     def _dummy_update(self):
#         print ("###_dummy_update### A", self)
#         
#         self.create_cof()
#         print ("###_dummy_update### B", self)
#         self.dummy_update = True

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_group_type_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            type_sales
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'
                        GROUP BY type_sales
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_group_sale(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where ru.id ='%s'
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

         
        

        

    @api.multi
    def cost_sheet_filter_dynamic(self):
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
#         search_view_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'hr_duty_trip_view_search_es')[1]
        
#         tree_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_tree_es')[1]
#         form_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_form_es')[1]
        
        action_window_id = data_obj.get_object_reference('nieve_cost_sheet', 'kk_cost_sheet_action')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])
        
        cost_sheet = self
        
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        product_allowed_access          = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_user_product')
        admin_allowed_access            = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_admin_all')
        group_invoice_admin             = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_admin')
        group_invoice_fin_mgr           = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_fin_mgr')
        group_invoice_accounting        = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_accounting')    
        group_invoice_tax               = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_tax')    

        
        team_ids = []
        filter_domain = []
        if admin_allowed_access or product_allowed_access or group_invoice_admin or group_invoice_fin_mgr or group_invoice_accounting or group_invoice_tax:
            filter_domain = []
        elif drc_sales_allowed_access:
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            filter_domain = [('creator_team_id','in',user_team_product)]
                else:
                    print ('1 tipe sales')
                    team_id = self.qwery_team_director(login_uid)
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid)
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid)
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        else:
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('id','in',obj_kk_cost.ids)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('id','in',obj_kk_cost.ids)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
            
        if type=='sale':
            AND = ['&']
            AND.extend(filter_domain)
            AND.append(('state', 'not in', ('draft', 'sent', 'cancel')))
            filter_domain = AND
            
        #Special for Admin
        if login_uid==1:
            filter_domain = []
        
        action = {
            #'id'        : action_view_id,
            'name'      : _('Cost Sheet'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'kk.cost.sheet',
            #'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            #'context'   : context_action,
        }
        return action
    
    @api.multi
    def cost_sheet_filter_view_only_dynamic(self):
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']
#         search_view_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'hr_duty_trip_view_search_es')[1]
        
#         tree_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_tree_es')[1]
#         form_id = data_obj.get_object_reference(cr, uid, 'nieve_employee_service', 'duty_view_form_es')[1]
        
        action_window_id = data_obj.get_object_reference('nieve_cost_sheet', 'kk_cost_sheet_action')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])
        
        
        
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        product_allowed_access          = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_user_product')
        admin_allowed_access            = self.env['res.users'].has_group('nieve_cost_sheet.group_costsheet_admin_all')
        group_invoice_admin             = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_admin')
        group_invoice_fin_mgr           = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_fin_mgr')
        group_invoice_accounting        = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_accounting')    
        group_invoice_tax               = self.env['res.users'].has_group('nieve_cost_sheet.group_invoice_tax') 
        
        team_ids = []
        filter_domain = []
        if admin_allowed_access or product_allowed_access or group_invoice_admin or group_invoice_fin_mgr or group_invoice_accounting or group_invoice_tax:
            filter_domain = []
        elif drc_sales_allowed_access:
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            filter_domain = [('creator_team_id','in',user_team_product)]
                else:
                    print ('1 tipe sales')
                    team_id = self.qwery_team_director(login_uid)
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid)
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid)
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]
        else:
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        filter_domain = [('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        filter_domain = [('creator_user_id','in',user_team)]
            else:
                filter_domain = [('creator_user_id','=',login_uid)]

        if filter_domain:
            AND = ['&']
            AND.extend(filter_domain)
            AND.append(('state', '!=', 'cancel'))
            filter_domain = AND
        else:
            filter_domain.append(('state', '!=', 'cancel'))
        
        print ("filter_domain---.",filter_domain)
        
        #Special for Admin
        if login_uid==1:
            filter_domain = []
        
#         if type=='sale':
#             filter_domain.append(('state', 'not in', ('draft', 'sent', 'cancel')))
        
        tree_id = self.env.ref('nieve_cost_sheet.view_only_kk_cost_sheet_tree_kk', False)
        form_id = self.env.ref('nieve_cost_sheet.view_only_kk_cost_sheet_form', False)
        
        action = {
            #'id'        : action_view_id,
            'name'      : _('Cost Sheet'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'kk.cost.sheet',
            #'search_view_id': search_view_id,
            'target'    : 'current',
            'views'     :  [(tree_id.id,'tree'),(form_id.id,'form')],
            #'view_id'   : compose_form.id,
            'domain'    : filter_domain,
            #'context'   : context_action,
        }
        return action



    @api.depends(
        'kk_cost_sheet_line_ids.price_total',
        'kk_cost_exp_line_ids',
        'kk_cost_exp_line_ids.expense_plan',
        'kk_cost_exp_line_ids.expense_actual',
        'kk_cost_fund_manual_line_ids.cof_plan',
        'kk_cost_fund_line_ids',
        'fal_invoice_milestone_line_date_ids.total_inv',
        'kk_sale_order_id','cof_cost_out','state')
    def _amount_all(self):
        """
        Compute the total amounts of the Cost Sheet.
        """
        # for order in self:
        #     amount_all = 0.0
        #     amount_exp = 0.0
        #     amount_sales = 0.0
        #     for line in order.kk_cost_sheet_line_ids:
        #         amount_all += line.cost_price_total
        #         amount_sales += line.price_total
        #     for expline in order.kk_cost_exp_line_ids:
        #         amount_exp += expline.expense_plan
        #     expense = amount_all + amount_exp + order.cof_total
        #     order.update({
        #         'amount_total': amount_all,
        #         'amount_expense_plan': amount_exp,
        #         'amount_sales': amount_sales,
        #         'cof_cost_out': amount_sales,
        #         'amount_gross_profit': amount_sales - expense,
        #         'percent_gross_profit': expense != 0.0 and ((amount_sales - expense) / expense) * 100 or 0.0
        #     })
        for order in self:
            amount_all = 0.0
            amount_exp = 0.0
            amount_cof = 0.0
            amount_sales = 0.0
            amount_gp = 0.0
            for line in order.kk_cost_sheet_line_ids:
                amount_all += line.cost_price_total
                amount_sales += line.price_total
            for expline in order.kk_cost_exp_line_ids:
                if order.state == 'close':
                    amount_exp += expline.expense_actual
                else:
                    amount_exp += expline.expense_plan
            for cofline in order.kk_cost_fund_line_ids:
                if order.state == 'close':
                    amount_cof += cofline.cof_value_actual
                else:
                    amount_cof += cofline.cof_value
                
            ##COF Manual
            #for cofline in order.kk_cost_fund_manual_line_ids:
            #    amount_cof += cofline.cof_plan
                
            rounding_discount = sum(i.discount for i in order.fal_invoice_milestone_line_date_ids)
            
            expense = amount_all + amount_exp + amount_cof
            amount_gp = (amount_sales-rounding_discount) - expense
            order.update({
                'amount': amount_sales,
                'rounding_discount': rounding_discount,
                'amount_sales': amount_sales - rounding_discount,
                
                'amount_total': amount_all,
                'amount_expense_plan': amount_exp,
                'cof_total': amount_cof,
                
                #'cof_cost_out': amount_sales,
                'amount_gross_profit': amount_gp,
                'percent_gross_profit': expense != 0.0 and (amount_gp / amount_sales) * 100 or 0.0,
            })
    
    @api.onchange('kk_cost_sheet_line_ids')
    def onchange_line_ids(self):
        self.cof_cost_out = self.amount_total
    
    @api.onchange('cof_cost_out')
    def onchange_cof_cost_out(self):
        self.kk_cost_fund_line_ids = False
        
    @api.model
    def _default_note(self):
        return self.env.user.company_id.sale_note

    def _default_cof_percentage(self):
        return self.env.user.company_id.cof_percentage
    
    #dummy_update    = fields.Boolean(string="Dummy", compute="_dummy_update")
    
    #Columns
    creator_user_id = fields.Many2one('res.users', string='CS Created by', track_visibility='onchange', readonly=True, default=lambda self: self.env.user.id)
    creator_team_id = fields.Many2one('crm.team', string='Team', track_visibility='onchange', default=lambda self: self.env.user.sale_team_id.id)
    approval_check  = fields.Boolean(string='Approval Check' )
    
    active = fields.Boolean('Active', default=1)
    name = fields.Char('Name', size=64)
    project_name = fields.Char('Project Name', size=128)
    partner_id = fields.Many2one(
        'res.partner', string='Customer', track_visibility='always')
    currency_id = fields.Many2one("res.currency", string="Currency", readonly=False, required=True)
#     currency_id = fields.Many2one(
#         "res.currency", related='company_id.currency_id',
#         string="Currency", readonly=True, required=True)
    amount = fields.Monetary(
        string='Amount', store=True, readonly=True,
        compute='_amount_all', track_visibility='always')
    rounding_discount = fields.Monetary(
        string='Rounding Discount', store=True, readonly=True,
        compute='_amount_all', track_visibility='always')
    amount_sales = fields.Monetary(
        string='Total Sales', store=True, readonly=True,
        compute='_amount_all', track_visibility='always')
    amount_total = fields.Monetary(
        string='Total Cost', store=True, readonly=True,
        compute='_amount_all', track_visibility='always')
    amount_expense_plan = fields.Monetary(
        string='Total Expense Plan', store=True, readonly=True,
        compute='_amount_all', track_visibility='always')
    amount_gross_profit = fields.Monetary(
        string='Gross Profit', store=True, readonly=True,
        compute='_amount_all', track_visibility='always')
    percent_gross_profit = fields.Float(
        string='% Gross Profit', store=True, readonly=True,
        compute='_amount_all', track_visibility='always')
    
    company_id = fields.Many2one(
        'res.company', 'Company',
        default=lambda self: self.env['res.company']._company_default_get(
            'kk.cost.sheet')
    )
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('appr_mgr', 'Approval Manager'),
            ('appr_gm', 'Approval G.Manager'),
            ('appr_drc', 'Approval Director'),
            #('appr_presdir', 'Approval Presdir'),
            ('post', 'Post'),
            ('close', 'Closed'),
            ('cancel', 'Cancel'),
        ], string='Status', readonly=True, copy=False,
        index=True, track_visibility='onchange', default='draft')
    kk_cost_sheet_line_ids = fields.One2many(
        'kk.cost.sheet.line', 'kk_cost_sheet_id', string="Cost Sheet Lines",
        copy=True)
    lock_exp_lines = fields.Boolean(string='Lock of Expense CS', default=False)
    lock_cof_lines = fields.Boolean(string='Lock of COF CS', default=False)
    kk_cost_exp_line_ids = fields.One2many(
        'kk.cost.expense.line', 'kk_cost_sheet_id', string="Cost Expenses Lines",
        track_visibility="onchange", copy=True)
    kk_sale_order_id = fields.Many2one(
        'sale.order',
        string="Sale Order")
    kk_purchase_requisition_ids = fields.One2many(
        'purchase.requisition', 'kk_cost_sheet_id',
        string="Purchase Requisition",
        copy=False)
    kk_purchase_order_ids = fields.One2many(
        'purchase.order', 'kk_cost_sheet_id',
        string="Purchase Order",
        copy=False)
    kk_stock_picking_ids = fields.One2many(
        'stock.picking', 'cost_sheet_id',
        string="Stock Picking",
        copy=False)
    kk_vendor_inv_ids = fields.One2many(
        'account.invoice', 'kk_cost_sheet_id',
        string="IN Invoice",
        copy=False, domain=[('type','=','in_invoice')])
    kk_inv_term_ids = fields.One2many(
        'fal.invoice.term.line', 'cs_cost_sheet_id',
        string="TOP",
        copy=False)
    
    note = fields.Text('Terms and conditions', default=_default_note)
    revisi_count = fields.Integer(string='Revision', readonly=True, copy=False)
    fal_invoice_milestone_line_date_ids = fields.One2many(
        'fal.invoice.term.line', 'cs_cost_sheet_id',
        string="Term Lines", copy=True)
    cof_start_date = fields.Date(
        string='COF Start Date',
        default=fields.Date.context_today,
        required=True)
    cof_percentage = fields.Float(
        string='COF Percentage',
        default=lambda self: self._default_cof_percentage())
    kk_cost_fund_line_ids = fields.One2many('kk.cost.fund.line', 'kk_cost_sheet_id',copy=False)
    kk_cost_fund_manual_line_ids = fields.One2many('kk.cost.fund.manual.line', 'kk_cost_sheet_id',copy=False)

#     cof_date_virtual = fields.Date(
#         string='COF Virtual Date')
#     cof_cost_virtual = fields.Float(
#         string='COF Cost Date')
    cof_cost_out = fields.Float(
        string='COF Out')
    cof_total = fields.Float(compute='_amount_all', track_visibility='always',
        string='COF Total')
    temp_top_ids = fields.Many2many('fal.invoice.term.line','rel_fal_inv_term_line',
                                                    'cs_cost_sheet_id',
                                                    string='Temporary Top',compute="_compute_temp_top_ids")
    type_sales = fields.Selection([('sales_product','Sales Product'),('sales_maintenance','Sales Maintenance')],compute="_compute_type_sales_team",store=True)

    @api.multi
    @api.depends('creator_team_id','type_sales')
    def _compute_type_sales_team(self):
        for item in self:
            if item.creator_team_id:
                item.type_sales = item.creator_team_id.type_sales

    

    @api.onchange('partner_id')
    def _onchange_partner(self):
        res_obj=self.env['res.users'].search([('id','=',self.env.user.id)])
        for res in res_obj:
            team_obj=self.env['crm.team'].search([('id','=',res.sale_team_id.id)])
            if team_obj.type_sales=='sales_product':
                arrRess = []
                res_obj=self.env['res.partner'].search([('user_id','=',self.env.user.id)])
                for i in res_obj:
                    arrRess.append(i.id)
                return {
                        'domain' : {
                            'partner_id' : [('id','in',arrRess)]
                            
                        }
                    }
                    
            if team_obj.type_sales=='sales_maintenance':
                res_obj=self.env['res.partner'].search([('user_maintenance_id','=',self.env.user.id)])
                arrRess = []
                for i in res_obj:
                    arrRess.append(i.id)
                return {
                        'domain' : {
                            'partner_id' : [('id','in',arrRess)]
                            
                        }
                    }

    
    @api.depends('temp_top_ids')
    def _compute_temp_top_ids(self):
        for item in self:
            arrHelp = []
            inv_obj = self.env['fal.invoice.term.line'].search([('cs_cost_sheet_id','=',item.id),('state_cs','=','post')])
            for i in inv_obj:
                arrHelp.append(i.id)
                item.temp_top_ids = arrHelp
    
    
    #@api.depends()    
    def _get_status(self):
        
        for line in self:
            so_status = ""
            for i in [('draft', 'Quotation'),('sent', 'Quotation Sent'),('appr_mgr', 'Approval Manager'),('appr_gm', 'Approval G.Manager'),
                      ('appr_drc', 'Approval Director'),('sale', 'Sales Order'),('done', 'Locked'),('cancel', 'Cancelled')]:
                if i[0]==line.kk_sale_order_id.state:
                    so_status = i[1]
                    break
            ###PR
            pr_status = """"""
            for lpr in self.env['purchase.requisition'].search([('kk_cost_sheet_id','=',line.id)]):
                lpr_state = 'Unknown'
                for i in [('draft','Draft'),('in_progress','Confirmed'),('open','Bid Selection'),('done','Done'),('cancel','Canceled')]:
                    if i[0]==lpr.state:
                        lpr_state = i[1]
                        break
                if lpr.state != 'cancel':
                    pr_status = pr_status + (lpr.name +' : '+ lpr_state) + "\n"
            ###  
            ###PO
            po_status = """"""
            for lpo in self.env['purchase.order'].search([('kk_cost_sheet_id','=',line.id)]):
                lpo_state = 'Unknown'
                for i in [('draft','RFQ'),('sent','RFQ Sent'),('to approve','To Approve'),('appr_sls','Approval Sales'),
                            ('appr_mgr','Approval Sales Manager'),('appr_drc','Approval Finance Director'),('purchase','Purchase Order'),('done','Locked'),('cancel','Cancelled')]:
                    if i[0]==lpo.state:
                        lpo_state = i[1]
                        break
                if lpo.state != 'cancel':
                    po_status = po_status + (lpo.name +' : '+ lpo_state) + "\n"
            ###  
            ###DropShip / Goods Receipt
            gr_status = """"""
            for lgr in self.env['stock.picking'].search([('cost_sheet_id','=',line.id)]):
                lgr_state = 'Unknown'
                for i in [('draft','Draft'),('waiting','Waiting Another Operation'),('confirmed','Waiting'),('assigned','Ready'),('done','Done'),('cancel','Cancelled')]:
                    if i[0]==lgr.state:
                        lgr_state = i[1]
                        break
                if lgr.state != 'cancel':
                    gr_status = gr_status + (lgr.name +' : '+ lgr_state) + "\n"
            ###  
            ###Invoice IN
            linvin_status = """"""
            for lpo in line.kk_purchase_order_ids:
                for linv in lpo.invoice_ids:
                    for i in [('draft','Draft'),('proforma','Pro-forma'),('proforma2','Pro-forma2'),
                                ('appr_slsmgr','Approval Sales Manager'),('appr_tax','Tax Validate'),('appr_sls','Approval Sales'),
                                ('appr_mgr','Director Sales'),('appr_accounting','Accounting Mgr'),('open','Open'),('payment','Payment'),
                                ('paid','Paid'),('cancel','Cancelled')]:
                        if i[0]==linv.state and linv.state != 'cancel':
                            linvin_status = linvin_status + ((linv.number or linv.origin) +' : '+ i[1]) + "\n"
                            break
            
            ###Invoice Milestone
            linvml_status = """"""
            for linvml in self.env['fal.invoice.term.line'].search([('cs_cost_sheet_id','=',line.id)]):
                linvml_state = 'Inv. Not Create yet'
                if linvml.invoice_id:
                    for i in [('draft','Draft'),('proforma','Pro-forma'),('proforma2','Pro-forma2'),
                                ('appr_slsmgr','Approval Sales Manager'),('appr_tax','Tax Validate'),('appr_sls','Approval Sales'),
                                ('appr_mgr','Director Sales'),('appr_accounting','Accounting Mgr'),('open','Open'),('payment','Payment'),
                                ('paid','Paid'),('cancel','Cancelled')]:
                        if i[0]==linvml.invoice_id.state and linvml.invoice_id.state != 'cancel':
                            linvml_state = i[1] + (linvml.invoice_id.number and (' *'+linvml.invoice_id.number) or '')
                            break
                linvml_status = linvml_status + (linvml.name +' : '+ linvml_state) + "\n"
            ###  
            line.update({
                    'so_status' : (line.kk_sale_order_id.state != 'cancel' and (line.kk_sale_order_id and (line.kk_sale_order_id.name or "") +' : '+ so_status)) or "",
                    'pr_status' : pr_status,
                    'po_status' : po_status,
                    'gr_status' : gr_status,
                    'linvin_status': linvin_status,
                    'linvml_status': linvml_status,
                    })
    
    ##View Status
    so_status   = fields.Text(string='SO',compute='_get_status')
    pr_status   = fields.Text(string='PR',compute='_get_status')
    po_status   = fields.Text(string='PO',compute='_get_status')
    gr_status   = fields.Text(string='DS/GR',compute='_get_status')
    linvin_status   = fields.Text(string='Inv In',compute='_get_status')
    linvml_status   = fields.Text(string='Inv Out',compute='_get_status')
    

    @api.onchange('name')
    def call_get_csl_domain(self):
        self.fal_invoice_milestone_line_date_ids._get_csl_domain()

    @api.model
    def create(self, vals):
        vals['name'] = _('Draft')

        company = self.env['res.company']._company_default_get(
            'kk.cost.sheet')

        detail_list = []
        for dtl in company.kk_cost_exp_line_ids:
            data = (
                0, 0, {
                    'product_id': dtl.product_id.id,
                    'name': dtl.name,
                    'expense_plan': dtl.expense_plan,
                }
            )
            detail_list.append(data)
        vals['kk_cost_exp_line_ids'] = detail_list
        res = super(CostSheet, self).create(vals)
        return res

    @api.multi
    def check_total_top(self):
        total_top = 0.0
        for top in self.fal_invoice_milestone_line_date_ids:
            total_top += top.total_inv
        
        #if abs(self.amount_sales - total_top) > 0.2:
        if round(self.amount_sales,2) != round(total_top,2):
            raise UserError(_(
                "Total Term of Payment should be equal with Total Sales."))

    @api.multi
    def check_profit(self):
        if self.amount_gross_profit <= 0.0:
            pass
            #raise UserError(_("You can not Confirm Cost Sheet when not have Profit"))

    @api.multi
    def action_recalculate_cof(self):
        self.create_cof()
        self._amount_all()
        #self.check_profit()

    @api.multi
    def action_confirm(self):
        # Check Total Invoice and Term of Payment.
        self.check_total_top()
        #Dimatikan
        #self.action_recalculate_cof()
        self.check_profit()
        if self.name == _('Draft'):
            self.name = self.env['ir.sequence'].next_by_code('cost.sheet')
        return self.write({'state': 'appr_mgr', 'lock_exp_lines': True, 'lock_cof_lines': True})

    @api.multi
    def action_appr_mgr(self):
        context = self._context
        #Check login_uid = context.get('uid') Validation login
        login_uid = context.get('uid')
        
        cs_admin_group = self.env.user.has_group('nieve_cost_sheet.group_costsheet_admin')
        
        if self.creator_team_id.user_id.id != login_uid and not cs_admin_group:
            raise UserError(_("This Cost Sheet need Approval from %s") % self.creator_team_id.user_id.name)
        
        if self.percent_gross_profit <= self.company_id.gm_cs_approval_gp:
            return self.write({'state': 'appr_gm'})
        else:
            self._action_sale_order_create()
            return self.write({'state': 'post'})

    @api.multi
    def action_appr_gm(self):
        context = self._context
        #Check login_uid = context.get('uid') Validation login
        login_uid = context.get('uid')
        
        cs_admin_group = self.env.user.has_group('nieve_cost_sheet.group_costsheet_admin')
        
        if self.creator_team_id.gm_user_id.id != login_uid and not cs_admin_group:
            raise UserError(_("This Cost Sheet need Approval from %s") % self.creator_team_id.gm_user_id.name)
        #
        if self.percent_gross_profit <= self.company_id.drc_cs_approval_gp:
            return self.write({'state': 'appr_drc'})
        else:
            self._action_sale_order_create()
            return self.write({'state': 'post'})

    @api.multi
    def action_appr_drc(self):
        self._action_sale_order_create()
        return self.write({'state': 'post'})

    @api.multi
    def action_close(self):
        for ltop in self.fal_invoice_milestone_line_date_ids:
            if not ltop.invoice_id and ltop.state != 'cancel':
                raise UserError(_("You can not Close Cost Sheet when not All Terms Invoiced"))
        for po in self.kk_purchase_order_ids:
            for vb in po.invoice_ids:
                if vb.state !='paid':
                    raise UserError(_("You can not Close Cost Sheet when All Vendor Bills not Paid "))
            
        self.action_recalculate_cof()
        return self.write({'state': 'close'})
    
    @api.multi
    def action_open(self):
        return self.write({'state': 'post'})

    @api.multi
    def action_set_to_draft(self):
        return self.write({'state': 'draft'})

    @api.multi
    def action_cancel(self):
        if self.kk_sale_order_id:
            self.kk_sale_order_id.action_cancel()
            self.kk_sale_order_id = False
        return self.write({'state': 'cancel','lock_exp_lines': False, 'lock_cof_lines': False})
    
    @api.multi
    def lock_unlock_exp_cs(self):
        if self.lock_exp_lines==False:
            return self.write({'lock_exp_lines': True}) 
        else:    
            return self.write({'lock_exp_lines': False}) 
    
    @api.multi
    def lock_unlock_cof_cs(self):
        if self.lock_cof_lines==False:
            return self.write({'lock_cof_lines': True}) 
        else:    
            return self.write({'lock_cof_lines': False}) 

    @api.multi
    def action_revision(self):
        self.ensure_one()
        if self.kk_sale_order_id:
            self.kk_sale_order_id.action_cancel()
            self.kk_sale_order_id = False
        doc_copy = self.copy()
        doc_copy.revisi_count = self.revisi_count + 1
        name = self.name
        head, sep, tail = name.partition('#')
        doc_copy.name = "%s#%s%s" % (head, 'REV', str(doc_copy.revisi_count))
        if doc_copy:
            sequence = self.env['ir.sequence'].search(
                [('code', '=', 'cost.sheet')])
            seq_update = sequence.number_next_actual - 1
            sequence.number_next_actual = seq_update
            self.action_cancel()
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'kk.cost.sheet',
                'res_id': doc_copy.id,
                'context': self.env.context,
                'flags': {'initial_mode': 'edit'},
            }

        return False

    @api.multi
    def _prepare_sale_order(self):
        self.ensure_one()
        company = self.env['res.company']._company_default_get('kk.cost.sheet')
        nama = 'Draft'
        date_order = datetime.now()
        
        #Search Product Pricelist
        pricelist = self.env['product.pricelist'].search([('currency_id','=',self.currency_id.id)], limit=1) or False
        if not pricelist:
            raise UserError(_(
                "Please contact your Administrator to Add Pricelist %s") % self.currency_id.name)
        #
        vals = {
            'name': nama,
            'date_order': date_order,
            'kk_cost_sheet_id': self.id,
            'partner_id': self.partner_id.id,
            'pricelist_id': pricelist.id,
            'currency_id': self.currency_id.id,
            'company_id': company.id,
            'user_id'   : self.creator_user_id.id,
            'creator_user_id' : self.creator_user_id.id,
            'team_id' : self.creator_team_id.id,
            ##
            'rounding_discount' : self.rounding_discount,
        }
        return vals

    @api.multi
    def _action_sale_order_create(self):
        """
        Create Sale Order based on Cost Sheet Order.
        """
        for sale in self:
            vals = sale._prepare_sale_order()
            saleorder = self.env["sale.order"].create(vals)
            for line in sale.kk_cost_sheet_line_ids:
                vals_line = line._prepare_order_line_sale_order(line.kk_cost_sheet_id.id)
                vals_line.update({'order_id': saleorder.id})
                
                so_line = self.env['sale.order.line'].create(vals_line)
                so_line.onchange_discount_amount()
                line.so_line_id = so_line.id
            for top in sale.fal_invoice_milestone_line_date_ids:
                so_line_list = []
                for cs in top.cost_sheet_line_m2m:
                    so_line_list.append((4, cs.so_line_id.id))
                for l in top.item_invoice_term_line:
                    l.sale_line_id = l.cost_sheet_line_id.so_line_id.id
                    
                top.so_line_m2m = so_line_list
                top.fal_sale_order_id = saleorder.id
        self.kk_sale_order_id = saleorder.id


    @api.multi
    def _action_sale_order_create_from_adendum(self):
        """
        Create Sale Order based on Cost Sheet Order.
        """
        for sale in self:
            for line in sale.kk_cost_sheet_line_ids:
                if line.is_adendum:
                    vals_line = line._prepare_order_line_sale_order(line.kk_cost_sheet_id.id)
                    vals_line.update({'order_id': self.kk_sale_order_id.id})
                    so_line = self.env['sale.order.line'].create(vals_line)
                    so_line.onchange_discount_amount()
                    line.so_line_id = so_line.id
                    line.is_adendum = False
            # for top in sale.fal_invoice_milestone_line_date_ids:
            #     so_line_list = []
            #     for cs in top.cost_sheet_line_m2m:
            #         so_line_list.append((4, cs.so_line_id.id))
            #     top.so_line_m2m = so_line_list
                    # SSSS
    
#####Hitungan Cara Lama#######
#     @api.multi
#     def create_cof(self):
#         for cs in self.kk_cost_fund_line_ids:
#             cs.unlink()
# 
#         self.cof_date_virtual = self.cof_start_date
#         self.cof_cost_virtual = self.amount_total
#         self.cof_total = 0.0
#         for cost in self:
#             for top in cost.fal_invoice_milestone_line_date_ids:
#                 vals = top._prepare_top_cof_value()
#                 date_from = dt.datetime.strptime(self.cof_date_virtual, '%Y-%m-%d')
#                 date_to = dt.datetime.strptime(top.date, '%Y-%m-%d')
#                 days_diff = months_between(date_from, date_to)
#                 cost_val = 0.0
#                 if cost.cof_cost_out:
#                     cost_val = (top.total_inv / cost.cof_cost_out) * cost.amount_total
#                 cof_value = self.cof_cost_virtual * (self.cof_percentage / 100) * days_diff
#                 self.cof_date_virtual = top.date
#                 self.cof_cost_virtual = self.cof_cost_virtual - cost_val
#                 vals.update(
#                     {
#                         'kk_cost_sheet_id': cost.id,
#                         'cost_value': cost_val,
#                         'days_diff': days_diff,
#                         'cof_value': cof_value
#                     }
#                 )
#                 self.cof_total += cof_value
#                 self.env["kk.cost.fund.line"].create(vals)
    
    @api.multi
    def create_cof(self):
        for cs in self.kk_cost_fund_line_ids:
            cs.unlink()

        self.cof_total = 0.0
        for cost in self:
            #First Date COF
            date_from           = dt.datetime.strptime(self.cof_start_date, '%Y-%m-%d')
            date_from_actual    = dt.datetime.strptime(self.cof_start_date, '%Y-%m-%d')
            cost_val            = self.cof_cost_out
            #cost_val_actual     = sum(lpo.amount_untaxed for lpo in cost.kk_purchase_order_ids)
            cost_val_actual     = 0.0
            
            for lpo in cost.kk_purchase_order_ids:
                for linv in lpo.invoice_ids:
                    for lmov in linv.move_id.line_ids:
                        cost_val_actual += (lmov.debit != 0.0 and not lmov.tax_base_amount and lmov.debit) or 0.0
            
            top_total = sum(i.total_inv for i in cost.fal_invoice_milestone_line_date_ids)
            first_top = False
            for top in cost.fal_invoice_milestone_line_date_ids:
                vals = top._prepare_top_cof_value()
                
                date_to     = dt.datetime.strptime(top.date, '%Y-%m-%d')
                #days_diff  = months_between(date_from, date_to)
                days_diff   = int((date_to - date_from).days) if int((date_to - date_from).days) > 0 else 0
                #date_from   = date_to#dt.datetime.strptime(date_to, '%Y-%m-%d')
                
                cof_value   = round((cost_val if cost_val > 0 else 0) * (self.cof_percentage / 100) * (days_diff / 30))
                
                if top.invoice_id.payment_date and cost_val_actual > 0.0:
                    ##Get Date From PO
                    cof_value_actual = 0.0
                    date_to_actual = dt.datetime.strptime(top.invoice_id.payment_date, '%Y-%m-%d') 
                    for lpo in cost.kk_purchase_order_ids:
                        cost_val_actual_per_po = 0.0
                        if lpo.payment_date:
                            for linv in lpo.invoice_ids:
                                for lmov in linv.move_id.line_ids:
                                    cost_val_actual_per_po += (lmov.debit != 0.0 and not lmov.tax_base_amount and lmov.debit) or 0.0
                            
                            date_from_actual    = dt.datetime.strptime(lpo.payment_date, '%Y-%m-%d')
                            days_diff_actual    = int(((date_to_actual - date_from_actual).days) / 1) if int(((date_to_actual - date_from_actual).days) / 1) > 0 else 0
                            cof_value_actual    += round((cost_val_actual if cost_val_actual > 0 else 0)*(cost_val_actual_per_po if cost_val_actual_per_po > 0 else 0 /cost_val_actual if cost_val_actual > 0 else 0) \
                                                    * (days_diff_actual / 30) * (self.cof_percentage / 100))
                            
                            
                            
                        else:
                            cost_val_actual     = 0.0
                            days_diff_actual    = 0.0
                            cof_value_actual    = 0.0
                            
                    ##Force set to 0
                    days_diff_actual = 0

                    
                    
                    
                else:
                    cost_val_actual     = 0.0
                    days_diff_actual    = 0.0
                    cof_value_actual    = 0.0
                                
                
                vals.update(
                    {
                        'kk_cost_sheet_id': cost.id,
                        'cost_value': (cost_val if cost_val >0 else 0),
                        'days_diff': days_diff,
                        'cof_value': cof_value,
                        
                        'cof_in_date_actual': top.invoice_id.payment_date,
                        'cost_value_actual' : (cost_val_actual if cost_val_actual >0 else 0),#cost_val,
                        'days_diff_actual'  : days_diff_actual,
                        'cof_value_actual'  : cof_value_actual})
                
                if cost.cof_cost_out:
                    cost_val -= top.total_inv#(top.total_inv / top_total) * self.cof_cost_out
                    cost_val_actual -= top.total_inv
                self.cof_total += cof_value
                print ("self----***", self)
                print ("vals---->", vals)
                self.env["kk.cost.fund.line"].create(vals)
#                 sql_query = """
#                             INSERT INTO kk_cost_fund_line (name,kk_cost_sheet_id,cof_in_date) VALUES ('XXX',72,'2019-01-01')
#                             """
#                 self.env.cr.execute(sql_query)

    
    @api.multi
    def call_wiz_top(self):
        print ("call wizard fucntion")
        wizard_form = self.env.ref('nieve_cost_sheet.view_costsheet_wizz', False)
        context = dict(self.env.context or {})
        if context.get('cost_sheet_id'):
            context['active_id'] = self.id
            
        return {
                'name': _('Term of Payment'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'cost.sheet.wizard',
                'view_id': wizard_form.id,
                'type': 'ir.actions.act_window',
                #'res_id': self.env.context.get('cost_sheet_id'),
                #'context': context,
                'target': 'new'
            }
    
class CostSheetLine(models.Model):
    _name = 'kk.cost.sheet.line'
    _description = 'Cost Sheet Lines'
    _order = 'id'
    _inherit = ['mail.thread']
    
    @api.multi
    def update_cost_price_wizz(self):
        print ("call wizard fucntion")
        wizard_form = self.env.ref('nieve_cost_sheet.view_costsheet_price_update_wizz', False)
        context = dict(self.env.context or {})
        if context.get('id'):
            context['active_id'] = self.id
            
        return {
                'name': _('Cost Price Update'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'cost.sheet.price.update.wizard',
                'view_id': wizard_form.id,
                'type': 'ir.actions.act_window',
                'res_id': self.env.context.get('id'),
                'context': context,
                'target': 'new'
            }
    
    @api.multi
    def copy_line(self):
        self.copy()

    # It can connect to Cost Sheet
    @api.depends(
        'cost_product_uom_qty', 'cost_discount', 'cost_price_unit', 'cost_tax_id', 'cost_discount_type','cost_discount_amount',
        'product_uom_qty', 'discount', 'price_unit', 'tax_id', 'discount_type', 'discount_amount'
    )
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        # self.onchange_discount_amount()
        # self.onchange_cost_discount_amount()
        for line in self:
            discount_type   = line.discount_type
            discount_amount = line.discount_amount
            price_unit      = line.price_unit
            product_uom_qty = line.product_uom_qty
            discount        = 0.0

            if discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0 and product_uom_qty != 0.0:
                if discount_type =='amount':
                    discount = (discount_amount / (price_unit*product_uom_qty)) * 100
                elif discount_type == 'percentage':
                    discount = discount_amount
                line.discount = discount
            else:
                line.discount_amount = 0.0
                line.discount = 0.0

            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(
                price, line.kk_cost_sheet_id.currency_id,
                line.product_uom_qty, product=line.product_id,
                partner=line.kk_cost_sheet_id.partner_id)
            line.update({
                'price_total': taxes['total_included'],
            })

        for line in self:
            cost_discount_type   = line.cost_discount_type
            cost_discount_amount = line.cost_discount_amount
            cost_price_unit      = line.cost_price_unit
            cost_product_uom_qty = line.cost_product_uom_qty
            cost_discount        = 0.0
            if cost_discount_amount >= 0.0 and cost_discount_amount != 0.0 and cost_price_unit != 0.0 and cost_product_uom_qty != 0.0:
                if cost_discount_type =='amount':
                    cost_discount = (cost_discount_amount / (cost_price_unit*cost_product_uom_qty)) * 100
                elif cost_discount_type == 'percentage':
                    cost_discount = cost_discount_amount
                line.cost_discount = cost_discount
            else:
                line.cost_discount_amount = 0.0
                line.cost_discount = 0.0

            price = line.cost_price_unit * (1 - (line.cost_discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(
                price, line.kk_cost_sheet_id.currency_id,
                line.cost_product_uom_qty, product=line.product_id,
                partner=line.kk_cost_sheet_id.partner_id)
            line.update({
                'cost_price_total': taxes['total_included'],
            })

    @api.depends('price_total', 'cost_price_total', 'gross_profit_percentage')
    def _compute_gp_percent(self):
        tot = 0.0
        for item in self:
            if item.price_total and item.cost_price_total:
                tot = ((item.price_total - item.cost_price_total) / item.price_total) * 100
                item.gross_profit_percentage = tot

    @api.multi
    def _prepare_order_line_sale_order(self, kk_cost_sheet_id):
        self.ensure_one()
        tax_list = []
        tax_obj = self.env['account.tax'].search(
            [('type_tax_use', '=', 'sale')])
        for tax in tax_obj:
            tax_list.append((4, tax.id))

        detail_list = []
        for dtl in self.kk_order_line_ids:
            data = (
                0, 0, {
                    'product_code': dtl.product_code,
                    'name': dtl.name,
                    'product_uom': dtl.product_uom.id,
                    'product_uom_qty': dtl.product_uom_qty
                }
            )
            detail_list.append(data)
        vals = {
            'name': self.name,
            'kk_cost_sheet_line_id': self.id,
            'product_id': self.product_id.id,
            'price_unit': self.price_unit,
            # 'discount': self.discount,
            'tax_id': tax_list,
            'kk_order_line_ids': detail_list,
            'discount_type': self.discount_type,
            'discount_amount': self.discount_amount,
            'price_subtotal': self.price_total,
            'product_uom_qty': self.product_uom_qty,
            'kk_cogs': self.cost_price_total,
            'product_uom': self.product_id.uom_id.id}
        return vals

    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet', ondelete='cascade')
    product_id = fields.Many2one(
        'product.product', 'Product', track_visibility="onchange")
    name = fields.Text(string='Description Line')
    currency_id = fields.Many2one(
        related='kk_cost_sheet_id.company_id.currency_id',
        store=True, string='Currency', readonly=True)
    product_uom_qty = fields.Float(
        string='Quantity Sales',
        digits=dp.get_precision('Product Unit of Measure'), default=1)
    price_unit = fields.Float(
        'Unit Price Sales', required=True,
        digits=dp.get_precision('Product Price'), default=0.0)
    price_total = fields.Monetary(
        compute='_compute_amount', string='Total Sales',
        readonly=True, store=True)
    discount = fields.Float(
        string='Discount Sales(%)',
        digits=dp.get_precision('Discount'), default=0.0)
    tax_id = fields.Many2many(
        'account.tax', string='Taxes Sales',
        domain=['|', ('active', '=', False), ('active', '=', True)])
    discount_type = fields.Selection(
        [
            ('amount', 'Amount'),
            ('percentage', 'Percentage')
        ], string='Disc. Type', default='percentage')
    discount_amount = fields.Float(string='Discount Amount', default=0.0)

    cost_product_uom_qty = fields.Float(
        string='Quantity Cost',
        digits=dp.get_precision('Product Unit of Measure'),
        required=True, default=1.0)
    cost_price_unit = fields.Float(
        'Unit Price Cost', required=True,
        digits=dp.get_precision('Product Price'), default=0.0)
    cost_price_total = fields.Monetary(
        compute='_compute_amount',
        string='Total Cost', readonly=True, store=True)
    cost_discount = fields.Float(
        string='Discount Cost(%)',
        digits=dp.get_precision('Discount'), default=0.0)
    cost_tax_id = fields.Many2many(
        'account.tax', string='Taxes Cost',
        domain=['|', ('active', '=', False), ('active', '=', True)])
    cost_discount_type = fields.Selection(
        [
            ('amount', 'Amount'),
            ('percentage', 'Percentage')
        ], string='Disc. Type', default='percentage')
    cost_discount_amount = fields.Float(string='Discount Amount', default=0.0)
    kk_order_line_ids = fields.One2many(
        'kk.order.line', 'cs_line_id', 'Order Detail', copy=True)
    cost_price_log_ids = fields.One2many(
        'cs.cost.price.log', 'cs_line_id', 'Cost Price Log', copy=False)
    so_line_id = fields.Many2one('sale.order.line', 'SO Line')
    gross_profit_percentage = fields.Float(
        compute='_compute_gp_percent',
        string='GP %', readonly=True, store=True)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('cancel', 'Cancel'),
            ('appr_mgr', 'Approval Manager'),
            ('appr_gm', 'Approval G.Manager'),
            ('appr_drc', 'Approval Director'),
            #('appr_presdir', 'Approval Presdir'),
            ('post', 'Post'),
        ], string='Status', related='kk_cost_sheet_id.state')
    is_adendum = fields.Boolean('Is Adendum')

    @api.multi
    def name_get(self):
        res = []
        for val in self:
            name1 = val.product_id.name
            name2 = val.name
            name = str(name1) +" - "+ str(name2)
            res.append((val.id, name))
        return res


    @api.onchange('product_id')
    def onchange_product_id(self):
        self.name = self.product_id.name

    @api.onchange(
        'cost_discount_type',
        'cost_discount_amount',
        'cost_price_unit',
        'cost_product_uom_qty')
    def onchange_cost_discount_amount(self):
        discount_type = self.cost_discount_type
        discount_amount = self.cost_discount_amount
        price_unit = self.cost_price_unit
        product_uom_qty = self.cost_product_uom_qty
        discount = 0.0

        if discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0 and product_uom_qty != 0.0:
            if discount_type == 'amount':
                discount = (discount_amount / (price_unit * product_uom_qty)) * 100
            elif discount_type == 'percentage':
                discount = discount_amount
            self.cost_discount = discount
        else:
            self.cost_discount_amount = 0.0
            self.cost_discount = 0.0

    @api.onchange(
        'discount_type',
        'discount_amount',
        'price_unit',
        'product_uom_qty')
    def onchange_discount_amount(self):
        discount_type   = self.discount_type
        discount_amount = self.discount_amount
        price_unit      = self.price_unit
        product_uom_qty = self.product_uom_qty
        discount        = 0.0

        if discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0 and product_uom_qty != 0.0:
            if discount_type=='amount':
                discount = (discount_amount / (price_unit*product_uom_qty)) * 100
            elif discount_type=='percentage':
                discount = discount_amount
            self.discount = discount
            return
        else:
            self.discount_amount = 0.0
            self.discount = 0.0
            
            
class CostPriceLog(models.Model):
    _name = 'cs.cost.price.log'
    
    date        = fields.Date(string='Date', readonly=True)
    amount      = fields.Float(string='Cost Price', readonly=True)
    cs_line_id  = fields.Many2one('kk.cost.sheet.line', string='Cost Sheet Line')


class CostExpenseLine(models.Model):
    _name = 'kk.cost.expense.line'

    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet')
    config_company_id = fields.Many2one('res.company', 'Company')
    product_id = fields.Many2one(
        'product.product', 'Product', track_visibility="onchange")
    name = fields.Char(string='Description')
    currency_id = fields.Many2one(
        related='kk_cost_sheet_id.company_id.currency_id',
        store=True, string='Currency', readonly=True)
    expense_plan = fields.Monetary(
        string='Expense Plan')
    expense_actual = fields.Monetary(
        string='Expense Actual', compute='_compute_expense_actual')

    @api.multi
    def _compute_expense_actual(self):
        print ("###_compute_expense_actual-->###", self)

        for actline in self:
            exp_act = 0.0
            #Membedakan Product Auto Actual
            if actline.product_id.generate_expense_product==True:
                exp_act = actline.expense_plan
            else:
                accmove_line_obj = self.env['account.move.line']
                accmove_line = accmove_line_obj.search(
                    [
                        ('kk_cost_expense_id', '=', actline.id),
                    ])
                for line in accmove_line:
                    exp_act += line.balance
            actline.expense_actual = exp_act
#         for expense in self:
#             print ("expense--->", expense)
#             expense.expense_actual = exp_act

class CostFundManualLine(models.Model):
    _name = 'kk.cost.fund.manual.line'
    
    name        = fields.Char(string='Description')
    cof_plan    = fields.Float(string='COF Plan')
    cof_actual  = fields.Float(string='COF Actual')
    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet', 'Cost Sheet', ondelete='cascade')

class CostFundLine(models.Model):
    _name = 'kk.cost.fund.line'
    _order = 'id'

    kk_cost_sheet_id    = fields.Many2one('kk.cost.sheet', 'Cost Sheet')
    name                = fields.Char(string='Description')
    #Plan
    sales_value         = fields.Float(string='Sales Value')
    cof_in_date         = fields.Date(string='Month In',required=False)
    days_diff          = fields.Integer(string='Days Difference')
    cost_value          = fields.Float(string='Cost Plan')
    cof_value           = fields.Float(string='COF Plan')
    
    #Actual
    cof_in_date_actual         = fields.Date(string='Payment In',required=False)
    days_diff_actual          = fields.Integer(string='Days Difference')
    cost_value_actual          = fields.Float(string='Cost Actual')
    cof_value_actual           = fields.Float(string='COF Actual')

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.model
    def _get_dropship_route(self):
        buy_route = self.env.ref('stock_dropshipping.route_drop_shipping', raise_if_not_found=False)
        if buy_route:
            return buy_route.ids
        return []

    purchase_requisition = fields.Selection(
        [('rfq', 'Create a draft purchase order'),
         ('tenders', 'Propose a call for tenders')],
        string='Procurement', default='tenders')

    route_ids = fields.Many2many(default=lambda self: self._get_dropship_route())
