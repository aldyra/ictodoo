# -*- coding: utf-8 -*-
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo import models, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
import time

class CostSheetFinanceDirectorReportXlsx(models.AbstractModel):
    print ("###CostSheetSalesDirectorReportXlsx###")
    _name = 'report.nieve_cost_sheet.report_cost_sheet_finance_director_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, wizard):
        
        print ("##generate_xlsx_report")
        
        sheet = workbook.add_worksheet('Cost Sheet Finance Director Reports')
        sheet.hide_gridlines(2)
        #######
        
        ###
        cs_obj = self.env['kk.cost.sheet']
        #Filter
        if wizard.filter_option == 'state':
            if wizard.filter_state:
                data_cs = cs_obj.search([('state','=',wizard.filter_state)])
            else:
                data_cs = cs_obj.search([])
        elif wizard.filter_option=='date':
            data_cs = cs_obj.search([('create_date','>=',wizard.filter_date_from),('create_date','<=',wizard.filter_date_to)])
        elif wizard.filter_option=='cs':
            data_cs = cs_obj.browse(wizard.filter_cost_sheet_ids.ids)
        #data_cs = cs_obj.browse([75,76])
        ###
        
        ##Style
        company_header_style = workbook.add_format({'bold': True, 'font_color': 'black'})
        company_header_style.set_font_size(16)
        company_header_style.set_align('center')
        
        report_label_header_style = workbook.add_format({'bold': True, 'font_color': '#000080'})
        report_label_header_style.set_font_size(26)
        report_label_header_style.set_align('center')
        
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        
        no_style = workbook.add_format({'bold': False, 'font_color': 'black'})
        no_style.set_border(1)
        no_style.set_align('top')
        
        number = workbook.add_format({'bold': False, 'font_color': 'black'})
        number.set_num_format('#,##0.00;(#,##0.00)')
        number.set_border(1)
        number.set_align('top')
        
        string_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold.set_num_format('#,##0.00;(#,##0.00)')
        percentage  = workbook.add_format({'num_format': '0.0%'})
        
        table_header_style = workbook.add_format({'bold': True})
        table_header_style.set_align('center')
        table_header_style.set_align('top')
        table_header_style.set_border(1)
        
        ##
        
        sheet.show_grid = 0
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.portrait = 0  # Landscape
        sheet.fit_width_to_pages = 1
        
        sheet.set_column('A:A', 5)
        sheet.set_column('B:N', 15)  # Source No.
        
        #sheet.write(3, 0, 'Date Range filter : ',)
        
        row = 5
        
        sheet.merge_range('A3:O4', 'Dashboard Sales & Finance Director', company_header_style)
        
        sheet.merge_range(row, 0, row+1, 0, 'No.', table_header_style)
        sheet.merge_range(row, 1, row+1, 1, 'Year.', table_header_style)
        sheet.merge_range(row, 2, row+1, 2, 'Project Name.', table_header_style)
        sheet.merge_range(row, 3, row+1, 3, 'Customer.', table_header_style)
        sheet.merge_range(row, 4, row+1, 4, 'Sales.', table_header_style)
        sheet.merge_range(row, 5, row+1, 5, 'No.CS', table_header_style)
        sheet.merge_range(row, 6, row+1, 6, 'Total Sales', table_header_style)
        sheet.merge_range(row, 7, row+1, 7, 'GP', table_header_style)
        sheet.merge_range(row, 8, row, 10, 'Vendor Bills', table_header_style)
        sheet.write(row+1, 8, 'Vendor', table_header_style)
        sheet.write(row+1, 9, 'Amount', table_header_style)
        sheet.write(row+1, 10, 'Status', table_header_style)
        sheet.merge_range(row, 11, row, 14, 'Customer Invoice Status', table_header_style)
        sheet.write(row+1, 11, 'Termin', table_header_style)
        sheet.write(row+1, 12, 'Amount', table_header_style)
        sheet.write(row+1, 13, 'Status', table_header_style)
        sheet.write(row+1, 14, 'Status Paid', table_header_style)
        
        row += 2
        
        no = 0
        for cs in data_cs:
            no += 1
            merge_cell_count_cust_inv = len(cs.fal_invoice_milestone_line_date_ids)
            
            merge_cell_count_supp_inv = 0
            for i in cs.kk_purchase_order_ids:
                merge_cell_count_supp_inv += len(i.invoice_ids)
            merge_cell_count_supp_inv = merge_cell_count_supp_inv
            
            if merge_cell_count_cust_inv > merge_cell_count_supp_inv:
                merge_cell_count = merge_cell_count_cust_inv - 1
            else:
                merge_cell_count = merge_cell_count_supp_inv - 1
            
            row_start_cs = row
            if merge_cell_count < 1:
                sheet.write(row, 0, no, no_style)
                sheet.write(row, 1, cs.kk_sale_order_id and time.strftime('%Y', time.strptime(cs.kk_sale_order_id.date_order, '%Y-%m-%d %H:%M:%S')) or "",number)
                sheet.write(row, 2, cs.project_name or "", number)
                sheet.write(row, 3, cs.partner_id.name or "", number)
                sheet.write(row, 4, cs.creator_user_id.name or "", number)
                sheet.write(row, 5, cs.name or "", number)
                sheet.write(row, 6, cs.amount_sales or "", number)
                sheet.write(row, 7, cs.amount_gross_profit or "", number)
            else:
                sheet.merge_range(row, 0, row+merge_cell_count, 0, no, no_style)
                sheet.merge_range(row, 1, row+merge_cell_count, 1, cs.kk_sale_order_id and time.strftime('%Y', time.strptime(cs.kk_sale_order_id.date_order, '%Y-%m-%d %H:%M:%S')) or "",number)
                sheet.merge_range(row, 2, row+merge_cell_count, 2, cs.project_name or "", number)
                sheet.merge_range(row, 3, row+merge_cell_count, 3, cs.partner_id.name or "", number)
                sheet.merge_range(row, 4, row+merge_cell_count, 4, cs.creator_user_id.name or "", number)
                sheet.merge_range(row, 5, row+merge_cell_count, 5, cs.name or "", number)
                sheet.merge_range(row, 6, row+merge_cell_count, 6, cs.amount_sales or "", number)
                sheet.merge_range(row, 7, row+merge_cell_count, 7, cs.amount_gross_profit or "", number)
            
            ##Loop Invoices
            row_bill = row_start_cs
            for po in cs.kk_purchase_order_ids:
                for bill in po.invoice_ids:
                    sheet.write(row_bill, 8, bill.partner_id.name or "", number)
                    sheet.write(row_bill, 9, bill.amount_total or "", number)
                    sheet.write(row_bill, 10, bill.state, number)
                    row_bill += 1
            ##Create Blacnk Line
            count_blank = merge_cell_count_cust_inv - merge_cell_count_supp_inv
            if count_blank >= 1:
                for i in range(0,count_blank):
                    sheet.write(row_bill, 8, "", number)
                    sheet.write(row_bill, 9, "", number)
                    sheet.write(row_bill, 10, "", number)
                    row_bill += 1
                
            
            ##Loop Termins
            row_inv = row_start_cs
#             if not cs.fal_invoice_milestone_line_date_ids:
#                 row_inv += 1
            for top in cs.fal_invoice_milestone_line_date_ids:
                sheet.write(row_inv, 11, top.name or "", number)
                sheet.write(row_inv, 12, top.total_inv or "", number)
                sheet.write(row_inv, 13, top.invoice_id and "Invoiced" or "Not Invoiced Yet" or "", number)
                paid_status = ''
                if top.invoice_id:
                    if top.invoice_id.state=='paid':
                        paid_status = 'Paid'
                    elif top.invoice_id.state=='cancel':
                        paid_status = 'Cancel'
                    else:
                        paid_status = 'Open'
                sheet.write(row_inv, 14, paid_status or "", number)
                row_inv += 1
            ##Create Blacnk Line
            count_blank =  merge_cell_count_supp_inv - merge_cell_count_cust_inv
            if count_blank >= 1:
                for i in range(0,count_blank):
                    sheet.write(row_inv, 11, "", number)
                    sheet.write(row_inv, 12, "", number)
                    sheet.write(row_inv, 13, "", number)
                    sheet.write(row_inv, 14, "", number)
                    row_inv += 1
            row = row_bill > row_inv and row_bill or row_inv
#              
            
