# -*- coding: utf-8 -*-
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo import models, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning


class ForecastingReportXlsx(models.AbstractModel):
    print ("###CostSheetReportXlsx###")
    _name = 'report.nieve_cost_sheet.forecasting_report_xlsx'
    _inherit = 'report.report_xlsx.abstract'


    def qwery_group_kk_cost_sheet(self,month,year_period):
        query = """ 
                        select  v_inv.cs_cost_sheet_id,
                            v_inv.date_month,
                            v_inv.year_period
                    from v_invoice_line_term_line v_inv
                    where v_inv.date_month = '%s' and v_inv.year_period = '%s'
                    group by cs_cost_sheet_id,
                            date_month,year_period
                            order by date_month asc
                            """ % (month,year_period)
        self.env.cr.execute(query)
        x = self.env.cr.dictfetchall()
        return x

    
    def qwery_month_years_acc_inv_out(self):
        self.env.cr.execute("""  
                        select 
                        ai.id,
                            ai.kk_cost_sheet_id,
                            rp.name name_vendor,
                            function_get_partner_name_id(ar.partner_id) as partner_name,
                            ai.date_invoice date_invoice_out,
                            ai.date date_actual_out,
                            ai.payment_date payment_date_out,
                            ai.amount_untaxed amount_untaxed_out,
                            to_char(ai.date_invoice,'mm') date_month,
                            to_char(ai.date_invoice,'yyyy') year_period,
                            ai.origin no_po,
                            ai.so_id no_so
                        from account_invoice ai
                            where ai.type = 'in_invoice' and ai.state ='open'
                            """)
        x = self.env.cr.dictfetchall()
        return x


    def qwery_kk_cost_sheet(self,month,year):
        self.env.cr.execute(""" 
            select  * from
                    (select 
                        cs_cost_sheet_id,
                        kcs.name,
                        kcs.project_name,
                        function_get_partner_name_id(kcs.partner_id) as partner_name,
                        fitl.name desc_name,
                        fitl.percentage,
                        fitl.date,
                        fitl.total_inv,
                        fitl.invoice_id,
                        to_char(fitl.date,'mm') date_month,
                        to_char(fitl.date,'yyyy') year_period,
                        fitl.fal_sale_order_id no_so,
                        kcs.state
                    from fal_invoice_term_line fitl
                    left join kk_cost_sheet kcs
                    on kcs.id = fitl.cs_cost_sheet_id
                    where kcs.state ='post'
                    order by kcs.name asc) cost_sheet
                    left join
                    (select 
                        ai.id,
                        ai.kk_cost_sheet_id,
                        ai.date_invoice,
                        ai.payment_date,
                        ai.amount_untaxed,
                        to_char(ai.date_invoice,'mm') date_month_in,
                        to_char(ai.date_invoice,'yyyy') year_period_in
                        from account_invoice ai
                            where ai.type = 'out_invoice' and ai.state in ('open','paid')) invoice_in
                    on cost_sheet.invoice_id = invoice_in.id
                    where date_month = '%s' and year_period = '%s'
                    order by name,date
        """ % (month,year))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_val_inv_out(self,month_period_out,kk_cost_sheet_id,year_period_out):
        self.env.cr.execute(""" 
          select 
                        ai.id,
                            ai.kk_cost_sheet_id,
                            function_get_partner_name_id(ai.partner_id) as name_vendor,
                            function_get_partner_name_id(ai.partner_id) as partner_name,
                            ai.date_invoice date_invoice_out,
                            ai.date date_actual_out,
                            ai.payment_date payment_date_out,
                            ai.amount_untaxed amount_untaxed_out,
                            to_char(ai.date_invoice,'mm') date_month_out,
                            to_char(ai.date_invoice,'yyyy') year_period_out,
                            ai.origin no_po,
                            ai.so_id no_so,
                            ai.currency_id,
                            rc.name currency,
                            po.amount_untaxed amount_untaxed_po
                        from account_invoice ai
                        left join res_currency rc
                        on rc.id = ai.currency_id
                        left join purchase_order po
                        on po.name = ai.origin
                        where ai.type = 'in_invoice' 
                        and ai.state in ('open','paid') 
                        and to_char(ai.date_invoice,'mm') = '%s'
                        and ai.kk_cost_sheet_id = %s
                        and to_char(ai.date_invoice,'yyyy') = '%s'
                        order by ai.date_invoice asc
                            
        """ % (month_period_out,str(kk_cost_sheet_id),year_period_out))
        x = self.env.cr.dictfetchall()
        return x
        
    
    def qwery_v_invoice_line_term_line(self,date_month,cs_cost_sheet_id,year_period):
        self.env.cr.execute(""" 
        select count(cs_cost_sheet_id),
                cs_cost_sheet_id,date_month,
                year_period from 
                v_invoice_line_term_line
                where date_month = '%s' and cs_cost_sheet_id = %s and year_period = '%s'
                    group by cs_cost_sheet_id,date_month,year_period
                            
        """ %(date_month,cs_cost_sheet_id,year_period))
        x = self.env.cr.dictfetchall()
        return x

    def generate_sheet(self,workbook,val_month,date_val):
        ##Style
        company_header_style = workbook.add_format({'bold': True, 'font_color': 'black'})
        company_header_style.set_font_size(16)
        company_header_style.set_align('left')
        
        report_label_header_style = workbook.add_format({'bold': True, 'font_color': '#000080'})
        report_label_header_style.set_font_size(26)
        report_label_header_style.set_align('center')
        
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        number = workbook.add_format({'bold': False, 'font_color': 'black'})
        number.set_num_format('#,##0.00;(#,##0.00)')
        
        string_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold.set_num_format('#,##0.00;(#,##0.00)')
        percentage  = workbook.add_format({'num_format': '0.0%'})
        
        table_header_style = workbook.add_format({'bold': True, 'font_color': '#B22222'})
        table_header_style.set_align('center')
        table_header_style.set_bottom(1)
        title                   = workbook.add_format({'bold': True,'font_size': 14,'align':'center'})
        title_status            = workbook.add_format({'bold': True,'font_size': 14,'align':'left'})
        header_table            = workbook.add_format({'bold': True, 'bg_color': '#CCCC00'})
        header                  = workbook.add_format({'font_size': 11,'align':'center','bottom':6,'top':6,'left':6,'right':6,})
        header_conv_bold        = workbook.add_format({'bold': True,'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,})
        header_subtotal_bold    = workbook.add_format({'bold': True,'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,})
        terbilang               = workbook.add_format({'bold': True,'font_size': 11,'align':'left','bottom':2,'top':2,'left':2,'right':2,})
        header_totalidr_bold    = workbook.add_format({'bold': True,'font_size': 11,'align':'center','bottom':6,'top':6,'left':6,'right':6,})
        header_grandtotal_bold  = workbook.add_format({'bold': True,'font_size': 11,'align':'right','bottom':6,'top':6,'left':6,'right':6,})
        header_terbilang_bold   = workbook.add_format({'bold': True,'font_size': 11,'align':'left','bottom':6,'top':6,'left':6,'right':6,})
        bold                    = workbook.add_format({'bold': True})
        f2d_bold                = workbook.add_format({'num_format': '#,##0.00_);(#,##0.00)','bold': True})
        f2d                     = workbook.add_format({'num_format': '#,##0.00_);(#,##0.00)','font_size': 11, 'border': True,'align':'right'})
        border_all              = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,'align':'center'})
        company                 = workbook.add_format({'bold': True, 'font_size': 11, 'font_color':'#0000FF','align':'center'})
        Type_report             = workbook.add_format({'bold': True, 'font_size': 12,'align':'center'})
        periode_tahun           = workbook.add_format({'bold': True, 'font_size': 11,'align':'center'})
        information             = workbook.add_format({'bold': True, 'font_size': 11,'align':'center'})
        parent                  = workbook.add_format({'bold': True,'font_size': 11,'align':'left'})
        balance_parent          = workbook.add_format({'bold': True,'font_size': 11,'align':'right'})
        child                   = workbook.add_format({'font_size': 11,'align':'left'})
        value_now               = workbook.add_format({'font_size': 11,'align':'right','num_format':'#,##0.00'})
        value_qty               = workbook.add_format({'font_size': 11,'align':'center','num_format':'#,##0.00'})
        value_valuee            = workbook.add_format({'font_size': 11,'align':'right','num_format':'#,##0.00','bottom':2,'top':2,'left':2,'right':2,})
        value_now_border        = workbook.add_format({'font_size': 11,'align':'right','bottom':6,'top':6,'left':6,'right':6,'num_format':'#,##0.00'})
        value_now_border_bold   = workbook.add_format({'bold': True,'font_size': 11,'align':'right','bottom':6,'top':6,'left':6,'right':6,'num_format':'#,##0.00'})
        border_all              = workbook.add_format({'font_size': 11,'align':'center','bottom':6,'top':6,'left':6,'right':6,})
        border_all_header       = workbook.add_format({'bg_color': '#ADD8E6','bold': True,'font_size': 13,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        border_all_header_none       = workbook.add_format({'bold': True,'font_size': 13,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        border_all_header.set_align('center')
        border_all_header.set_align('vcenter')
        border_all_header_blue       = workbook.add_format({'bg_color': '#E6E6FA','bold': True,'font_size': 13,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        border_all_header_red       = workbook.add_format({'bg_color': '#FFEFD5','bold': True,'font_size': 13,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        border_all_header_yel      = workbook.add_format({'bg_color': '#FFFF00','bold': True,'font_size': 13,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        border_all_header_right       = workbook.add_format({'num_format':'#,##0.00','bold': True,'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,})
        balance_child           = workbook.add_format({'font_size': 11,'align':'right'})
        date                    = workbook.add_format({'bold': True, 'font_size': 11,'align':'center'})
        date2                   = workbook.add_format({'bold': True, 'font_size': 11,'align':'center','bottom':6})
        border_all_line         = workbook.add_format({'font_size': 11, 'border': True,'align':'left'})
        border_all_center       = workbook.add_format({'font_size': 11, 'border': True,'align':'center'})
        border_all_right        = workbook.add_format({'font_size': 11, 'border': True,'align':'right'})
        sign                    = workbook.add_format({'font_size': 11,'align':'center','bold': True})
        sign_thin               = workbook.add_format({'font_size': 11,'align':'center' })
        number                  = workbook.add_format({'font_size': 11,'bottom':2,'top':2,'left':2,'right':2,'align':'center'})
        normal                  = workbook.add_format({'font_size': 11,'bottom':6,'top':6,'left':6,'right':6})
        string_total_bold   = workbook.add_format({'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,'num_format':'#,##0.00','bold':True})
        string                  = workbook.add_format({'font_size': 11,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        string_value_right                  = workbook.add_format({'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,'num_format':'#,##0.00'})
        string_no               = workbook.add_format({'font_size': 11,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        string_no.set_align('center')
        string_no.set_align('top')
        string_left             = workbook.add_format({'font_size': 11,'align':'left','bottom':2,'top':2,'left':2,'right':2,})
        string_bank             = workbook.add_format({'font_size': 11,'align':'center','bottom':6,'top':6,'left':6,'right':6,})
        string_total            = workbook.add_format({'font_size': 11,'align':'right','bottom':6,'top':6,'left':6,'right':6,})
        string_total        = workbook.add_format({'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,'num_format':'#,##0.00'})
        string_value            = workbook.add_format({'font_size': 11,'align':'right','bottom':6,'top':6,'left':6,'right':6,})
        total_label             = workbook.add_format({'font_size': 11})
        total_inc               = workbook.add_format({'num_format': '#,##0.00_);(#,##0.00)','font_size': 11})
        footer                  = workbook.add_format({'font_size': 11})

        sheet = workbook.add_worksheet(val_month)
        sheet.hide_gridlines(2)
        sheet.show_grid = 0
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.portrait = 0  # Landscape
        sheet.fit_width_to_pages = 1
        
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 30)  # Source No.
        sheet.set_column('C:C', 40)  # Source No.
        sheet.set_column('D:D', 20)  # Source No.
        sheet.set_column('E:E', 20)  # Source No.
        sheet.set_column('F:F', 20)  # Source No.
        sheet.set_column('G:G', 20)  # Source No.
        sheet.set_column('H:H', 20)  # Source No.
        sheet.set_column('I:I', 20)  
        sheet.set_column('J:J', 20)  
        sheet.set_column('K:K', 30)  # Source No.
        sheet.set_column('L:L', 25)  # Source No.
        sheet.set_column('M:M', 25)  # Source No.
        sheet.set_column('N:N', 25)  # Source No.
        sheet.set_column('O:O', 25)  # Source No.
        sheet.set_column('P:P', 25)  # Source No.
        sheet.set_column('Q:Q', 20)  # Source No.
        sheet.set_column('R:R', 20)  # Source No.
        sheet.set_column('S:S', 20)  # Source No.
        
        
        sheet.write('A1', 'Forecasting Report', company_header_style)      
            
        sheet.merge_range('A3:A4', 'No. CS', border_all_header)
        sheet.merge_range('B3:B4', 'Project Name', border_all_header)
        sheet.merge_range('C3:C4', 'Customer', border_all_header)
        sheet.merge_range('D3:S3', date_val, border_all_header)
        sheet.write('D4', 'Desc', border_all_header_blue)
        sheet.write('E4', 'Est.Invoice Date', border_all_header_blue)
        sheet.write('F4', 'Est. Cash In', border_all_header_blue)
        sheet.write('G4', 'Actual Invoice Date', border_all_header_yel)
        sheet.write('H4', 'Invoice Value (IDR)', border_all_header_yel)
        sheet.write('I4', 'Actual Paid Date', border_all_header_yel)
        sheet.write('J4', 'Cash In', border_all_header_yel)
        sheet.write('K4', 'Vendor', border_all_header_red)
        sheet.write('L4', 'Est. Payment Vendor', border_all_header_red)
        sheet.write('M4', 'Est. Vendor Bill (IDR)', border_all_header_red)
        sheet.write('N4', 'Est. Vendor Bill (USD)', border_all_header_red)
        sheet.write('O4', 'Actual Vendor bill (IDR)', border_all_header_red)
        sheet.write('P4', 'Actual Vendor bill (USD)', border_all_header_red)
        sheet.write('Q4', 'Actual Paid Date', border_all_header_red)
        sheet.write('R4', 'Cash Out (IDR)', border_all_header_red)
        sheet.write('S4', 'Cash Out (USD)', border_all_header_red)
        return sheet
    
    def generate_xlsx_report(self, workbook, data, wizard):
        
        print ("##generate_xlsx_report")
                
        year_period =wizard.year_period
        cs_obj = self.env['kk.cost.sheet']
        data_cs = cs_obj.search([('state','in',['post','close'])])
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        string_value_left                  = workbook.add_format({'font_size': 11,'align':'left','bottom':2,'top':2,'left':2,'right':2,'num_format':'#,##0.00'})
        string_value_center                  = workbook.add_format({'font_size': 11,'align':'center','bottom':2,'top':2,'left':2,'right':2,'num_format':'#,##0.00'})
        string_value_right                  = workbook.add_format({'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,'num_format':'#,##0.00'})
        border_all_header_none       = workbook.add_format({'bold': True,'font_size': 13,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        border_all_header_right       = workbook.add_format({'num_format':'#,##0.00','bold': True,'font_size': 11,'align':'right','bottom':2,'top':2,'left':2,'right':2,})
        string_value_left.set_align('left')
        string_value_left.set_align('top')
        string_value_center_cs = workbook.add_format({'font_size': 11,'align':'center','bottom':2,'top':2,'left':2,'right':2,'num_format':'#,##0.00'})
        string_value_center_cs.set_align('center')
        string_value_center_cs.set_align('top')        
        
        
        month = ['01','02','03','04','05','06','07','08','09','10','11','12']
        last_code=0
        last_cost_sheet_id = 0
        sumamount_total_inv_1 =0.0
        amount_untaxed_po_idr_1 =0.0
        amount_untaxed_po_usd_1 =0.0
        amount_untaxed_out_idr_1 =0.0
        amount_untaxed_out_usd_1 =0.0
        sumamount_untaxed_out_idr_1 =0.0
        sumamount_untaxed_out_usd_1 =0.0
        sumamount_untaxed_1=0.0
        last_date = ''
        for i in month:
            val_month = 'Month January' if i == '01' else 'Month Feburary' if i == '02' else 'Month March' if i == '03' else 'Month April' if i == '04' else 'Month May' if i == '05' else 'Month June' if i == '06' else 'Month July' if i == '07' else 'Month Agust' if i == '08' else 'Month September' if i == '09' else 'Month October' if i == '10' else 'Month November' if i == '11' else 'Month December'
            date_val = 'Jan' if i == '01'  else 'Feb' if i == '02' else 'Mar' if i == '03' else 'Apr' if i == '04' else 'May' if i == '05' else 'June' if i == '06' else 'July' if i == '07' else 'Ags' if i == '08' else 'Sep' if i == '09' else 'Oct' if i == '10' else 'Nov' if i == '11' else 'Des' 
            sheet = self.generate_sheet(workbook,val_month,date_val)
            obj_kk = self.qwery_kk_cost_sheet(i,year_period)
            val_date_trx_before = ''
            row = 5
            date_invoice = ''
            payment_date = ''
            amount_untaxed = ''
            name_vendor = ''
            date_invoice_out = ''
            date_actual_out = ''
            payment_date_out = ''
            amount_untaxed_out = ''
            val_date_before = ''
            sumamount_untaxed = 0.0
            sumamount_total_inv = 0.0
            amount_untaxed_po_usd_inv_inv_out =0.0
            amount_untaxed_po_idr_inv_inv_out=0.0
            amount_untaxed_out_idr_inv_inv_out =0.0
            amount_untaxed_out_usd_inv_inv_out=0.0
            sumamount_untaxed_out_idr_inv_inv_out =0.0
            sumamount_untaxed_out_usd_inv_inv_out=0.0
            
            
            amount_untaxed_po_usd_inv_inv_out_1 =0.0
            amount_untaxed_po_idr_inv_inv_out_1 =0.0
            for a in obj_kk:   
                if not a.get('invoice_id'):      
                    sheet.write('K'+str(row), '', string_value_left)
                    sheet.write('L'+str(row), '', string_value_left)
                    sheet.write('M'+str(row), '', string_value_left)
                    sheet.write('N'+str(row), '', string_value_left)
                    sheet.write('O'+str(row), '', string_value_left)
                    sheet.write('P'+str(row), '', string_value_left)
                    sheet.write('Q'+str(row), '', string_value_left)
                    sheet.write('R'+str(row), '', string_value_left)
                    sheet.write('S'+str(row), '', string_value_left)  
                 
                amount_untaxed_po_idr =0.0
                
                
                
                
                for o in self.qwery_v_invoice_line_term_line(i,str(a.get('cs_cost_sheet_id')),str(year_period)):
                    print ('cost',o.get('cs_cost_sheet_id'))
                    sheet.write('K'+str(row), '', string_value_left)
                    sheet.write('L'+str(row), '', string_value_left)
                    sheet.write('M'+str(row), '', string_value_left)
                    sheet.write('N'+str(row), '', string_value_left)
                    sheet.write('O'+str(row), '', string_value_left)
                    sheet.write('P'+str(row), '', string_value_left)
                    sheet.write('Q'+str(row), '', string_value_left)
                    sheet.write('R'+str(row), '', string_value_left)
                    sheet.write('S'+str(row), '', string_value_left)
                    
                    amount_untaxed_po_usd =0.0
                    amount_untaxed_out_idr =0.0
                    amount_untaxed_out_usd =0.0
                    sumamount_untaxed_out_idr = 0.0
                    sumamount_untaxed_out_usd = 0.0
                    for item in self.qwery_val_inv_out(i,o.get('cs_cost_sheet_id'),year_period):
                        name_vendor = item.get('name_vendor')
                        date_invoice_out = item.get('date_invoice_out')
                        date_actual_out = item.get('date_actual_out')
                        payment_date_out = item.get('payment_date_out')
                        amount_untaxed_out = item.get('amount_untaxed_out') or ''
                        sheet.write('K'+str(row), name_vendor, string_value_left)
                        sheet.write('L'+str(row), date_invoice_out, string_value_center)
                        sheet.write('M'+str(row), '', string_value_right)
                        sheet.write('O'+str(row), '', string_value_right)
                        sheet.write('R'+str(row), '', string_value_right)
                        if item.get('currency')=='IDR':
                            amount_untaxed_po = item.get('amount_untaxed_po') or 0.0
                            amount_untaxed_po_idr +=item.get('amount_untaxed_po') or 0.0
                            amount_untaxed_po_idr_1 +=item.get('amount_untaxed_po') or 0.0
                            sheet.write('M'+str(row), amount_untaxed_po, string_value_right)
                        if item.get('currency')!='IDR':
                            amount_untaxed_po = item.get('amount_untaxed_po') or 0.0
                            amount_untaxed_po_usd +=item.get('amount_untaxed_po') or 0.0
                            amount_untaxed_po_usd_1 +=item.get('amount_untaxed_po') or 0.0
                            print ('usd_amount_untaxed_po',amount_untaxed_po)
                            print ('usd_amount_untaxed_po2',amount_untaxed_po_usd)
                            sheet.write('N'+str(row), amount_untaxed_po, string_value_right)
                        if item.get('currency')=='IDR':
                            amount_untaxed_out_idr +=item.get('amount_untaxed_out') or 0.0
                            amount_untaxed_out_idr_1 +=item.get('amount_untaxed_out') or 0.0
                            sheet.write('O'+str(row), amount_untaxed_out, string_value_right)
                        if item.get('currency')!='IDR':
                            amount_untaxed_out_usd +=item.get('amount_untaxed_out') or 0.0
                            amount_untaxed_out_usd_1 +=item.get('amount_untaxed_out') or 0.0
                            sheet.write('P'+str(row), amount_untaxed_out, string_value_right)
                        sheet.write('Q'+str(row), payment_date_out, string_value_center)
                        if item.get('payment_date_out'):
                            if item.get('currency')=='IDR':
                                sheet.write('R'+str(row), amount_untaxed_out, string_value_right)
                                sumamount_untaxed_out_idr += item.get('amount_untaxed_out') or 0.0
                                sumamount_untaxed_out_idr_1 += item.get('amount_untaxed_out') or 0.0
                            if item.get('currency')!='IDR':
                                sheet.write('S'+str(row), amount_untaxed_out, string_value_right)
                                sumamount_untaxed_out_usd += item.get('amount_untaxed_out') or 0.0
                                sumamount_untaxed_out_usd_1 += item.get('amount_untaxed_out') or 0.0
                        amount_untaxed_po_usd_inv_inv_out = amount_untaxed_po_usd 
                        amount_untaxed_po_idr_inv_inv_out = amount_untaxed_po_idr
                        amount_untaxed_out_idr_inv_inv_out = amount_untaxed_out_idr
                        amount_untaxed_out_usd_inv_inv_out = amount_untaxed_out_usd
                        sumamount_untaxed_out_idr_inv_inv_out = sumamount_untaxed_out_idr
                        sumamount_untaxed_out_usd_inv_inv_out = sumamount_untaxed_out_usd
                    row3 = o.get('count')
                    if o.get('count') > 1:
                        sheet.merge_range('A'+str(row)+str(':')+'A'+str(row + row3 - 1),a.get('name') or '', string_value_center_cs)
                        sheet.merge_range('B'+str(row)+str(':')+'B'+str(row + row3 - 1),a.get('project_name') or '', string_value_left)
                        sheet.merge_range('C'+str(row)+str(':')+'C'+str(row + row3 - 1),a.get('partner_name') or '', string_value_left)
                    else:
                        sheet.write('A'+str(row),a.get('name') or '', string_value_center_cs)    
                        sheet.write('B'+str(row), a.get('project_name') or '', string_value_left)
                        sheet.write('C'+str(row), a.get('partner_name') or '', string_value_left)                  
                sheet.write('D'+str(row), a.get('desc_name') or '', string_value_left)
                sheet.write('E'+str(row), a.get('date') or '', string_value_center)
                sheet.write('F'+str(row), a.get('total_inv') or '', string_value_right)
                date_invoice = a.get('date_invoice')
                payment_date = a.get('payment_date')
                if a.get('payment_date'):
                    amount_untaxed = a.get('amount_untaxed')
                    amount_untaxed_sum = a.get('amount_untaxed')
                else:
                    amount_untaxed = ''
                    amount_untaxed_sum = 0.0
                sumamount_untaxed += amount_untaxed_sum
                sumamount_untaxed_1 +=amount_untaxed_sum
                sumamount_total_inv += a.get('total_inv')
                sumamount_total_inv_1 += a.get('total_inv')
                sheet.write('G'+str(row), date_invoice, string_value_center)
                sheet.write('H'+str(row), a.get('amount_untaxed'), string_value_right)
                sheet.write('I'+str(row), payment_date, string_value_center)
                sheet.write('J'+str(row), amount_untaxed, string_value_right)
                        
                row +=1     

            ###Rows Sum 1###      
            sheet.merge_range('A'+str(row)+str(':')+'E'+str(row), 'Total Monthly', border_all_header_none)                    
            sheet.write('F'+str(row),str('{:20,.2f}'.format(sumamount_total_inv)), border_all_header_right)
            sheet.write('G'+str(row),'', border_all_header_right)
            sheet.write('H'+str(row),str('{:20,.2f}'.format(sumamount_untaxed)), border_all_header_right)
            sheet.write('I'+str(row),'', border_all_header_right)
            sheet.write('J'+str(row),str('{:20,.2f}'.format(sumamount_untaxed)), border_all_header_right)
            sheet.write('K'+str(row),'', border_all_header_right)
            sheet.write('L'+str(row),'', border_all_header_right)
            sheet.write('M'+str(row),str('{:20,.2f}'.format(amount_untaxed_po_idr_inv_inv_out)), border_all_header_right)
            sheet.write('N'+str(row),str('{:20,.2f}'.format(amount_untaxed_po_usd_inv_inv_out)), border_all_header_right)
            sheet.write('O'+str(row),str('{:20,.2f}'.format(amount_untaxed_out_idr_inv_inv_out)), border_all_header_right)
            sheet.write('P'+str(row),str('{:20,.2f}'.format(amount_untaxed_out_usd_inv_inv_out)), border_all_header_right)
            sheet.write('Q'+str(row),'', border_all_header_right)
            sheet.write('R'+str(row),str('{:20,.2f}'.format(sumamount_untaxed_out_idr_inv_inv_out)), border_all_header_right)
            sheet.write('S'+str(row),str('{:20,.2f}'.format(sumamount_untaxed_out_usd_inv_inv_out)), border_all_header_right)
            ###Rows Sum 2###  
            sheet.merge_range('A'+str(row+1)+str(':')+'E'+str(row+1), 'Total Year to Date', border_all_header_none)                    
            sheet.write('F'+str(row+1),str('{:20,.2f}'.format(sumamount_total_inv_1)), border_all_header_right)
            sheet.write('G'+str(row+1),'', border_all_header_right)
            sheet.write('H'+str(row+1),str('{:20,.2f}'.format(sumamount_untaxed_1)), border_all_header_right)
            sheet.write('I'+str(row+1),'', border_all_header_right)
            sheet.write('J'+str(row+1),str('{:20,.2f}'.format(sumamount_untaxed_1)), border_all_header_right)
            sheet.write('K'+str(row+1),'', border_all_header_right)
            sheet.write('L'+str(row+1),'', border_all_header_right)
            sheet.write('M'+str(row+1),str('{:20,.2f}'.format(amount_untaxed_po_idr_1)), border_all_header_right)
            sheet.write('N'+str(row+1),str('{:20,.2f}'.format(amount_untaxed_po_usd_1)), border_all_header_right)
            sheet.write('O'+str(row+1),str('{:20,.2f}'.format(amount_untaxed_out_idr_1)), border_all_header_right)
            sheet.write('P'+str(row+1),str('{:20,.2f}'.format(amount_untaxed_out_usd_1)), border_all_header_right)
            sheet.write('Q'+str(row+1),'', border_all_header_right)
            sheet.write('R'+str(row+1),str('{:20,.2f}'.format(sumamount_untaxed_out_idr_1)), border_all_header_right)
            sheet.write('S'+str(row+1),str('{:20,.2f}'.format(sumamount_untaxed_out_usd_1)), border_all_header_right)
            
            
            
            sheet.merge_range('A'+str(row+2)+str(':')+'C'+str(row+2), 'Total', border_all_header_none)
            sheet.merge_range('D'+str(row+2)+str(':')+'J'+str(row+2), str('Rp. ')+str('{:20,.2f}'.format(sumamount_untaxed)), border_all_header_right)
            sheet.merge_range('K'+str(row+2)+str(':')+'R'+str(row+2), str('Rp. ')+str('{:20,.2f}'.format(sumamount_untaxed_out_idr_inv_inv_out)), border_all_header_right)
            sheet.write('S'+str(row+2), str('USD')+str('{:20,.2f}'.format(sumamount_untaxed_out_usd_inv_inv_out)), border_all_header_right)
            total_cash_in = sumamount_untaxed - sumamount_untaxed_out_idr_inv_inv_out
            sheet.merge_range('D'+str(row+3)+str(':')+'R'+str(row+3), str('Rp. ')+str('{:20,.2f}'.format(total_cash_in)), border_all_header_right)
            sheet.write('S'+str(row+2),'', border_all_header_right)
            sumamount_untaxed = 0.0
            sumamount_untaxed_out = 0.0

