# -*- coding: utf-8 -*-
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo import models, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
import time

class CostSheetSalesDirectorReportXlsx(models.AbstractModel):
    print ("###CostSheetSalesDirectorReportXlsx###")
    _name = 'report.nieve_cost_sheet.report_cost_sheet_sales_director_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, wizard):
        
        print ("##generate_xlsx_report")
        
        sheet = workbook.add_worksheet('Cost Sheet Sales Director Reports')
        sheet.hide_gridlines(2)
        #######
        
        ###
        cs_obj = self.env['kk.cost.sheet']
        data_cs = cs_obj.search([('state','in',['post','close'])])
        ###
        
        ##Style
        company_header_style = workbook.add_format({'bold': True, 'font_color': 'black'})
        company_header_style.set_font_size(16)
        company_header_style.set_align('center')
        
        report_label_header_style = workbook.add_format({'bold': True, 'font_color': '#000080'})
        report_label_header_style.set_font_size(26)
        report_label_header_style.set_align('center')
        
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        number = workbook.add_format({'bold': False, 'font_color': 'black'})
        number.set_num_format('#,##0.00;(#,##0.00)')
        number.set_border(1)
        number.set_align('top')
        
        string_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold.set_num_format('#,##0.00;(#,##0.00)')
        percentage  = workbook.add_format({'num_format': '0.0%'})
        
        table_header_style = workbook.add_format({'bold': True})
        table_header_style.set_align('center')
        table_header_style.set_align('top')
        table_header_style.set_border(1)
        
        ##
        
        sheet.show_grid = 0
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.portrait = 0  # Landscape
        sheet.fit_width_to_pages = 1
        
        sheet.set_column('A:A', 5)
        sheet.set_column('B:J', 15)  # Source No.
        
        #sheet.write(3, 0, 'Date Range filter : ',)
        
        row = 5
        
        sheet.merge_range('A3:J4', 'Dashboard Sales Director', company_header_style)
        
        sheet.merge_range(row, 0, row+1, 0, 'No.', table_header_style)
        sheet.merge_range(row, 1, row+1, 1, 'Year.', table_header_style)
        sheet.merge_range(row, 2, row+1, 2, 'Project Name.', table_header_style)
        sheet.merge_range(row, 3, row+1, 3, 'Sales.', table_header_style)
        sheet.merge_range(row, 4, row+1, 4, 'No.CS', table_header_style)
        sheet.merge_range(row, 5, row+1, 5, 'Total Sales', table_header_style)
        sheet.merge_range(row, 6, row+1, 6, 'GP', table_header_style)
        sheet.merge_range(row, 7, row, 9, 'Customer Invoice Status', table_header_style)
        sheet.write(row+1, 7, 'Termin', table_header_style)
        sheet.write(row+1, 8, 'Amount', table_header_style)
        sheet.write(row+1, 9, 'Status', table_header_style)
        
        row += 2
        
        no = 0
        for cs in data_cs:
            no += 1
            print ("cs---->", cs)
            
            merge_cell_count = len(cs.fal_invoice_milestone_line_date_ids) > 1 and len(cs.fal_invoice_milestone_line_date_ids)-1 or 0
            
            if merge_cell_count < 1:
                sheet.write(row, 0, no, )
                sheet.write(row, 1, time.strftime('%Y', time.strptime(cs.kk_sale_order_id.date_order, '%Y-%m-%d %H:%M:%S')),number)
                sheet.write(row, 2, cs.project_name or "", number)
                sheet.write(row, 3, cs.creator_user_id.name or "", number)
                sheet.write(row, 4, cs.name or "", number)
                sheet.write(row, 5, cs.amount_sales or "", number)
                sheet.write(row, 6, cs.amount_gross_profit or "", number)
            else:
                sheet.merge_range(row, 0, row+merge_cell_count, 0, no, )
                sheet.merge_range(row, 1, row+merge_cell_count, 1, time.strftime('%Y', time.strptime(cs.kk_sale_order_id.date_order, '%Y-%m-%d %H:%M:%S')),number)
                sheet.merge_range(row, 2, row+merge_cell_count, 2, cs.project_name or "", number)
                sheet.merge_range(row, 3, row+merge_cell_count, 3, cs.creator_user_id.name or "", number)
                sheet.merge_range(row, 4, row+merge_cell_count, 4, cs.name or "", number)
                sheet.merge_range(row, 5, row+merge_cell_count, 5, cs.amount_sales or "", number)
                sheet.merge_range(row, 6, row+merge_cell_count, 6, cs.amount_gross_profit or "", number)
            
            ##Loop Termins
            if not cs.fal_invoice_milestone_line_date_ids:
                row += 1
            for top in cs.fal_invoice_milestone_line_date_ids:
                sheet.write(row, 7, top.name or "", number)
                sheet.write(row, 8, top.total_inv or "", number)
                sheet.write(row, 9, top.invoice_id and "Invoiced" or "Not Invoiced Yet" or "", number)
                row += 1
#             sheet.write(row, 2, cs.partner_id.name or "",)
#              
#             sheet.write(row, 4, cs.kk_sale_order_id.name or "",)
#             sheet.write(row, 5, cs.name or "",)
#              
#             COGS = (cs.amount_total + cs.amount_expense_plan + cs.cof_total)
#             sheet.write(row, 6, COGS or "", number)
#             sheet.write(row, 7, cs.amount_sales or "", number)
#             sheet.write(row, 8, cs.percent_gross_profit/100 or "",percentage)
#             sheet.write(row, 9, cs.amount_gross_profit or "", number)
#              
#             for l in cs.kk_cost_sheet_line_ids:
#                 sheet.write(row, 3, l.product_id.name or "",)
#                 row += 1
#             if len(cs.kk_cost_sheet_line_ids) >= 1:
#             row += 3
#              
            
