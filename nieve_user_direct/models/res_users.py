import json
import re
import uuid
from functools import partial

from lxml import etree
from dateutil.relativedelta import relativedelta
from werkzeug.urls import url_encode

from odoo import api, exceptions, fields, models, _
from odoo.tools import float_is_zero, float_compare, pycompat
from odoo.tools.misc import formatLang

from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

from odoo.addons import decimal_precision as dp
import logging

class ResUsers(models.Model):
    _inherit = 'res.users'
    
    #password_dummy  = fields.Char(string="Password Direct")
    password_lock   = fields.Selection([('lock','Lock'),('unlock','Unlock')], string='Password Lock Status', default='lock')
    
    @api.multi
    def open_password(self):
        self.env.cr.execute('UPDATE res_users set password = %s,password_lock = %s where id = %s', ('nieve_pass', 'unlock', self.id))
    
    @api.multi
    def close_password(self):
        self.env.cr.execute('UPDATE res_users set password = %s,password_lock = %s where id = %s', ('', 'lock', self.id))