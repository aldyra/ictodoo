from openerp import api, fields, models, _
from datetime import datetime
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.depends(
        'fal_invoice_milestone_line_date_ids.percentage')
    def _amount_percent(self):
        """
        Compute the total amounts of the Cost Sheet.
        """
        for order in self:
            amount_all = 0.0
            for line in order.fal_invoice_milestone_line_date_ids:
                amount_all += line.percentage
            order.update({
                'total_percentage': amount_all,
            })

    kk_invoice_term_id = fields.Many2one('fal.invoice.term', 'Invoice Term')
    fal_invoice_milestone_line_date_ids = fields.One2many(
        'fal.invoice.term.line', 'fal_sale_order_id',
        string="Term Lines", copy=True)
    revisi_count = fields.Integer(string='Revision', readonly=True, copy=False)
    total_percentage = fields.Float(
        string='Total Percentage',
        compute='_amount_percent')
    state = fields.Selection(
        [
            ('draft', 'Quotation'),
            ('sent', 'Quotation Sent'),
            ('appr_mgr', 'Approval Manager'),
            ('appr_gm', 'Approval G.Manager'),
            ('appr_drc', 'Approval Director'),
            ('sale', 'Sales Order'),
            ('done', 'Locked'),
            ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False,
        index=True, track_visibility='onchange', default='draft')
    kk_week_closed = fields.Char('Quarter & Week Closed')
    kk_lead_by = fields.Char('Partner/ Pricipal/ ICT')

    kk_pr_name = fields.Char('Name')
    kk_pr_sales_rep = fields.Char('Sales Rep')
    kk_pr_phone = fields.Char('Phone')
    kk_pr_email = fields.Char('E-mail')

    kk_dist_name = fields.Char('Name')
    kk_dist_sales_rep = fields.Char('Sales Rep')
    kk_dist_phone = fields.Char('Phone')
    kk_dist_email = fields.Char('E-mail')

    kk_update = fields.Char('Update Status')
    kk_things = fields.Char('Things To Do')
    kk_state_act = fields.Char('Status')

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            vals['name'] = _('Draft')
        result = super(SaleOrder, self).create(vals)
        return result

    @api.multi
    def check_total_top(self):
        total_top = 0.0
        for top in self.fal_invoice_milestone_line_date_ids:
            total_top += top.total_inv
        
        if round(self.amount_untaxed,2) != round(total_top,2):
            raise UserError(_(
                "Total Term of Payment should be equal with Total Sales."))

    @api.multi
    def action_confirm_user(self):
        self.check_total_top()
        total_inv = 0
        for inv in self.fal_invoice_milestone_line_date_ids:
            total_inv += inv.total_inv

        self.name = self.env['ir.sequence'].next_by_code('sale.order')
        return self.write({'state': 'appr_mgr'})

    @api.multi
    def action_confirm_manager(self):
        if (self.kk_cost_sheet_id.percent_gross_profit or 0.0) <= self.company_id.gm_cs_approval_gp:
            return self.write({'state': 'appr_gm'})
        else:
            return self.action_confirm()
        
        
    @api.multi
    def action_confirm_general_manager(self):
        context = self._context
        login_uid = context.get('uid')
        
        cs_admin_group = self.env.user.has_group('nieve_cost_sheet.group_costsheet_admin')
        
        if self.team_id.gm_user_id.id != login_uid and not cs_admin_group:
            raise UserError(_("This Cost Sheet need Approval from %s") % self.team_id.gm_user_id.name)
        #
        if (self.kk_cost_sheet_id.percent_gross_profit or 0.0) <= self.company_id.drc_cs_approval_gp:
            return self.write({'state': 'appr_drc'})
        else:
            return self.action_confirm()
    
    @api.multi
    def action_confirm_director(self):
        return self.action_confirm()
        
    
    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        res = super(SaleOrder, self).action_invoice_create(grouped, final)
        # Add Invoice in the Term Line
        original_term_line = self._context.get("term_line", False)
        if original_term_line and res:
            original_term_line.invoice_id = res[0]
        return res

    @api.multi
    def action_duplicate(self):
        self.ensure_one()
        so_copy = self.copy()
        if so_copy:
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'sale.order',
                'res_id': so_copy.id,
                'context': self.env.context,
                'flags': {'initial_mode': 'edit'},
            }
        return False

    @api.multi
    def action_revision(self):
        self.ensure_one()
        so_copy = self.copy()
        so_copy.revisi_count = self.revisi_count + 1 
        name = self.name
        head, sep, tail = name.partition('#')
        so_copy.name = "%s#%s%s" % (head, 'REV', str(so_copy.revisi_count))
        if so_copy:
            sequence = self.env['ir.sequence'].search([('code', '=', 'sale.order')])
            seq_update = sequence.number_next_actual - 1
            sequence.number_next_actual = seq_update
            self.action_cancel()
            return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'sale.order',
                'res_id': so_copy.id,
                'context': self.env.context,
                'flags': {'initial_mode': 'edit'},
            }

        return False

    @api.multi
    def fill_milestone(self):
        self.fal_invoice_milestone_line_date_ids = False
        if self.kk_invoice_term_id:
            temp = []
            if self.kk_invoice_term_id.term_type == 'date':
                for line in self.kk_invoice_term_id.fal_invoice_term_line_ids:
                    total_inv = (line.percentage / 100) * self.amount_total
                    temp.append((0, 0, {
                        'fal_sale_order_id': self.id,
                        'percentage': line.percentage,
                        'total_inv': total_inv,
                        'date': line.date,
                        'sequence': line.sequence,
                        'product_id': line.product_id.id,
                        'name': line.name,
                        'description': line.description,
                        'is_final': line.is_final,
                    }))
            self.fal_invoice_milestone_line_date_ids = temp
