from openerp import api, fields, models, _
from openerp.exceptions import UserError
from datetime import datetime


class FalInvoiceTerm(models.Model):
    _name = 'fal.invoice.term'
    _description = 'Invoice Term'
    _inherit = ['mail.thread']

    active = fields.Boolean('Active', default=1)
    name = fields.Char('Name', size=64, required=1)
    sequence = fields.Integer('Sequence', default=10)
    term_type = fields.Selection(
        [
            ('date', 'By Date'),
            ('number_of_days', 'Days Interval'),
            ('date_monthly', 'Exact Date each Month'),
            ('date_yearly', 'Exact Date each Year'),
        ], 'Type', default="date")
    fal_invoice_term_line_ids = fields.One2many(
        'fal.invoice.term.line', 'fal_invoice_term_id',
        track_visibility="onchange")
    total_percentage = fields.Float(
        compute="_compute_total_percentage",
        string="Total Percentage(%)", store=1)
    interval_percentage = fields.Float(
        string="Interval Percentage(%)")
    interval_day = fields.Integer(
        string="Interval Day")
    start_day = fields.Integer(
        string="Start Day")
    product_id = fields.Many2one('product.product', string='Product')
    start_day = fields.Selection(
        [
            (1, '01'),
            (2, '02'),
            (3, '03'),
            (4, '04'),
            (5, '05'),
            (6, '06'),
            (7, '07'),
            (8, '08'),
            (9, '09'),
            (10, '10'),
            (11, '11'),
            (12, '12'),
            (13, '13'),
            (14, '14'),
            (15, '15'),
            (16, '16'),
            (17, '17'),
            (18, '18'),
            (19, '19'),
            (20, '20'),
            (21, '21'),
            (22, '22'),
            (23, '23'),
            (24, '24'),
            (25, '25'),
            (26, '26'),
            (27, '27'),
            (28, '28'),
            (29, '29'),
            (30, '30'),
            (31, '31'),


        ],
        'Start Day', default=1)
    start_month = fields.Selection(
        [
            (1, 'January'),
            (2, 'February'),
            (3, 'March'),
            (4, 'April'),
            (5, 'May'),
            (6, 'June'),
            (7, 'July'),
            (8, 'August'),
            (9, 'September'),
            (10, 'October'),
            (11, 'November'),
            (12, 'December'),
        ],
        'Start Month', default=1)

    @api.one
    @api.constrains('total_percentage')
    def _check_total_percentage(self):
        if self.term_type == 'date' or not self.term_type:
            self.term_type = 'date'
            if self.total_percentage > 100:
                raise UserError(_("Total Percentage cannot be greater than 100"))
            if self.total_percentage < 100:
                raise UserError(_("Total Percentage cannot be lower than 100"))
        else:
            if self.interval_percentage:
                if self.interval_percentage > 100:
                    raise UserError(_(
                        "Interval Percentage cannot be greater than 100"))
                if self.interval_percentage < 0:
                    raise UserError(_(
                        "Interval Percentage cannot be lower than 0"))
            else:
                raise UserError(_(
                    "Please fill Interval Percentage."))

    @api.multi
    @api.depends(
        'fal_invoice_term_line_ids',
        'fal_invoice_term_line_ids.percentage')
    def _compute_total_percentage(self):
        for invoiceTerm in self:
            temp = 0
            for line in invoiceTerm.fal_invoice_term_line_ids:
                temp += line.percentage
            invoiceTerm.total_percentage = temp

# end of FalInvoiceTerm()


class FalInvoiceTermLine(models.Model):
    _name = 'fal.invoice.term.line'
    _inherit = ['mail.thread']

    # It can connect to Invoice Term
    fal_invoice_term_id = fields.Many2one('fal.invoice.term', 'Invoice Term')
    # Or to Sale Order
    fal_sale_order_id = fields.Many2one('sale.order', 'Sale Order')

    invoice_term_type = fields.Selection('Invoice Term Type', related='fal_invoice_term_id.term_type', track_visibility="onchange")
    state_line = fields.Selection('State Sale', related='fal_sale_order_id.state', store=True)
    sequence = fields.Integer('Sequence', default=10)

    product_id = fields.Many2one('product.product', 'Product', track_visibility="onchange")
    name = fields.Char(string='Description')
    description = fields.Char(string='Description Line')
    percentage = fields.Float('Percentage (%)', track_visibility="onchange")
    amount      = fields.Float('Amount', track_visibility="onchange")
    discount    = fields.Float('Discount', track_visibility="onchange")
    total_inv   = fields.Float('Total Invoice', track_visibility="onchange")
    date = fields.Date('Estimate to Invoice', track_visibility="onchange")
    is_final = fields.Boolean('Is Final Term', compute='_compute_is_final', track_visibility="onchange", store=True)

    invoice_id = fields.Many2one("account.invoice", string="Invoice", copy=False)
    invoice_actual_date = fields.Date("Actual Invoice Date", related='invoice_id.date_invoice', store=True)
    payment_date = fields.Date("Payment Date", related='invoice_id.date_invoice', store=True)
    invoice_state = fields.Selection("Invoice State", related='invoice_id.state', store=True)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('post', 'Inv.Requested'),
            ('done', 'Inv.Created'),
            ('cancel', 'Cancel'),
        ], 'State', default="draft")
    note = fields.Text('Term Note')
    term_of_payment = fields.Text('Term of Payment')
    attn = fields.Char('Attn')
    is_show_create_inv = fields.Boolean(compute='_methods_compute')
    is_delete = fields.Boolean(compute='_methods_compute')
    active  = fields.Boolean(string='Active', default=True)

    @api.multi
    @api.depends('invoice_id')
    def _methods_compute(self):
        for x in self:
            if x.invoice_id:
                x.is_show_create_inv = False
                x.is_delete = False
            else:
                x.is_show_create_inv = True
                x.is_delete = True

    @api.onchange('product_id')
    def onchange_product_id(self):
        self.description = self.product_id.name

    @api.multi
    def action_cancel(self):
        return self.write({'state': 'cancel'})

    @api.multi
    def action_draft(self):
        return self.write({'state': 'draft'})

    @api.multi
    def action_done(self):
        if self.state_line != 'sale':
            raise UserError(_(
                "State SO should be in Sale Order."))
        return self.write({'state': 'post'})

    @api.one
    @api.depends('sequence', 'date', 'fal_invoice_term_id')
    def _compute_is_final(self):
        # It either on Invoice Term / Sales order Line
        if self.fal_invoice_term_id:
            last_invoice_term = False
            # We trust odoo order to make the last sequence as Final
            for invoice_term_line in self.fal_invoice_term_id.fal_invoice_term_line_ids:
                invoice_term_line.is_final = False
                last_invoice_term = invoice_term_line
            if last_invoice_term:
                last_invoice_term.is_final = True
        elif self.fal_sale_order_id:
            last_invoice_term = False
            # We trust odoo order to make the last sequence as Final
            for invoice_term_line in self.fal_sale_order_id.fal_invoice_milestone_line_date_ids:
                invoice_term_line.is_final = False
                last_invoice_term = invoice_term_line
            if last_invoice_term:
                last_invoice_term.is_final = True

    @api.multi
    def action_invoice_create(self):
        inv_obj = self.env['account.invoice']
        soline_obj = self.env['sale.order.line']
        order = self.fal_sale_order_id

        inv_data = order._prepare_invoice()
        if 'date_invoice' not in inv_data:
            inv_data['date_invoice'] = self.date or fields.Date.today()
        invoice = inv_obj.create(inv_data)

        sum_total_invl = 0.0
        
        if self.item_invoice_term_line:
            for line in self.item_invoice_term_line:
                inv_line = line.sale_line_id.invoice_line_create(invoice.id, 1)
                inv_line.price_unit = round(line.amount, 2)
                sum_total_invl += inv_line.price_unit
                inv_line.discount_amount = 0.0
                inv_line.discount        = 0.0
        ##Case Before Update
        elif self.so_line_m2m:
            sum_total_sale_selected = 0.0
            sum_total_sale_selected = sum(line.price_subtotal for line in self.so_line_m2m)
            
            for line in self.so_line_m2m:
                #inv_line = line.invoice_line_create(invoice.id, line.product_uom_qty)
                inv_line = line.invoice_line_create(invoice.id, 1)
                inv_line.price_unit = round((self.total_inv/sum_total_sale_selected) * line.price_subtotal, 2)
                sum_total_invl += inv_line.price_subtotal 
                inv_line.discount_amount = 0.0
                inv_line.discount        = 0.0
        else:
            for line in order.order_line:
                inv_line = line.invoice_line_create(invoice.id, line.product_uom_qty)
                inv_line.price_unit = round((self.total_inv/order.amount_untaxed) * line.price_unit, 2)
                sum_total_invl += inv_line.price_subtotal
                inv_line.discount_amount = 0.0
                inv_line.discount        = 0.0
        
        print ("inv_line---1", inv_line.price_unit)
        print ("Cgeck", sum_total_invl, self.total_inv)
        
        if abs(sum_total_invl - self.total_inv) < 100.0 :
            print ("##Masuk ERROR")
            try:
                inv_line.price_unit = inv_line.price_unit + (self.total_inv - sum_total_invl)
            except:
                pass  
        print ("inv_line---2", inv_line.price_unit)    
            
        if not invoice.invoice_line_ids:
            raise UserError(_('There is no invoicable line.'))
            
        if invoice.amount_untaxed < 0:
            invoice.type = 'out_refund'
            for line in invoice.invoice_line_ids:
                line.quantity = -line.quantity
        for line in invoice.invoice_line_ids:
            line._set_additional_fields(invoice)
        invoice.compute_taxes()
        self.invoice_id = invoice.id
        self.state = 'done'
        #raise UserError(_("XXX"))
        return invoice

    @api.multi
    def term_unlink(self):
        for x in self:
            x.unlink()

# end of FalInvoiceTermLine()
