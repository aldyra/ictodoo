from odoo import api, fields, models, _
from openerp.exceptions import UserError


class TermWizard(models.TransientModel):
    _name = 'term.wizard'

    @api.multi
    def _get_sol_domain_wizz(self):
        sol_ids = []
        val_obj = self.env.context.get('active_id')
        so_obj = self.env['sale.order'].browse(val_obj)
        for line in so_obj.order_line:
            sol_ids.append(line.id)
        return [('id', 'in', sol_ids)]
    
    @api.depends('amount','discount')
    def _amount_all(self):
        self.update({'total_inv': self.amount - self.discount})

    name = fields.Char(string='Description')
    so_line_m2m = fields.Many2many('sale.order.line', 'term_so_line_m2m_rel','term_id', 'so_line_id', 'Sale Order Line',domain=_get_sol_domain_wizz)
    item_invoice_term_line = fields.One2many('item.invoice.term.sale.line.wizz', 'sale_term_wizard_id', string='Item Invoice Terms Line')
    percentage = fields.Float('Percentage (%)', track_visibility="onchange")
    amount     = fields.Float('Amount', track_visibility="onchange")
    discount  = fields.Float('Discount', )
    total_inv = fields.Float('Total Invoice', track_visibility="onchange", compute="_amount_all")
    date = fields.Date('Estimate to Invoice', track_visibility="onchange")

    @api.onchange('percentage', 'so_line_m2m','total_inv','amount')
    def onchange_percentage(self):
        self.item_invoice_term_line = False
        total_sale = 0.0
        item_invoice_term_line_list = []
        
        sum_total_sale_selected = 0.0
        sum_total_sale_selected = sum(line.price_subtotal for line in self.so_line_m2m)
        
        for cost in self.so_line_m2m:
            amount = round((self.total_inv / sum_total_sale_selected) * cost.price_subtotal, 2)
            vals = {'sale_line_id'  : cost.id,
                    'product_id'    : cost.product_id.id,
                    'name'          : cost.name,
                    'amount'        : amount
                    }
            item_invoice_term_line_list.append((0,0, vals))
            total_sale += cost.price_subtotal
        if self.percentage != 0.0:
            amount = (self.percentage / 100) * total_sale
            self.amount = round(amount,2)
        
        self.item_invoice_term_line = item_invoice_term_line_list
    
#     @api.onchange('percentage', 'so_line_m2m')
#     def onchange_percentage(self):
#         total_sale = 0.0
#         for cost in self.so_line_m2m:
#             total_sale += cost.price_subtotal
#         total_inv = (self.percentage / 100) * total_sale
#         self.total_inv = total_inv

#     @api.onchange('total_inv', 'so_line_m2m')
#     def onchange_total_inv(self):
#         total_sale = 0.0
#         for so in self.so_line_m2m:
#             total_sale += so.price_subtotal
#         if total_sale:
#             percentage = (self.total_inv / total_sale) * 100
#             self.percentage = percentage

    @api.onchange('name')
    def onchange_name(self):
        self._get_sol_domain_wizz()
    
#     @api.multi
#     def confirm(self):
#         cost_sheet = self.env.context.get('active_id')
#         cs = self.env['kk.cost.sheet']
#         cs_obj = cs.browse(cost_sheet)
#         term_obj = self.env['fal.invoice.term.line']
#         
#         #Check Validation
#         if self.item_invoice_term_line:
#             total_check = 0.0
#             for l in self.item_invoice_term_line:
#                 total_check += l.amount
#             if self.total_inv != total_check:
#                 raise UserError(_('Total Invoice should be same with detail'))
#         
#         if cs_obj:
#             cs_line_list = []
#             for cs in self.cost_sheet_line_m2m:
#                 cs_line_list.append((4, cs.id))
#             
#             item_cs_line_list = []
#             for l in self.item_invoice_term_line:
#                 vals = {'product_id'    : l.product_id.id,
#                         'name'          : l.name,
#                         'amount'        : l.amount}
#                 item_cs_line_list.append((0,0, vals))
#                 
#             cs_val = {
#                 'name': self.name,
#                 'cost_sheet_line_m2m': cs_line_list,
#                 'item_invoice_term_line': item_cs_line_list,
#                 'percentage': self.percentage,
#                 'total_inv': self.total_inv,
#                 'date': self.date,
#                 'cs_cost_sheet_id': cs_obj.id
#             }
#             term_obj.create(cs_val)
    
    @api.multi
    def confirm(self):
        so_val = self.env.context.get('active_id')
        cs = self.env['sale.order']
        cs_obj = cs.browse(so_val)
        term_obj = self.env['fal.invoice.term.line']
        
        #Check Validation
        if self.item_invoice_term_line:
            total_check = 0.0
            for l in self.item_invoice_term_line:
                total_check += l.amount
            print ("self.total_inv != total_check:", self.total_inv , total_check)
            if self.total_inv != total_check and (abs(self.total_inv - total_check) > 0.5):
                raise UserError(_('Total Invoice should be same with detail'))

        if cs_obj:
            cs_line_list = []
            for cs in self.so_line_m2m:
                cs_line_list.append((4, cs.id))
                
            item_cs_line_list = []
            for l in self.item_invoice_term_line:
                vals = {'sale_line_id'  : l.sale_line_id.id,
                        'product_id'    : l.product_id.id,
                        'name'          : l.name,
                        'amount'        : l.amount}
                item_cs_line_list.append((0,0, vals))    
            
            cs_val = {
                'name': self.name,
                'so_line_m2m': cs_line_list,
                'item_invoice_term_line': item_cs_line_list,
                'percentage': self.percentage,
                'amount': self.amount,
                'discount': self.discount,
                'total_inv': self.total_inv,
                'date': self.date,
                'fal_sale_order_id': cs_obj.id,
                'cs_cost_sheet_id': cs_obj.kk_cost_sheet_id.id or False
            }
            term_obj.create(cs_val)
            #Dimatikan
            #cs_obj.kk_cost_sheet_id.action_recalculate_cof()
            
class ItemInvoiceTermSaleLineWizz(models.TransientModel):
    _name = 'item.invoice.term.sale.line.wizz'
    
    sale_line_id        = fields.Many2one('sale.order.line', string='Sale Order Line')
    product_id          = fields.Many2one('product.product', string='Product')
    name                = fields.Char(string='Description')
    amount              = fields.Float(string='Amount')
    sale_term_wizard_id = fields.Many2one('term.wizard', string='Sale Term Wizard')

