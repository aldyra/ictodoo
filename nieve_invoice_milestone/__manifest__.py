# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Invoice Milestone",
    "version": "11.1.1.0.0",
    'author': 'Mochamad Rezki',
    'website': 'https://www.sriwijayaair.co.id',
    'category': 'Invoicing Management',
    'summary': 'Split Invoice in Sale Order 26-10-2019',
    "description": """
    Module to add invoice milestone

    """,
    "depends": [
        'sale_management'
    ],
    'data': [
        'wizard/term_wizard_views.xml',
        'views/fal_invoice_milestone_views.xml',
        'views/sale_views.xml',
        'security/ir.model.access.csv',
        'security/sale_security.xml',
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
