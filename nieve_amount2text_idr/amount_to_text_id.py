from num2words import num2words

def amount_to_text(amount, lang='id', cur=False):
    return num2words(amount, lang='id')