from odoo import models, fields, api

class AccountingReport(models.TransientModel):
    _inherit = 'accounting.report'
    
    filter_12months    = fields.Boolean(string='Filter 12 Months')
    acc_ending_balance = fields.Boolean(string='Hide Account Ending Balance at 0')
    type_filter        =fields.Selection([('filter_12months','Filter 12 Months'),('monthly','Monthly')],default="filter_12months")

    @api.onchange('type_filter')
    def _onchange_type_filter_acc(self):
        for item in self:
            item.filter_12months=False
            item.acc_ending_balance=False
    
    @api.multi
    def action_financial_report_xlsx(self, report_type):
        report_type = 'xlsx'
        self.ensure_one()
        if report_type == 'xlsx':
            if self.account_report_id.name=='Balance Sheet':
                report_name = 'nieve_financial_report.report_balance_sheet_xlsx'
            elif self.account_report_id.name=='Profit and Loss':
                report_name = 'nieve_financial_report.report_indonesia_profit_loss_xlsx'
        else:
            report_name = 'account_financial_report.' \
                          'report_aged_partner_balance_qweb'
        report = self.env['ir.actions.report'].search(
            [('report_name', '=', report_name),
             ('report_type', '=', report_type)], limit=1)
        return report.report_action(self)

    @api.cr_uid_ids_context
    def action_financial_report_pdf(self,context=None):
        if context is None:
            context = {}
        # print ("AAAAAA")
        self.ensure_one()
        datas = {'ids': self.env.context.get('active_ids')}
        datas['model'] = 'accounting.report'
        datas['form'] = self.read(self.env.context)[0]

        if self.account_report_id.name == 'Balance Sheet':
            return self.env.ref('nieve_financial_report.action_balance_sheet_pdf').report_action(self,data=datas)
        else:
            return self.env.ref('nieve_financial_report.action_profit_loss_pdf').report_action(self,data=datas)