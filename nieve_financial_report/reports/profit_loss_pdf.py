from odoo import models, api

from odoo import models, api
from odoo.addons.nieve_amount2text_idr import amount_to_text_id
from openerp.exceptions import UserError
from odoo.addons.nieve_financial_report.reports.report_indonesia_profit_loss_xlsx import ProfitLossReportXlsx

class ProfitLossPdf(models.AbstractModel):
    _name = 'report.nieve_financial_report.profit_loss_pdf'

    @api.model
    def get_report_values(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))

        if docs.filter_12months:
            get_data = ProfitLossReportXlsx.compute_values_12month(self,docs)
        else:
            get_data = ProfitLossReportXlsx.compute_values_monthly(self,docs)

        docargs = {
            'get_data': get_data,
            'doc_ids': docids,
            'doc_model': 'account.account',
            # 'docs': get_data,
        }
        return docargs