from odoo import models, api

from odoo import models, api, _
from odoo.addons.nieve_amount2text_idr import amount_to_text_id
from openerp.exceptions import UserError
from odoo.addons.nieve_financial_report.reports.report_balance_sheet_xlsx import BalanceSheetComputeReport


class BalanceSheetPdf(models.AbstractModel):
    _name = 'report.nieve_financial_report.balance_sheet_pdf'

    @api.model
    def get_report_values(self, docids, data=None):

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_id'))
        # print('get_data---',self.compute_values())
        docargs = {
            'get_data': BalanceSheetComputeReport.compute_values(self,docs),
            'doc_ids': docids,
            'doc_model': 'account.account',
            # 'docs': get_data,
        }
        return docargs