# -*- coding: utf-8 -*-
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo import models, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning

class BalanceSheetComputeReport(models.AbstractModel):
    _name = 'balance.sheet.compute.report'
    
    def compute_values_12month(self, data):
        account_obj             = self.env['account.account']
        account_fin_report_obj  = self.env['account.financial.report']
        move_line_obj           = self.env['account.move.line']
        
        date_from   = data.date_from
        date_to     = data.date_to
        target_move = data.target_move
        
        filter_state_earning = target_move=='posted' and " and pl_move.state = '%s' " % target_move or ""
        filter_state= target_move=='posted' and " where state = '%s' " % target_move or ""
        
        result = []
        
        print ("##data.account_report_id--->>", data)
        for report in data.account_report_id:
            account_ids = []
            search_domain = []
            if date_from:
                search_domain += [('date', '>=', date_from)]
            if date_to:
                search_domain += [('date', '<=', date_to)]
            if target_move == 'posted':
                search_domain += [('move_id.state', '=', 'posted')]
            
            if data.account_report_id.name == 'Balance Sheet':
                ##RE & Current Period Account
                RE_account   = account_obj.search([('user_type_id.name','=','Return Year Earnings')])
                CE_account   = account_obj.search([('user_type_id.name','=','Current Year Earnings')])
                if not RE_account:
                    raise UserError(_("Please Setting Return Year Eearnings Account First!"))
        
                if not CE_account:
                    raise UserError(_("Please Setting Return Current Eearnings Account First!"))
                
                for report in data.account_report_id.children_ids:
                    account_type_ids = []
                    account_type_ids += ([i.id for i in report.account_type_ids])
                    if not account_type_ids:
                        continue
                    print ("###str(RE_account.id)--->", str(RE_account.id))
                    query_earning = """
                                    /*Debit*/
                                    CASE 
                                        /*Return Earning-Debit*/
                                        WHEN account.id="""+str(RE_account.id)+""" THEN (SELECT SUM(balance) FROM ( (select COALESCE(SUM(pl_movel.debit),0.0) AS balance from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                        LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where (pl_movel.account_id=account.id or pl_account.user_type_id in """+str(tuple(account_type_ids))+""") and pl_movel.date < '"""+date_to+"""'"""+filter_state_earning+""") 
                                        UNION
                                        (select COALESCE(SUM(pl_movel.debit),0.0) AS balance from account_move_line pl_movel 
                                        left join account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where pl_movel.account_id=account.id and pl_movel.date >= '"""+date_from+"""' and pl_movel.date <= '"""+date_to+"""'"""+filter_state_earning+""") ) AS balance) 
                                        
                                        /*Current Earning-Debit*/
                                        WHEN account.id="""+str(CE_account.id)+""" THEN (select COALESCE(SUM(pl_movel.debit),0.0) from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                        LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where (pl_movel.account_id=account.id or pl_account.user_type_id in """+str(tuple(account_type_ids))+""") and pl_movel.date >= '"""+date_from+"""' and pl_movel.date <= '"""+date_to+"""'"""+filter_state_earning+""")
                                        ELSE COALESCE(SUM(movel.debit),0.0) END AS debit,
                                    /*Credit*/
                                    CASE 
                                        /*Return Earning-Credit*/
                                        WHEN account.id="""+str(RE_account.id)+""" THEN (SELECT SUM(balance) FROM ( (select COALESCE(SUM(pl_movel.credit),0.0) AS balance from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                        LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where (pl_movel.account_id=account.id or pl_account.user_type_id in """+str(tuple(account_type_ids))+""") and pl_movel.date < '"""+date_to+"""'"""+filter_state_earning+""") 
                                        UNION
                                        (select COALESCE(SUM(pl_movel.credit),0.0) AS balance from account_move_line pl_movel 
                                        left join account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where pl_movel.account_id=account.id and pl_movel.date >= '"""+date_from+"""' and pl_movel.date <= '"""+date_to+"""'"""+filter_state_earning+""") ) AS balance) 
                                        
                                        /*Current Earning-Credit*/
                                        WHEN account.id="""+str(CE_account.id)+""" THEN (select COALESCE(SUM(pl_movel.credit),0.0) from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                        LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where (pl_movel.account_id=account.id or pl_account.user_type_id in """+str(tuple(account_type_ids))+""") and pl_movel.date >= '"""+date_from+"""' and pl_movel.date <= '"""+date_to+"""'"""+filter_state_earning+""")
                                        ELSE COALESCE(SUM(movel.credit),0.0) END AS credit,
                                    /*Balance*/
                                    CASE 
                                        /*Return Earning-Balance*/
                                        WHEN account.id="""+str(RE_account.id)+""" THEN (SELECT SUM(balance) FROM ( (select COALESCE(SUM(pl_movel.balance),0.0) AS balance from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                        LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where (pl_movel.account_id=account.id or pl_account.user_type_id in """+str(tuple(account_type_ids))+""") and pl_movel.date < '"""+date_to+"""'"""+filter_state_earning+""") 
                                        UNION
                                        (select COALESCE(SUM(pl_movel.balance),0.0) AS balance from account_move_line pl_movel 
                                        left join account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where pl_movel.account_id=account.id and pl_movel.date >= '"""+date_from+"""' and pl_movel.date <= '"""+date_to+"""'"""+filter_state_earning+""") ) AS balance)
                                        
                                        /*Current Earning-Balance*/
                                        WHEN account.id="""+str(CE_account.id)+""" THEN (select COALESCE(SUM(pl_movel.balance),0.0) from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                        LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                        where (pl_movel.account_id=account.id or pl_account.user_type_id in """+str(tuple(account_type_ids))+""") and pl_movel.date >= '"""+date_from+"""' and pl_movel.date <= '"""+date_to+"""'"""+filter_state_earning+""")
                                        ELSE COALESCE(SUM(movel.balance),0.0) END AS balance
                                    """
                    
                    query = """
                            SELECT * FROM (
                                WITH RECURSIVE CHILD AS (
                                    SELECT  account.id, 
                                            account.code as code, 
                                            account.name, 
                                            account.level,
                                            account_type.type,
                                            account.parent_id,
                                            
                                            (case when month = '01' then """+query_earning+""" else 0.0 end) as jan,
                                            (case when month = '02' then """+query_earning+""" else 0.0 end) as feb,
                                            (case when month = '03' then """+query_earning+""" else 0.0 end) as mar,
                                            (case when month = '04' then """+query_earning+""" else 0.0 end) as apr,
                                            (case when month = '05' then """+query_earning+""" else 0.0 end) as may,
                                            (case when month = '06' then """+query_earning+""" else 0.0 end) as jun,
                                            (case when month = '07' then """+query_earning+""" else 0.0 end) as jul,
                                            (case when month = '08' then """+query_earning+""" else 0.0 end) as aug,
                                            (case when month = '09' then """+query_earning+""" else 0.0 end) as sep,
                                            (case when month = '10' then """+query_earning+""" else 0.0 end) as oct,
                                            (case when month = '11' then """+query_earning+""" else 0.0 end) as nov,
                                            (case when month = '12' then """+query_earning+""" else 0.0 end) as dec
        
                                            FROM account_account account 
                                            LEFT JOIN account_move_line movel ON account.id=movel.account_id AND movel.date <= '%s' AND movel.move_id in (select id from account_move """+filter_state+""")
                                            INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                                            WHERE account_type.id in %s
                                        /*GROUP BY account.id,account_type.type*'
                                        /*ORDER BY account.parent_left DESC*/
                                    UNION ALL
                                        
                                    SELECT  account.id, 
                                        account.code as code, 
                                        account.name, 
                                        account.level,
                                        account_type.type,
                                        account.parent_id,
                                        COALESCE(CHILD.jan,0.0) as jan,
                                        COALESCE(CHILD.feb,0.0) as feb,
                                        COALESCE(CHILD.mar,0.0) as mar,
                                        COALESCE(CHILD.apr,0.0) as apr,
                                        COALESCE(CHILD.may,0.0) as may,
                                        COALESCE(CHILD.jun,0.0) as jun,
                                        COALESCE(CHILD.jul,0.0) as jul,
                                        COALESCE(CHILD.aug,0.0) as aug,
                                        COALESCE(CHILD.sep,0.0) as sep,
                                        COALESCE(CHILD.oct,0.0) as oct,
                                        COALESCE(CHILD.nov,0.0) as nov,
                                        COALESCE(CHILD.dec,0.0) as dec
                                        
                                        FROM account_account account LEFT JOIN account_move_line movel ON account.id=movel.account_id 
                                        INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                                        INNER JOIN CHILD ON account.id=CHILD.parent_id
                                        WHERE account_type.type = 'view' AND account.code != '0'
                                    )
                                SELECT  CHILD.code, 
                                    CHILD.name,
                                    CHILD.level,
                                    CHILD.type,
                                    CHILD.parent_id,
                                    COALESCE(SUM(CHILD.jan),0.0) as jan,
                                    COALESCE(SUM(CHILD.feb),0.0) as feb,
                                    COALESCE(SUM(CHILD.mar),0.0) as mar,
                                    COALESCE(SUM(CHILD.apr),0.0) as apr,
                                    COALESCE(SUM(CHILD.may),0.0) as may,
                                    COALESCE(SUM(CHILD.jun),0.0) as jun,
                                    COALESCE(SUM(CHILD.jul),0.0) as jul,
                                    COALESCE(SUM(CHILD.aug),0.0) as aug,
                                    COALESCE(SUM(CHILD.sep),0.0) as sep,
                                    COALESCE(SUM(CHILD.oct),0.0) as oct,
                                    COALESCE(SUM(CHILD.nov),0.0) as nov,
                                    COALESCE(SUM(CHILD.dec),0.0) as dec
                                FROM CHILD
                                GROUP BY CHILD.code,CHILD.name,CHILD.level,CHILD.type,CHILD.parent_id
                                
                                ) AS RESULT
                                ORDER BY RESULT.code
                            """
                            
                    print ((query % (date_to, str(tuple(account_type_ids)))))
                    
                    self.env.cr.execute(query % (date_to, str(tuple(account_type_ids))))
                    
                    
                    for account in self.env.cr.dictfetchall():
                        balance = 0.0
                        credit  = 0.0
                        debit   = 0.0
                        
                        vals = {  # 'id'        : account.id,
                                'parent_id' : account['parent_id'],
                                'code'      : account['code'],
                                'name'      : account['name'],
                                'level'     : account['level'],
                                'sign'      : -1,#report.sign,
                                'type'      : account['type'],
                                # 'parent_left': account.parent_left
                                'jan'      : account['jan'],
                                'feb'      : account['feb'],
                                'mar'      : account['mar'],
                                'apr'      : account['apr'],
                                'may'      : account['may'],
                                'jun'      : account['jun'],
                                'jul'      : account['jul'],
                                'aug'      : account['aug'],
                                'sep'      : account['sep'],
                                'oct'      : account['oct'],
                                'nov'      : account['nov'],
                                'dec'      : account['dec'],
                                }
                            
                        result.append(vals)
            else:
                continue
        return result
    
    
    def compute_values_monthly(self, data):
        account_obj             = self.env['account.account']
        account_fin_report_obj  = self.env['account.financial.report']
        move_line_obj           = self.env['account.move.line']
        
        date_from   = data.date_from
        date_to     = data.date_to
        target_move = data.target_move
        
        account_report   = account_fin_report_obj.search([('id', '=', data.account_report_id.id)])
        child_reports = account_report._get_children_by_order()
        
        ##ProfitLoss
        account_report_pl   = account_fin_report_obj.search([('name', '=', 'Profit and Loss')])
        
        #Profit Loss
        account_pl_type_ids = []
        for report in account_report_pl.children_ids:
            account_pl_type_ids += ([i.id for i in report.account_type_ids])
        
            ##RE & Current Period Account
            RE_account   = account_obj.search([('user_type_id.name','=','Return Year Earnings')])
            CE_account   = account_obj.search([('user_type_id.name','=','Current Year Earnings')])
            if not RE_account:
                raise UserError(_("Please Setting Return Year Eearnings Account First!"))
    
            if not CE_account:
                raise UserError(_("Please Setting Return Current Eearnings Account First!"))
            # print ("##RE_account--->>", RE_account.id)
            # print ("##CE_account--->>", CE_account.id)
            result = []
            
            for report in account_report:
                account_ids     = []
                
                if data.account_report_id.name=='Balance Sheet':
                    filter_state_earning = target_move=='posted' and " and pl_move.state = '%s' " % target_move or ""
                    filter_state         = target_move=='posted' and " where state = '%s' " % target_move or ""
                    
                    for report in report.children_ids:
                        account_type_ids = []
                        print ("report.name---->>", report.name)
                        if report.children_ids:
                            for child in report.children_ids:
                                account_type_ids += ([i.id for i in child.account_type_ids])
                        account_type_ids += ([i.id for i in report.account_type_ids])
                        print ("account_type_ids---->>", account_type_ids)
                        
                        query = """
                                SELECT * FROM (
                                    WITH RECURSIVE CHILD AS (
                                        SELECT  account.id, 
                                                account.code as code, 
                                                account.name, 
                                                account.level,
                                                account_type.type,
                                                account.parent_id,
                                                /*Debit*/
                                                CASE 
                                                    /*Return Earning-Debit*/
                                                    WHEN account.id=%s THEN (SELECT SUM(balance) FROM ( (select COALESCE(SUM(pl_movel.debit),0.0) AS balance from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                                    LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where (pl_movel.account_id=account.id or pl_account.user_type_id in %s) and pl_movel.date < '%s'"""+filter_state_earning+""") 
                                                    UNION
                                                    (select COALESCE(SUM(pl_movel.debit),0.0) AS balance from account_move_line pl_movel 
                                                    left join account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where pl_movel.account_id=account.id and pl_movel.date >= '%s' and pl_movel.date <= '%s'"""+filter_state_earning+""") ) AS balance) 
                                                    
                                                    /*Current Earning-Debit*/
                                                    WHEN account.id=%s THEN (select COALESCE(SUM(pl_movel.debit),0.0) from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                                    LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where (pl_movel.account_id=account.id or pl_account.user_type_id in %s) and pl_movel.date >= '%s' and pl_movel.date <= '%s'"""+filter_state_earning+""")
                                                    ELSE COALESCE(SUM(movel.debit),0.0) END AS debit,
                                                /*Credit*/
                                                CASE 
                                                    /*Return Earning-Credit*/
                                                    WHEN account.id=%s THEN (SELECT SUM(balance) FROM ( (select COALESCE(SUM(pl_movel.credit),0.0) AS balance from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                                    LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where (pl_movel.account_id=account.id or pl_account.user_type_id in %s) and pl_movel.date < '%s'"""+filter_state_earning+""") 
                                                    UNION
                                                    (select COALESCE(SUM(pl_movel.credit),0.0) AS balance from account_move_line pl_movel 
                                                    left join account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where pl_movel.account_id=account.id and pl_movel.date >= '%s' and pl_movel.date <= '%s'"""+filter_state_earning+""") ) AS balance) 
                                                    
                                                    /*Current Earning-Credit*/
                                                    WHEN account.id=%s THEN (select COALESCE(SUM(pl_movel.credit),0.0) from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                                    LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where (pl_movel.account_id=account.id or pl_account.user_type_id in %s) and pl_movel.date >= '%s' and pl_movel.date <= '%s'"""+filter_state_earning+""")
                                                    ELSE COALESCE(SUM(movel.credit),0.0) END AS credit,
                                                /*Balance*/
                                                CASE 
                                                    /*Return Earning-Balance*/
                                                    WHEN account.id=%s THEN (SELECT SUM(balance) FROM ( (select COALESCE(SUM(pl_movel.balance),0.0) AS balance from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                                    LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where (pl_movel.account_id=account.id or pl_account.user_type_id in %s) and pl_movel.date < '%s'"""+filter_state_earning+""") 
                                                    UNION
                                                    (select COALESCE(SUM(pl_movel.balance),0.0) AS balance from account_move_line pl_movel 
                                                    left join account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where pl_movel.account_id=account.id and pl_movel.date >= '%s' and pl_movel.date <= '%s'"""+filter_state_earning+""") ) AS balance)
                                                    
                                                    /*Current Earning-Balance*/
                                                    WHEN account.id=%s THEN (select COALESCE(SUM(pl_movel.balance),0.0) from account_move_line pl_movel JOIN account_account pl_account ON pl_movel.account_id=pl_account.id LEFT JOIN account_account_type pl_account_type ON pl_account.user_type_id=pl_account_type.id
                                                    LEFT JOIN account_move pl_move ON pl_movel.move_id=pl_move.id
                                                    where (pl_movel.account_id=account.id or pl_account.user_type_id in %s) and pl_movel.date >= '%s' and pl_movel.date <= '%s'"""+filter_state_earning+""")
                                                    ELSE COALESCE(SUM(movel.balance),0.0) END AS balance
            
                                                FROM account_account account 
                                                LEFT JOIN account_move_line movel ON account.id=movel.account_id AND movel.date <= '%s' AND movel.move_id in (select id from account_move """+filter_state+""")
                                                INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                                                WHERE account_type.id in %s
                                                GROUP BY account.id,account_type.type
                                            /*ORDER BY account.parent_left DESC*/
                                        UNION ALL
                                            
                                        SELECT  account.id, 
                                            account.code as code, 
                                            account.name, 
                                            account.level,
                                            account_type.type,
                                            account.parent_id,
                                            COALESCE(CHILD.debit,0.0) as debit,
                                            COALESCE(CHILD.credit,0.0) as credit,
                                            COALESCE(CHILD.balance,0.0) as balance
                                            
                                            FROM account_account account LEFT JOIN account_move_line movel ON account.id=movel.account_id 
                                            INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                                            INNER JOIN CHILD ON account.id=CHILD.parent_id
                                            WHERE account_type.type = 'view' AND account.code != '0'
                                        )
                                    SELECT  CHILD.code, 
                                        CHILD.name,
                                        CHILD.level,
                                        CHILD.type,
                                        CHILD.parent_id,
                                        COALESCE(SUM(CHILD.debit),0.0) as debit,
                                        COALESCE(SUM(CHILD.credit),0.0) as credit,
                                        COALESCE(SUM(CHILD.balance),0.0) as balance
                                    FROM CHILD
                                    GROUP BY CHILD.code,CHILD.name,CHILD.level,CHILD.type,CHILD.parent_id
                                    
                                    ) AS RESULT
                                    ORDER BY RESULT.code
                        """
                        
                        self.env.cr.execute(query % (
                                        #Debit
                                        RE_account.id,str(tuple(account_pl_type_ids)),date_from, date_from,date_to,#Debit
                                        CE_account.id,str(tuple(account_pl_type_ids)),date_from,date_to,#Debit
                                        #Credit
                                        RE_account.id,str(tuple(account_pl_type_ids)),date_from, date_from,date_to,#Credit
                                        CE_account.id,str(tuple(account_pl_type_ids)),date_from,date_to,#Credit
                                        #Balance
                                        RE_account.id,str(tuple(account_pl_type_ids)),date_from, date_from,date_to,#Balance
                                        CE_account.id,str(tuple(account_pl_type_ids)),date_from,date_to,#Balance
                                        
                                        date_to,str(tuple(account_type_ids))))
                        
                        for account in self.env.cr.dictfetchall():
                            balance = 0.0
                            credit  = 0.0
                            debit   = 0.0
                            
                            vals = {#'id'        : account.id,
                                    'parent_id' : account['parent_id'],
                                    'code'      : account['code'],
                                    'name'      : account['name'],
                                    'balance'   : account['balance'],
                                    'debit'     : account['debit'],
                                    'credit'    : account['credit'],
                                    'level'     : account['level'],
                                    'sign'      : report.sign,
                                    'type'      : account['type'],
                                    #'parent_left': account.parent_left
                                    }
                                
                            result.append(vals)
            else:
                continue
                    
        #print "######result----->>", result
        return result

class BalanceSheetReportXlsx(models.AbstractModel):
    _name = 'report.nieve_financial_report.report_balance_sheet_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, wizard):
        if wizard.filter_12months:
            #self.balance_sheet_12monthly(workbook, data, wizard)
            self.balance_sheet_monthly(workbook, data, wizard)
        else:
            self.balance_sheet_monthly(workbook, data, wizard)
    
    def balance_sheet_12monthly(self, workbook, data, wizard):
        print ("##balance_sheet_12monthly")
        bs_compute_obj = self.env['balance.sheet.compute.report']
        report = wizard.account_report_id
        
        print ("##wizard.account_report_id--->>", wizard.account_report_id)
        
        sheet = workbook.add_worksheet(report.name)
        sheet.hide_gridlines(2)
        #######
        
        ##Style
        company_header_style = workbook.add_format({'bold': True, 'font_color': 'black'})
        company_header_style.set_font_size(16)
        company_header_style.set_align('center')
        
        report_label_header_style = workbook.add_format({'bold': True, 'font_color': '#000080'})
        report_label_header_style.set_font_size(26)
        report_label_header_style.set_align('center')
        
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        number = workbook.add_format({'bold': False, 'font_color': 'black'})
        number.set_num_format('#,##0.00;(#,##0.00)')
        
        string_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold.set_num_format('#,##0.00;(#,##0.00)')
        
        table_header_style = workbook.add_format({'bold': True, 'font_color': '#B22222'})
        table_header_style.set_align('center')
        table_header_style.set_bottom(1)
        
        ##
        
        sheet.show_grid = 0
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.portrait = 0  # Landscape
        sheet.fit_width_to_pages = 1
        
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 60)  # Source No.
        sheet.set_column('C:C', 20)  # Source No.
        
        sheet.merge_range('A1:C1', wizard.company_id.name, company_header_style)
        sheet.merge_range('A2:C2', report.name, report_label_header_style)
        
        sheet.write(3, 0, 'Date Range filter : ',)
        sheet.write(3, 1, 'From: ' + wizard.date_from + " To " + wizard.date_to,)
        
        sheet.write(4, 0, 'Post Filter : ',)
        sheet.write(4, 1, wizard.target_move.capitalize() or "",)
        
        row = 9
         
        sheet.write(row, 0, 'Code', table_header_style)
        sheet.write(row, 1, 'Account', table_header_style)
        sheet.write(row, 2, 'January', table_header_style)
        sheet.write(row, 3, 'February', table_header_style)
        sheet.write(row, 4, 'March', table_header_style)
        sheet.write(row, 5, 'April', table_header_style)
        sheet.write(row, 6, 'May', table_header_style)
        sheet.write(row, 7, 'June', table_header_style)
        sheet.write(row, 8, 'July', table_header_style)
        sheet.write(row, 9, 'August', table_header_style)
        sheet.write(row, 10, 'September', table_header_style)
        sheet.write(row, 11, 'October', table_header_style)
        sheet.write(row, 12, 'November', table_header_style)
        sheet.write(row, 13, 'December', table_header_style)
        row += 1
          
        for o in bs_compute_obj.compute_values_12month(wizard):
            if o['type']=='view':
                string_style = string_bold
                number_style = number_bold
            else:
                string_style = string
                number_style = number
            
            sheet.write(row,0,o['code'], string_style)
            sheet.write(row,1,o['level']*"        " +o['name'],string_style)
            sheet.write(row,2,o['jan']*o['sign'],number_style)
            sheet.write(row,3,o['feb']*o['sign'],number_style)
            sheet.write(row,4,o['mar']*o['sign'],number_style)
            sheet.write(row,5,o['apr']*o['sign'],number_style)
            sheet.write(row,6,o['may']*o['sign'],number_style)
            sheet.write(row,7,o['jun']*o['sign'],number_style)
            sheet.write(row,8,o['jul']*o['sign'],number_style)
            sheet.write(row,9,o['aug']*o['sign'],number_style)
            sheet.write(row,10,o['sep']*o['sign'],number_style)
            sheet.write(row,11,o['oct']*o['sign'],number_style)
            sheet.write(row,12,o['nov']*o['sign'],number_style)
            sheet.write(row,13,o['dec']*o['sign'],number_style)
            row += 1
            
    
    def balance_sheet_monthly(self, workbook, data, wizard):
        bs_compute_obj = self.env['balance.sheet.compute.report']
        report = wizard.account_report_id
        
        print ("##wizard.account_report_id--->>", wizard.account_report_id)
        
        sheet = workbook.add_worksheet(report.name)
        sheet.hide_gridlines(2)
        #######
        
        ##Style
        company_header_style = workbook.add_format({'bold': True, 'font_color': 'black'})
        company_header_style.set_font_size(16)
        company_header_style.set_align('center')
        
        report_label_header_style = workbook.add_format({'bold': True, 'font_color': '#000080'})
        report_label_header_style.set_font_size(26)
        report_label_header_style.set_align('center')
        
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        number = workbook.add_format({'bold': False, 'font_color': 'black'})
        number.set_num_format('#,##0.00;(#,##0.00)')
        
        string_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold.set_num_format('#,##0.00;(#,##0.00)')
        
        table_header_style = workbook.add_format({'bold': True, 'font_color': '#B22222'})
        table_header_style.set_align('center')
        table_header_style.set_bottom(1)
        
        ##
        
        sheet.show_grid = 0
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.portrait = 0  # Landscape
        sheet.fit_width_to_pages = 1
        
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 60)  # Source No.
        sheet.set_column('C:C', 20)  # Source No.
        
        sheet.merge_range('A1:C1', wizard.company_id.name, company_header_style)
        sheet.merge_range('A2:C2', report.name, report_label_header_style)
        
        sheet.write(3, 0, 'Date Range filter : ',)
        sheet.write(3, 1, 'From: ' + wizard.date_from + " To " + wizard.date_to,)
        
        sheet.write(4, 0, 'Post Filter : ',)
        sheet.write(4, 1, wizard.target_move.capitalize() or "",)
        
        row = 9
         
        sheet.write(row, 0, 'Code', table_header_style)
        sheet.write(row, 1, 'Account', table_header_style)
        sheet.write(row, 2, 'Balance', table_header_style)
        row += 1
          
        for o in bs_compute_obj.compute_values_monthly(wizard):
            if o['type']=='view':
                string_style = string_bold
                number_style = number_bold
            else:
                string_style = string
                number_style = number
            
            sheet.write(row,0,o['code'], string_style)
            sheet.write(row,1,o['level']*"        " +o['name'],string_style)
            sheet.write(row,2,o['balance']*o['sign'],number_style)
            row += 1
            
