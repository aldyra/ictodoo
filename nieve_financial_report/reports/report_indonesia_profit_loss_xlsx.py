# -*- coding: utf-8 -*-
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo import models, _

class ProfitLossComputeReport(models.AbstractModel):
    _name = 'profit.loss.compute.report'
    
    def compute_values_12month(self, data):
        account_obj             = self.env['account.account']
        account_fin_report_obj  = self.env['account.financial.report']
        move_line_obj           = self.env['account.move.line']
        
        date_from   = data.date_from
        date_to     = data.date_to
        target_move = data.target_move
        filter_state= target_move=='posted' and " where state = '%s' " % target_move or ""
        
        result = []
        

        for report in data.account_report_id:
            account_ids = []
            search_domain = []
            if date_from:
                search_domain += [('date', '>=', date_from)]
            if date_to:
                search_domain += [('date', '<=', date_to)]
            if target_move == 'posted':
                search_domain += [('move_id.state', '=', 'posted')]
            
            if data.account_report_id.name == 'Profit and Loss':
                account_type_ids = []
                for report in data.account_report_id.children_ids:
                    account_type_ids += ([i.id for i in report.account_type_ids])
            else:
                continue
            
            query = """
                    SELECT * FROM (
                        WITH RECURSIVE CHILD AS (
                 SELECT account_id,
                code,
                name,
                level,
                type,
                parent_id,
                (case when month = '01' then balance else 0.0 end) as jan,
                (case when month = '02' then balance else 0.0 end) as feb,
                (case when month = '03' then balance else 0.0 end) as mar,
                (case when month = '04' then balance else 0.0 end) as apr,
                (case when month = '05' then balance else 0.0 end) as may,
                (case when month = '06' then balance else 0.0 end) as jun,
                (case when month = '07' then balance else 0.0 end) as jul,
                (case when month = '08' then balance else 0.0 end) as aug,
                (case when month = '09' then balance else 0.0 end) as sep,
                (case when month = '10' then balance else 0.0 end) as oct,
                (case when month = '11' then balance else 0.0 end) as nov,
                (case when month = '12' then balance else 0.0 end) as dec
                    FROM (
                    SELECT  account.id as account_id, 
                    account.code as code, 
                    account.name as name, 
                    account.level as level,
                    account_type.type as type,
                    account.parent_id as parent_id,
                    to_char(movel.date, 'MM') as month,
                    COALESCE(movel.balance,0.0) as balance
                    
                    FROM account_account account 
                    LEFT JOIN account_move_line movel ON account.id=movel.account_id AND movel.date >= '%s' AND movel.date <= '%s' AND movel.move_id in (select id from account_move """+filter_state+""")
                    INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                    WHERE account_type.id in %s
                    /*GROUP BY account.id,account_type.type*/
                    /*ORDER BY account.parent_left DESC*/
                    ) monthly
                            UNION ALL
                            SELECT  account.id as account_id, 
                                account.code as code, 
                                account.name as name, 
                                account.level as level,
                                account_type.type as type,
                                account.parent_id as parent_id,
                                COALESCE(CHILD.jan,0.0) as jan,
                                COALESCE(CHILD.feb,0.0) as feb,
                                COALESCE(CHILD.mar,0.0) as mar,
                                COALESCE(CHILD.apr,0.0) as apr,
                                COALESCE(CHILD.may,0.0) as may,
                                COALESCE(CHILD.jun,0.0) as jun,
                                COALESCE(CHILD.jul,0.0) as jul,
                                COALESCE(CHILD.aug,0.0) as aug,
                                COALESCE(CHILD.sep,0.0) as sep,
                                COALESCE(CHILD.oct,0.0) as oct,
                                COALESCE(CHILD.nov,0.0) as nov,
                                COALESCE(CHILD.dec,0.0) as dec
                                
                                FROM account_account account LEFT JOIN account_move_line movel ON account.id=movel.account_id 
                                INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                                INNER JOIN CHILD ON account.id=CHILD.parent_id
                                WHERE account_type.type = 'view'
                            )
                        SELECT  CHILD.code, 
                            CHILD.name,
                            CHILD.level,
                            CHILD.type,
                            CHILD.parent_id,
                            COALESCE(SUM(CHILD.jan),0.0) as jan,
                            COALESCE(SUM(CHILD.feb),0.0) as feb,
                            COALESCE(SUM(CHILD.mar),0.0) as mar,
                            COALESCE(SUM(CHILD.apr),0.0) as apr,
                            COALESCE(SUM(CHILD.may),0.0) as may,
                            COALESCE(SUM(CHILD.jun),0.0) as jun,
                            COALESCE(SUM(CHILD.jul),0.0) as jul,
                            COALESCE(SUM(CHILD.aug),0.0) as aug,
                            COALESCE(SUM(CHILD.sep),0.0) as sep,
                            COALESCE(SUM(CHILD.oct),0.0) as oct,
                            COALESCE(SUM(CHILD.nov),0.0) as nov,
                            COALESCE(SUM(CHILD.dec),0.0) as dec
                        FROM CHILD
                        GROUP BY CHILD.code,CHILD.name,CHILD.level,CHILD.type,CHILD.parent_id
                        UNION ALL
                        
                        SELECT  'Net', 
                            'Net Profit',
                            0,
                            'view',
                            0,
                            COALESCE(SUM(CHILD.jan),0.0) as jan,
                            COALESCE(SUM(CHILD.feb),0.0) as feb,
                            COALESCE(SUM(CHILD.mar),0.0) as mar,
                            COALESCE(SUM(CHILD.apr),0.0) as apr,
                            COALESCE(SUM(CHILD.may),0.0) as may,
                            COALESCE(SUM(CHILD.jun),0.0) as jun,
                            COALESCE(SUM(CHILD.jul),0.0) as jul,
                            COALESCE(SUM(CHILD.aug),0.0) as aug,
                            COALESCE(SUM(CHILD.sep),0.0) as sep,
                            COALESCE(SUM(CHILD.oct),0.0) as oct,
                            COALESCE(SUM(CHILD.nov),0.0) as nov,
                            COALESCE(SUM(CHILD.dec),0.0) as dec
                        FROM CHILD WHERE CHILD.type != 'view'
                        ) AS RESULT
                        ORDER BY RESULT.code
            """
            #print ("#x--->>", query % (date_from,date_to,str(tuple(account_type_ids))))
            self.env.cr.execute(query % (date_from, date_to, str(tuple(account_type_ids))))
            
            
            for account in self.env.cr.dictfetchall():
                balance = 0.0
                credit  = 0.0
                debit   = 0.0
                
                vals = {  # 'id'        : account.id,
                        'parent_id' : account['parent_id'],
                        'code'      : account['code'],
                        'name'      : account['name'],
                        'level'     : account['level'],
                        'sign'      : -1,#report.sign,
                        'type'      : account['type'],
                        # 'parent_left': account.parent_left
                        'jan'      : account['jan'],
                        'feb'      : account['feb'],
                        'mar'      : account['mar'],
                        'apr'      : account['apr'],
                        'may'      : account['may'],
                        'jun'      : account['jun'],
                        'jul'      : account['jul'],
                        'aug'      : account['aug'],
                        'sep'      : account['sep'],
                        'oct'      : account['oct'],
                        'nov'      : account['nov'],
                        'dec'      : account['dec'],
                        }
                    
                result.append(vals)
        return result
    
    def compute_values_monthly(self, data):
        account_obj             = self.env['account.account']
        account_fin_report_obj  = self.env['account.financial.report']
        move_line_obj           = self.env['account.move.line']
        
        date_from   = data.date_from
        date_to     = data.date_to
        target_move = data.target_move
        filter_state= target_move=='posted' and " where state = '%s' " % target_move or ""
        
        result = []
        

        for report in data.account_report_id:
            account_ids = []
            search_domain = []
            if date_from:
                search_domain += [('date', '>=', date_from)]
            if date_to:
                search_domain += [('date', '<=', date_to)]
            if target_move == 'posted':
                search_domain += [('move_id.state', '=', 'posted')]
            
            
            if data.account_report_id.name == 'Profit and Loss':
                account_type_ids = []
                for report in data.account_report_id.children_ids:
                    account_type_ids += ([i.id for i in report.account_type_ids])
            else:
                continue
            
            # account_ids.reverse()
            
            query = """
                    SELECT * FROM (
                        WITH RECURSIVE CHILD AS (
                            SELECT  account.id, 
                                account.code as code, 
                                account.name, 
                                account.level,
                                account_type.type,
                                account.parent_id,
                                COALESCE(SUM(movel.debit),0.0) as debit,
                                COALESCE(SUM(movel.credit),0.0) as credit,
                                COALESCE(SUM(movel.balance),0.0) as balance
                                
                                FROM account_account account 
                                LEFT JOIN account_move_line movel ON account.id=movel.account_id AND movel.date >= '%s' AND movel.date <= '%s' AND movel.move_id in (select id from account_move """+filter_state+""") 
                                INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                                WHERE account_type.id in %s
                                GROUP BY account.id,account_type.type
                                /*ORDER BY account.parent_left DESC*/
                            UNION ALL
                            SELECT  account.id, 
                                account.code as code, 
                                account.name, 
                                account.level,
                                account_type.type,
                                account.parent_id,
                                COALESCE(CHILD.debit,0.0) as debit,
                                COALESCE(CHILD.credit,0.0) as credit,
                                COALESCE(CHILD.balance,0.0) as balance
                                
                                FROM account_account account LEFT JOIN account_move_line movel ON account.id=movel.account_id 
                                INNER JOIN account_account_type account_type ON account.user_type_id=account_type.id
                                INNER JOIN CHILD ON account.id=CHILD.parent_id
                                WHERE account_type.type = 'view'
                            )
                        SELECT  
                        CHILD.id,
                        CHILD.code, 
                            CHILD.name,
                            CHILD.level,
                            CHILD.type,
                            CHILD.parent_id,
                            COALESCE(SUM(CHILD.debit),0.0) as debit,
                            COALESCE(SUM(CHILD.credit),0.0) as credit,
                            COALESCE(SUM(CHILD.balance),0.0) as balance
                        FROM CHILD
                        GROUP BY CHILD.id,CHILD.code,CHILD.name,CHILD.level,CHILD.type,CHILD.parent_id
                        
                        UNION ALL
                        
                        SELECT  
                        0,
                        'Net', 
                            'Net Profit',
                            0,
                            'view',
                            0,
                            COALESCE(SUM(CHILD.debit),0.0) as debit,
                            COALESCE(SUM(CHILD.credit),0.0) as credit,
                            COALESCE(SUM(CHILD.balance),0.0) as balance
                        FROM CHILD WHERE CHILD.type != 'view'
                        ) AS RESULT
                        ORDER BY RESULT.code
            """
            #print ("#x--->>", query % (date_from,date_to,str(tuple(account_type_ids))))
            self.env.cr.execute(query % (date_from, date_to, str(tuple(account_type_ids))))
            
            
            for account in self.env.cr.dictfetchall():
                balance = 0.0
                credit  = 0.0
                debit   = 0.0
                
                
                vals = {  
                        'id'        : account['id'],
                        'parent_id' : account['parent_id'],
                        'code'      : account['code'],
                        'name'      : account['name'],
                        'balance'   : account['balance'],
                        'debit'     : account['debit'],
                        'credit'    : account['credit'],
                        'level'     : account['level'],
                        'sign'      : -1,#report.sign,
                        'type'      : account['type'],
                        # 'parent_left': account.parent_left
                        }
                    
                result.append(vals)
        return result

class ProfitLossReportXlsx(models.AbstractModel):
    _name = 'report.nieve_financial_report.report_indonesia_profit_loss_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    
    def generate_xlsx_report(self, workbook, data, wizard):
        if wizard.type_filter=='filter_12months':
            self.profit_loss_12monthly(workbook, data, wizard)
        else:
            self.profit_loss_monthly(workbook, data, wizard)
        
    
    def profit_loss_12monthly(self, workbook, data, wizard):
        pl_compute_obj = self.env['profit.loss.compute.report']
        report = wizard.account_report_id
        
        print ("##wizard.account_report_id--->>", wizard.account_report_id)
        
        sheet = workbook.add_worksheet(report.name)
        sheet.hide_gridlines(2)
        #######
        
        ##Style
        company_header_style = workbook.add_format({'bold': True, 'font_color': 'black'})
        company_header_style.set_font_size(16)
        company_header_style.set_align('center')
        
        report_label_header_style = workbook.add_format({'bold': True, 'font_color': '#000080'})
        report_label_header_style.set_font_size(26)
        report_label_header_style.set_align('center')
        
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        number = workbook.add_format({'bold': False, 'font_color': 'black'})
        number.set_num_format('#,##0.00;(#,##0.00)')
        
        string_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold.set_num_format('#,##0.00;(#,##0.00)')
        
        table_header_style = workbook.add_format({'bold': True, 'font_color': '#B22222'})
        table_header_style.set_align('center')
        table_header_style.set_bottom(1)
        
        ##
        
        sheet.show_grid = 0
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.portrait = 0  # Landscape
        sheet.fit_width_to_pages = 1
        
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 60)  # Source No.
        sheet.set_column('C:C', 20)  # Source No.
        
        sheet.merge_range('A1:N1', wizard.company_id.name, company_header_style)
        sheet.merge_range('A2:N2', report.name, report_label_header_style)
        
        sheet.write(3, 0, 'Date Range filter : ',)
        sheet.write(3, 1, 'From: ' + wizard.date_from + " To " + wizard.date_to,)
        
        sheet.write(4, 0, 'Post Filter : ',)
        sheet.write(4, 1, wizard.target_move.capitalize() or "",)
        
        row = 9
         
        sheet.write(row, 0, 'Code', table_header_style)
        sheet.write(row, 1, 'Account', table_header_style)
        sheet.write(row, 2, 'January', table_header_style)
        sheet.write(row, 3, 'February', table_header_style)
        sheet.write(row, 4, 'March', table_header_style)
        sheet.write(row, 5, 'April', table_header_style)
        sheet.write(row, 6, 'May', table_header_style)
        sheet.write(row, 7, 'June', table_header_style)
        sheet.write(row, 8, 'July', table_header_style)
        sheet.write(row, 9, 'August', table_header_style)
        sheet.write(row, 10, 'September', table_header_style)
        sheet.write(row, 11, 'October', table_header_style)
        sheet.write(row, 12, 'November', table_header_style)
        sheet.write(row, 13, 'December', table_header_style)
        row += 1
          
        for o in pl_compute_obj.compute_values_12month(wizard):
            if o['type']=='view':
                string_style = string_bold
                number_style = number_bold
            else:
                string_style = string
                number_style = number
              
            if o['name']=='Net Profit':
                sheet.write(row,0,"", number_style)
                sheet.write(row,1,o['level']*"        " +o['name'],number_style)
                sheet.write(row,2,o['jan']*o['sign'],number_style)
                sheet.write(row,3,o['feb']*o['sign'],number_style)
                sheet.write(row,4,o['mar']*o['sign'],number_style)
                sheet.write(row,5,o['apr']*o['sign'],number_style)
                sheet.write(row,6,o['may']*o['sign'],number_style)
                sheet.write(row,7,o['jun']*o['sign'],number_style)
                sheet.write(row,8,o['jul']*o['sign'],number_style)
                sheet.write(row,9,o['aug']*o['sign'],number_style)
                sheet.write(row,10,o['sep']*o['sign'],number_style)
                sheet.write(row,11,o['oct']*o['sign'],number_style)
                sheet.write(row,12,o['nov']*o['sign'],number_style)
                sheet.write(row,13,o['dec']*o['sign'],number_style)
            else:
                sheet.write(row,0,o['code'], string_style)
                sheet.write(row,1,o['level']*"        " +o['name'],string_style)
                sheet.write(row,2,o['jan']*o['sign'],number_style)
                sheet.write(row,3,o['feb']*o['sign'],number_style)
                sheet.write(row,4,o['mar']*o['sign'],number_style)
                sheet.write(row,5,o['apr']*o['sign'],number_style)
                sheet.write(row,6,o['may']*o['sign'],number_style)
                sheet.write(row,7,o['jun']*o['sign'],number_style)
                sheet.write(row,8,o['jul']*o['sign'],number_style)
                sheet.write(row,9,o['aug']*o['sign'],number_style)
                sheet.write(row,10,o['sep']*o['sign'],number_style)
                sheet.write(row,11,o['oct']*o['sign'],number_style)
                sheet.write(row,12,o['nov']*o['sign'],number_style)
                sheet.write(row,13,o['dec']*o['sign'],number_style)
            row += 1

    def qwery_account_id(self,acc_id):
        self.env.cr.execute("""  
                        select 
                            id,
                            code
                        from account_account
                        where code ='%s'
                        """ % (str(acc_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def profit_loss_monthly(self, workbook, data, wizard):
        pl_compute_obj = self.env['profit.loss.compute.report']
        report = wizard.account_report_id
        
        print ("##wizard.account_report_id--->>", wizard.account_report_id)
        print ("##wizard.acc_ending_balance--->>", wizard.acc_ending_balance)
        
        sheet = workbook.add_worksheet(report.name)
        sheet.hide_gridlines(2)
        #######
        
        ##Style
        company_header_style = workbook.add_format({'bold': True, 'font_color': 'black'})
        company_header_style.set_font_size(16)
        company_header_style.set_align('center')
        
        report_label_header_style = workbook.add_format({'bold': True, 'font_color': '#000080'})
        report_label_header_style.set_font_size(26)
        report_label_header_style.set_align('center')
        
        string = workbook.add_format({'bold': False, 'font_color': 'black'})
        number = workbook.add_format({'bold': False, 'font_color': 'black'})
        number.set_num_format('#,##0.00;(#,##0.00)')
        border_all_header_none       = workbook.add_format({'bold': True,'font_size': 13,'align':'center','bottom':2,'top':2,'left':2,'right':2,})
        
        
        string_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        string_bold_colour_ungu = workbook.add_format({'bold': True, 'font_color': '#C606FF','font_size': 14})
        number_bold = workbook.add_format({'bold': True, 'font_color': '#000080'})
        number_bold.set_num_format('#,##0.00;(#,##0.00)')
        number_bold_2 = workbook.add_format({'bold': True, 'font_color': '#C606FF','font_size': 14})
        number_bold_2.set_num_format('#,##0.00;(#,##0.00)')
        
        table_header_style = workbook.add_format({'bold': True, 'font_color': '#B22222'})
        table_header_style.set_align('center')
        table_header_style.set_bottom(1)
        
        ##
        
        sheet.show_grid = 0
        sheet.panes_frozen = True
        sheet.remove_splits = True
        sheet.portrait = 0  # Landscape
        sheet.fit_width_to_pages = 1
        
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 30)  # Source No.
        sheet.set_column('C:C', 30)  # Source No.
        
        sheet.merge_range('A1:C1', wizard.company_id.name, company_header_style)
        sheet.merge_range('A2:C2', report.name, report_label_header_style)
        
        sheet.write(3, 0, 'Date Range filter : ',)
        sheet.write(3, 1, 'From: ' + wizard.date_from + " To " + wizard.date_to,)
        
        sheet.write(4, 0, 'Post Filter : ',)
        sheet.write(4, 1, wizard.target_move.capitalize() or "",)
        
        row = 9
         
        sheet.write(row, 0, 'Code', table_header_style)
        sheet.write(row, 1, 'Account', table_header_style)
        sheet.write(row, 2, 'Balance', table_header_style)
        row += 1
        Gross_Margin =0.0
        total_pendapatan=0.0
        total_cogs=0.0
        total_pengeluaran =0.0
        operating_profit=0.0
        total_pendapatan_lain =0.0
        total_pengeluaran_lain=0.0
        net_profit=0.0     
        account_dicts = pl_compute_obj.compute_values_monthly(wizard)
        for o in account_dicts:
            if o['type']=='view':
                string_style = string_bold
                number_style = number_bold
            else:
                string_style = string
                number_style = number
            if o['type']=='view' and o['parent_id']==None and o['level']==0:
                sheet.write(row,0,o['level']*"        " +o['name'],string_style)
                code_level_1 =o['code']
                row += 1
                obj_parent =  list(filter(lambda d: d.get('parent_id') == o['id'],account_dicts ))
                for i in obj_parent:
                    if i.get('type')=='view':
                        string_style = string_bold
                        number_style = number_bold
                    else:
                        string_style = string
                        number_style = number
                    sheet.write(row,0,i.get('level')*"        " +i.get('name'),string_style)
                    code_level_2 =i.get('code')
                    row += 1
                    obj_parent_2 =  list(filter(lambda s: s.get('parent_id') == i.get('id'),account_dicts ))
                    for s in obj_parent_2:  
                        if s.get('type')=='view':
                            string_style = string_bold
                            number_style = number_bold
                        else:
                            string_style = string
                            number_style = number
                        if wizard.acc_ending_balance==True and s.get('balance') in [0.0,-0.0]:
                            continue
                        sheet.write(row,0,s.get('level')*"        " +s.get('code'), string_style)
                        sheet.write(row,1,s.get('level')*"        " +s.get('name'),string_style)
                        sheet.write(row,2,s.get('code')[:1] in ('4','8') and s.get('balance')*s.get('sign') or s.get('balance'),number_style) 
                        ['4']
                        row += 1
                    sheet.write(row,0,i.get('level')*"        " +'Total '+i.get('name'),string_bold)  
                    sheet.write(row,2,i.get('code')[:1] in ('4','8') and i.get('balance')*i.get('sign') or i.get('balance'),number_bold) 
                    row += 1
                    row = row+1
                v_total_pendapatan          = self.env['config.report.profit.loss'].search([('name','=','code_account_total_pendapatan')]).value
                v_total_cogs                = self.env['config.report.profit.loss'].search([('name','=','code_account_total_cogs')]).value
                v_total_pengeluaran         = self.env['config.report.profit.loss'].search([('name','=','code_account_total_pengeluaran')]).value
                v_total_pengeluaran_lain    = self.env['config.report.profit.loss'].search([('name','=','code_account_total_pengeluaran_lain')]).value
                v_total_pendapatan_lain    = self.env['config.report.profit.loss'].search([('name','=','code_account_total_pendapatan_lain')]).value
                if o['code']==v_total_pendapatan:
                    total_pendapatan = o['balance']*o['sign']
                if o['code']==v_total_cogs:
                    total_cogs = o['balance']
                if o['code']==v_total_pengeluaran:
                    total_pengeluaran = o['balance']
                if o['code']==v_total_pengeluaran_lain:
                    total_pengeluaran_lain = o['balance']
                if o['code']==v_total_pendapatan_lain:
                    total_pendapatan_lain = o['balance']*o['sign']
                Gross_Margin = (total_pendapatan - total_cogs)
                operating_profit = (Gross_Margin - total_pengeluaran)
                net_profit = (operating_profit + total_pendapatan_lain - total_pengeluaran_lain)
                sheet.write(row,0,o['level']*"        " +'Total '+o['name'],string_bold)  
                sheet.write(row,2,o['code'][:1] in ('4','8') and o['balance']*o['sign'] or o['balance'],number_bold) 
                row += 1
                row = row+1
                v_gros_margin      = self.env['config.report.profit.loss'].search([('name','=','set_position_gross_margin')]).value
                v_operating_profit = self.env['config.report.profit.loss'].search([('name','=','set_position_operating_profit')]).value
                v_net_profit       = self.env['config.report.profit.loss'].search([('name','=','set_position_net_profit')]).value
                if o['code']==v_gros_margin:
                    sheet.write(row-1,1,'Gross Margin',string_bold_colour_ungu)  
                    sheet.write(row-1,2,Gross_Margin,number_bold_2) 
                    row += 1
                if o['code']==v_operating_profit:
                    sheet.write(row-1,1,'Operating Profit',string_bold_colour_ungu)  
                    sheet.write(row-1,2,operating_profit,number_bold_2) 
                    row += 1
                if o['code']==v_net_profit:
                    sheet.write(row-1,1,'Net Profit',string_bold_colour_ungu)  
                    sheet.write(row-1,2,net_profit,number_bold_2) 
                    row += 1
                #         # sheet.merge_range('B'+str(row+2)+str(':')+'C'+str(row+2),'Approval Report Template',border_all_header_none)  
                #         # sheet.merge_range('B'+str(row+3)+str(':')+'B'+str(row+7),'',border_all_header_none)  
                #         # sheet.merge_range('C'+str(row+3)+str(':')+'C'+str(row+7),'',border_all_header_none)  
                #         # sheet.write('B'+str(row+8),'Hari Setyarini',border_all_header_none)
                #         # sheet.write('C'+str(row+8),'Stephen Sudyatmiko',border_all_header_none)
