{
    "name": "Nieve Financial Report",
    "version": "11.0",
    "depends": ['account','report_xlsx'],
    "author": "NIEVE",
    "category": "",
    "description": """
       Nieve Financial Report
       last update 04-11-2018
    """,
    "init_xml": [],
    'data': [
             'data/param_config_pnl.xml',
             'reports/reports.xml',
             'wizard/account_report_common_view.xml',
             'reports/balance_sheet_pdf_view.xml',
             'reports/profit_loss_pdf_view.xml',
             'views/config_report_profit_loss.xml',
             
            #'report/report_qweb_profit.xml',
            #'report/report_qweb_balance_sheet.xml'
             ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
