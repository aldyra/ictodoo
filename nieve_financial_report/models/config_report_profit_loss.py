from odoo import api, fields, models, _
from odoo.exceptions import UserError

class ConfigReportProfitLoss(models.Model):
    _name = 'config.report.profit.loss'

    name  =fields.Char('Key')
    value =fields.Char('Value')