from odoo import api, fields, models, _

class DocumentCheck(models.Model):
    _name = 'document.check'
    
    name        = fields.Char(string='Document Name', required=True)   
    validate    = fields.Boolean(string='Validate')
    
    partner_id  = fields.Many2one('res.partner', string='Partner')
    term_id     = fields.Many2one('fal.invoice.term.line', string='Terms Payment')
    invoice_id  = fields.Many2one('account.invoice', string='Invoice')
    