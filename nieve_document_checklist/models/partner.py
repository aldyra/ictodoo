from odoo import api, fields, models, _



class Partner(models.Model):
    _inherit = 'res.partner'
    

    
    def default_document_checklist(self):
        doc_list = ['PO/Contract','BAST','DN','Others']
        return [(0, 0, {'name': i}) for i in doc_list]
    
    document_checklist  = fields.One2many('document.check', 'partner_id', string='Document Checklist', default=default_document_checklist)