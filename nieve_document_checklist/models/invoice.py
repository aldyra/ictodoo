from odoo import api, fields, models, _
from odoo.exceptions import UserError

class Invoice(models.Model):
    _inherit = 'account.invoice'
    
#     def default_get(self, fields):
#         res = super(Partner, self).default_get(fields)
#         srd = self.env['schoolresults.detail']
#         ids=[]
#         school_result={'subject_id':1,'result':0} #dict for fields and their values
#         sr = srd.create(school_result)
#         ids.append(sr.id)
#     
#     
#         res['result_ids'] = ids
#         return res
    
    @api.onchange('partner_id')
    def onchange_document_list(self):
        ctx = dict(self.env.context)
        
        if not ctx['type'] == 'out_invoice':
            self.document_checklist = []
        
        doc_list = []
        if self.partner_id:
            if not self.partner_id.document_checklist:
                doc_list = ['PO/Contract','BAST','DN','Others']
            else:
                for i in self.partner_id.document_checklist:
                    doc_list.append(i.name)
        self.document_checklist = [(0, 0, {'name': i}) for i in doc_list]
    
#     def default_document_checklist(self):
#         print ("###default_document_checklist###")
#         doc_list = ['PO/Contract','BAST','DN','Others']
#         return [(0, 0, {'name': i}) for i in doc_list]
    
    document_checklist  = fields.One2many('document.check', 'invoice_id', string='Document Checklist')
    
    
    
class FalInvoiceTermLine(models.Model):
    _inherit = 'fal.invoice.term.line'
    
    @api.model
    def create(self, vals):
        print ("vals---->", vals)
        # set up context used to find the lead's sales channel which is needed
        # to correctly set the default stage_id
        context = dict(self._context or {})
        
        cs_obj = self.env['kk.cost.sheet']
        cost_sheet_id = cs_obj.browse([vals['cs_cost_sheet_id']])
        
        doc_list = []
        if cost_sheet_id.partner_id:
            if not cost_sheet_id.partner_id.document_checklist:
                doc_list = ['PO/Contract','BAST','DN','Others']
            else:
                for i in cost_sheet_id.partner_id.document_checklist:
                    doc_list.append(i.name)
        vals['document_checklist'] = [(0, 0, {'name': i}) for i in doc_list]
        
        
#         if vals.get('type') and not self._context.get('default_type'):
#             context['default_type'] = vals.get('type')
#         if vals.get('team_id') and not self._context.get('default_team_id'):
#             context['default_team_id'] = vals.get('team_id')
# 
#         if vals.get('user_id') and 'date_open' not in vals:
#             vals['date_open'] = fields.Datetime.now()
# 
#         if context.get('default_partner_id') and not vals.get('email_from'):
#             partner = self.env['res.partner'].browse(context['default_partner_id'])
#             vals['email_from'] = partner.email

        # context: no_log, because subtype already handle this
        return super(FalInvoiceTermLine, self.with_context(context, mail_create_nolog=True)).create(vals)
    
    document_checklist  = fields.One2many('document.check', 'term_id', string='Document Checklist')
    
    @api.multi
    def action_invoice_create(self):
        res = super(FalInvoiceTermLine, self).action_invoice_create()
        
        doc_list = []
        
        partner_id = self.cost_sheet_id.partner_id
        
        if self.document_checklist:
            for i in self.document_checklist:
                doc_list.append({'name': i.name, 'value': i.validate})
        elif not partner_id.document_checklist:
            doc_list = ['PO/Contract','BAST','DN','Others']
        else:
            for i in partner_id.document_checklist:
                doc_list.append({'name': i.name, 'value': False})
        res.document_checklist = [(0, 0, {'name': i['name'], 'validate': i['value']}) for i in doc_list]
        
        return res