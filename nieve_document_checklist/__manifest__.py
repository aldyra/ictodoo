# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Nieve Document Checklist",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.nievetechnology.com',
    'category': 'General',
    'summary': 'Discount',
    "description": """
        Last Update 24 oct 2019
        Add list Default Checklist
    """,
    "depends": [
        'base','account','nieve_cost_sheet','nieve_invoice_milestone'],
    'data': [
        'views/partner_view.xml',
        'views/invoice_view.xml',
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
