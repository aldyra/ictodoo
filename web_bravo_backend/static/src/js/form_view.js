/*  Copyright 2018 UNIBRAVO
  * License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl). */

odoo.define('web_bravo_backend.FormView', function (require) {
"use strict";

var config = require('web.config');
var FormRenderer = require('web.FormRenderer');
var FormView = require('web.FormView');
var MobileFormRenderer = require('web_bravo_backend.MobileFormRenderer');

FormView.include({

    getRenderer: function () {
        this.config.Renderer = config.device.isMobile ? MobileFormRenderer : FormRenderer;
        return this._super.apply(this, arguments);
    }
});

});
