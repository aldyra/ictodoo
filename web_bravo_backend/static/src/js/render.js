/* Copyright 2018 UNIBRAVO
 * License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl). */

odoo.define('web_bravo_backend.MobileFormRenderer', function (require) {
"use strict";

var core = require('web.core');
var FormRenderer = require('web.FormRenderer');
var qweb = core.qweb;
var MobileFormRenderer = FormRenderer.extend({

    _renderHeaderButtons: function (node) {
        var $headerButtons = $();
        var buttonChildren = _.filter(node.children, {tag: 'button'});
        var buttons = _.map(buttonChildren, this._renderHeaderButton.bind(this));

        if (buttons.length) {
            $headerButtons = $(qweb.render('StatusbarButtons'));
            var $dropdownMenu = $headerButtons.find('.dropdown-menu');
            _.each(buttons, function ($button) {
                $dropdownMenu.append($('<li>').append($button));
            });
        }

        return $headerButtons;
    },
});

return MobileFormRenderer;

});

odoo.define('web_bravo_backend.relational_fields', function (require) {
"use strict";

var config = require('web.config');
var core = require('web.core');
var relational_fields = require('web.relational_fields');

var FieldStatus = relational_fields.FieldStatus;
var qweb = core.qweb;

var _t = core._t;

FieldStatus.include({

    _render: function () {
        if (config.device.isMobile) {
            this.$el.html(qweb.render("FieldStatus.content.mobile", {
                selection: this.status_information,
                status: _.findWhere(this.status_information, {selected: true}),
                clickable: !!this.attrs.clickable,
            }));
        } else {
            return this._super.apply(this, arguments);
        }
    },
});

relational_fields.FieldOne2Many.include({

    _renderButtons: function () {
        if (this.activeActions.create) {
            _.extend(this.nodeOptions, { create_text: _t('Add an Item')});
            this._super.apply(this, arguments);
        }
    },
});

});