# -*- coding: utf-8 -*-
# OPL-1 Licensing. See LICENSE file for full copyright and licensing details.
# Copyright 2018 UNIBRAVO. @author UNIBRAVO <info@unibravo.com>  License OPL-1 (http://www.odoo.com)
{
    'name': 'Bravo Backend Theme v11',
    'category': 'Themes/Backend',
    'version': '11.0.6.0',
    'author': 'UNIBRAVO',
    "website": "https://www.unibravo.com",
    'description':
        """
Good-looking Backend User Interface for Odoo v11.0
==================================================

This well-favored backend theme module modifies the Odoo Backend User Interface to provide Professional design and responsiveness.
        """,
    'depends': ['web'],
    'images':['images/theme_page.png', 'images/tablet_screenshot.png'],
    'application': False,
    'auto_install': False,
    'installable': True,
    "data": [
        'views/assets.xml',
        'views/web.xml',
    ],
    'qweb': [
        'static/src/xml/*',
    ],
    'license': 'OPL-1',
    'live_test_url': 'http://212.237.62.27:8011/web?db=odoo11_bravo',
    'price': 110,
    'currency': 'EUR',
}
