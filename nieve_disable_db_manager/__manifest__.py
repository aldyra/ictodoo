# -*- coding: utf-8 -*-
{
    'name': "Nieve - Disable Database Manager",

    'summary': """
        this module provide disable database manager""",

    'description': """
        Disable database manager
    """,

    'author': "Novallingga Dirgantara",
    'website': "http://www.nievetechnology.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Web',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['web'],

    # always loaded
    'data': [
        'views/login.xml',
    ],
}