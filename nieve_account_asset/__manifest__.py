

{
    "name"          : "Asset Management New",
    "version"       : "1.0",
    "author"        : "Nieve",
    "category"      : "Asset",
    'complexity'    : "normal",
    "description"   : """Last Update 5-12-2018 Asset Management
    \n\nAdded :\
    \n    - Porting V11\
    \n    - Set Mandatory Purchase Date\
    \n
    """,
    "website"       : "",
    'images'        : [],
    "init_xml"      : [],
    "depends": ['account','account_asset', 'report_xlsx'
                ],
    "update_xml"    : [
                'report/asset_data.xml',
                'asset_view.xml',
                'wizard_report/account_asset_data_view.xml',
                'wizard/asset_confirm_view.xml',
                'product_view.xml',

                    ],
    "demo_xml"      : [],
    'test'          : [],
    "active"        : False,
    "installable"   : True,
    'auto_install'  : False,
    'certificate'   : '',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
