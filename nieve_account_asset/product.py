from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class product_product(models.Model):
    _inherit = 'product.template'


    asset_product = fields.Selection([('non_asset','Non Asset'),
                              ('asset','Asset')],'Asset/Non Asset', default='non_asset')
    asset_category_id = fields.Many2one('account.asset.category', 'Asset Category')
    brand = fields.Char('Brand', size=64)
    part_number = fields.Char('Part Number', size=64)


product_product()
