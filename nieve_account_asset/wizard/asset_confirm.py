from odoo import models, fields, api, _, osv
from odoo.exceptions import except_orm, Warning, RedirectWarning
from datetime import datetime, date, time, timedelta
import time
from dateutil.relativedelta import relativedelta

class asset_confirm_wizz(models.TransientModel):
    _name = "asset.confirm.wizz"
    _description = "Asset Confirm"

    _columns = {
                }

    def asset_confirm(self, cr, uid, ids, context=None):

        asset_obj = self.env['account.asset.asset']
        mod_obj =self.env['ir.model.data']
        if context is None:
            context = {}

        for asset in asset_obj.browse(cr,uid,context.get('active_ids',[])):
            if asset.state == 'draft':
                asset_obj.validate(cr, uid, [asset.id])

        #raise osv.except_osv(_('Error!'), _('XXXXXXXX123'))
        #asset_obj.check_asset_register_merge(cr, uid, context.get('active_ids',[]), context)
        #allorders = asset_reg_obj.asset_register_merge(cr, uid, context.get('active_ids',[]), context)

        return True
#         return {
#             'domain': "[('id','in', [" + ','.join(map(str, allorders.keys())) + "])]",
#             'name': _('Asset'),
#             'view_type': 'form',
#             'view_mode': 'tree,form',
#             'res_model': 'account.asset.asset',
#             'view_id': False,
#             'type': 'ir.actions.act_window',
#             'search_view_id': id['res_id']
#         }

# asset_confirm_wizz()
