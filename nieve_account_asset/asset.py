
import calendar
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.tools import float_compare, float_is_zero


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'

    @api.multi
    def write(self, vals):
        res = super(AccountAssetAsset, self).write(vals)
#         if 'depreciation_line_ids' not in vals and 'state' not in vals:
#             for rec in self:
#                 rec.compute_depreciation_board()
        return res

    @api.multi
    def manual_asset_close(self):
        self.write({'state' : 'close'})
        return True

    product_id          = fields.Many2one('product.product','Product')
    brand               = fields.Char('Brand', size=64)
    part_number         = fields.Char('Part Number', size=64)
    serial_number       = fields.Char('Serial Number', size=64)
    # parent_district_id  = fields.Many2one('hr.parent.district', 'District')
    # district_id         = fields.Many2one('hr.district', 'Sub District')
    location            = fields.Char('Location', size=128)
    asset_type          = fields.Selection([('tangible','Tangible'), ('intangible','Intangible')], 'Asset Type')

    purchase_date       = fields.Date(string='Purchase Date')
    date       = fields.Date(string='Depreciation Start Date')

    @api.multi
    def compute_depreciation_board(self):
        self.ensure_one()

        posted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: x.move_check).sorted(key=lambda l: l.depreciation_date)
        unposted_depreciation_line_ids = self.depreciation_line_ids.filtered(lambda x: not x.move_check)

        # Remove old unposted depreciation lines. We cannot use unlink() with One2many field
        commands = [(2, line_id.id, False) for line_id in unposted_depreciation_line_ids]

        if self.value_residual != 0.0:
            amount_to_depr = residual_amount = self.value_residual
            if self.prorata:
                # if we already have some previous validated entries, starting date is last entry + method period
                if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                    last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date, DF).date()
                    depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                else:
                    depreciation_date = datetime.strptime(self._get_last_depreciation_date()[self.id], DF).date()
            else:
                # depreciation_date = 1st of January of purchase year if annual valuation, 1st of
                # purchase month in other cases

                #print "Before : ", self.date
                #print "After  : ", self.date + relativedelta(months=+self.method_period)
                if self.method_period >= 12:
                    asset_date = datetime.strptime(self.date[:4] + '-01-01', DF).date()
                else:
                    asset_date = datetime.strptime(self.date[:7] + '-01', DF).date()
                # if we already have some previous validated entries, starting date isn't 1st January but last entry + method period
                if posted_depreciation_line_ids and posted_depreciation_line_ids[-1].depreciation_date:
                    last_depreciation_date = datetime.strptime(posted_depreciation_line_ids[-1].depreciation_date, DF).date()
                    depreciation_date = last_depreciation_date + relativedelta(months=+self.method_period)
                else:
                    depreciation_date = asset_date
            day = depreciation_date.day
            month = depreciation_date.month
            year = depreciation_date.year
            total_days = (year % 4) and 365 or 366

            undone_dotation_number = self._compute_board_undone_dotation_nb(depreciation_date, total_days)

            for x in range(len(posted_depreciation_line_ids), undone_dotation_number):
                sequence = x + 1
                amount = self._compute_board_amount(sequence, residual_amount, amount_to_depr, undone_dotation_number, posted_depreciation_line_ids, total_days, depreciation_date)
                amount = self.currency_id.round(amount)
                if float_is_zero(amount, precision_rounding=self.currency_id.rounding):
                    continue
                residual_amount -= amount

                ##Set to Last date of Month
                lastdate_of_month = calendar.monthrange(depreciation_date.year, depreciation_date.month)[-1]
                depreciation_date = datetime.strptime(str(depreciation_date)[:7] + "-"+str(lastdate_of_month), DF).date()
                ##
                vals = {
                    'amount': amount,
                    'asset_id': self.id,
                    'sequence': sequence,
                    'name': (self.code or '') + '/' + str(sequence),
                    'remaining_value': residual_amount,
                    'depreciated_value': self.value - (self.salvage_value + residual_amount),
                    'depreciation_date': depreciation_date.strftime(DF),
                }
                commands.append((0, False, vals))
                # Considering Depr. Period as months
                depreciation_date = date(year, month, day) + relativedelta(months=+self.method_period)
                day = depreciation_date.day
                month = depreciation_date.month
                year = depreciation_date.year

        self.write({'depreciation_line_ids': commands})

        return True

#     def _get_image(self, cr, uid, ids, name, args, context=None):
#         result = dict.fromkeys(ids, False)
#         for obj in self.browse(cr, uid, ids, context=context):
#             result[obj.id] = tools.image_get_resized_images(obj.image)
#         return result
#
#     def _set_image(self, cr, uid, id, name, value, args, context=None):
#         return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)

#         'image': fields.binary("Photo",
#                                help="This field holds the image used as photo for the employee, limited to 1024x1024px."),
#         'image_medium': fields.function(_get_image, fnct_inv=_set_image,
#                 string="Medium-sized photo", type="binary", multi="_get_image",
#                 store = {
#                     'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
#                 },
#                 help="Medium-sized photo of the employee. It is automatically "\
#                      "resized as a 128x128px image, with aspect ratio preserved. "\
#                      "Use this field in form views or some kanban views."),
#         'image_small': fields.function(_get_image, fnct_inv=_set_image,
#                 string="Small-sized photo", type="binary", multi="_get_image",
#                 store = {
#                     'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
#                 },
#                 help="Small-sized photo of the employee. It is automatically "\
#                      "resized as a 64x64px image, with aspect ratio preserved. "\
#                      "Use this field anywhere a small image is required."),
#
#         'image_1': fields.binary("Image 1",
#                                help="This field holds the image used as avatar for this contact, limited to 1024x1024px"),
#         'image_2': fields.binary("Image 2",
#                                help="This field holds the image used as avatar for this contact, limited to 1024x1024px"),
#         'image_3': fields.binary("Image 3",
#                                help="This field holds the image used as avatar for this contact, limited to 1024x1024px"),


        #'value_residual': fields.function(_amount_residual, method=True, digits_compute=dp.get_precision('Account'), string='Residual Value'),

#     def onchange_product_id(self, cr, uid, ids, product_id):
#         print "onchange_product_id", product_id
#         res = {'value':{'brand' : '', 'part_number': '', 'category_id' : False}}
#
#         product_obj = self.pool.get('product.product')
#
#         if not product_id:
#             return res
#
#         for val in product_obj.browse(cr,uid,[product_id],context=None):
#             brand           = val.brand or ''
#             part_number     = val.part_number or ''
#             category_id     = val.asset_category_id.id or False
#         res['value']['brand']       = brand
#         res['value']['part_number'] = part_number
#         res['value']['category_id'] = category_id
#         return res

AccountAssetAsset()

class AccountAssetDepreciationLine(models.Model):
    _inherit = 'account.asset.depreciation.line'

    move_check = fields.Boolean(string='Posted',compute='_get_move_check',track_visibility='always',store=True)
    direct_move_check = fields.Boolean(string='Direct Move',store=True,default=False)

    @api.one
    @api.depends('move_id','move_check','direct_move_check')
    def _get_move_check(self):
        if self.direct_move_check == True:
            self.move_check = True
        else:
            if bool(self.move_id) == False:
                self.move_check = False
            else:
                self.move_check = True


AccountAssetDepreciationLine()
