# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    Copyright (c) 2009 Zikzakmedia S.L. (http://zikzakmedia.com) All Rights Reserved.
#                       Jordi Esteve <jesteve@zikzakmedia.com>
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
# from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
# from odoo.report import report_sxw
from odoo import _, models, fields
import time
import datetime
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
from odoo import models


class asset_data_xls(models.AbstractModel):
    _name = 'report.nieve_account_asset.asset_data'
    _inherit = 'report.report_xlsx.abstract'



    def generate_xlsx_report(self, workbook, data, objects):

        today = dt.today()
        dateyear_start = today.replace(month=1, day=1)
        dateyear_stop = today.replace(month=12, day=31)

        sheet   = workbook.add_worksheet('Asset')
        title    = workbook.add_format({'bold': True,'font_size': 12,})
        header_table = workbook.add_format({'bold': True, 'bg_color': '#CCCC00'})
        bold    = workbook.add_format({'bold': True})
        f2d_bold     = workbook.add_format({'num_format': '#,##0.00_);(#,##0.00)','bold': True})
        f2d     = workbook.add_format({'num_format': '#,##0.00_);(#,##0.00)','font_size': 11, 'border': True,'align':'right'})
        border_all = workbook.add_format({'bold': True, 'font_size': 11, 'border': True,'align':'center'})
        border_all_line = workbook.add_format({'font_size': 11, 'border': True,'align':'left'})
        border_all_center = workbook.add_format({'font_size': 11, 'border': True,'align':'center'})
        border_all_right = workbook.add_format({'font_size': 11, 'border': True,'align':'right'})

        sheet.set_column('A:A', 10)  # Columns F-H width set to 30.
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 15)  # Columns F-H width set to 30.
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)  # Columns F-H width set to 30.
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 15)
        sheet.set_column('K:K', 20)
        sheet.set_column('L:L', 20)
        sheet.set_column('M:M', 20)
        sheet.set_column('N:N', 20)
        sheet.set_column('O:O', 20)
        sheet.set_column('P:P', 20)



        sheet.merge_range('A2:A3','Asset Code', border_all)
        sheet.merge_range('B2:B3','Asset Name', border_all)
        sheet.merge_range('C2:C3','Serial Number', border_all)
        sheet.merge_range('D2:D3','Period', border_all)
        sheet.merge_range('E2:E3','Purchase Date', border_all)
        sheet.merge_range('F2:F3','Category', border_all)
        sheet.merge_range('G2:G3','Location', border_all)
        sheet.write(1,7,'Masa manfaat', border_all)
        sheet.write(2,7,'Komersial', border_all)

        sheet.write(1,8,'Acqt. Cost', border_all)
        sheet.write(2,8,'Komersial', border_all)

        sheet.write(1,9,'Sisa Manfaat (Bulan)', border_all)
        sheet.write(2,9,'Komersial', border_all)

        sheet.write(1,10,'Depresiasi (Amount) (Perbulan)', border_all)
        sheet.write(2,10,'Komersial', border_all)

        sheet.write(1,11,'Akumulasi this year', border_all)
        sheet.write(2,11,'Komersial', border_all)

        sheet.write(1,12,'Akumulasi Depresiasi', border_all)
        sheet.write(2,12,'Komersial', border_all)

        sheet.write(1,13,'NBV (nilai Buku)', border_all)
        sheet.write(2,13,'Komersial', border_all)


        rows = 3
        data_report = self.env['asset.data.xls.report'].get_asset(data)
        for l in data_report:
            date_1 = dt.strptime(l.purchase_date, "%Y-%m-%d").strftime("%B-%Y")
            date_2 = dt.strptime(l.purchase_date, "%Y-%m-%d").strftime("%d-%m-%Y")
            sheet.write(rows, 0,'',border_all_line)
            sheet.write(rows, 1, l.name,border_all_line)
            sheet.write(rows, 2, l.serial_number,border_all_line)
            sheet.write(rows, 3, date_1,border_all_center)
            sheet.write(rows, 4, date_2,border_all_center)
            sheet.write(rows, 5, l.category_id.name,border_all_line)
            sheet.write(rows, 6, l.location,border_all_line)
            sheet.write(rows, 7, l.method_number,border_all_right)
            sheet.write(rows,13, l.value_residual,f2d)

            total_actual = 0.0
            residual = 0.0
            total_actual_2 = 0.0
            posted = 0.0
            plan = l.method_number
            sheet.write(rows,8,l.value or '', f2d)

            for n in l.depreciation_line_ids:
                if n.move_check:
                    total_actual += n.amount

                if n.move_check:
                    posted += 1
                    residual = plan-posted


                if str(n.depreciation_date) >= str(dateyear_start) and str(n.depreciation_date) <= str(dateyear_stop) and n.move_check:
                        total_actual_2 += n.amount

                sheet.write(rows,9,residual or 0.0, f2d)
                sheet.write(rows,10, n['amount'],f2d)
                sheet.write(rows,11, total_actual_2 or 0.0, f2d)
                sheet.write(rows,12, total_actual or 0.0,f2d)

            rows += 1
        pass
