# -*- coding: utf-8 -*-
##############################################################################
#
#   OpenERP, Open Source Management Solution
#   Copyright (C) 2013 DATABIT (<http://www.databit.co.id>). All Rights Reserved
#
##############################################################################

from odoo import api, fields, models


class wizz_asset_data(models.TransientModel):
    _name = 'wizz.asset.data'
    _description = 'Asset Report Data'


    company_id=fields.Many2one('res.company', 'Company', required=True)
    period_id=fields.Many2one('account.period', 'Period', required=False)

    @api.multi
    def create_asset_data_report(self, report_type):
        report_type = 'xlsx'
        self.ensure_one()
        report_name = 'nieve_account_asset.asset_data'
        report = self.env['ir.actions.report'].search(
                    [('report_type', '=', report_type), ('model', '=', 'wizz.asset.data')], limit=1)
        return report.report_action(self)
