# -*- coding: utf-8 -*-
##############################################################################
#
#   OpenERP, Open Source Management Solution
#   Copyright (C) 2013 DATABIT (<http://www.databit.co.id>). All Rights Reserved
#
##############################################################################

from . import account_asset_data

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
