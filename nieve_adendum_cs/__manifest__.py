# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Nieve Addendum Cost Sheet",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.sriwijayaair.co.id',
    'category': 'Sales',
    'summary': 'Adendum Cost Sheet',
    "description": """
    Module to group SO to Cost Sheet Last Updated 13-05-2020 

    """,
    "depends": ['nieve_cost_sheet'
    ],
    'data': ['security/access_security.xml',
            'data/adendum_cs_sequence.xml',
             'views/adendum_cost_sheet.xml'
       
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
