from odoo import fields, models, api, _
import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError
from datetime import datetime
from odoo import models, fields, api, _, osv
from odoo.exceptions import except_orm, Warning, RedirectWarning,ValidationError
import time
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

class AdendumCS(models.Model):
    _name = 'adendum.cost.sheet'
    _description = 'Addendum Cost Sheet'
    _inherit = ['mail.thread']
    _order = 'name'

    def qwery_team_manager(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where user_id ='%s'    
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_team_gm(self,gm_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where gm_user_id ='%s'    
                        """ % (str(gm_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'    
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x
    
    def qwery_group_type_team_director(self,dir_user_id):
        self.env.cr.execute("""  
                        select 
                            type_sales
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where dir_user_id ='%s'
                        GROUP BY type_sales
                        """ % (str(dir_user_id)))
        x = self.env.cr.dictfetchall()
        return x

    def qwery_group_sale(self,user_id):
        self.env.cr.execute("""  
                        select 
                            ct.id id_team,
                            user_id,
                            gm_user_id,
                            dir_user_id,
                            type_sales,
                            ru.id id_res_user
                        from crm_team  ct
                        left join (select id,sale_team_id from res_users) ru
                        on ru.sale_team_id = ct.id
                        where ru.id ='%s'
                        """ % (str(user_id)))
        x = self.env.cr.dictfetchall()
        return x


    @api.multi
    def cost_sheet_adendum_filter_dynamic(self):
        condition = True
        context = self._context
        data_obj = self.env['ir.model.data']
        act_window_obj = self.env['ir.actions.act_window']       
        action_window_id = data_obj.get_object_reference('nieve_adendum_cs', 'action_adendum_cost_sheet')[1]
        action_window    = self.env['ir.actions.act_window'].browse([action_window_id])
        
        cost_sheet = self
        
        login_uid = context.get('uid')
        user = self.env['res.users'].browse(login_uid)
        sales_allowed_access            = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_user')
        mgr_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_manager')
        gmgr_sales_allowed_access       = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_general_manager')
        drc_sales_allowed_access        = self.env['res.users'].has_group('nieve_invoice_milestone.group_saleorder_director')
        group_adendum_costsheet_all     = self.env['res.users'].has_group('nieve_adendum_cs.group_adendum_costsheet_all')
        
        team_ids = []
        filter_domain = []
        if group_adendum_costsheet_all:
            filter_domain = []
        elif drc_sales_allowed_access:
            team_director= self.qwery_group_type_team_director(login_uid)
            if team_director:
                jml = 0
                for team in team_director:
                    print ('type sales',team.get('type_sales'))
                    jml +=len(team)
                if jml > 1:
                    team_id = self.qwery_team_director(login_uid)   
                    print ('2 tipe sales',) 
                    if team_id:
                        user_team_product=[]
                        for item in team_id:
                            user_team_product.append(item.get('id_team'))
                            arrCost = []
                            for cost in self.env['kk.cost.sheet'].search([('creator_team_id','in',user_team_product)]):
                                arrCost.append(cost.id)
                            filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                else:
                    print ('1 tipe sales')
                    team_id = self.qwery_team_director(login_uid) 
                    if team_id:
                        user_team_product=[]
                        user_team=[]
                        for item in team_id:
                            if item.get('type_sales')=='sales_product':
                                user_team_product.append(item.get('id_team'))
                                user_team.append(item.get('id_res_user'))
                                user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                            if item.get('type_sales')=='sales_maintenance':
                                user_team.append(item.get('id_res_user'))
                                arrCost = []
                                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                                    arrCost.append(cost.id)
                                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        elif gmgr_sales_allowed_access:
            team_id = self.qwery_team_gm(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        elif mgr_sales_allowed_access:
            team_id = self.qwery_team_manager(login_uid) 
            if team_id:
                user_team_product=[]
                user_team=[]
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_team_product.append(item.get('id_team'))
                        user_team.append(item.get('id_res_user'))
                        user_sales_id = self.env['res.partner'].search([('user_id','in',user_team)])
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_team.append(item.get('id_res_user'))
                        arrCost = []
                        for cost in self.env['kk.cost.sheet'].search([('creator_user_id','in',user_team)]):
                            arrCost.append(cost.id)
                        filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
        else:            
            team_id = self.qwery_group_sale(login_uid)
            if team_id:
                for item in team_id:
                    if item.get('type_sales')=='sales_product':
                        user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
                    if item.get('type_sales')=='sales_maintenance':
                        user_sales_id = self.env['res.partner'].search([('user_maintenance_id','=',login_uid)])
                        obj_kk_cost = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_maintenance')])
                        filter_domain = [('kk_cost_sheet_id','in',obj_kk_cost.ids)]
            else:
                arrCost = []
                for cost in self.env['kk.cost.sheet'].search([('creator_user_id','=',login_uid)]):
                    arrCost.append(cost.id)
                filter_domain = [('kk_cost_sheet_id','in',arrCost)]
            
        if login_uid==1:
            filter_domain = []
        
        action = {
            #'id'        : action_view_id,
            'name'      : _('Addendum Cost Sheet'),
            'type'      : 'ir.actions.act_window',
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'res_model' : 'adendum.cost.sheet',
            #'search_view_id': search_view_id,
            'target'    : 'current',
            #'views'     :  [(tree_id,'tree'),(form_id,'form')],
            'domain'    : filter_domain,
            #'context'   : context_action,
        }
        return action

    
    name     = fields.Char('Name',track_visibility='onchange',default="New")
    date     = fields.Date('Date',track_visibility='onchange',default=datetime.today())
    kk_cost_sheet_id = fields.Many2one('kk.cost.sheet','Cost Sheet',track_visibility='onchange')
    partner_id = fields.Many2one('res.partner','Customer',track_visibility='onchange',compute="_compute_kk_cost_sheet",store=True)
    project_name = fields.Char('Project Name',track_visibility='onchange',compute="_compute_kk_cost_sheet",store=True)
    company_id = fields.Many2one('res.company','Company',track_visibility='onchange',compute="_compute_kk_cost_sheet",store=True)
    creator_user_id = fields.Many2one('res.users','Created By',track_visibility='onchange',readonly=True, default=lambda self: self.env.user.id)
    creator_team_id = fields.Many2one('crm.team','Team',track_visibility='onchange',compute="_compute_kk_cost_sheet",store=True)
    state = fields.Selection(
                            [
                                ('draft', 'Draft'),
                                ('appr_mgr', 'Approval Manager'),
                                ('appr_gm', 'Approval G.Manager'),
                                ('appr_drc', 'Approval Director'),
                                ('post', 'Post'),
                                ('cancel', 'Cancel'),
                            ], string='Status', readonly=True, copy=False,
                            index=True, track_visibility='onchange', default='draft')

    adendum_cost_sheet_line_ids = fields.One2many('adendum.cost.sheet.line','adendum_cost_sheet_id','adendum line')
    currency_id = fields.Many2one('res.currency','Currency',compute="_compute_kk_cost_sheet",store=True)
    type_adendum=fields.Selection([('add','Add Product'),('reduce','Price Adjustment')],default='add',string="Type Addendum")

    # @api.onchange('kk_cost_sheet_id')
    # def _onchange_kk_cost_sheet_id(self):
    #     context = self._context
    #     login_uid = context.get('uid')
    #     user_sales_id = self.env['res.partner'].search([('user_id','=',login_uid)])
    #     aarCost = []
    #     for item in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids)]):
    #         aarCost.append(item.id)
                   
    #     return {
    #             'domain' : {
    #                 'kk_cost_sheet_id' : [('id','in',aarCost)]
                    
    #             }
    #         }
    

    @api.onchange('kk_cost_sheet_id')
    def _onchange_kk_cost_sheet_id(self):
        res_obj=self.env['res.users'].search([('id','=',self.env.user.id)])
        for res in res_obj:
            team_obj=self.env['crm.team'].search([('id','=',res.sale_team_id.id)])
            if team_obj:
                if team_obj.type_sales=='sales_product':
                    user_team = []
                    for line in team_obj:
                        user_team.append(line.id)
                    user_sales_id=self.env['res.partner'].search([('user_id','=',self.env.user.id)])
                    # aarCost = []
                    obj_kk = self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids),('type_sales','=','sales_product'),('state','=','post')])
                    print ('dddd1111', obj_kk)
                        # aarCost.append(item.id)
                    kk_ids = [l.id for l in obj_kk] if obj_kk else []
                    return {
                            'domain' : {
                                'kk_cost_sheet_id' : [('id','in',kk_ids)]
                                
                            }
                        }   
                if team_obj.type_sales=='sales_maintenance':
                    user_sales_id=self.env['res.partner'].search([('user_maintenance_id','=',self.env.user.id)])
                    # aarCost = []
                    # for item in self.env['kk.cost.sheet'].search([('partner_id','in',user_sales_id.ids)]):
                    #     aarCost.append(item.id)
                    obj_kk = self.env['kk.cost.sheet'].search([('creator_user_id','=',self.env.user.id),('state','=','post')])
                    print ('dddd22222', obj_kk)
                        # aarCost.append(item.id)
                    kk_ids = [l.id for l in obj_kk] if obj_kk else []
                    return {
                            'domain' : {
                                'kk_cost_sheet_id' : [('id','in',kk_ids)]
                                
                            }
                        }   
            else:
                obj_kk = self.env['kk.cost.sheet'].search([('creator_user_id','=',self.env.user.id),('state','=','post')])
                print ('dddd22222', obj_kk)
                    # aarCost.append(item.id)
                kk_ids = [l.id for l in obj_kk] if obj_kk else []
                return {
                        'domain' : {
                            'kk_cost_sheet_id' : [('id','in',kk_ids)]
                            
                        }
                    }   

    
    @api.multi
    @api.depends('kk_cost_sheet_id','partner_id','project_name','company_id','creator_team_id','currency_id')
    def _compute_kk_cost_sheet(self):
        for item in self:
            item.partner_id = item.kk_cost_sheet_id.partner_id.id
            item.project_name =item.kk_cost_sheet_id.project_name
            item.company_id=item.kk_cost_sheet_id.company_id.id
            item.creator_team_id=item.kk_cost_sheet_id.creator_team_id.id
            item.currency_id=item.kk_cost_sheet_id.currency_id.id

    @api.multi
    def unlink(self):
        for o in self:
            if o.name != 'New':
                raise ValidationError('You cannot delete which is Number document Created. \n You can cancel Transaction')
            elif o.state not in ('draft'):
                raise ValidationError('You cannot delete which is not draft.')

        return super(AdendumCS, self).unlink()

    @api.multi
    def action_confirm(self):
        for item in self:
            no_number=item.name
            if no_number =='New':
                seq_name = 'cost.sheet.adendum'
                no_number = item.env['ir.sequence'].next_by_code(seq_name)
                item.name = str(no_number)
            item.write({'state':'appr_mgr'})

    @api.multi
    def action_cancel(self):
        return self.write({'state':'cancel'})
    
    @api.multi
    def action_appr_mgr(self):
        return self.write({'state':'appr_gm'})
    
    @api.multi
    def action_appr_gm(self):
        return self.write({'state':'appr_drc'})
    
    @api.multi
    def action_appr_drc(self):
        for item in self:
            if item.type_adendum=='add':
                print ('add')
                for line in item.adendum_cost_sheet_line_ids:
                    vals = {
                        'kk_cost_sheet_id': item.kk_cost_sheet_id.id,
                        'product_id':line.product_id.id,
                        'name':line.name,
                        'currency_id':line.currency_id.id,
                        'product_uom_qty':line.product_uom_qty,
                        'price_unit':line.price_unit,
                        'price_total':line.price_total,
                        'discount':line.discount,
                        'tax_id':line.tax_id.id,
                        'discount_type':line.discount_type,
                        'discount_amount':line.discount_amount,
                        'cost_product_uom_qty':line.cost_product_uom_qty,
                        'cost_price_unit':line.cost_price_unit,
                        'cost_price_total':line.cost_price_total,
                        'cost_discount':line.cost_discount,
                        'cost_discount_type':line.cost_discount_type,
                        'cost_discount_amount':line.cost_discount_amount,
                        'cost_tax_id':line.cost_tax_id.id,
                        'so_line_id':line.so_line_id.id,
                        'gross_profit_percentage':line.gross_profit_percentage,
                        'state':line.state,
                        'is_adendum':True
                    }
                    obj_cs = self.env['kk.cost.sheet.line'].create(vals)
                if item.kk_cost_sheet_id.kk_sale_order_id:
                    item.kk_cost_sheet_id._action_sale_order_create_from_adendum()
                item.write({'state':'post'})
            if item.type_adendum=='reduce':
                for line in item.adendum_cost_sheet_line_ids:
                    vals = {
                        'price_unit':line.price_unit,
                        'cost_price_unit':line.cost_price_unit,
                    }
                    obj_cs = self.env['kk.cost.sheet.line'].search([('id','=',line.kk_cost_sheet_line_id.id)]).write(vals)
                    if item.kk_cost_sheet_id.kk_sale_order_id:
                        vals_so = {
                        'price_unit':line.price_unit,
                        'kk_cogs':line.cost_price_unit,
                        }
                        obj_so = self.env['sale.order.line'].search([('kk_cost_sheet_line_id','=',line.kk_cost_sheet_line_id.id),('order_id','=',item.kk_cost_sheet_id.kk_sale_order_id.id)]).write(vals_so)
                item.write({'state':'post'})

    
    @api.multi
    def action_set_to_draft(self):
        return self.write({'state':'draft'})



class AdendumCSLine(models.Model):
    _name = 'adendum.cost.sheet.line'
    _description = 'Addendum Cost Sheet Line'
    _inherit = ['mail.thread']


    @api.depends(
        'cost_product_uom_qty', 'cost_discount', 'cost_price_unit', 'cost_tax_id', 'cost_discount_type','cost_discount_amount',
        'product_uom_qty', 'discount', 'price_unit', 'tax_id', 'discount_type', 'discount_amount'
    )
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            discount_type   = line.discount_type
            discount_amount = line.discount_amount
            price_unit      = line.price_unit
            product_uom_qty = line.product_uom_qty
            discount        = 0.0

            if discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0 and product_uom_qty != 0.0:
                if discount_type =='amount':
                    discount = (discount_amount / (price_unit*product_uom_qty)) * 100
                elif discount_type == 'percentage':
                    discount = discount_amount
                line.discount = discount
            else:
                line.discount_amount = 0.0
                line.discount = 0.0

            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(
                price, line.adendum_cost_sheet_id.currency_id,
                line.product_uom_qty, product=line.product_id,
                partner=line.adendum_cost_sheet_id.partner_id)
            line.update({
                'price_total': taxes['total_included'],
            })

        for line in self:
            cost_discount_type   = line.cost_discount_type
            cost_discount_amount = line.cost_discount_amount
            cost_price_unit      = line.cost_price_unit
            cost_product_uom_qty = line.cost_product_uom_qty
            cost_discount        = 0.0
            if cost_discount_amount >= 0.0 and cost_discount_amount != 0.0 and cost_price_unit != 0.0 and cost_product_uom_qty != 0.0:
                if cost_discount_type =='amount':
                    cost_discount = (cost_discount_amount / (cost_price_unit*cost_product_uom_qty)) * 100
                elif cost_discount_type == 'percentage':
                    cost_discount = cost_discount_amount
                line.cost_discount = cost_discount
            else:
                line.cost_discount_amount = 0.0
                line.cost_discount = 0.0

            price = line.cost_price_unit * (1 - (line.cost_discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(
                price, line.adendum_cost_sheet_id.currency_id,
                line.cost_product_uom_qty, product=line.product_id,
                partner=line.adendum_cost_sheet_id.partner_id)
            line.update({
                'cost_price_total': taxes['total_included'],
            })

    @api.depends('price_total', 'cost_price_total', 'gross_profit_percentage')
    def _compute_gp_percent(self):
        tot = 0.0
        for item in self:
            if item.price_total and item.cost_price_total:
                tot = ((item.price_total - item.cost_price_total) / item.price_total) * 100
                item.gross_profit_percentage = tot


    adendum_cost_sheet_id = fields.Many2one('adendum.cost.sheet', 'Addendum Cost Sheet', ondelete='cascade')
    product_id = fields.Many2one(
        'product.product', 'Product', track_visibility="onchange")
    kk_cost_sheet_line_id = fields.Many2one(
        'kk.cost.sheet.line', 'kk cost sheet line', track_visibility="onchange")
    # sale_order_line_id = fields.Many2one(
    #     'sale.order.line', 'sale order line', track_visibility="onchange")
    name = fields.Text(string='Description Line')
    name = fields.Text(string='Description Line')
    currency_id = fields.Many2one(
        related='adendum_cost_sheet_id.company_id.currency_id',
        store=True, string='Currency', readonly=True)
    product_uom_qty = fields.Float(
        string='Quantity Sales',
        digits=dp.get_precision('Product Unit of Measure'), default=1)
    price_unit = fields.Float(
        'Unit Price Sales', required=True,
        digits=dp.get_precision('Product Price'), default=0.0)
    price_total = fields.Monetary(
        compute='_compute_amount', string='Total Sales',
        readonly=True, store=True)
    discount = fields.Float(
        string='Discount Sales(%)',
        digits=dp.get_precision('Discount'), default=0.0)
    tax_id = fields.Many2many(
        'account.tax', string='Taxes Sales',
        domain=['|', ('active', '=', False), ('active', '=', True)])
    discount_type = fields.Selection(
        [
            ('amount', 'Amount'),
            ('percentage', 'Percentage')
        ], string='Disc. Type', default='percentage')
    discount_amount = fields.Float(string='Discount Amount', default=0.0)

    cost_product_uom_qty = fields.Float(
        string='Quantity Cost',
        digits=dp.get_precision('Product Unit of Measure'),
        required=True, default=1.0)
    cost_price_unit = fields.Float(
        'Unit Price Cost', required=True,
        digits=dp.get_precision('Product Price'), default=0.0)
    cost_price_total = fields.Monetary(
        compute='_compute_amount',
        string='Total Cost', readonly=True, store=True)
    cost_discount = fields.Float(
        string='Discount Cost(%)',
        digits=dp.get_precision('Discount'), default=0.0)
    cost_tax_id = fields.Many2many(
        'account.tax', string='Taxes Cost',
        domain=['|', ('active', '=', False), ('active', '=', True)])
    cost_discount_type = fields.Selection(
        [
            ('amount', 'Amount'),
            ('percentage', 'Percentage')
        ], string='Disc. Type', default='percentage')
    cost_discount_amount = fields.Float(string='Discount Amount', default=0.0)
    # kk_order_line_ids = fields.One2many(
    #     'kk.order.line', 'cs_line_id', 'Order Detail', copy=True)
    # cost_price_log_ids = fields.One2many(
    #     'cs.cost.price.log', 'cs_line_id', 'Cost Price Log', copy=False)
    so_line_id = fields.Many2one('sale.order.line', 'SO Line')
    gross_profit_percentage = fields.Float(
        compute='_compute_gp_percent',
        string='GP %', readonly=True, store=True)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('cancel', 'Cancel'),
            ('appr_mgr', 'Approval Manager'),
            ('appr_gm', 'Approval G.Manager'),
            ('appr_drc', 'Approval Director'),
            #('appr_presdir', 'Approval Presdir'),
            ('post', 'Post'),
        ], string='Status', related='adendum_cost_sheet_id.state')
    type_adendum=fields.Selection([('add','Add'),('reduce','Reduce')],related="adendum_cost_sheet_id.type_adendum",string="Type Addendum")

    @api.onchange('kk_cost_sheet_line_id')
    def onchange_product_id_line(self):
        for item in self:
            if item.adendum_cost_sheet_id.type_adendum=='add':
                self.name = self.product_id.name
            if item.adendum_cost_sheet_id.type_adendum=='reduce':
                if item.kk_cost_sheet_line_id:
                    obj_pro = self.env['kk.cost.sheet.line'].search([('id','=',item.kk_cost_sheet_line_id.id)])
                    if obj_pro:
                        # for item in 
                        self.name = obj_pro.name
                        self.product_uom_qty=obj_pro.product_uom_qty
                        self.cost_product_uom_qty=obj_pro.cost_product_uom_qty
                        self.discount_type=obj_pro.discount_type
                        self.cost_discount_type=obj_pro.cost_discount_type
                        self.cost_discount_type=obj_pro.cost_discount_type
                        self.price_unit=obj_pro.price_unit
                        self.cost_price_unit=obj_pro.cost_price_unit
                        self.product_id=obj_pro.product_id.id
    
    @api.multi
    @api.onchange('product_uom_qty','cost_product_uom_qty','discount_type','cost_discount_type')
    def onchange_product_uom_qty(self):
        for item in self:
            if item.adendum_cost_sheet_id.type_adendum=='reduce':
                if item.kk_cost_sheet_line_id:
                    # obj_pro = self.env['kk.cost.sheet.line'].search([('kk_cost_sheet_id','=',item.adendum_cost_sheet_id.kk_cost_sheet_id.id),('product_id','=',item.product_id.id)],limit=1)
                    obj_pro = self.env['kk.cost.sheet.line'].search([('id','=',item.kk_cost_sheet_line_id.id)])
                    if item.product_uom_qty != obj_pro.product_uom_qty:
                        raise ValidationError('quantity cannot be changed')
                    if item.cost_product_uom_qty != obj_pro.cost_product_uom_qty:
                        raise ValidationError('quantity cannot be changed')
                    if item.discount_type != obj_pro.discount_type:
                        raise ValidationError('Discount type cannot be changed')
                    if item.cost_discount_type != obj_pro.cost_discount_type:
                        raise ValidationError('Cost Discount type cannot be changed')
                    item.product_uom_qty=obj_pro.product_uom_qty
                    item.cost_product_uom_qty=obj_pro.cost_product_uom_qty
                    item.discount_type=obj_pro.discount_type
                    item.cost_discount_type=obj_pro.cost_discount_type
                
                    

    @api.multi
    @api.onchange('product_id')
    def onchange_product_id(self):
        for item in self:
            if item.adendum_cost_sheet_id.type_adendum=='reduce':
                obj_pro = self.env['kk.cost.sheet.line'].search([('kk_cost_sheet_id','=',item.adendum_cost_sheet_id.kk_cost_sheet_id.id)])
                aarProid=[]
                for item in obj_pro:
                    aarProid.append(item.id)
                return {
                        'domain' : {
                            'kk_cost_sheet_line_id' : [('id','in',aarProid)],                            
                        }
                    }  

    @api.onchange(
        'cost_discount_type',
        'cost_discount_amount',
        'cost_price_unit',
        'cost_product_uom_qty')
    def onchange_cost_discount_amount(self):
        discount_type = self.cost_discount_type
        discount_amount = self.cost_discount_amount
        price_unit = self.cost_price_unit
        product_uom_qty = self.cost_product_uom_qty
        discount = 0.0

        if discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0 and product_uom_qty != 0.0:
            if discount_type == 'amount':
                discount = (discount_amount / (price_unit * product_uom_qty)) * 100
            elif discount_type == 'percentage':
                discount = discount_amount
            self.cost_discount = discount
        else:
            self.cost_discount_amount = 0.0
            self.cost_discount = 0.0

    @api.onchange(
        'discount_type',
        'discount_amount',
        'price_unit',
        'product_uom_qty')
    def onchange_discount_amount(self):
        discount_type   = self.discount_type
        discount_amount = self.discount_amount
        price_unit      = self.price_unit
        product_uom_qty = self.product_uom_qty
        discount        = 0.0

        if discount_amount >= 0.0 and discount_amount != 0.0 and price_unit != 0.0 and product_uom_qty != 0.0:
            if discount_type=='amount':
                discount = (discount_amount / (price_unit*product_uom_qty)) * 100
            elif discount_type=='percentage':
                discount = discount_amount
            self.discount = discount
            return
        else:
            self.discount_amount = 0.0
            self.discount = 0.0
