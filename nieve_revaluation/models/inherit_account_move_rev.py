from odoo import api, fields, models, _
from openerp.exceptions import UserError

class InheritAccountMove(models.Model):
    _inherit = 'account.move'

    is_revaluation = fields.Boolean('Is Revaluation',default=False)

   