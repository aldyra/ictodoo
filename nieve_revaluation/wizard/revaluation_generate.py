from odoo import api, fields, models, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError
import time
import calendar
import datetime
from odoo.tools import float_round

class WizardRevaluationGenerate(models.TransientModel):
    _name='generate.revaluation.wizz'

    date            = fields.Date('As Of Date')
    date_last_month = fields.Date('From Date')
    date_early_month = fields.Date('Early Date')
    currency_id     = fields.Many2one('res.currency','Currency')
    rate            = fields.Float('Rate')
    profit_account  = fields.Many2one('account.account','Profit Account')
    loss_account    = fields.Many2one('account.account','Loss Account')
    description     = fields.Char('Description')
    filter_account  = fields.Many2one('account.account.type','Filter Account')
    journal_id      = fields.Many2one('account.journal','Journal')
    account_ids     = fields.Many2many(
        comodel_name='account.account',
        string='Filter accounts',
    )

    @api.onchange('filter_account','currency_id')
    def onchange_type_accounts_only(self):
        """Handle receivable/payable accounts only change."""
        if self.filter_account:
            domain = []
            if self.filter_account and self.currency_id:
                domain += [('user_type_id', '=',self.filter_account.id),('currency_id','=',self.currency_id.id)]
            elif self.filter_account:
                domain += [('user_type_id', '=',self.filter_account.id),('currency_id','=',self.currency_id.id)]
            elif self.currency_id:
                domain += [('user_type_id', '=',self.filter_account.id),('currency_id','=',self.currency_id.id)]
            self.account_ids = self.env['account.account'].search(domain)
        elif self.currency_id:
            domain = []
            if self.filter_account and self.currency_id:
                domain += [('user_type_id', '=',self.filter_account.id),('currency_id','=',self.currency_id.id)]
            elif self.filter_account:
                domain += [('user_type_id', '=',self.filter_account.id),('currency_id','=',self.currency_id.id)]
            elif self.currency_id:
                domain += [('user_type_id', '=',self.filter_account.id),('currency_id','=',self.currency_id.id)]
            self.account_ids = self.env['account.account'].search(domain)
        else:
            self.account_ids = None

    def qwery_account_move(self,currency_id,date_start,account_id):
        self.env.cr.execute("""  
                        select 
                            sum(aml.balance) balance,
                            aml.account_id,
                            aml.currency_id,
                            aa.name name_account,
                            sum(aml.amount_currency) amount_currency
                        from account_move am 
                        left join (select id,name,balance,move_id,account_id,currency_id,date,amount_currency
                        from account_move_line)aml
                        on aml.move_id = am.id
                        left join(select id,name,user_type_id from account_account) aa
                        on aa.id = aml.account_id
                        left join (select id,name from account_account_type)aat
                        on aat.id = aa.user_type_id
                        left join (select id,name from account_journal)aj
                        on aj.id=am.journal_id
                        where aml.currency_id='%s' and aml.date <='%s' and aa.id in %s and am.is_revaluation ='false'
                        group by 
                            aml.account_id,
                            aml.currency_id,
                            aa.name
                        """ % (str(currency_id),str(date_start),account_id))
        x = self.env.cr.dictfetchall()
        print('x---------->',x)
        return x

    def qwery_check_reval(self,currency_id,date_start,date_end):
        self.env.cr.execute("""  

        select aml.date,aml.currency_id from account_move am
            left join (select id,name,debit,credit,move_id,account_id,currency_id,date,amount_currency
            from account_move_line)aml
            on aml.move_id = am.id
            where aml.currency_id='%s' and aml.date >='%s' and  aml.date <='%s' and am.is_revaluation='true'
            group by 
                aml.date,
                aml.currency_id
                        """ % (str(currency_id),str(date_start),str(date_end)))
        x = self.env.cr.dictfetchall()
        return x


    @api.multi
    def create_move_id_1(self,rec,journal_id,move_pool,line_mv_ids):
        """
            create move id in journal entries
        :return: move ID
        """
        ref = rec.description
        date = rec.date_last_month
        date_month = time.strftime('%m/%Y', time.strptime(str(rec.date_last_month), '%Y-%m-%d'))
        rate = float_round(rec.rate, precision_digits=2, rounding_method='HALF-UP')
        note = "Revaluation/"+ str(rate) + "/"+ str(date_month) +"/01"
        currency_id = rec.currency_id.id

        move = {
            'name': '/',
            'journal_id': journal_id,
            'date': date,
            'ref': note,
            'narration': ref,
            'line_ids' : line_mv_ids,
            'is_revaluation':True,
            'currency_id':currency_id
        }
        move_id = move_pool.create(move)
        return move_id

    @api.multi
    def create_move_id_2(self,rec,journal_id,move_pool,line_mv_ids):
        """
            create move id in journal entries
        :return: move ID
        """
        ref = rec.description
        date = rec.date_early_month
        date_month = time.strftime('%m/%Y', time.strptime(str(rec.date_early_month), '%Y-%m-%d'))
        rate = float_round(rec.rate, precision_digits=2, rounding_method='HALF-UP')
        note = "Revaluation/"+ str(rate) + "/"+ str(date_month) +"/02"
        currency_id = rec.currency_id.id

        move = {
            'name': '/',
            'journal_id': journal_id,
            'date': date,
            'ref': note,
            'narration': ref,
            'line_ids' : line_mv_ids,
            'is_revaluation':True,
            'currency_id':currency_id
        }
        move_id = move_pool.create(move)
        return move_id


    @api.multi
    @api.onchange('date')
    def _onchange_date(self):
        for item in self:
            if item.date:
                date = datetime.datetime.strptime(item.date, '%Y-%m-%d')
                start_date = datetime.datetime(date.year, date.month, 1)
                end_date = datetime.datetime(date.year, date.month, calendar.mdays[date.month])
                date_early_month = end_date + relativedelta(days=1)
                item.date_last_month = end_date
                item.date_early_month = date_early_month

    @api.multi
    def generate(self):
        for rec in self:
            obj_qwery =self.qwery_check_reval(self.currency_id.id,str(self.date),str(self.date))
            if obj_qwery:
                raise ValidationError(_("Already Available Revaluation"))
            date = datetime.datetime.strptime(rec.date, '%Y-%m-%d')
            start_date = datetime.datetime(date.year, date.month, 1)
            end_date = datetime.datetime(date.year, date.month, calendar.mdays[date.month])
            date_end_conv = end_date.date()
            if rec.rate ==0:
                raise ValidationError(_("Rate must be greater than zero"))
            else:
                if rec.date == rec.date_last_month:
                    move_pool = self.env['account.move']
                    move_line_pool = self.env['account.move.line']
                    journal_id = rec.journal_id.id
                    # Journal ke 1
                    line_mv_ids_1 = rec.create_journal_rev_1(rec, journal_id, move_line_pool)
                    move_id_1 = rec.create_move_id_1(rec, journal_id, move_pool,line_mv_ids_1)
                    # Journal ke 2
                    line_mv_ids_2 = rec.create_journal_rev_2(rec, journal_id, move_line_pool)
                    move_id_2 = rec.create_move_id_2(rec, journal_id, move_pool,line_mv_ids_2)
                else:
                    raise ValidationError(_("Please select end date of the month."))
            
    

    @api.multi
    def create_journal_rev_1(self,rec,journal_id,move_line_pool):
        """
            this method use to create move live line
        :return: journal accont revaluation
        """
        if len(self.account_ids) >=1:
            accc= []
            for acc in self.account_ids:
                accc.append(acc.id)
            account_id = tuple(accc)
        if len(self.account_ids) ==1:
            accc= []
            for acc in self.account_ids:
                accc.append(acc.id)
            t = tuple(accc)
            e, = t
            account_id =  '('+str(e)+')'
        obj_qwery =self.qwery_account_move(self.currency_id.id,str(self.date),account_id)
        if not obj_qwery:
            raise ValidationError(_("Data Not Found"))
        list_ap_mv = []
        amount = 0.0
        total_debit_sum=0.0
        for q in obj_qwery:
            if q.get('account_id'):
                balance =q.get('balance')
                amount_curr  =q.get('amount_currency')
                total_curr = amount_curr * self.rate
                total_debit = total_curr - balance
                total_debit = float_round(total_debit, precision_digits=2, rounding_method='HALF-UP')
                total_debit_sum +=total_debit
                if total_debit > 0:
                    move_line_debit = {
                        'name': q.get('name_account'),
                        'debit': total_debit,
                        'credit': 0.0,
                        'account_id': q.get('account_id'),
                        'journal_id': journal_id,
                        'currency_id': rec.currency_id.id,
                        'amount_currency': 0.0,
                        'quantity': 1
                    }
                    list_ap_mv.append((0, 0, move_line_debit))
                else:
                    move_line_debit = {
                        'name': q.get('name_account'),
                        'debit': 0.0,
                        'credit': total_debit * -1,
                        'account_id': q.get('account_id'),
                        'journal_id': journal_id,
                        'currency_id': rec.currency_id.id,
                        'amount_currency': 0.0,
                        'quantity': 1
                    }
                    list_ap_mv.append((0, 0, move_line_debit))
        total_debit_sum =total_debit_sum
        total_debit_sum = float_round(total_debit_sum, precision_digits=2, rounding_method='HALF-UP')
        if total_debit_sum > 0:
            move_line_credit = {
                'name': rec.profit_account.name,
                'debit': 0.0,
                'credit': total_debit_sum,
                'account_id': rec.profit_account.id,
                'journal_id': journal_id,
                'currency_id': rec.currency_id.id,
                'amount_currency': 0.0,
                'quantity': 1
                
            }
            list_ap_mv.append((0, 0, move_line_credit))
            return list_ap_mv
        else:
            move_line_credit = {
                'name': rec.loss_account.name,
                'debit': total_debit_sum * -1,
                'credit': 0.0,
                'account_id': rec.loss_account.id,
                'journal_id': journal_id,
                'currency_id': rec.currency_id.id,
                'amount_currency': 0.0,
                'quantity': 1
                
            }
            list_ap_mv.append((0, 0, move_line_credit))
            return list_ap_mv

    
    @api.multi
    def create_journal_rev_2(self,rec,journal_id,move_line_pool):
        """
             this method use to create move live line
        :return: journal accont revaluation
        """
        if len(self.account_ids) >=1:
            accc= []
            for acc in self.account_ids:
                accc.append(acc.id)
            account_id = tuple(accc)
        if len(self.account_ids) ==1:
            accc= []
            for acc in self.account_ids:
                accc.append(acc.id)
            t = tuple(accc)
            e, = t
            account_id =  '('+str(e)+')'
        obj_qwery =self.qwery_account_move(self.currency_id.id,str(self.date),account_id)
        list_ap_mv = []
        
        amount = 0.0
        total_debit_sum=0.0
        for q in obj_qwery:
            if q.get('account_id'):
                balance =q.get('balance')
                amount_curr  =q.get('amount_currency')
                total_curr = amount_curr * self.rate
                total_debit =total_curr - balance
                total_debit = float_round(total_debit, precision_digits=2, rounding_method='HALF-UP')
                total_debit_sum +=total_debit 
                if total_debit > 0:
                    move_line_debit = {
                        'name': q.get('name_account'),
                        'debit': 0.0,
                        'credit': total_debit,
                        'account_id': q.get('account_id'),
                        'journal_id': journal_id,
                        'currency_id': rec.currency_id.id,
                        'amount_currency': 0.0,
                        'quantity': 1
                    }
                    list_ap_mv.append((0, 0, move_line_debit))
                else:
                    move_line_debit = {
                        'name': q.get('name_account'),
                        'debit': total_debit * -1,
                        'credit': 0.0,
                        'account_id': q.get('account_id'),
                        'journal_id': journal_id,
                        'currency_id': rec.currency_id.id,
                        'amount_currency': 0.0,
                        'quantity': 1
                    }
                    list_ap_mv.append((0, 0, move_line_debit))
        total_debit_sum =total_debit_sum
        total_debit_sum = float_round(total_debit_sum, precision_digits=2, rounding_method='HALF-UP')
        if total_debit_sum > 0:
            move_line_credit = {
                'name': rec.profit_account.name,
                'debit': total_debit_sum,
                'credit': 0.0,
                'account_id': rec.profit_account.id,
                'journal_id': journal_id,
                'currency_id': rec.currency_id.id,
                'amount_currency': 0.0,
                'quantity': 1
                
            }
            list_ap_mv.append((0, 0, move_line_credit))
            return list_ap_mv
        else:
            move_line_credit = {
                'name': rec.loss_account.name,
                'debit': 0.0,
                'credit': total_debit_sum * -1,
                'account_id': rec.loss_account.id,
                'journal_id': journal_id,
                'currency_id': rec.currency_id.id,
                'amount_currency': 0.0,
                'quantity': 1
                
            }
            list_ap_mv.append((0, 0, move_line_credit))
            return list_ap_mv