# -*- coding: utf-8 -*-
# Part of Odoo Falinwa Edition.
# See LICENSE file for full copyright and licensing details.
{
    "name": "Nieve Revaluation",
    "version": "11.1.1.0.0",
    'author': 'Nieve',
    'website': 'https://www.nievetechnology.com',
    'category': 'General',
    'summary': 'Revaluation',
    "description": """
        Last Update 28 April 2021
        Add list Default Checklist
    """,
    "depends": ['account'
        ],
    'data': [
        'views/account_revaluation.xml',
        'wizard/revaluation_generate.xml'
        
    ],
    'css': [],
    'js': [],
    'installable': True,
    'active': False,
    'application': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

