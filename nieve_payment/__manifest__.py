{
    "name": "Nieve Payment",
    "version": "1.0",
    "depends": ['base','account','account_cancel'],
    "author": "NIEVE",
    "category": "",
    "description": """
       Nieve Standard Payment
       last update 31-08-2021
    """,
    "init_xml": [],
    'data': ["security/security.xml",
             "wizard/wizard_take_invoice_view.xml",
             "views/account_payment_view.xml",
             "views/account_receive_view.xml",
             "views/account_payment_transfer_view.xml",
             "views/account_payment_not_match.xml",
             "views/account_receive_not_match.xml"
             ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
