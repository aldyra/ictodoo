import time
from lxml import etree

from openerp import models, fields, api
from datetime import datetime, timedelta
from dateutil import relativedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp import tools
from openerp.exceptions import Warning,ValidationError


class VoucherMoveLineWizz(models.TransientModel):

    _name = 'voucher.move.line.wizz'

    @api.model
    def default_get(self, fields):
        """
            thie method use to get context active payment id
        :param fields:
        :return:
        """
        context = self._context or {}
        if 'active_id' in context:
            res = super(VoucherMoveLineWizz, self).default_get(fields)
            voucher_ids = context.get('active_ids', [])
            active_model = context.get('active_model')

            assert active_model in ('account.payment'), 'Bad context propagation'
            vouchers = self.env['account.payment'].browse(voucher_ids)
            items = []

            for voucher in vouchers:
                payment_id = voucher.id
                partner_id = voucher.partner_id.id
            res.update(partner_id=partner_id, type='debit', payment_id=payment_id)
            return res


    payment_id = fields.Many2one('account.payment', string='Voucher')
    partner_id = fields.Many2one('res.partner', string='Supplier')
    type = fields.Selection([('debit', 'Debit'), ('credit', 'Credit')], string='Type')
    move_line_ids = fields.Many2many('account.move.line', 'voucher_move_line_rel','move_line_id',
                                         'voucher_move_line_id', 'Invoice')
    move_receive_line_ids = fields.Many2many('account.move.line', 'voucher_move_receive_line_rel', 'move_line_id',
                                     'voucher_move_receive_line_id', 'Invoice Costumer')

    @api.multi
    def create_line_debit(self,move_line,item,payment_id,pay_amount):
        """

        :param move_line:
        :param item:
        :return:
        """
        vals = {
            'move_line_id': move_line.id,
            'account_id': move_line.account_id.id,
            'type': 'dr',
            'payment_id': payment_id.id,
            'currency_id': move_line.currency_id.id if move_line.currency_id else move_line.company_currency_id.id,
            'amount_unreconciled': abs(move_line.amount_residual),
            'amount_to_pay': move_line.amount_residual * -1 if payment_id.amount == abs(sum(item.amount_residual  for item in item.move_line_ids)) else pay_amount,
            'full_payment' : False if payment_id.amount != abs(sum(item.amount_residual  for item in item.move_line_ids)) else True,
            'ref': move_line.ref,
        }
        result = self.env['account.payment.line'].create(vals)
        return result

    @api.multi
    def create_line_credit(self, move_line, item,payment_id,pay_amount):
        """

		:param move_line:
		:param item:
		:return:
		"""
        vals = {
            'move_line_id': move_line.id,
            'account_id': move_line.account_id.id,
            'type': 'dr',
            'payment_id': payment_id.id,
            'currency_id': move_line.currency_id.id if move_line.currency_id else move_line.company_currency_id.id,
            'amount_unreconciled': abs(move_line.amount_residual),
            'amount_to_pay' : move_line.amount_residual if payment_id.amount == abs(sum(item.amount_residual  for item in item.move_line_ids)) else pay_amount,
            'full_payment' : False if payment_id.amount != abs(sum(item.amount_residual  for item in item.move_line_ids)) else True,
            'ref': move_line.ref,
        }
        result = self.env['account.payment.line'].create(vals)
        return result

    @api.multi
    def create_payment_line(self,context):
        """
            this method use to create payment or receive line
        :return:
        """
        voucher_line_obj = self.env['account.payment.line']
        for item in self:
            payment_id = self.env['account.payment'].search([('id', '=',context['active_id'])])
            if item.move_line_ids:
                pay_amount = payment_id.amount / len(item.move_line_ids)
                for move_line in item.move_line_ids:
                    item.create_line_debit(move_line,item,payment_id,pay_amount)

            if item.move_receive_line_ids:
                pay_amount = payment_id.amount / len(item.move_receive_line_ids)
                for move_line in item.move_receive_line_ids:
                    item.create_line_credit(move_line, item,payment_id,pay_amount)

            else:
                ValidationError('Please Fill Your Line')

