import re
import time
import math

from odoo import models, fields, api, exceptions
from openerp import tools
from openerp.tools import float_round, float_is_zero, float_compare
import json

class res_currency(models.Model):

    _inherit = "res.currency"

    # @api.multi
    # @api.depends('')
    # def _get_current_rate(self, cr, uid, ids, name, arg, context=None):
    #     print "## Custom _get_current_rate :"
    #     if context is None:
    #         context = {}
    #     res = super(res_currency, self)._get_current_rate(cr, uid, ids, name, arg, context=context)
    #     print "First REST--->>", res
    #     if context.get('payment_special_currency') in ids and context.get('payment_special_currency_rate'):
    #         print "##MASUK***", context.get('payment_special_currency')
    #         res[context.get('payment_special_currency')] = context.get('payment_special_currency_rate')
    #     print "###Second res---->>", res
    #     return res
    
    # _columns = {
    #     'rate': fields.function(_get_current_rate, string='Current Rate', digits=(12,6),
    #         help='The rate of the currency to the currency of rate 1.'),
    # }

    @api.multi
    def _compute_current_rate(self):
        date = self._context.get('date') or fields.Datetime.now()
        company_id = self._context.get('company_id') or self.env['res.users']._get_company().id
        # the subquery selects the last rate before 'date' for the given currency/company
        query = """SELECT c.id, (SELECT r.rate FROM res_currency_rate r
                                      WHERE r.currency_id = c.id AND r.name <= %s
                                        AND (r.company_id IS NULL OR r.company_id = %s)
                                   ORDER BY r.company_id, r.name DESC
                                      LIMIT 1) AS rate
                       FROM res_currency c
                       WHERE c.id IN %s"""
        self._cr.execute(query, (date, company_id, tuple(self.ids)))
        currency_rates = dict(self._cr.fetchall())
        for currency in self:
            currency.rate = currency_rates.get(currency.id) or 1.0

    rate = fields.Float(compute='_compute_current_rate',digits=(12,6),string='Current Rate',help='The rate of the currency to the currency of rate 1.')

res_currency()
