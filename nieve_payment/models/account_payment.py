from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_round

import logging
_logger = logging.getLogger(__name__)

class account_payment(models.Model):
    _inherit = "account.payment"
    
    @api.multi
    def cancel(self):
        for rec in self:
            for move in rec.move_line_ids.mapped('move_id'):
                if rec.invoice_ids or rec.move_line_ids:
                    move.line_ids.remove_move_reconcile()
                move.button_cancel()
                move.unlink()
            rec.state = 'draft'
            
#     @api.one
#     @api.depends('invoice_ids', 'amount', 'payment_date', 'currency_id')
#     def _compute_payment_difference(self):
#         if len(self.invoice_ids) == 0:
#             return
#         if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
#             self.payment_difference = self.amount - self._compute_total_invoices_amount()
#         else:
#             self.payment_difference = self._compute_total_invoices_amount() - self.amount
    
    @api.one
    @api.depends('invoice_ids', 'amount', 'payment_date', 'currency_id','line_debit_ids','line_credit_ids')
    def _compute_payment_difference(self):
        ##Edit
        if self.line_debit_ids:
            total_dr = 0.0
            for line_dr in self.line_debit_ids:
                total_dr += line_dr.amount_to_pay
            _logger.debug("total_dr : %s" % total_dr)
            
            self.payment_difference = self.amount - total_dr

        if self.line_credit_ids:
            total_dr = 0.0
            for line_dr in self.line_credit_ids:
                total_dr += line_dr.amount_to_pay
            _logger.debug("total_dr : %s" % total_dr)

            self.payment_difference = self.amount - total_dr
                
        
        if len(self.invoice_ids) == 0:
            return
#         if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
#             self.payment_difference = self.amount - self._compute_total_invoices_amount()
#         else:
#             self.payment_difference = self._compute_total_invoices_amount() - self.amount

    @api.model
    def _get_default_requested_by(self):
        return self.env['res.users'].browse(self.env.uid)
    
    set_manual_currency     = fields.Selection([('current_rate','Current Rate'),('manual_rate','Manual Rate')], string='Rate', default='current_rate')
    state = fields.Selection(
        selection_add=[('confirm', 'Confirm'),])
    special_currency_rate   = fields.Float(string='Rate',digits=(16, 8))
    line_debit_ids          = fields.One2many('account.payment.line', 'payment_id', string='Debits')
    line_credit_ids = fields.One2many('account.payment.line', 'payment_id', string='Credits')
    payment_difference      = fields.Monetary(compute='_compute_payment_difference', readonly=True)
    payment_adm = fields.Selection([
        ('transfer', 'Transfer')], 'Payment Adm')
    payment_date_temp =  fields.Date(default=fields.Date.context_today)
    adm_amount = fields.Float('Amount Adm')
    adm_acc_id = fields.Many2one('account.account', 'Account Adm')
    adm_comment = fields.Char('Comment Adm', size=128)
    user_create_id = fields.Many2one('res.users',default=_get_default_requested_by)
    payment_date = fields.Date(string='Payment Date', default=fields.Date.context_today, required=False, copy=False)
    validation_user = fields.Boolean('Validation User',compute='_compute_validation_user')
    validation_cancel = fields.Boolean('Validation Cancel', compute='_compute_validation_cancel',store=True)
    validation_pay_no_source = fields.Boolean('Pay Not Source',store=True)
    validation_receive_no_source = fields.Boolean('Receive Not Source', store=True)
    remark = fields.Text('Remark')
    currency_bank_adm = fields.Many2one('res.currency',string='Currency', required=True,
                                  default=lambda self: self.env.user.company_id.currency_id)

    @api.multi
    def unlink(self):
        if any(rec.state != 'draft' for rec in self):
            raise UserError(_("You can not delete a payment that state not in draft"))
        return super(account_payment, self).unlink()

    @api.onchange('journal_id')
    def _onchange_journal(self):
        if self.journal_id:
            self.currency_id = self.journal_id.currency_id or self.company_id.currency_id
            self.currency_bank_adm = self.journal_id.currency_id or self.company_id.currency_id
            # Set default payment method (we consider the first to be the default one)
            payment_methods = self.payment_type == 'inbound' and self.journal_id.inbound_payment_method_ids or self.journal_id.outbound_payment_method_ids
            self.payment_method_id = payment_methods and payment_methods[0] or False
            # Set payment method domain (restrict to methods enabled for the journal and to selected payment type)
            payment_type = self.payment_type in ('outbound', 'transfer') and 'outbound' or 'inbound'
            return {
                'domain': {
                    'payment_method_id': [('payment_type', '=', payment_type), ('id', 'in', payment_methods.ids)]
                }
            }
        return {}

    @api.depends('state') #'validation_user',
    def _compute_validation_user(self):
        for item in self:
            GroupFS_access = self.env.user.has_group('nieve_general_journal.group_general_journal_finance_staff')
            GroupFH_access = self.env.user.has_group('nieve_general_journal.group_general_journal_finance_head')
            GroupGJS_access = self.env.user.has_group('nieve_general_journal.group_general_journal_staff')
            GroupGJH_access = self.env.user.has_group('nieve_general_journal.group_general_journal_head')
            GroupPS_access = self.env.user.has_group('nieve_payment.group_payment_finance_staff')
            GroupPH_access = self.env.user.has_group('nieve_payment.group_payment_finance_head')
            if item.state != 'draft' and (GroupPS_access):
                item.validation_user = False

            if item.state != 'draft' and (GroupPH_access):
                item.validation_user = True

            if item.state == 'draft':
                item.validation_user = True

            if item.state == 'posted' and (GroupPS_access or GroupPH_access):
                item.validation_user = False

    @api.depends('state') #'validation_cancel',
    def _compute_validation_cancel(self):
        for item in self:
            GroupFS_access = self.env.user.has_group('nieve_general_journal.group_general_journal_finance_staff')
            GroupFH_access = self.env.user.has_group('nieve_general_journal.group_general_journal_finance_head')
            GroupGJS_access = self.env.user.has_group('nieve_general_journal.group_general_journal_staff')
            GroupGJH_access = self.env.user.has_group('nieve_general_journal.group_general_journal_head')
            GroupPS_access = self.env.user.has_group('nieve_payment.group_payment_finance_staff')
            GroupPH_access = self.env.user.has_group('nieve_payment.group_payment_finance_head')
            _logger.debug("GroupFS_access : %s" % GroupFS_access)
            _logger.debug("GroupFH_access : %s" % GroupFH_access)
            if item.state != 'draft' and (GroupPS_access):
                item.validation_cancel = False

            if item.state != 'draft' and (GroupPH_access):
                item.validation_cancel = True

            if item.state == 'draft':
                item.validation_cancel = True

            if item.state == 'posted' and (GroupPS_access):
                item.validation_cancel = False

            if item.state == 'posted' and (GroupPH_access):
                item.validation_cancel = True

    @api.onchange('payment_date_temp')
    def _onchange_payment_date(self):
        if self.payment_date_temp:
            self.payment_date = self.payment_date_temp

    
    #@api.one
    @api.onchange('currency_id','payment_date')
    def onchange_currency(self):
        payment_rate = 1
        for o in self:
            if o.line_debit_ids:
                from_rate   = self.env['res.currency'].with_context({'date' : self.payment_date}).browse(self.currency_id.id)['rate']
                to_rate     = self.env['res.currency'].with_context({'date' : self.payment_date}).browse(o.line_debit_ids[0].currency_id.id)['rate']
        
        self.special_currency_rate = payment_rate

    @api.multi
    def create_move_id(self,rec,journal_id,move_pool,line_mv_ids):
        """
            create move id in journal entries
        :return: move ID
        """
        name = rec.name
        ref = rec.remark
        date = rec.payment_date

        move = {
            'name': name,
            'journal_id': journal_id,
            'date': date,
            'ref': ref,
            'line_ids' : line_mv_ids
        }
        move_id = move_pool.create(move)
        return move_id

    @api.multi
    def create_move_bank_transfer_id(self, rec, journal_id, move_pool, line_mv_ids):
        """
			create move id bank transfer in journal entries
		:return: move ID
		"""
        name = rec.name
        ref = rec.name
        date = rec.payment_date

        move = {
            'name': name,
            'journal_id': journal_id,
            'date': date,
            'ref': ref,
            'line_ids': line_mv_ids
        }
        move_id = move_pool.create(move)
        return move_id

    @api.multi
    def create_move_payment_no_source_id(self, rec, journal_id, move_pool, line_mv_ids):
        """
			create move id payment no sourcec in journal entries
		:return: move ID
		"""
        name = rec.name
        ref = rec.name
        date = rec.payment_date

        move = {
            'name': name,
            'journal_id': journal_id,
            'date': date,
            'ref': ref,
            'line_ids': line_mv_ids
        }
        move_id = move_pool.create(move)
        return move_id

    @api.multi
    def create_move_receive_no_source_id(self, rec, journal_id, move_pool, line_mv_ids):
        """
			create move id payment no sourcec in journal entries
		:return: move ID
		"""
        name = rec.name
        ref = rec.name
        date = rec.payment_date

        move = {
            'name': name,
            'journal_id': journal_id,
            'date': date,
            'ref': ref,
            'line_ids': line_mv_ids
        }
        move_id = move_pool.create(move)
        return move_id

    @api.multi
    def create_move_gain_loss_id(self, rec, journal_gain_id, move_pool, line_mv_ids):
        """
            create move gain loss id in journal entries
        :return: move ID
        """
        name = rec.name
        ref = rec.name
        date = rec.payment_date

        move = {
            'name': name,
            'journal_id': journal_gain_id.id,
            'date': date,
            'ref': ref,
            'line_ids': line_mv_ids
        }
        move_id = move_pool.create(move)
        return move_id

    @api.multi
    def create_move_adm_bankid(self, rec, journal_id, move_pool, line_mv_ids):
        """
            create move admin bank id in journal entries
        :return: move ID
        """
        name = rec.name
        ref = rec.name
        date = rec.payment_date

        move = {
            'name': name,
            'journal_id': journal_id,
            'date': date,
            'ref': ref,
            'line_ids': line_mv_ids
        }
        move_id = move_pool.create(move)
        _logger.debug("Move ID %d" % move_id)
        return move_id

    @api.multi
    def conv_amount_currency(self,line,rec,amount):
        """
            this method use to conv amount currency
        :return:
        """
        
        print ("Line: %s ; rec: %s ; amount: %s ", line,rec,amount)
        
        self.with_context()
        move_line = line
        payment_date = rec.payment_date
        currency_payment = move_line.currency_id
        voucher_rate = \
        self.env['res.currency'].with_context({'date': payment_date}).browse(currency_payment.id)['rate']
        
        print ("VOUCHER %d" % voucher_rate)
        
        _logger.debug("VOUCHER %d" % voucher_rate)
        _logger.debug("CURRENCY PAYMENT %d %s" % (currency_payment.id,currency_payment.name))
        company_currency = rec.company_id.currency_id
        payment_currency = rec.currency_id and rec.currency_id or company_currency

        tcurrency_id = move_line.currency_id.with_context(date=payment_date)

        if rec.set_manual_currency == 'manual_rate' and rec.special_currency_rate != 0.0:
            payment_rate = rec.special_currency_rate
            amount_original = (amount or 0.0) / payment_rate
            _logger.debug("Amount Convert To pay Manual: %s" % amount_original)
            return amount_original
        else:
            # amount_original = tcurrency_id.compute(amount or 0.0,rec.currency_id)
            
            print ("*******", payment_currency, company_currency, payment_currency, line.currency_id)
            
            if payment_currency != company_currency and payment_currency==line.currency_id:
                amount_original = amount
            else:
                amount_original = (amount or 0.0) / voucher_rate
            _logger.debug("Amount Convert To pay Matic: %s" % amount_original)
            print ("amount_original--->", amount_original)
            return amount_original

    @api.multi
    def conv_currency(self,rec, amount):
        """
			this method use to conv amount currency
		:return:
		"""
        self.with_context()
        payment_date = rec.payment_date
        currency_payment = rec.destination_journal_id.currency_id if rec.destination_journal_id.currency_id \
            else rec.currency_id
        voucher_rate = \
            self.env['res.currency'].with_context({'date': payment_date}).browse(currency_payment.id)['rate']

        company_currency = rec.currency_id

        if rec.set_manual_currency == 'manual_rate' and rec.special_currency_rate != 0.0:
            payment_rate = rec.special_currency_rate
            if rec.destination_journal_id.currency_id:
                amount_original = (
                                  amount or 0.0) / payment_rate if rec.destination_journal_id.currency_id != rec.currency_id \
                    else (amount or 0.0) * payment_rate
            else:
                amount_original = (amount or 0.0) * payment_rate
            return amount_original
        else:
            # amount_original = tcurrency_id.compute(amount or 0.0,rec.currency_id)
            if rec.destination_journal_id.currency_id:
                amount_original = (amount or 0.0) / voucher_rate  if rec.destination_journal_id.currency_id != rec.currency_id \
                    else (rec.destination_journal_id.currency_id != rec.company_id.currency_id and amount or 0.0) * voucher_rate
            else:
                amount_original = (amount or 0.0) * voucher_rate
            return amount_original

    @api.multi
    def conv_currency_div(self, rec, amount):
        """
			this method use to conv amount currency
		:return:
		"""
        self.with_context()
        payment_date = rec.payment_date
        currency_payment = rec.destination_journal_id.currency_id if rec.destination_journal_id.currency_id else rec.currency_id
        voucher_rate = \
            self.env['res.currency'].with_context({'date': payment_date}).browse(currency_payment.id)['rate']
        company_currency = rec.currency_id

        if rec.set_manual_currency == 'manual_rate' and rec.special_currency_rate != 0.0:
            payment_rate = rec.special_currency_rate
            if rec.destination_journal_id.currency_id:
                amount_original = amount
            else:
                amount_original = (amount or 0.0) * payment_rate
            return amount_original
        else:
            # amount_original = tcurrency_id.compute(amount or 0.0,rec.currency_id)
            if rec.destination_journal_id.currency_id:
                amount_original = amount
            else:
                amount_original = (amount or 0.0) * voucher_rate
            return amount_original

    @api.multi
    def create_journal_gain_loss(self, rec, line,gain_or_loss):
        """
            this method use to create move live for journal AP
        :return: journal accont payable
        """
        if not rec.company_id.currency_exchange_journal_id:
            raise UserError(_(
                "You should configure the 'Exchange Rate Journal' in the accounting settings, to manage automatically the booking of accounting entries related to differences between exchange rates."))
        if not self.company_id.income_currency_exchange_account_id.id:
            raise UserError(_(
                "You should configure the 'Gain Exchange Rate Account' in the accounting settings, to manage automatically the booking of accounting entries related to differences between exchange rates."))
        if not self.company_id.expense_currency_exchange_account_id.id:
            raise UserError(_(
                "You should configure the 'Loss Exchange Rate Account' in the accounting settings, to manage automatically the booking of accounting entries related to differences between exchange rates."))

        list_ap_mv = []
        amount = line.amount_to_pay
        # amount_diff = rec.company_id.currency_id.round(rec.amount_currency * rate_diff)
        voucher_rate = self.env['res.currency'].with_context({'date': rec.payment_date}).browse(rec.currency_id.id)['rate']
        gain_or_loss_rate = gain_or_loss * rec.conv_amount_currency(line, rec,amount) if rec.currency_id.id == rec.company_id.currency_id.id else (gain_or_loss * rec.conv_amount_currency(line, rec,amount)) * voucher_rate
        name = rec.name
        ref = rec.name
        date = rec.payment_date
        move_line_pool = rec.env['account.move.line']
        move_vals = {
            'name': name,
            'journal_id': rec.company_id.currency_exchange_journal_id.id,
            'date': date,
            'ref': ref,
        }
        move = rec.env['account.move'].create(
            {
                'journal_id': rec.company_id.currency_exchange_journal_id.id,

            }
        )

        move_line_a = {
            'name': _('Currency exchange rate difference'),
            'debit': gain_or_loss_rate  < 0 and -gain_or_loss_rate or 0.0,
            'credit': gain_or_loss_rate > 0 and gain_or_loss_rate or 0.0,
            'account_id': line.account_id.id,
            'currency_id': line.currency_id.id,
            'move_id': move.id,
            'partner_id': rec.partner_id.id,
        }
        line_to_reconcile = rec.env['account.move.line'].with_context(check_move_validity=False).create(move_line_a)

        move_line_b = {
            'name': _('Currency exchange rate difference'),
            'debit': gain_or_loss_rate > 0 and gain_or_loss_rate or 0.0,
            'credit': gain_or_loss_rate < 0 and -gain_or_loss_rate or 0.0,
            'account_id': gain_or_loss > 0 and rec.company_id.currency_exchange_journal_id.default_debit_account_id.id or rec.company_id.currency_exchange_journal_id.default_credit_account_id.id,
            'currency_id': rec.currency_id.id,
            'move_id': move.id,
            'partner_id': rec.partner_id.id,
        }
        b = rec.env['account.move.line'].create(move_line_b)
        mv_gain_line_debit = move_line_pool.search([('move_id', '=', move.id), ('debit', '>', 0.0)])
        mv_gain_line_credit = move_line_pool.search([('move_id', '=', move.id), ('credit', '>', 0.0)])
        create_recon = rec.env['account.partial.reconcile'].create({
            'debit_move_id': gain_or_loss_rate < 0 and line_to_reconcile.id or mv_gain_line_debit.id,
            'credit_move_id': gain_or_loss_rate > 0 and line_to_reconcile.id or mv_gain_line_credit.id,
            'amount': abs(gain_or_loss),
            'amount_currency': 0.0,
            'currency_id': line.currency_id.id,
        })

        # list_ap_mv.append((0, 0, move_line_a))
        move.write({'rate_diff_partial_rec_id': create_recon.id})
        move.post()
        # list_ap_mv.append((0, 0, move_line_b))
        # return list_ap_mv

    @api.multi
    def create_recon_journal_gain_loss(self,rec,move_line_pool,move_gain_id,gain_or_loss,line):
        """
            this method use to create journal recon gain loss
        :param rec:
        :param move_line_pool:
        :param move_gain_id:
        :param gain_or_loss:
        :param line:
        :return:
        """
        mv_gain_line_debit = move_line_pool.search([('move_id', '=', move_gain_id.id), ('debit', '>', 0.0)])
        mv_gain_line_credit = move_line_pool.search([('move_id', '=', move_gain_id.id), ('credit', '>', 0.0)])
        partial_rec = rec.env['account.partial.reconcile'].create({
            'debit_move_id': mv_gain_line_debit.id,
            'credit_move_id': mv_gain_line_credit.id,
            'amount': abs(gain_or_loss),
            'amount_currency': 0.0,
            'currency_id': line.currency_id.id,
        })

        return partial_rec

    @api.multi
    def create_journal_payment_no_source(self, rec, journal_id, company_currency, sign):
        """
			this method use to create move live for journal payment no source
		:return: journal accont payable
		"""
        list_ap_mv = []
        amount = rec.amount

        if rec.writeoff_account_id.currency_id:
            if bool(rec.writeoff_account_id.currency_id != rec.company_id.currency_id) and bool(
                    rec.journal_id.currency_id) == False:
                debit_vals = rec.amount
            else:
                debit_vals = rec.conv_currency(rec, amount)
        else:
            debit_vals = rec.conv_currency(rec, amount)

        if rec.writeoff_account_id.currency_id:
            if bool(rec.writeoff_account_id.currency_id != rec.company_id.currency_id) and bool(
                    rec.journal_id.currency_id) == False:
                credit_vals = rec.amount
            else:
                credit_vals = rec.conv_currency(rec, amount)
        else:
            credit_vals = rec.conv_currency(rec, amount)

        des_journal_currency = rec.writeoff_account_id.currency_id != rec.company_id.currency_id if rec.writeoff_account_id.currency_id else False

        if des_journal_currency == False:
            vals_amount_cur = 1 * (des_journal_currency and rec.amount or 0.0)
        else:
            vals_amount_cur = 1 * (rec.conv_currency(rec,
                                                     amount) or 0.0) if rec.writeoff_account_id.currency_id != rec.currency_id else 1 * (
            rec.conv_currency_div(rec, amount) or 0.0)

        move_line_debit = {
            'name': rec.writeoff_account_id.name or '/',
            'debit': debit_vals,
            'credit': 0.0,
            'account_id': rec.writeoff_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': des_journal_currency and rec.writeoff_account_id.currency_id.id or False,
            'amount_currency': vals_amount_cur,
            'quantity': 1,
            'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_debit))

        journal_currency = rec.currency_id != rec.company_id.currency_id if rec.currency_id else bool(
            rec.company_id.currency_id)
        # journal line bank from
        move_line_credit = {
            'name': rec.journal_id.name or '/',
            'debit': 0.0,
            'credit': credit_vals,
            'account_id': rec.journal_id.default_credit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': journal_currency and rec.journal_id.currency_id.id or False,
            'amount_currency': sign * (journal_currency and rec.amount or 0.0) if rec.currency_id else sign * (
            rec.conv_currency(rec, amount) or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_credit))
        return list_ap_mv

    @api.multi
    def create_journal_receive_no_source(self, rec, journal_id, company_currency, sign):
        """
			this method use to create move live for journal payment no source
		:return: journal accont payable
		"""
        list_ap_mv = []
        amount = rec.amount

        if rec.writeoff_account_id.currency_id:
            if bool(rec.writeoff_account_id.currency_id != rec.company_id.currency_id) and bool(
                    rec.journal_id.currency_id) == False:
                debit_vals = rec.amount
            else:
                debit_vals = rec.conv_currency(rec, amount)
        else:
            debit_vals = rec.conv_currency(rec, amount)

        if rec.writeoff_account_id.currency_id:
            if bool(rec.writeoff_account_id.currency_id != rec.company_id.currency_id) and bool(
                    rec.journal_id.currency_id) == False:
                credit_vals = rec.amount
            else:
                credit_vals = rec.conv_currency(rec, amount)
        else:
            credit_vals = rec.conv_currency(rec, amount)

        des_journal_currency = rec.writeoff_account_id.currency_id != rec.company_id.currency_id if rec.writeoff_account_id.currency_id else False

        if des_journal_currency == False:
            vals_amount_cur = 1 * (des_journal_currency and rec.amount or 0.0)
        else:
            vals_amount_cur = 1 * (rec.conv_currency(rec,
                                                     amount) or 0.0) if rec.writeoff_account_id.currency_id != rec.currency_id else 1 * (
                rec.conv_currency_div(rec, amount) or 0.0)

        move_line_debit = {
            'name': rec.writeoff_account_id.name or '/',
            'debit': 0.0,
            'credit': debit_vals,
            'account_id': rec.writeoff_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': des_journal_currency and rec.writeoff_account_id.currency_id.id or False,
            'amount_currency': vals_amount_cur,
            'quantity': 1,
            'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_debit))

        journal_currency = rec.currency_id != rec.company_id.currency_id if rec.currency_id else bool(
            rec.company_id.currency_id)
        # journal line bank from
        move_line_credit = {
            'name': rec.journal_id.name or '/',
            'debit': credit_vals,
            'credit': 0.0,
            'account_id': rec.journal_id.default_credit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': journal_currency and rec.journal_id.currency_id.id or False,
            'amount_currency': sign * (journal_currency and rec.amount or 0.0) if rec.currency_id else sign * (
                rec.conv_currency(rec, amount) or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_credit))
        return list_ap_mv

    @api.multi
    def create_journal_bank_transfers(self, rec, journal_id, company_currency, sign):
        """
			this method use to create move live for journal bank transfer
		:return: journal accont payable
		"""
        list_ap_mv = []
        amount = rec.amount

        if rec.destination_journal_id.currency_id:
            if bool(rec.destination_journal_id.currency_id != rec.company_id.currency_id) and \
                            bool(rec.journal_id.currency_id) == False:
                debit_vals =  rec.amount
            else :
                debit_vals = rec.conv_currency(rec, amount)
        else:
            debit_vals =  rec.conv_currency(rec, amount)

        if rec.destination_journal_id.currency_id:
            if bool(rec.destination_journal_id.currency_id != rec.company_id.currency_id) \
                    and bool(rec.journal_id.currency_id) == False:
                credit_vals =  rec.amount
            else :
                credit_vals = rec.conv_currency(rec, amount)
        else:
            credit_vals =  rec.conv_currency(rec, amount)

        des_journal_currency = rec.destination_journal_id.currency_id != rec.company_id.currency_id \
            if rec.destination_journal_id.currency_id else False

        if des_journal_currency ==  False:
            vals_amount_cur = 1 * (des_journal_currency and rec.amount or 0.0)
        else:
            vals_amount_cur =  1 * (rec.conv_currency(rec, amount) or 0.0) \
                if rec.destination_journal_id.currency_id != rec.currency_id \
                else 1 * (rec.conv_currency_div(rec, amount) or 0.0)

        move_line_debit = {
            'name': rec.destination_journal_id.name or '/',
            'debit':  debit_vals,
            'credit': 0.0,
            'account_id': rec.destination_journal_id.default_debit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': des_journal_currency and rec.destination_journal_id.currency_id.id or False,
            'amount_currency' : vals_amount_cur,
            'quantity': 1,
            'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_debit))

        journal_currency = rec.currency_id != rec.company_id.currency_id if rec.currency_id \
            else bool(rec.company_id.currency_id)
        #journal line bank from
        move_line_credit = {
            'name': rec.journal_id.name or '/',
            'debit': 0.0,
            'credit': credit_vals,
            'account_id': rec.journal_id.default_credit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': journal_currency and rec.journal_id.currency_id.id or False,
            'amount_currency': sign * (journal_currency and rec.amount or 0.0) if rec.currency_id
            else sign * (rec.conv_currency(rec, amount) or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_credit))
        return list_ap_mv

    @api.multi
    def create_journal_ap(self,rec,journal_id,company_currency,sign,move_line_pool):
        """
            this method use to create move live for journal AP
        :return: journal accont payable
        """
        list_ap_mv = []
        prec = self.env['decimal.precision'].precision_get('Account')
        for line_debit in rec.line_debit_ids:
            account_credit_id = line_debit.account_id.id
            line = line_debit
            amount = line_debit.amount_to_pay
            validation_currency = False if line_debit.currency_id.id == rec.company_id.currency_id.id else True
            print ("(line,rec,amount)---->", line,rec,amount)
            print ("(rec.conv_amount_currency(line,rec,amount))---->", (rec.conv_amount_currency(line,rec,amount)))
            print ("line_debit.currency_id",line_debit.currency_id)
            print ("company_currency",company_currency)
            
            if rec.currency_id != company_currency and rec.set_manual_currency=='manual_rate':
                amount_debit = line_debit.amount_to_pay * rec.special_currency_rate or rec.currency_id.compute(line_debit.amount_to_pay or 0.0,company_currency)
            elif rec.currency_id != company_currency and rec.set_manual_currency=='current_rate':
                amount_debit, credit, amount_currency, currency_id = self.env['account.move.line'].with_context(date=self.payment_date).compute_amount_fields(line_debit.amount_to_pay, rec.currency_id, self.company_id.currency_id, line_debit.currency_id)
                #amount_debit = line_debit.amount_to_pay * rec.special_currency_rate or rec.currency_id.compute(line_debit.amount_to_pay or 0.0,company_currency)
            else:
                amount_debit = line_debit.amount_to_pay 
            
            if line_debit.currency_id == company_currency:
                amount_currency = 0.0
            else:
                amount_currency = (line_debit.currency_id!=company_currency and rec.currency_id == company_currency and (rec.conv_amount_currency(line,rec,amount)) or amount)
            move_line_debit = {
                'name': line_debit.move_line_id.move_id.name or '/',
                'debit': round(amount_debit, prec),
#                 'debit': line_debit.currency_id != company_currency and rec.currency_id.compute(line_debit.amount_to_pay or 0.0,company_currency) 
#                         or line_debit.amount_to_pay,
                'credit': 0.0,
                'account_id': account_credit_id,
                'journal_id': journal_id,
                'partner_id': rec.partner_id.id,
                'currency_id': line_debit.currency_id != rec.company_id.currency_id and line_debit.currency_id.id or False, #rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False ,#
                'amount_currency': amount_currency,
                #if validation_currency == True else 1 * (rec.currency_id != rec.company_id.currency_id and rec.amount or 0.0) ,
                'quantity': 1,
                'payment_id':rec.id
            }
            list_ap_mv.append((0, 0, move_line_debit))

        # amount_writeoff = rec.currency_id != company_currency and rec.set_manual_currency=='manual_rate' and rec.payment_difference * rec.special_currency_rate or rec.currency_id.compute(rec.payment_difference or 0.0,company_currency) 
        if rec.currency_id != company_currency and rec.set_manual_currency=='manual_rate':
            amount_writeoff = rec.payment_difference * rec.special_currency_rate or rec.currency_id.compute(rec.payment_difference or 0.0,company_currency)
        elif rec.currency_id != company_currency and rec.set_manual_currency=='current_rate':
            res_debit, res_credit, res_amount_currency, res_currency_id = self.env['account.move.line'].with_context(date=self.payment_date).compute_amount_fields(rec.payment_difference, rec.currency_id, self.company_id.currency_id, line_debit.currency_id)
            amount_writeoff = res_debit-res_credit
        else:
            amount_writeoff = rec.payment_difference
        # amount_writeoff = rec.currency_id != company_currency 
        #                   and rec.set_manual_currency=='manual_rate'
        #                   and rec.payment_difference * rec.special_currency_rate
        #                   or rec.currency_id.compute(rec.payment_difference or 0.0,company_currency) or rec.payment_difference
        move_line_debit_paymentdiv = {
            'name': rec.name or '/',
            'debit': amount_writeoff > 0.0 and abs(amount_writeoff),
            'credit': amount_writeoff < 0.0 and abs(amount_writeoff),
            'account_id': rec.writeoff_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': 1 * (rec.currency_id != rec.company_id.currency_id and rec.payment_difference or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        print ('move line debit payment', move_line_debit_paymentdiv)
        if rec.payment_difference_handling == 'reconcile':
            list_ap_mv.append((0, 0, move_line_debit_paymentdiv))
        amount_credit = float_round(rec.amount, precision_digits=2, rounding_method='HALF-UP')
        if rec.currency_id != company_currency and rec.set_manual_currency=='manual_rate':
            amount_credit = amount_credit * rec.special_currency_rate or rec.currency_id.compute(amount_credit or 0.0,company_currency)
        elif rec.currency_id != company_currency and rec.set_manual_currency=='current_rate':
            res_debit, res_credit, res_amount_currency, res_currency_id = self.env['account.move.line'].with_context(date=self.payment_date).compute_amount_fields(sign*amount_credit, rec.currency_id, self.company_id.currency_id, rec.currency_id)
            amount_credit = res_credit
        else:
            amount_credit = amount_credit
        move_line_credit = {
            'name': rec.name or '/',
            'debit': 0.0,
            'credit': amount_credit,
#             'credit': rec.currency_id != company_currency and rec.currency_id.compute(rec.amount or 0.0,
#                                                                                       company_currency) or rec.amount,
            'account_id': rec.journal_id.default_credit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': sign * (rec.currency_id != rec.company_id.currency_id and rec.amount or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        list_ap_mv.append((0, 0, move_line_credit))
        
        print ("###list_ap_mv---->>", list_ap_mv)
        
        return list_ap_mv

    @api.multi
    def create_journal_ar(self,rec,journal_id,company_currency,sign,move_line_pool):
        """
            this method use to create move live for journal AR
        :return: journal account receiveable
        """
        list_ar_mv = []
        prec = self.env['decimal.precision'].precision_get('Account')
        for line_credit in rec.line_credit_ids:
            account_credit_id = line_credit.account_id.id
            line = line_credit
            amount = line_credit.amount_to_pay
            validation_currency = False if line_credit.currency_id.id == rec.company_id.currency_id.id else True
            credit_ar_bank_adm = line_credit.amount_to_pay
            credit_ar = line_credit.currency_id != company_currency and rec.currency_id.compute(
                    line_credit.amount_to_pay or 0.0,
                    company_currency) or line_credit.amount_to_pay if rec.payment_adm != 'transfer' else \
                    line_credit.currency_id != company_currency and rec.currency_id.compute(
                        credit_ar_bank_adm or 0.0,
                    company_currency) or credit_ar_bank_adm

            if line_credit.currency_id == company_currency:
                amount_currency = 0.0
            else:
                amount_currency = sign*(line_credit.currency_id != company_currency and rec.currency_id == company_currency and (rec.conv_amount_currency(line,rec,amount)) or amount)
            
            move_line_credit = {
                'name': line_credit.move_line_id.move_id.name or '/',
                'debit': 0.0,
                'credit': round(credit_ar, prec),
                'account_id': account_credit_id,
                'journal_id': journal_id,
                'partner_id': rec.partner_id.id,
                'currency_id': line_credit.currency_id != rec.company_id.currency_id and line_credit.currency_id.id or False,
                'amount_currency': amount_currency,
                #'amount_currency': sign * (rec.conv_amount_currency(line, rec,amount) or 0.0)
#                 if validation_currency == True else sign * (
#             rec.currency_id != rec.company_id.currency_id and rec.amount or 0.0),
                'quantity': 1,
                'payment_id': rec.id
            }
            list_ar_mv.append((0, 0, move_line_credit))
        
        amount_writeoff = rec.currency_id != company_currency and rec.currency_id.compute(rec.payment_difference or 0.0,company_currency) or rec.payment_difference
        move_line_credit_paymentdiv = {
            'name': rec.name or '/',
            'debit': amount_writeoff < 0.0 and abs(amount_writeoff),
            'credit':  amount_writeoff > 0.0 and abs(amount_writeoff),
            'account_id': rec.writeoff_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': 1 * (rec.currency_id != rec.company_id.currency_id and -1*rec.payment_difference or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        if rec.payment_difference_handling == 'reconcile':
            list_ar_mv.append((0, 0, move_line_credit_paymentdiv))

        debit_ar = rec.currency_id != company_currency and rec.currency_id.compute(rec.amount or 0.0,
                 company_currency) or rec.amount if rec.payment_adm != 'transfer' \
            else (rec.currency_id != company_currency and rec.currency_id.compute(rec.amount or 0.0,
                 company_currency) or rec.amount)- (rec.currency_id != company_currency
                                                    and rec.currency_id.compute(rec.adm_amount or 0.0,
                 company_currency) or rec.adm_amount)

        move_line_debit = {
            'name': rec.name or '/',
            'debit': debit_ar,
            'credit': 0.0,
            'account_id': rec.journal_id.default_credit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': 1 * (rec.currency_id != rec.company_id.currency_id and rec.amount or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        move_line_debit_admbank = {
            'name': rec.name or '/',
            'debit': rec.currency_id != company_currency and rec.currency_id.compute(rec.adm_amount or 0.0,
                                                                                     company_currency) or rec.adm_amount,
            'credit': 0.0,
            'account_id': rec.adm_acc_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': 1 * (rec.currency_id != rec.company_id.currency_id and rec.adm_amount or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }

        if rec.payment_adm == 'transfer':
            list_ar_mv.append((0, 0, move_line_debit))
            list_ar_mv.append((0, 0, move_line_debit_admbank))
        else:
            list_ar_mv.append((0, 0, move_line_debit))
        return list_ar_mv

    @api.multi
    def create_journal_adm_bank(self, rec, journal_id, company_currency, sign):
        """
            this method use to create move live for journal Admin Bank
        :return: journal admin bank
        """
        list_bank_mv = []
        move_line_credit = {
            'name': rec.name or '/',
            'debit': 0.0,
            'credit': rec.currency_id != company_currency and rec.currency_id.compute(rec.adm_amount or 0.0,
                                                                                     company_currency) or rec.adm_amount,
            'account_id': rec.journal_id.default_credit_account_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': sign * (rec.currency_id != rec.company_id.currency_id and rec.adm_amount or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        move_line_debit_admbank = {
            'name': rec.name or '/',
            'debit': rec.currency_id != company_currency and rec.currency_id.compute(rec.adm_amount or 0.0,
                                                                                     company_currency) or rec.adm_amount,
            'credit': 0.0,
            'account_id': rec.adm_acc_id.id,
            'journal_id': journal_id,
            'partner_id': rec.partner_id.id,
            'currency_id': rec.currency_id != rec.company_id.currency_id and rec.currency_id.id or False,
            'amount_currency': 1 * (rec.currency_id != rec.company_id.currency_id and rec.adm_amount or 0.0),
            'quantity': 1,
            'payment_id': rec.id
        }
        list_bank_mv.append((0, 0, move_line_credit))
        list_bank_mv.append((0, 0, move_line_debit_admbank))
        return list_bank_mv
    
#     @api.multi
#     def register_payment(self, payment_line, writeoff_acc_id=False, writeoff_journal_id=False):
#         """ Reconcile payable/receivable lines from the invoice with payment_line """
#         line_to_reconcile = self.env['account.move.line']
#         for inv in self:
#             line_to_reconcile += inv.move_id.line_ids.filtered(lambda r: not r.reconciled and r.account_id.internal_type in ('payable', 'receivable'))
#         return (line_to_reconcile + payment_line).reconcile(writeoff_acc_id, writeoff_journal_id)
    
    
    @api.multi
    def recon_journal(self,list_recon,line,mv_line,move_line_pool):
        """
            this method use to recon journal AP or AR
        :param list_recon:
        :param line_debit:
        :param mv_line:
        :param move_line_pool:
        :return:
        """
        
        print ("list_recon,line,mv_line,move_line_pool", list_recon,line,mv_line,move_line_pool)

        list_recon.append(mv_line.id)
        list_recon.append(line.move_line_id.id)
        recon = move_line_pool.browse(list_recon).reconcile()
        del list_recon[:]

        return recon

    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:
            if rec.state != 'confirm':
                raise UserError(_("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            ##Edit
            move_pool = self.env['account.move']
            move_line_pool = self.env['account.move.line']
            company_currency = rec.company_id.currency_id

            #create Move ID
            sign = -1
            journal_id = rec.journal_id.id

            if rec.line_debit_ids and rec.payment_type == 'outbound':
                print ('masukkkkkkkk manual rate pout')
                list_recon = []
                #Create Journal AP
                line_mv_ids = rec.create_journal_ap(rec, journal_id, company_currency, sign, move_line_pool)
                #raise ValidationError(_("###Trial###"))
                #voucher currency pay rate
                voucher_pay_rate = rec.special_currency_rate \
                    if rec.set_manual_currency == 'manual_rate' and rec.special_currency_rate != 0.0 \
                    else self.env['res.currency'].with_context({'date': rec.payment_date}).browse(rec.currency_id.id)['rate']

                amt_debit = sum([val.get('debit') and val['debit'] for (x, y, val) in line_mv_ids])
                amount_debit = float_round(amt_debit, precision_digits=2, rounding_method='HALF-UP')
                amt_credit = sum([val.get('credit') and val['credit'] for (x, y, val) in line_mv_ids])
                amount_credit = float_round(amt_credit, precision_digits=2, rounding_method='HALF-UP')
                if bool(amount_debit != amount_credit):
                    raise UserError(_("Journal UNBALANCE: %s") % line_mv_ids)

                move_id = rec.create_move_id(rec, journal_id, move_pool,line_mv_ids)
                for line_debit in rec.line_debit_ids:
                    line = line_debit
                    mv_line = move_line_pool.search([('move_id', '=', move_id.id), ('debit','>', 0.0),
                                                     ('name', '=', line_debit.move_line_id.move_id.name)])
                # insert move credit and move debet from transaction
                    voucher_bill_rate_actual = \
                    self.env['res.currency'].with_context({'date': rec.payment_date}).browse(line_debit.currency_id.id)['rate']
                    voucher_bill_rate = \
                        self.env['res.currency'].with_context({'date': line_debit.move_line_id.date}).browse(
                            line_debit.currency_id.id)['rate']
                    if rec.set_manual_currency == 'manual_rate':
                        print ('masukkkkkkkk manual rate 2')
                        if rec.currency_id.id == line_debit.currency_id.id:
                            if voucher_bill_rate or voucher_bill_rate_actual == voucher_pay_rate:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                        else:
                            if voucher_bill_rate != voucher_pay_rate:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                    elif rec.set_manual_currency == 'current_rate':
                        print ("Curr Head",rec.currency_id.name, "Curr Line",line_debit.currency_id.name)
                        if rec.currency_id.id == line_debit.currency_id.id:
                            print ("Bill Rate :", voucher_bill_rate, "Voc Rate :",voucher_bill_rate_actual)
                            if voucher_bill_rate != voucher_bill_rate_actual:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:
                                print ("#--->", list_recon,line,mv_line,move_line_pool)
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                        else:
                            if rec.currency_id.id == rec.company_id.currency_id.id:
                                if voucher_bill_rate != voucher_bill_rate_actual:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                                else:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:
                                if voucher_bill_rate != voucher_pay_rate:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                                else:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                    else:
                        recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                if rec.payment_adm == 'transfer':
                    line_mv_ids = rec.create_journal_adm_bank(rec, journal_id, company_currency, sign)
                    journal_adm_bank = rec.create_move_adm_bankid(rec, journal_id, move_pool, line_mv_ids)
                    journal_adm_bank.post()
                    move_id.write({'state': 'posted'})
                else:
                    move_id.write({'state': 'posted'})

            elif rec.line_credit_ids and rec.payment_type == 'inbound':
                list_recon = []
                # Create Journal AR
                line_mv_ids = rec.create_journal_ar(rec, journal_id, company_currency, sign, move_line_pool)

                # voucher currency pay rate
                voucher_pay_rate = rec.special_currency_rate if rec.set_manual_currency == 'manual_rate' \
                                                                and rec.special_currency_rate != 0.0 else \
                self.env['res.currency'].with_context({'date': rec.payment_date}).browse(rec.currency_id.id)['rate']

                move_id = rec.create_move_id(rec, journal_id, move_pool, line_mv_ids)
                # insert move credit and move debet from transaction
                for line_credit in rec.line_credit_ids:
                    mv_line = move_line_pool.search([('move_id', '=', move_id.id), ('credit', '>', 0.0),
                                                     ('name', '=', line_credit.move_line_id.move_id.name)])
                    voucher_bill_rate_actual = \
                        self.env['res.currency'].with_context({'date': rec.payment_date}).browse(
                            line_credit.currency_id.id)['rate']
                    voucher_bill_rate = \
                        self.env['res.currency'].with_context({'date': line_credit.move_line_id.date}).browse(
                            line_credit.currency_id.id)['rate']
                    line = line_credit
                    if rec.set_manual_currency == 'manual_rate':
                        print ('masukkkkkkkk manual rate inbound')
                        if rec.currency_id.id == line_credit.currency_id.id:
                            if voucher_bill_rate or voucher_bill_rate_actual == voucher_pay_rate:
                                recon =rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:

                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)

                        else:
                            if voucher_bill_rate != voucher_pay_rate:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                    elif rec.set_manual_currency == 'current_rate':
                        if rec.currency_id.id == line_credit.currency_id.id:
                            if voucher_bill_rate != voucher_bill_rate_actual:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:
                                recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                        else:
                            if rec.currency_id.id == rec.company_id.currency_id.id:
                                if voucher_bill_rate != voucher_bill_rate_actual:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                                else:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                            else:
                                if voucher_bill_rate != voucher_pay_rate:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                                else:
                                    recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                    else:
                        recon = rec.recon_journal(list_recon,line,mv_line,move_line_pool)
                move_id.write({'state': 'posted'})
            else:
                if rec.validation_pay_no_source == True and rec.payment_type == 'outbound':
                    line_mv_ids = rec.create_journal_payment_no_source(rec, journal_id, company_currency, sign)
                    move_id = rec.create_move_payment_no_source_id(rec, journal_id, move_pool, line_mv_ids)
                    if rec.payment_adm == 'transfer':
                        line_mv_ids = rec.create_journal_adm_bank(rec, journal_id, company_currency, sign)
                        journal_adm_bank = rec.create_move_adm_bankid(rec, journal_id, move_pool, line_mv_ids)
                        journal_adm_bank.post()
                        move_id.write({'state': 'posted'})
                    else:
                        move_id.write({'state': 'posted'})

                if rec.validation_receive_no_source == True and rec.payment_type == 'inbound':
                    line_mv_ids = rec.create_journal_receive_no_source(rec, journal_id, company_currency, sign)
                    move_id = rec.create_move_receive_no_source_id(rec, journal_id, move_pool, line_mv_ids)
                    if rec.payment_adm == 'transfer':
                        line_mv_ids = rec.create_journal_adm_bank(rec, journal_id, company_currency, sign)
                        journal_adm_bank = rec.create_move_adm_bankid(rec, journal_id, move_pool, line_mv_ids)
                        journal_adm_bank.post()
                        move_id.write({'state': 'posted'})
                    else:
                        move_id.write({'state': 'posted'})

                # Create the journal entry
                # amounts = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
                # move = rec._create_payment_entry(amounts)

                # In case of a transfer, the first journal entry created debited the source liquidity account and credited
                # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
                if rec.payment_type == 'transfer':
                    # DEPRECATED Because This company cannot accept temporary account
                    # transfer_credit_aml = move.line_ids.filtered(lambda r: r.account_id == rec.company_id.transfer_account_id)
                    # transfer_debit_aml = rec._create_transfer_entry(amounts)
                    # (transfer_credit_aml + transfer_debit_aml).reconcile()
                    line_mv_ids = rec.create_journal_bank_transfers(rec, journal_id, company_currency, sign)
                    move_id = rec.create_move_bank_transfer_id(rec, journal_id, move_pool, line_mv_ids)
                    if rec.payment_adm == 'transfer':
                        line_mv_ids = rec.create_journal_adm_bank(rec, journal_id, company_currency, sign)
                        journal_adm_bank = rec.create_move_adm_bankid(rec, journal_id, move_pool, line_mv_ids)
                        journal_adm_bank.post()
                        move_id.write({'state': 'posted'})
                    else:
                        move_id.write({'state': 'posted'})

            rec.state = 'posted'

    def _create_transfer_entry(self, amount):
        """ Create the journal entry corresponding to the 'incoming money' part of an internal transfer, return the reconciliable move line
        """
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        debit = 0
        credit = 0
        amount_currency = 0
        dummy = 0

        if self.payment_type == 'transfer' and self.set_manual_currency == 'manual_rate':
            debit,credit,amount_currency, dummy = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id)
            debit = amount * self.special_currency_rate
        else:
            debit, credit, amount_currency, dummy = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id)
            amount_currency =  self.destination_journal_id.currency_id and self.currency_id.with_context(date=self.payment_date).compute(amount, self.destination_journal_id.currency_id) or 0
        dst_move = self.env['account.move'].create(self._get_move_vals(self.destination_journal_id))
        dst_liquidity_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, dst_move.id)
        dst_liquidity_aml_dict.update({
            'name': _('Transfer from %s') % self.journal_id.name,
            'account_id': self.destination_journal_id.default_credit_account_id.id,
            'currency_id': self.destination_journal_id.currency_id.id or self.currency_id.id,
            'payment_id': self.id,
            'journal_id': self.destination_journal_id.id})
        aml_obj.create(dst_liquidity_aml_dict)

        transfer_debit_aml_dict = self._get_shared_move_line_vals(credit, debit, 0, dst_move.id)

        transfer_debit_aml_dict.update({
            'name': self.name,
            'payment_id': self.id,
            'account_id': self.company_id.transfer_account_id.id,
            'journal_id': self.destination_journal_id.id})
        if self.currency_id != self.company_id.currency_id:
            transfer_debit_aml_dict.update({
                'currency_id': self.currency_id.id,
                'amount_currency': -self.amount,
            })
        transfer_debit_aml = aml_obj.create(transfer_debit_aml_dict)

        dst_move.post()
        return transfer_debit_aml

    def _create_payment_entry(self, amount):
        """ Create a journal entry corresponding to a payment, if the payment references invoice(s) they are reconciled.
            Return the journal entry.
        """
        aml_obj = self.env['account.move.line'].with_context(check_move_validity=False)
        invoice_currency = False
        debit = 0
        credit = 0
        amount_currency = 0
        dummy = 0

        if self.payment_type == 'transfer' and self.set_manual_currency == 'manual_rate':
            if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
                # if all the invoices selected share the same currency, record the paiement in that currency too
                invoice_currency = self.invoice_ids[0].currency_id
            debit, credit, amount_currency, currency_id = aml_obj.with_context(
                date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id,
                                                              invoice_currency)
            debit = amount * self.special_currency_rate
        else:
            if self.invoice_ids and all([x.currency_id == self.invoice_ids[0].currency_id for x in self.invoice_ids]):
                #if all the invoices selected share the same currency, record the paiement in that currency too
                invoice_currency = self.invoice_ids[0].currency_id

            debit, credit, amount_currency, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(amount, self.currency_id, self.company_id.currency_id, invoice_currency)

        move = self.env['account.move'].create(self._get_move_vals())

        #Write line corresponding to invoice payment
        counterpart_aml_dict = self._get_shared_move_line_vals(debit, credit, amount_currency, move.id, False)
        counterpart_aml_dict.update(self._get_counterpart_move_line_vals(self.invoice_ids))
        counterpart_aml_dict.update({'currency_id': currency_id})
        counterpart_aml = aml_obj.create(counterpart_aml_dict)

        #Reconcile with the invoices
        if self.payment_difference_handling == 'reconcile' and self.payment_difference:
            writeoff_line = self._get_shared_move_line_vals(0, 0, 0, move.id, False)
            debit_wo, credit_wo, amount_currency_wo, currency_id = aml_obj.with_context(date=self.payment_date).compute_amount_fields(self.payment_difference, self.currency_id, self.company_id.currency_id, invoice_currency)
            writeoff_line['name'] = _('Counterpart')
            writeoff_line['account_id'] = self.writeoff_account_id.id
            writeoff_line['debit'] = debit_wo
            writeoff_line['credit'] = credit_wo
            writeoff_line['amount_currency'] = amount_currency_wo
            writeoff_line['currency_id'] = currency_id
            writeoff_line = aml_obj.create(writeoff_line)
            if counterpart_aml['debit']:
                counterpart_aml['debit'] += credit_wo - debit_wo
            if counterpart_aml['credit']:
                counterpart_aml['credit'] += debit_wo - credit_wo
            counterpart_aml['amount_currency'] -= amount_currency_wo
        self.invoice_ids.register_payment(counterpart_aml)

        #Write counterpart lines
        if not self.currency_id != self.company_id.currency_id:
            amount_currency = 0
        liquidity_aml_dict = self._get_shared_move_line_vals(credit, debit, -amount_currency, move.id, False)
        liquidity_aml_dict.update(self._get_liquidity_move_line_vals(-amount))
        aml_obj.create(liquidity_aml_dict)

        move.post()
        return move

    @api.multi
    def button_prorate_payment(self):
        """
            this method use to prorate payment
        :return:
        """
        for item in self:
            len_rec_line_debit = len(item.line_debit_ids)
            if len_rec_line_debit > 1:
                amount_pay_prorate = item.amount / len_rec_line_debit
                for record in item.line_debit_ids:
                    record.write({'amount_to_pay': amount_pay_prorate})
            else:
                raise ValidationError(_("The payment cannot be prorate because just one invoice, Please insert more than one invoice!"))

    @api.multi
    def button_confirm(self):
        """
            this method use to confirm state
        :return:
        """
        for item in self:
            # Use the right sequence to set the name
            if item.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if item.partner_type == 'customer':
                    if item.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if item.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if item.partner_type == 'supplier':
                    if item.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if item.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
            item.name = self.env['ir.sequence'].with_context(ir_sequence_date=item.payment_date).next_by_code(sequence_code)
            item.write({'state':'confirm'})
            if item.partner_type=='customer':
                item.post()

    @api.multi
    @api.constrains('line_credit_ids','line_debit_ids')
    def _constraint_line_ids(self):
        # constraint total amount to pay must be same with amount pay
        for item in self:

            if item.payment_type == 'outbound':
                if len(item.line_debit_ids) > 1:
                    total_amount_topay = sum(rec.amount_to_pay for rec in item.line_debit_ids)
                    if item.payment_difference_handling =='open':
                        if item.amount != total_amount_topay:
                            raise ValidationError('Please Check Your Amount to Pay !!, Cause Amount To Pay Must be equal to Amount')
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
            elif item.payment_type == 'inbound':
                if len(item.line_credit_ids) > 1:
                    total_amount_topay = sum(rec.amount_to_pay for rec in item.line_credit_ids)
                    if item.payment_difference_handling =='open':
                        if item.amount != total_amount_topay:
                            raise ValidationError('Please Check Your Amount to Pay !!, Cause Amount To Pay Must be equal to  Amount')
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
            else:
                pass

account_payment()

class account_payment_line(models.Model):
    _name = 'account.payment.line'
    
    @api.one
    @api.depends('payment_id.currency_id','payment_id.payment_date','payment_id.set_manual_currency',
                 'payment_id.special_currency_rate','payment_id','move_line_id')
    def _compute_balance(self):
        print ("##_compute_balance##")
        self.with_context()
        currency_pool = self.env['res.currency']
        rs_data = {}
        
        amount_original     = 0.0
        amount_unreconciled = 0.0
        
        for line in self:
            payment_date = line.payment_id.payment_date
            
            voucher_rate = self.env['res.currency'].with_context({'date' : payment_date}).browse(line.payment_id.currency_id.id)['rate']
            
            company_currency = line.payment_id.company_id.currency_id
            payment_currency = line.payment_id.currency_id and line.payment_id.currency_id or company_currency
            
            tcurrency_id = line.currency_id.with_context(date=payment_date)
            if self.payment_id.set_manual_currency=='manual_rate' and self.payment_id.special_currency_rate!=0.0:

                tcurrency_id = tcurrency_id.with_context(payment_special_currency = line.currency_id.id,
                                                       payment_special_currency_rate  = line.payment_id.special_currency_rate)
            
            
            move_line = line.move_line_id or False
            
            if not move_line:
                amount_original     = 0.0
                amount_unreconciled = 0.0
                
            #USD vs IDR
            elif move_line.currency_id and payment_currency==company_currency:
                if self.payment_id.set_manual_currency=='manual_rate' and self.payment_id.special_currency_rate!=0.0:
                    payment_rate = self.payment_id.special_currency_rate
                    amount_original     = abs(move_line.amount_currency or 0.0) * payment_rate
                    amount_unreconciled = abs(move_line.amount_residual_currency) * payment_rate
                else:
                    amount_original     = tcurrency_id.compute(abs(move_line.amount_currency) or 0.0, company_currency)
                    amount_unreconciled = tcurrency_id.compute(abs(move_line.amount_residual_currency), company_currency)
            #USD vs USD
            elif move_line.currency_id and payment_currency==move_line.currency_id:
                amount_original     = abs(move_line.amount_currency)
                amount_unreconciled = abs(move_line.amount_residual_currency)
            #USD vs SGD
            elif move_line.currency_id and (payment_currency != company_currency):
                amount_original     = tcurrency_id.compute(abs(move_line.amount_currency) or 0.0, payment_currency)
                amount_unreconciled = tcurrency_id.compute(abs(move_line.amount_residual_currency), payment_currency)
            #IDR vs USD
            elif not move_line.currency_id and payment_currency != company_currency:
                if self.payment_id.set_manual_currency=='manual_rate' and self.payment_id.special_currency_rate!=0.0:
                    payment_rate = self.payment_id.special_currency_rate
                    amount_original     = (move_line.credit or move_line.debit or 0.0) * payment_rate
                    amount_unreconciled = abs(move_line.amount_residual) * payment_rate
                else:
                    amount_original     = tcurrency_id.compute(move_line.credit or move_line.debit or 0.0, line.payment_id.currency_id)
                    amount_unreconciled = tcurrency_id.compute(abs(move_line.amount_residual), line.payment_id.currency_id)
            else:
                #always use the amount booked in the company currency as the basis of the conversion into the voucher currency
                amount_original     = tcurrency_id.compute(move_line.credit or move_line.debit or 0.0, move_line.company_id.currency_id)
                amount_unreconciled = tcurrency_id.compute(abs(move_line.amount_residual), move_line.company_id.currency_id)
        self.amount_original = amount_original
        self.amount_unreconciled = amount_unreconciled

    
    
    @api.onchange('move_line_id')
    def onchange_move_line_id(self):
        res = {}
        move_line_pool = self.env['account.move.line']
        move_line_id = self.move_line_id or False
        
        account_id  = False
        type        = False
        currency_id = False
        
        if move_line_id:
            move_line = move_line_id
            if move_line.credit:
                ttype = 'dr'
            else:
                ttype = 'cr'

            account_id = move_line.account_id.id
            type       = ttype
            currency_id= move_line.currency_id and move_line.currency_id.id or move_line.company_id.currency_id.id
        
        self.account_id = account_id
        self.type       = type
        self.currency_id= currency_id

    @api.multi
    @api.depends('move_line_id') #'amount_currency',
    def _amount_currency(self):
        for item in self:
            if item.move_line_id:
                item.amount_currency = abs(item.move_line_id.amount_currency)

    payment_id      = fields.Many2one('account.payment', string='Payment')
    move_line_id    = fields.Many2one('account.move.line', string='Journal Items')
    account_id      = fields.Many2one('account.account', string='Account')
    
    date            = fields.Date(related='move_line_id.date', store=False)
    date_due        = fields.Date(related='move_line_id.date_maturity', store=True)
    currency_id     = fields.Many2one('res.currency', string='Currency')
    amount_currency = fields.Float(compute='_amount_currency',string='Amount Currency',store=True)
    amount_original = fields.Float(compute="_compute_balance",string='Amount Original',store=True)
    amount_unreconciled = fields.Float(compute="_compute_balance",string='Outstanding Amount',store=True)
    #amount_unreconciled = fields.Float(string='Outstanding Amount')
    amount_to_pay = fields.Float(string='Amount to Pay')
    type = fields.Selection([('dr','Debit'),('cr','Credit')], string='Type')
    full_payment = fields.Boolean('Full Payment')

    @api.onchange('full_payment')
    def onchange_amount_to_pay(self):
        """
            onchange full payment
        :return:
        """
        for item in self:
            if item.full_payment == True:
                item.amount_to_pay = item.amount_unreconciled

    
account_payment_line()


