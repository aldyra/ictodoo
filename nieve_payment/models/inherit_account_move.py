from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class InheritAccountMoveLine(models.Model):

	_inherit = 'account.move.line'

	pph = fields.Boolean('PPH?')
	rate =fields.Float('Rate')

	@api.onchange('rate','currency_id','amount_currency')
	def _onchange_amount_rate(self):
		for item in self:
			if item.currency_id.name =='IDR':
				item.rate = 0.0
			if item.rate > 0:
				if item.amount_currency < 0 and item.currency_id.name !='IDR':
					item.credit = item.rate * item.amount_currency
				if item.amount_currency > 0 and item.currency_id.name !='IDR':
					item.debit  = item.rate * item.amount_currency